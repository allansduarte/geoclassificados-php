<?php
namespace Survey\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * Entidade Question - responsável pelas questões de um questionário
 * 
 * @category Survey
 * @package Model
 *
 * @ORM\Entity
 * @ORM\Table(name="questionario_questao")
 *
 */
class Question extends Entity
{

    public function __set($key, $value) 
    {
        $this->$key = $this->valid($key, $value);
    }

    /**
     * @param string $key
     * @return mixed 
     */
    public function __get($key) 
    {
        return $this->$key;
    }
 
    /**
     * @var integer
     * @access private
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="Survey\Model\Survey", inversedBy="id")
     * @ORM\JoinColumn(name="questionario_id", referencedColumnName="id")
     * @var Survey|null
     */
    private $questionario_id;
 
    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $titulo;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="Survey\Model\Type", inversedBy="id")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     * @var Type|null
     */
    private $tipo_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="Survey\Model\Choice", inversedBy="id")
     * @ORM\JoinColumn(name="escolha_id", referencedColumnName="id")
     * @var Choice|null
     */
    private $escolha_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\Column(type="integer")
     */
    private $ordem;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'questionario_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'titulo',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'tipo_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'escolha_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'ordem',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}