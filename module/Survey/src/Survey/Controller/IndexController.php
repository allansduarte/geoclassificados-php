<?php
namespace Marketing\Controller;

use Zend\View\Model\ViewModel;

use Core\Controller\ActionController;

/**
 * Controlador que gerencia as páginas promocionais ou de divulgação
 * 
 * @category Marketing
 * @package Controller
 * @author  Allan Duarte <allan.duarte@temprafesta.com>
 */
class IndexController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getService('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Descrição
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Página Landing Page promocional
     * @return ViewModel
     */
    public function conhecaAction()
    {
        $this->layout("layout/layout_conheca");
        $this->layout()->title = "Descubra todas as vantagens";

        $view =  new ViewModel();

        return $view;
    }
}