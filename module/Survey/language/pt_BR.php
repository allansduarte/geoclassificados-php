<?php

return array(
	'Home' 		=> 'Página inicial',
	'Logout' 	=> 'Sair',
	'Login'		=> 'Entrar',
	'Skeleton Application' => 'Aplicação Exemplo',
	'isEmpty'	=> 'Este campo é obrogatório e não deve ser nulo',
);