<?php

namespace Core\View\Helper;

use Zend\View\Helper\Partial;
use Zend\View\Model\ViewModel;
use Zend\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Helper que formata uma string para uma URL
 * 
 * @category Application
 * @package View\Helper
 * @author  Allan Duarte <allansduarte@temprafesta.com>
 */
class FormatURL extends AbstractHelper
{
    public function __invoke($string)
    {
    	$string = strtolower($string);
    	$string = trim($string);
	    $string = str_replace(" ", "-", $string);
	    $string = str_replace("á", "a", $string);
	    $string = str_replace("à", "a", $string);
	    $string = str_replace("ã", "a", $string);
	    $string = str_replace("á", "a", $string);
	    $string = str_replace("ä", "a", $string);

	    $string = str_replace("é", "e", $string);
	    $string = str_replace("è", "e", $string);
	    $string = str_replace("ë", "e", $string);
	    $string = str_replace("ê", "e", $string);

	    $string = str_replace("í", "i", $string);
	    $string = str_replace("ì", "i", $string);
	    $string = str_replace("ï", "i", $string);
	    $string = str_replace("î", "i", $string);

	    $string = str_replace("ó", "o", $string);
	    $string = str_replace("ò", "o", $string);
	    $string = str_replace("õ", "o", $string);
	    $string = str_replace("ö", "o", $string);
	    $string = str_replace("ô", "o", $string);

	    $string = str_replace("ú", "u", $string);
	    $string = str_replace("ù", "u", $string);
	    $string = str_replace("ü", "u", $string);
	    $string = str_replace("û", "u", $string);

	    $string = str_replace("ç", "c", $string);

	    $string = str_replace("ý", "y", $string);
	    $string = str_replace("ý", "y", $string);

	    $string = str_replace("´", "", $string);
	    $string = str_replace("*", "-", $string);
	    $string = str_replace("&", "-", $string);
	    $string = str_replace("!", "", $string);
	    $string = str_replace("@", "-", $string);
	    $string = str_replace("#", "", $string);
	    $string = str_replace("$", "", $string);
	    $string = str_replace("'", "", $string);
	    $string = str_replace(",", "", $string);
	    $string = str_replace("ª", "", $string);
	    $string = str_replace("º", "", $string);

        $url = $string;

        return $url;
    }
}