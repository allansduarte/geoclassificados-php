<?php

namespace Core\View\Helper;

use Zend\View\Helper\Partial;
use Zend\View\Model\ViewModel;
use Zend\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Helper que verifica se existe partial em cache se não existial faz a leitura da partial existente
 * 
 * @category Application
 * @package View\Helper
 * @author  Allan Duarte <allan.sduarte@gmail.com.br>
 */
class CachedPartial extends AbstractHelper implements ServiceLocatorAwareInterface
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    public function __invoke($file)
    {

        $helperPluginManager = $this->getServiceLocator();
        $serviceManager = $helperPluginManager->getServiceLocator();

        $viewModel  = new ViewModel(array($file));
        $viewModel->setTemplate("application/index/index");

        $viewRenderer = $serviceManager->get('ViewRenderer');

        $cache      = $serviceManager->get('Cache');
        $config     = $serviceManager->get('Config');
        //$partial    = new Partial();

        if($config['cache']['enable']){

        }

        return $viewRenderer->render($viewModel);
    }
}