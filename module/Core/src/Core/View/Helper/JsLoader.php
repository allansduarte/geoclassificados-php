<?php  
/*******************************************************************************
 * Zend Temple - Decemember 2012
*
* Class: JsLoader.php
*
* Zend Framework (http://framework.zend.com/)
*
* @link   https://github.com/jdellostritto/zf2tutorial.git.
* @license   http://framework.zend.com/license/new-bsd New BSD License
********************************************************************************/
namespace  Core\View\Helper;

use Zend\Http\Request;
use Zend\View\Helper\AbstractHelper;

use Zend\Mvc\Router\Http\RouteMatch;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Helper\HeadScript;
use Zend\View\HelperPluginManager;



class JsLoader extends AbstractHelper 
{ 
	protected $request;
	protected $route;
	protected $phprenderer;

    public function __construct(Request $_request = null, PhpRenderer $_phprenderer = null, RouteMatch $_route = null)
    {
        $this->request = $_request;
    	$this->route = $_route;
        $this->phprenderer = $_phprenderer;
    }

    public function __invoke()
    {    

        if( isset($this->request) && isset($this->route) && isset($this->phprenderer) ){
            $_params = $this->route->getParams();

            if(!$_params)
                return $this->phprenderer->headLink();
            
            $module      = $_params['module'];
            $controllers = explode("\\", $_params['controller']);
            $controller  = strtolower(end($controllers));
            $action      = $_params['action'];
            $diretorio     = $this->phprenderer->basePath()."/js/entry-view/".$module."/".$controller."/".$action;
            $arquivo     = $action."_dev.js";
            $jsFile = "";

            if(file_exists(__DIR__."/../../../../../../public".$diretorio."/".$arquivo) ){
                //$this->phprenderer->headScript()->prependFile($diretorio."/".$arquivo);
                $jsFile = '<script defer type="text/javascript" src="'.$this->phprenderer->serverUrl($diretorio."/".$arquivo).'"></script>';
            }

            return $jsFile;
        }
    }
}
