<?php

namespace Core;

use Zend\Mvc\MvcEvent;
use Core\View\Helper\CssLoader;
use Core\View\Helper\JsLoader;

class Module
{

    public function onBootstrap($e)
    {
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['module_layout'][$moduleNamespace])) {
                $controller->layout($config['module_layout'][$moduleNamespace]);
            }
        }, 100);


        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, 
            function($e) {
                /*
                 * The MvcEvent has a lot in it.
                 * It contains the viewhelpermanager which is an instance of the HelperPluginManager
                 * The HelperPluginManager has a factory that we can add objects to and use them as
                 * we need (in our layout for example). The call to setFactory below adds an object
                 * of:closure to our factory. THis can be called directly in the view.  
                 */
                $_viewpluginmanager = $e->getApplication()->getServiceManager()->get('viewhelpermanager');
                $_phpRenderer = $_viewpluginmanager->getRenderer();

                /*
                 * The call to setFactory below takes the $sm (service manager) and uses the $e the 
                 * MvcEvent as well as another important object the PhpRenderer which we grab from
                 * the viewpluginmanager.The PHPRenderer object has things like our headLink and 
                 * headScript which if you have used ZF1 will recognize. You'll see how each is 
                 * used when we get to the actual loaders.  
                 */
                $_viewpluginmanager->setFactory('CssLoader', 
                    /*This is what is called when invoked in layout.phtml*/
                    function($sm) use ($e, $_phpRenderer) {
                        $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
                        return new CssLoader($locator->get('Request'), $_phpRenderer, $e->getRouteMatch());
                    }
                );
                $_viewpluginmanager->setFactory('JsLoader', 
                    /*This is what is called when invoked in layout.phtml*/
                    function($sm) use ($e, $_phpRenderer) {
                        $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
                        return new JsLoader($locator->get('Request'), $_phpRenderer, $e->getRouteMatch());
                    }
                );
            }
        );

    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'DbAdapter' => 'Core\Db\AdapterServiceFactory'
            )
        );
    }
}