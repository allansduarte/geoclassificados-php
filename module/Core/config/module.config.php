<?php
return array(
	//	'di' => array(),
    'view_helpers' => array(
        'invokables'=> array(
            'session' => 'Core\View\Helper\Session',
            'formatURL' => 'Core\View\Helper\FormatURL',
            'CssLoader'		=> 'Core\View\Helper\CssLoader',
            'JsLoader'		=> 'Core\View\Helper\JsLoader',
        )
    ),
	'service_manager' => array(
		'factories' => array(
			'Core\Acl\Builder' => function($sm){
				return new Core\Acl\Builder();
			},
	        'Cache' => function($sm) {
			    $config = $sm->get('Config');
			    $cache = \Zend\Cache\StorageFactory::factory(
			        array(
			            'adapter' => $config['cache']['adapter'],
			            'ttl'     => $config['cache']['ttl'],
			            'plugins' => $config['cache']['plugins'],
			        )
			    );

			    return $cache;
			}
		)
	)
);