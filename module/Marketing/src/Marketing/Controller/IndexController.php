<?php
namespace Marketing\Controller;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

use Zend\View\Model\ViewModel;

use Core\Controller\ActionController;

use Marketing\Form\Cupom as CupomForm;
use Marketing\Model\Cupom as CupomModel;

/**
 * Controlador que gerencia as páginas promocionais ou de divulgação
 * 
 * @category Marketing
 * @package Controller
 * @author  Allan Duarte <allan.duarte@temprafesta.com>
 */
class IndexController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getService('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Descrição
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Página Landing Page promocional
     * @return ViewModel
     */
    public function conhecaAction()
    {
        $this->layout("layout/layout_conheca");
        $this->layout()->title = "Descubra todas as vantagens";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Página Landing Page promocional de cupom
     * @return ViewModel
    */
    public function cupomAction()
    {
        $this->layout("layout/layout_cupom");
        $this->layout()->title = "Os 100 primeiros ganham cupons!";

        $cupomForm = new CupomForm;

        $qtdCupons = (int) $this->getEntityManager()
             ->getRepository("Marketing\Model\Cupom")
             ->createQueryBuilder('c')
             ->select('count(c.type)')
             ->where('c.type = 1')
             ->getQuery()
             ->getSingleScalarResult();

        $cuponsRestantes = 100 - $qtdCupons;

        $view =  new ViewModel(array(
            "cupomForm"       => $cupomForm,
            "cuponsRestantes" => $cuponsRestantes
        ));

        return $view;
    }

    /**
     * Método que vai gerar o cupom
     * @return ViewModel
    */
    public function gerarCupomAction(){
        $request = $this->getRequest();

        if(!$request->isXMLHttpRequest())
            return $this->redirect()->toUrl("/ganhe-cupons");

        $response   = $this->getResponse();
        $response->setStatusCode(500);
        $email      = $this->params()->fromRoute("email");

        $emailExiste = $this->getEntityManager()
                       ->getRepository("Marketing\Model\Cupom")
                       ->findBy(array(
                            "email"      => $email
                ));

        if(count($emailExiste) > 0){
            $response->setContent(json_encode(array("error" => 1)));
            return $response;
        }

        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try{
            $type = $i = 0;
            $tokenArray = array();

            while($i < 2){
                $type = $i += 1;
                $token      = bin2hex(openssl_random_pseudo_bytes(4));
                array_push($tokenArray, $token);
                $created    = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

                $cupomModel = new CupomModel;
                $cupomModel->__set('token', $token);
                $cupomModel->__set('type', $type);
                $cupomModel->__set('email', $email);
                $cupomModel->__set('created', $created);

                $em->persist($cupomModel);
                $em->flush();
            }
            $jsonToken  = json_encode($tokenArray);
            $response->setStatusCode(200);
            $response->setContent($jsonToken);

            $em->getConnection()->commit();

            $this->sendMailCupom($tokenArray, $email);
        } catch(\Exception $e){
            $em->getConnection()->rollback();
            $response->setStatusCode(500);
        }

        return $response;
    }

    /**
     * Realiza o envio de e-mail de cupom
     * @return boolean
     */
    private function sendMailCupom($tokenArray, $remetente) {
        $sent = true;

        $message = new Message();
        $message->addTo($remetente)
            ->addFrom('suporte@temprafesta.com')
            ->setSubject('Chegou os cupons do Tem Pra Festa');

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase = $helper->__invoke();
        $urlImg  = $helper->__invoke("/img/marketing/landing-page/cupom/cabecalho-email.jpg");

        $textHtml = '<div style="margin:0;padding:40px;background-color:#eee">';
        $textHtml .= '<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF">';
        $textHtml .= '<tbody>';
        $textHtml .= '<tr>';
        $textHtml .= '<td colspan="3" bgcolor="#F7F7F7"><img src="'.$urlImg.'" alt="GUARDE SEUS CUPONS" /></td>';
        $textHtml .= '</tr>';
        $textHtml .= '<td width="60" bgcolor="#F7F7F7"></td><td width="510" bgcolor="#F7F7F7" style="font:12px/150% Arial;color:#000">';
        $textHtml .= '<br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Ol&aacute; ,<br><br>Aqui est&atilde;o os cupons dos anúncios Destaque e Empresa. Guarde e utilize no campo CUPOM quando anunciar na plataforma do Tem Pra Festa.<br></font>';
        $textHtml .= '<br><br><br><div align="center">';

        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center" style="display:block;padding:20px;border:1px solid #ccc;font-size:20px">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td>';
        $textHtml .= '<strong>Destaque:</strong> '.$tokenArray[0];
        $textHtml .= '</td>';
        $textHtml .= '</tr></tbody></table>';

        $textHtml .= '<br><br><br>';

        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center" style="display:block;padding:20px;border:1px solid #ccc;font-size:20px">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td>';
        $textHtml .= '<strong>Empresa:</strong> '.$tokenArray[1];
        $textHtml .= '</td>';
        $textHtml .= '</tr></tbody></table>';

        $textHtml .= '<br><br><br>';
        $textHtml .= '<a style="text-decoration: none; color: #FFF;" href="'.$urlBase.'/user/register?tour=true" target="_blank">';
        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center" style="display:block;padding:20px;border:1px solid #ccc;font-size:20px;background-color:#5d257d">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td align="center">';
        $textHtml .= 'UTILIZAR CUPOM AGORA';
        $textHtml .= '</td>';
        $textHtml .= '</tr></tbody></table>';
        $textHtml .= '</a>';

        $textHtml .= '</div><br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Equipe Temprafesta</font>';
        $textHtml .= '</td><td width="30" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '<tr><td width="600" colspan="3" height="40" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '</tbody>';
        $textHtml .= '</table>';
        $textHtml .= '</div>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
        } catch(\Exception $e){
            $sent = false;
        }

        return $sent;
    }
}