<?php
namespace Marketing\Form;

use Zend\Form\Form;

class Cupom extends Form
{
    public function __construct()
    {
        parent::__construct('cupom');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control',
                'placeholder' => 'Informe seu e-mail...',
                'id'    => 'email',
                'maxLength' => 255,
            ),
            'options' => array(
                'label' => 'Informe seu e-mail',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'GANHAR CUPOM',
                'class' => 'btn btn-primary btn-lg',
            ),
        ));
    }
}