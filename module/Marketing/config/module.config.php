<?php

namespace Marketing;

return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /marketing/:controller/:action
            'marketing' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/marketing',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Marketing\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'marketing',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                ),
            ),
            'conheca' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/conheca-o-tem-pra-festa',
                    'defaults' => array(
                        'controller' => 'Marketing\Controller\Index',
                        'action'     => 'conheca',
                        'module'     => 'marketing'
                    ),
                ),
            ),
            'cupom' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/ganhe-cupons',
                    'defaults' => array(
                        'controller' => 'Marketing\Controller\Index',
                        'action'     => 'cupom',
                        'module'     => 'marketing'
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
           //'translator' => 'MvcTranslator',
        ),
    ),
    /*'translator' => array(
        'locale' => 'pt_BR',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__. '/../language',
                'pattern'  => '%s.php'
            ),
        ),
    ),*/
    'controllers' => array(
        'invokables' => array(
            'Marketing\Controller\Index' => 'Marketing\Controller\IndexController',
        ),
    ),
    'module_layout' => array(
        'Marketing' => 'layout/layout_mkt.phtml'
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout_mkt.phtml',
            'marketing/index/index' => __DIR__ . '/../view/marketing/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'marketing' => __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model'),
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache' => 'array',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
            ),
        ),
    )
);