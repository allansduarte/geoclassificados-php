<?php
namespace Site\Controller;

use Core\Controller\ActionController;

use Doctrine\ORM\EntityManager;

use Zend\View\Model\ViewModel;

use Site\Form\ContatoAnunciante as ContatoAnuncianteForm;
use System\Model\ContatoAnunciante as ContatoAnuncianteModel;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

/**
 * Controlador responsável pelos classificados no site
 * 
 * @category Site
 * @package Controller
 * @author  Allan Duarte <allan.duarte@temprafesta.com>
 */
class ClassificadoController extends ActionController
{

    private $periodoAnuncio = "-30 days";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Apresenta os classificados
     * @return ViewModel
     */
    public function indexAction()
    {
        $nomeClassificado = $this->params()->fromRoute('nomeClassificado', "");
        $classificado_id  = $this->params()->fromRoute('classificadoId', 0);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.descricao",
            "c.valor",
            //"c.status",
            //"c.tipo",
            "c.subcategoria_id",
            "c.tags",
            "c.reactivated",
            "c.email",
            "c.latitude",
            "c.longitude",
            "c.website",
            "c.telefone",
            "c.celular",
            "c.visualizacoes",
            "cidade.nome as nome_cidade",
            "estado.uf",
            "p.file_memory as perfil_imagem_file_memory",
            "p.file_mime_type as perfil_imagem_mime_type",
            "p.nome as perfil_nome",
            "u.id as userId"
        );

        $twentyDaysAgo = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where("c.status = 1")
             ->andWhere('c.reactivated >= :date')
             ->andWhere('c.id = :id')
             ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('date', $twentyDaysAgo, \Doctrine\DBAL\Types\Type::DATETIME);
        $qb->setParameter('id', $classificado_id);

        $classificado = $qb->getQuery()->getResult();

        if(count($classificado) <= 0)
            return $this->notFoundAction();

        $this->layout()->title = $classificado[0]["titulo"];

        $subcategoria = $this->getEntityManager()->find("System\Model\Subcategoria", $classificado[0]["subcategoria_id"]);

        $imagens = $this->getEntityManager()
                        ->getRepository("System\Model\ClassificadoImagem")
                        ->findBy(array('classificado_id' => $classificado[0]['id']));

        $this->insertViewsClassificado($classificado[0]['id']);

        $favorito = false;
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        if($userSession)
            $favorito = $this->checkAnuncioFavorito($userSession[0]->id, $classificado[0]['id']);

        $contatoAnuncianteForm = new ContatoAnuncianteForm;

        $view =  new ViewModel(array(
            "classificado"  => $classificado[0],
            "subcategoria"  => $subcategoria,
            "imagens"       => $imagens,
            "favorito"      => $favorito,
            "contatoAnuncianteForm" => $contatoAnuncianteForm,
		));

        return $view;
    }

    /**
     * Apresenta a página de empresa
     * @return ViewModel
     */
    public function empresaAction()
    {
        $nomeEmpresa = $this->params()->fromRoute('nome', null);
        $empresaId   = $this->params()->fromRoute('id', 0);

        $fields = array(
                        "e.id",
                        "e.nome",
                        "e.created",
                        "e.descricao",
                        "e.imagem",
                        "e.mime_type_logo",
                        "ce.visualizacoes",
                        "cidade.nome as cidade_nome",
                        "estado.uf",
                        "endereco.rua",
                        "endereco.numero",
                        "endereco.bairro",
                        "contato.email",
                        "contato.telefone",
                        "contato.celular",
                        "contato.website",
                        "u.id as userId"

        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Empresa")
             ->createQueryBuilder("e");
        $qb->select($fields)
             ->innerJoin("System\Model\ClassificadoEmpresa", "ce", "WITH", "e.id = ce.empresa_id AND ce.status = 1")
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = e.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = e.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->innerJoin("System\Model\Endereco", "endereco", "WITH", "e.id = endereco.empresa_id")
             ->leftJoin("System\Model\Contato", "contato", "WITH", "e.id = contato.empresa_id")
             ->where("e.status = 1")
             ->andWhere('e.id = :id')
             ->andWhere('ce.reactivated >= :date');

        $qb->setParameter('id', $empresaId);
        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $empresa = $qb->getQuery()->getResult();//Info da empresa

        if(count($empresa) <= 0)
            return $this->notFoundAction();

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select(array("c.id", "c.valor"))
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where("c.status = 1")
             ->andWhere('c.reactivated >= :date')
             ->andWhere('c.empresa_id = :id')
             ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $qb->setParameter('id', $empresaId);

        $classificados = $qb->getQuery()->getResult();//ID`s dos classificados da empresa
        if(count($classificados) <= 0)
            return $this->redirect()->toUrl('/');//Não possui classificado

        $classificado_id = array();
        foreach ($classificados as $classificado) {
            array_push($classificado_id, $classificado["id"]);
        }
        
        $qb = $this->getEntityManager()
             ->getRepository("System\Model\ClassificadoImagem")
             ->createQueryBuilder("ci");
        $qb->select(array("ci.nome", "ci.mime_type", "ci.capa", "c.id as classificado_id", "c.valor"))
             ->innerJoin("System\Model\Classificado", "c", "WITH", "c.id = ci.classificado_id")
             ->where("ci.capa = 1")
             ->andWhere("ci.classificado_id IN (:classificado_id)")
             ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('classificado_id', $classificado_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        $imagens = $qb->getQuery()->getResult();//Imagens de capa dos classificados da empresa

        if(count($imagens) <= 0)
            return $this->redirect()->toUrl('/');//Não possui imagens

        $this->insertViewsClassificadoEmpresa($empresaId);

        $this->layout()->title = $empresa[0]["nome"];

        $contatoAnuncianteForm = new ContatoAnuncianteForm;

        $view =  new ViewModel(array(
            "empresa"  => $empresa[0],
            //"subcategoria"  => $subcategoria,
            "imagens"       => $imagens,
            //"favorito"      => $favorito,
            "contatoAnuncianteForm" => $contatoAnuncianteForm,
        ));

        return $view;
    }

    private function insertViewsClassificadoEmpresa($classificadoEmpresa_id){
        $commit = true;

        $classificadoEmpresa = $this->getEntityManager()
                                    ->getRepository('System\Model\ClassificadoEmpresa')
                                    ->findBy(array('empresa_id' => $classificadoEmpresa_id));

        if(count($classificadoEmpresa) <= 0)
            return false;

        try{
            $classificadoEmpresa = $classificadoEmpresa[0];
            $visualizacoes = $classificadoEmpresa->__get('visualizacoes')+1;
            $classificadoEmpresa->__set('visualizacoes', $visualizacoes);
            $this->getEntityManager()->persist($classificadoEmpresa);
            $this->getEntityManager()->flush();
        } catch(\Exception $e){
            $commit = false;
        }

        return $commit;
    }

    /**
     * Realiza o envio de e-mail ao anunciante e também registra este e-mail
     * @return ViewModel
     */
    public function contatarAnuncianteAction()
    {
        $request = $this->getRequest();
        if(!$request->isXmlHttpRequest())
            return $this->redirect()->toUrl('/');

        $response = $this->getResponse();
        $anuncio_id = $request->getQuery('anuncio');
        $nome       = $request->getQuery('contactName');
        $mensagem   = $request->getQuery('comments');
        $email_from = $request->getQuery('email');
        $date       = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

        $anuncio = $this->getEntityManager()->find("System\Model\Classificado", $anuncio_id);
        if(!$anuncio){
            $response->setStatusCode(500);
            return $response;
        }

        $usuario = $this->getEntityManager()->find("System\Model\User", $anuncio->usuario_id);
        if(!$usuario){
            $response->setStatusCode(500);
            return $response;
        }

        try{
            $contatoAnuncianteModel = new ContatoAnuncianteModel;
            $contatoAnuncianteModel->__set('nome', $nome);
            $contatoAnuncianteModel->__set('email', $email_from);
            $contatoAnuncianteModel->__set('mensagem', $mensagem);
            $contatoAnuncianteModel->__set('usuario_id', $usuario);
            $contatoAnuncianteModel->__set('classificado_id', $anuncio);
            $contatoAnuncianteModel->__set('created', $date);

            $this->getEntityManager()->persist($contatoAnuncianteModel);
            $this->getEntityManager()->flush();
            $response->setStatusCode(200);
        } catch(\Exception $e){
            $response->setStatusCode(500);
            return $response;
        }

        $tituloAnuncio = $anuncio->titulo;
        $remetente     = $anuncio->email;
        $sendMailAnunciante = $this->sendMailAnunciante($tituloAnuncio, $remetente, $email_from, $mensagem, $anuncio->valor, $anuncio_id, $nome);
        if(!$sendMailAnunciante)
            $response->setStatusCode(500);
        
        return $response;
    }

    /**
     * Realiza o envio de e-mail ao anunciante responsável a empresa e também registra este e-mail
     * @return ViewModel
     */
    public function contatarEmpresaAction()
    {
        $request = $this->getRequest();
        if(!$request->isXmlHttpRequest())
            return $this->redirect()->toUrl('/');

        $response = $this->getResponse();
        $empresa_id = $request->getQuery('empresa');
        $nome       = $request->getQuery('contactName');
        $mensagem   = $request->getQuery('comments');
        $email_from = $request->getQuery('email');
        $date       = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

        $empresa = $this->getEntityManager()->find("System\Model\Empresa", $empresa_id);
        if(!$empresa){
            $response->setStatusCode(500);
            return $response;
        }

        $usuario = $this->getEntityManager()->find("System\Model\User", $empresa->usuario_id);
        if(!$usuario){
            $response->setStatusCode(500);
            return $response;
        }

        try{
            $contatoAnuncianteModel = new ContatoAnuncianteModel;
            $contatoAnuncianteModel->__set('nome', $nome);
            $contatoAnuncianteModel->__set('email', $email_from);
            $contatoAnuncianteModel->__set('mensagem', $mensagem);
            $contatoAnuncianteModel->__set('usuario_id', $usuario);
            $contatoAnuncianteModel->__set('empresa_id', $empresa);
            $contatoAnuncianteModel->__set('created', $date);

            $this->getEntityManager()->persist($contatoAnuncianteModel);
            //$this->getEntityManager()->flush();
            $response->setStatusCode(200);
        } catch(\Exception $e){
            $response->setStatusCode(500);
            return $response;
        }

        try{
            $contato = $this->getEntityManager()
                            ->getRepository("System\Model\Contato")
                            ->findBy(array("empresa_id" => $empresa->id));

            if(count($contato[0]) <= 0){
                $response->setStatusCode(500);
                $response->setContent("contato not found");
                return $response;
            }
        } catch(\Exception $e){
            $response->setStatusCode(500);
            return $response;
        }

        $tituloAnuncio = $empresa->nome;
        $remetente     = $contato[0]->email;
        $sendMailEmpresa = $this->sendMailEmpresa($tituloAnuncio, $remetente, $email_from, $mensagem, $empresa_id, $nome);

        if(!$sendMailEmpresa)
            $response->setStatusCode(500);
        
        return $response;
    }

    /**
     * Realiza o envio de e-mail ao anunciante
     * @return boolean
     */
    private function sendMailAnunciante($titulo, $remetente, $from, $mensagem, $preco, $anuncio_id, $nomeFrom) {
        $sent = true;

        $message = new Message();
        $message->addTo($remetente)
            ->addFrom($from)
            ->setSubject('Tem Pra Festa: Contato realizado para o an&uacute;ncio '.$titulo);

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase    = $helper->__invoke();
        $urlAnuncio = $urlBase."/classificado/anuncio/".$anuncio_id;

        $textHtml = '<table cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;line-height:100%;color:#333" bgcolor="#f5f5f5" width="100%">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td valign="top" align="center">';
        $textHtml .= '<div style="margin:auto;max-width:600px;min-width:320px">';
        $textHtml .= '<div style="border:1px solid #d0d0d0;max-width:600px;min-width:320px;text-align:left">';
        $textHtml .= '<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" style="max-width:600px;min-width:320px">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td>';
        $textHtml .= '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF" style="max-width:600px;min-width:320p">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td width="100%" valign="top">';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/cabecalho.png" alt="Tem Pra Festa Encontre e Anuncie" title="Tem Pra Festa Encontre e Anuncie">';
        $textHtml .= '</td>';
        $textHtml .= '</tr><tr><td>';
        $textHtml .= '<table width="100%" cellpadding="20" cellspacing="0" align="center" bgcolor="#FFFFFF">';
        $textHtml .= '<tbody><tr><td>';
        $textHtml .= '<strong style="font-size:18px">Voc&ecirc; recebeu uma mensagem sobre o an&uacute;ncio:</strong>';
        $textHtml .= '<div style="border:1px solid #cccccc;padding:20px 10px;margin:20px 0">';
        $textHtml .= '<table border="0">';
        $textHtml .= '<tbody><tr><td>';
        $textHtml .= '<div style="border:1px solid #cccccc;text-align:center;width:100px;min-height:80px">';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/icone_anuncie.png" alt="'.$titulo.'" style="width:100px;min-height:auto">';
        $textHtml .= '</div></td><td>&nbsp;</td>';
        $textHtml .= '<td><a href="'.$urlAnuncio.'" style="color:#3388bb;text-decoration:none" target="_blank">'.$titulo.'</a>';
        $textHtml .= '<br><br>';
        $textHtml .= '<strong>R$ '.$preco.'</strong>';
        $textHtml .= '</td></tr></tbody></table></div>';
        $textHtml .= '<table width="100%" border="1" style="border:1px solid #cccccc;margin:0 0 20px">';
        $textHtml .= '<tbody><tr><td style="padding:20px 10px">';
        $textHtml .= '<strong>Mensagem nova:</strong>';
        $textHtml .= $mensagem;
        $textHtml .= '</td></tr><tr>';
        $textHtml .= '<td style="padding:20px 10px;background:#eeeeee;line-height:24px">Mensagem enviada por:';
        $textHtml .= '<strong>'.$nomeFrom.'</strong><br>';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/mail.png" style="margin-right:4px">';
        $textHtml .= '<a style="font-size:16px;color:#38b;text-decoration:none" href="mailto:'.$from.'" target="_blank">'.$from.'</a>';
        $textHtml .= '</td></tr></tbody></table>';
        $textHtml .= '<div style="padding:20px;border:1px solid #7bd;background:#eff">';
        $textHtml .= '<strong style="font-size:14px"> Dicas de seguran&ccedil;a</strong>';
        $textHtml .= '<ul style="padding-left:15px;margin:5px 0 0 0;color:#333">';
        $textHtml .= '<li style="margin-bottom:8px">Encontre com o comprador em um local p&uacute;blico e movimentado.</li>';
        $textHtml .= '<li style="margin-bottom:8px">N&atilde;o entregue o produto antes de ter certeza do pagamento.</li>';
        $textHtml .= '<li style="margin-bottom:8px">Evite receber em cheque, j&aacute; que n&atilde;o &eacute; poss&iacute;vel ter certeza que o mesmo ter&aacute; fundos.</li>';
        $textHtml .= '</ul></div><br>Encontre e anuncie!<br>';
        $textHtml .= 'Equipe <span class="il">Tem Pra Festa</span>';
        $textHtml .= '</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div><br>';
        $textHtml .= '<table cellpadding="20" cellspacing="0" border="0" align="center" style="max-width:600px;min-width:320px;font-size:12px">';
        $textHtml .= '<tbody><tr><td width="100%" valign="top"><br>';
        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center">';
        $textHtml .= '<tbody><tr><td colspan="3" align="center" style="font-size:16px;color:#999">Fique conectado!<br><br></td></tr><tr>';
        $textHtml .= '<td><a href="https://www.facebook.com/temprafesta"><img src="'.$urlBase.'/img/site/email/contato_anunciante/facebook.png" alt="Facebook"></a>&nbsp;</td>';
        $textHtml .= '<td><a href="https://twitter.com/temprafesta" target="_blank"><img src="'.$urlBase.'/img/site/email/contato_anunciante/twitter.png" alt="Twitter"></a>&nbsp;</td>';
        $textHtml .= '<td><a href="https://plus.google.com/u/0/115802595672962402517/posts" target="_blank"><img src="'.$urlBase.'/img/site/email/contato_anunciante/google_plus.png" alt="Google+"></a></td>';
        $textHtml .= '</tr></tbody></table><br>';
        $textHtml .= 'Se tiver d&uacute;vidas, <a href="mailto:suporte@temprafesta.com" target="_blank">Fale com o <span class="il">Tem Pra Festa</span></a>. Para garantir o recebimento dos nossos e-mails adicione <a style="color:#3388bb;text-decoration:none" href="mailto:suporte@temprafesta.com" target="_blank">suporte@<span class="il">temprafesta</span>.com</a> na sua lista de contatos.';
        $textHtml .= '</td></tr></tbody></table></div></td></tr></tbody></table>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
        } catch(\Exception $e){
            $sent = false;
            echo $e->getMessage();
        }

        return $sent;
    }

    /**
     * Realiza o envio de e-mail ao responsável de uma empresa
     * @return boolean
     */
    private function sendMailEmpresa($titulo, $remetente, $from, $mensagem, $empresa_id, $nomeFrom) {
        $sent = true;

        $message = new Message();
        $message->addTo($remetente)
            ->addFrom($from)
            ->setSubject('Tem Pra Festa: Contato realizado para a empresa '.$titulo);

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase    = $helper->__invoke();
        $urlAnuncio = $urlBase."/empresa/empresa/".$empresa_id;

        $textHtml = '<table cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;line-height:100%;color:#333" bgcolor="#f5f5f5" width="100%">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td valign="top" align="center">';
        $textHtml .= '<div style="margin:auto;max-width:600px;min-width:320px">';
        $textHtml .= '<div style="border:1px solid #d0d0d0;max-width:600px;min-width:320px;text-align:left">';
        $textHtml .= '<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF" style="max-width:600px;min-width:320px">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td>';
        $textHtml .= '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF" style="max-width:600px;min-width:320p">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td width="100%" valign="top">';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/cabecalho.png" alt="Tem Pra Festa Encontre e Anuncie" title="Tem Pra Festa Encontre e Anuncie">';
        $textHtml .= '</td>';
        $textHtml .= '</tr><tr><td>';
        $textHtml .= '<table width="100%" cellpadding="20" cellspacing="0" align="center" bgcolor="#FFFFFF">';
        $textHtml .= '<tbody><tr><td>';
        $textHtml .= '<strong style="font-size:18px">Voc&ecirc; recebeu uma mensagem sobre a empresa:</strong>';
        $textHtml .= '<div style="border:1px solid #cccccc;padding:20px 10px;margin:20px 0">';
        $textHtml .= '<table border="0">';
        $textHtml .= '<tbody><tr><td>';
        $textHtml .= '<div style="border:1px solid #cccccc;text-align:center;width:100px;min-height:80px">';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/icone_empresa.png" alt="'.$titulo.'" style="width:100px;min-height:auto">';
        $textHtml .= '</div></td><td>&nbsp;</td>';
        $textHtml .= '<td><a href="'.$urlAnuncio.'" style="color:#3388bb;text-decoration:none" target="_blank">'.$titulo.'</a>';
        $textHtml .= '</td></tr></tbody></table></div>';
        $textHtml .= '<table width="100%" border="1" style="border:1px solid #cccccc;margin:0 0 20px">';
        $textHtml .= '<tbody><tr><td style="padding:20px 10px">';
        $textHtml .= '<strong>Mensagem nova:</strong>';
        $textHtml .= $mensagem;
        $textHtml .= '</td></tr><tr>';
        $textHtml .= '<td style="padding:20px 10px;background:#eeeeee;line-height:24px">Mensagem enviada por:';
        $textHtml .= '<strong>'.$nomeFrom.'</strong><br>';
        $textHtml .= '<img src="'.$urlBase.'/img/site/email/contato_anunciante/mail.png" style="margin-right:4px">';
        $textHtml .= '<a style="font-size:16px;color:#38b;text-decoration:none" href="mailto:'.$from.'" target="_blank">'.$from.'</a>';
        $textHtml .= '</td></tr></tbody></table>';
        $textHtml .= '<div style="padding:20px;border:1px solid #7bd;background:#eff">';
        $textHtml .= '<strong style="font-size:14px"> Dicas de seguran&ccedil;a</strong>';
        $textHtml .= '<ul style="padding-left:15px;margin:5px 0 0 0;color:#333">';
        $textHtml .= '<li style="margin-bottom:8px">Encontre com o comprador em um local p&uacute;blico e movimentado.</li>';
        $textHtml .= '<li style="margin-bottom:8px">N&atilde;o entregue o produto antes de ter certeza do pagamento.</li>';
        $textHtml .= '<li style="margin-bottom:8px">Evite receber em cheque, j&aacute; que n&atilde;o &eacute; poss&iacute;vel ter certeza que o mesmo ter&aacute; fundos.</li>';
        $textHtml .= '</ul></div><br>Encontre e anuncie!<br>';
        $textHtml .= 'Equipe <span class="il">Tem Pra Festa</span>';
        $textHtml .= '</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div><br>';
        $textHtml .= '<table cellpadding="20" cellspacing="0" border="0" align="center" style="max-width:600px;min-width:320px;font-size:12px">';
        $textHtml .= '<tbody><tr><td width="100%" valign="top"><br>';
        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center">';
        $textHtml .= '<tbody><tr><td colspan="3" align="center" style="font-size:16px;color:#999">Fique conectado!<br><br></td></tr><tr>';
        $textHtml .= '<td><a href="https://www.facebook.com/temprafesta"><img src="'.$urlBase.'/img/site/email/contato_anunciante/facebook.png" alt="Facebook"></a>&nbsp;</td>';
        $textHtml .= '<td><a href="https://twitter.com/temprafesta" target="_blank"><img src="'.$urlBase.'/img/site/email/contato_anunciante/twitter.png" alt="Twitter"></a>&nbsp;</td>';
        $textHtml .= '<td><a href="https://plus.google.com/u/0/115802595672962402517/posts" target="_blank"><img src="'.$urlBase.'/img/site/email/contato_anunciante/google_plus.png" alt="Google+"></a></td>';
        $textHtml .= '</tr></tbody></table><br>';
        $textHtml .= 'Se tiver d&uacute;vidas, <a href="mailto:suporte@temprafesta.com" target="_blank">Fale com o <span class="il">Tem Pra Festa</span></a>. Para garantir o recebimento dos nossos e-mails adicione <a style="color:#3388bb;text-decoration:none" href="mailto:suporte@temprafesta.com" target="_blank">suporte@<span class="il">temprafesta</span>.com</a> na sua lista de contatos.';
        $textHtml .= '</td></tr></tbody></table></div></td></tr></tbody></table>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
        } catch(\Exception $e){
            $sent = false;
        }

        return $sent;
    }

    private function checkAnuncioFavorito($user_id, $classificado_id){
        $retorno = false;

        $favorito = $this->getEntityManager()
                             ->getRepository('System\Model\Favoritos')
                             ->findBy(
                                array(
                                    "usuario_id" => $user_id,
                                    "classificado_id" => $classificado_id,
                                )
                              );

        if(count($favorito) > 0)
            $retorno = true;

        return $retorno;
    }

    private function insertViewsClassificado($classificado_id){
        $commit = true;

        $classificado = $this->getEntityManager()->find('System\Model\Classificado', $classificado_id);

        if(!$classificado)
            return false;

        try{
            $visualizacoes = $classificado->__get('visualizacoes')+1;
            $classificado->__set('visualizacoes', $visualizacoes);
            $this->getEntityManager()->persist($classificado);
            $this->getEntityManager()->flush();
        } catch(\Exception $e){
            $commit = false;
        }

        return $commit;
    }

    private function findAnuncioEmpresa(){

        $retorno = null;

        $twentyDaysAgo = new \DateTime("-20 days");
        $anuncios = $this->getEntityManager()
            ->getRepository("System\Model\ClassificadoEmpresa")
            ->createQueryBuilder("ce");
        $anuncios->select(array("ce"))
            ->leftJoin('ce.empresa_id', 'e', "WITH")
            ->where("ce.status = 1")
            ->andWhere('ce.created >= :date')
            ->orderBy("ce.created", "DESC");

        $anuncios->setParameter('date', $twentyDaysAgo, \Doctrine\DBAL\Types\Type::DATETIME);

        if(count($anuncios->getQuery()->getResult()) > 0){
            $anuncios = $anuncios->getQuery()->getResult();
            shuffle($anuncios);
            $anuncios = array_slice($anuncios, 0, 2);
            $retorno = $anuncios;
        }

        return $retorno;
    }

    private function findAnuncioNormal(){

        $retorno = null;

        $anunciosDestaque = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $anunciosDestaque->select(array("c"))
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("ci.capa = 1")
             ->orderBy("c.reactivated", "DESC");

        $paginatorAnuncioDestaqueAdapter = new DoctrineAdapter(new ORMPaginator($anunciosDestaque));
        $paginatorAnuncioDestaque        = new ZendPaginator($paginatorAnuncioDestaqueAdapter);
        $paginatorAnuncioDestaque->setDefaultItemCountPerPage(12);
        $paginatorAnuncioDestaque->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($anunciosDestaque->getQuery()->getResult()) > 0){
            $retorno = $paginatorAnuncioDestaque;
        }

        return $retorno;
    }

    /**
     * Apresenta tela de cadastro de usuário
     * @return ViewModel
     */
    public function registerAction()
    {
        $this->layout()->title = "Cadastro de Usuário";
        $registerForm   = new RegisterForm;
        $comboboxEstado = new ComboboxEstado($this->getServiceLocator());

        return new ViewModel(
            array(
                'registerForm'  => $registerForm,
                'comboboxEstado'  => $comboboxEstado
            )
        );
    }
}