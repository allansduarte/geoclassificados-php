<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

use Core\Controller\ActionController;

use Zend\Paginator\Paginator as ZendPaginator;
use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Site\Form\ContatoSite as ContatoSiteForm;
use Site\Form\MainSearch as MainSearchForm;
use Site\Form\Register as RegisterForm;
use System\Form\ComboboxEstado;

use System\Model\Classificado as ClassificadoModel;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

/**
 * Controlador que gerencia os anuncios
 * 
 * @category Site
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class IndexController extends ActionController
{

    private $periodoAnuncio = "-30 days";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {

        if (null === $this->_em) {
            $this->_em = $this->getService('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Mostra os posts cadastrados
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";
        $classificadoModel = new ClassificadoModel;
        $mainSearchForm    = new MainSearchForm;

        $view =  new ViewModel(array(
            'mainSearchForm' => $mainSearchForm,
		));

        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        $anunciosMapa = $this->findAnuncioMapa();
        if($anunciosMapa){

            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        return $view;
    }

    /**
     * Apresenta tela sobre
     * @return ViewModel
     */
    public function sobreAction()
    {
        $this->layout()->title = "Sobre o Tem Pra Festa";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Apresenta tela de categorias
     * @return ViewModel
     */
    public function categoriasAction()
    {
        $this->layout()->title = "Categorias do Tem Pra Festa";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Apresenta tela planos de anúncio
     * @return ViewModel
     */
    public function planosDeAnuncioAction()
    {
        $this->layout()->title = "Planos de Anúncio do Tem Pra Festa";

        $view =  new ViewModel();

        return $view;
    }

    /**
     * Apresenta tela contato
     * @return ViewModel
     */
    public function contatoAction()
    {
        $this->layout()->title = "Entre em contato com o Tem Pra Festa";

        $contatoSiteForm = new ContatoSiteForm;
        $view =  new ViewModel(array(
            'contatoSiteForm' => $contatoSiteForm
        ));

        return $view;
    }

    /**
     * Realiza o envio de e-mail para suporte@temprafesta.com
     * @return ViewModel
     */
    public function contatarSiteAction()
    {
        $request = $this->getRequest();
        if(!$request->isXmlHttpRequest())
            return $this->redirect()->toUrl('/');

        $response   = $this->getResponse();
        $subject    = $request->getQuery('subject');
        $nome       = $request->getQuery('contactName');
        $mensagem   = $request->getQuery('comments');
        $from       = $request->getQuery('email');

        $message = new Message();
        $message->addTo("suporte@temprafesta.com")
            ->addFrom($from)
            ->setSubject($subject);

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase    = $helper->__invoke();

        $textHtml = '<table cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,Sans-Serif;font-size:14px;line-height:100%;color:#333" bgcolor="#f5f5f5" width="100%">';
        $textHtml .= '<tbody>';
        $textHtml .= '<tr><td>Nome:</td><td>Mensagem:</td><td>Enviado por:</td></tr>';
        $textHtml .= '<tr><td>'.$nome.'</td><td>'.$mensagem.'</td><td>'.$from.'</td></tr>';
        $textHtml .= '</tbody>';
        $textHtml .= '</table>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
            $response->setStatusCode(200);
        } catch(\Exception $e){
            $response->setStatusCode(500);
        }
        
        return $response;
    }

    private function findAnuncioMapa(){
        $retorno = null;

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        /*$cacheDriver = new \Doctrine\Common\Cache\ApcCache();
        if($cacheDriver->contains('findAnuncioMapa_index')){
            $cacheDriver->delete('findAnuncioMapa_index');
            return $cacheDriver->fetch('findAnuncioMapa_index');
        }*/

        $fields = array(
            "c.id",
            "u.id as userId",
            "c.titulo",
            "c.valor",
            "c.latitude",
            "c.longitude",
            "c.subcategoria_id",
            "ci.nome as imagemNome",
            "categoria.nome as nome_categoria",
            "categoria.icone",
            "categoria.map_marker"
        );
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select($fields)
           ->from('System\Model\Classificado', 'c')
           ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
           ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
           ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
           ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
           ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
           ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
           ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
           ->where($qb->expr()->gte('c.reactivated', ':periodoAnuncio'))
           ->andWhere($qb->expr()->eq('c.status', '1'))
           ->andWhere($qb->expr()->isNotNull('c.latitude'))
           ->andWhere($qb->expr()->neq("c.latitude", "''"))
           ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('periodoAnuncio', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $query    = $qb->getQuery();
        $anuncios = $query->getScalarResult();

        //$cacheDriver->save('findAnuncioMapa_index', $anuncios, 60);

        if(count($anuncios) > 0){
            $retorno = $anuncios;
        }

        return $retorno;
    }

    private function findAnuncioEmpresa(){

        $retorno = null;

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $anuncios = $this->getEntityManager()
            ->getRepository("System\Model\ClassificadoEmpresa")
            ->createQueryBuilder("ce");
        $anuncios->select(array("ce"))
            ->leftJoin('ce.empresa_id', 'e', "WITH")
            ->where("ce.status = 1")
            ->andWhere('ce.created >= :date')
            ->orderBy("ce.created", "DESC");

        $anuncios->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $results = $anuncios->getQuery()->getResult();

        if(count($results) > 0){
            $anuncios = $results;
            shuffle($anuncios);
            $anuncios = array_slice($anuncios, 0, 2);
            $retorno = $anuncios;
        }

        return $retorno;
    }

    private function findAnuncioNormal(){

        $retorno = null;

        $anunciosDestaque = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $anunciosDestaque->select(array("c"))
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("ci.capa = 1")
             ->orderBy("c.created", "DESC");

        $paginatorAnuncioDestaqueAdapter = new DoctrineAdapter(new ORMPaginator($anunciosDestaque));
        $paginatorAnuncioDestaque        = new ZendPaginator($paginatorAnuncioDestaqueAdapter);
        $paginatorAnuncioDestaque->setDefaultItemCountPerPage(12);
        $paginatorAnuncioDestaque->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($anunciosDestaque->getQuery()->getResult()) > 0){
            $retorno = $paginatorAnuncioDestaque;
        }

        return $retorno;
    }

    /**
     * Apresenta tela de cadastro de usuário
     * @return ViewModel
     */
    public function registerAction()
    {
        $this->layout()->title = "Cadastro de Usuário";
        $registerForm   = new RegisterForm;
        $comboboxEstado = new ComboboxEstado($this->getServiceLocator());

        return new ViewModel(
            array(
                'registerForm'  => $registerForm,
                'comboboxEstado'  => $comboboxEstado
            )
        );
    }
}