<?php
namespace Site\Controller;

use Zend\View\Model\ViewModel;

use Core\Controller\ActionController;

use Doctrine\ORM\EntityManager;

use System\Form\ComboboxEstado;
use Site\Form\MainSearch as MainSearchForm;

/**
 * Controlador que gerencia os anuncios
 * 
 * @category Site
 * @package Controller
 * @author  Allan Duarte <allan@temprafesta.com.br>
 */
class SearchController extends ActionController
{

    private $periodoAnuncio = "-30 days";
    protected $qtdEmpresa     = 1;

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    private function setQtdEmpresa($qtdEmpresa){
        $this->qtdEmpresa = $qtdEmpresa;
    }
    private function getQtdEmpresa(){
        return $this->qtdEmpresa;
    }

    /**
     * Tela de busca principal
     * @return ViewModel
     */
    public function allAction()
    {
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";
        $request = $this->getRequest();
        $mainSearchForm    = new MainSearchForm;

        if(strlen($request->getQuery('categoria')) > 0){
            $categoria_explode = explode('_', $request->getQuery('categoria'));
            $categoria_tipo    = $categoria_explode[0];
            $categoria_id      = (int) $categoria_explode[1];
        }
        
        $estado            = (int) $request->getQuery('estado');
        $cidade            = (int) $request->getQuery('cidade');
        $palavra_chave     = $request->getQuery('palavra_chave');

        $view =  new ViewModel(array(
            'mainSearchForm' => $mainSearchForm,
        ));

        if(strlen($request->getQuery('categoria')) > 0){
            $subcategoria_id = 0;
            if($categoria_tipo == "s"){
                $subcategoria_id = $categoria_id;
            } else if($categoria_tipo == "c"){
                $subcategorias = $this->getEntityManager()
                                 ->getRepository("System\Model\Subcategoria")
                                 ->findBy(array("categoria_id" => $categoria_id));
                $subcategoria_id = array();
                foreach($subcategorias as $subcategoria){
                    array_push($subcategoria_id, $subcategoria->id);
                }
            }
        } else{
            $subcategoria_id = null;
        }

        $cidade_id = $cidade;
        if($cidade_id <= 0){
            $cidades = $this->getEntityManager()
                             ->getRepository("System\Model\Cidades")
                             ->findBy(array("estado_id" => $estado));
            $cidade_id = array();
            foreach($cidades as $cidade){
                array_push($cidade_id, $cidade->id);
            }
        }

        $anunciosMapa = $this->findAnuncioMapa($subcategoria_id, $cidade_id, $palavra_chave);
        if($anunciosMapa){
            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        $this->setQtdEmpresa(2);
        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        return $view;
    }

    /**
     * Apresenta tela de busca por tag
     * @return ViewModel
     */
    public function tagAction()
    {
        $tag = $this->params()->fromRoute('tag', null);

        /*if(!$tag)
            return //erro 404
        */

        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";
        $mainSearchForm    = new MainSearchForm;

        $view =  new ViewModel(array(
            'tag' => $tag,
            'mainSearchForm' => $mainSearchForm,
        ));

        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        $anunciosMapa = $this->findAnuncioMapa(null, null, null, $tag);
        if($anunciosMapa){
            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        return $view;
    }


    /**
     * Apresenta tela de busca por categoria
     * @return ViewModel
     */
    public function categoryAction()
    {
        $id = $this->params()->fromRoute('id', 0);
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";
        $mainSearchForm    = new MainSearchForm;

        $view =  new ViewModel(array(
            'categoria_id' => $id,
            'mainSearchForm' => $mainSearchForm,
        ));

        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        $subcategorias = $this->getEntityManager()
                             ->getRepository("System\Model\Subcategoria")
                             ->findBy(array("categoria_id" => $id));
        $subcategoria_id = array();
        foreach($subcategorias as $subcategoria){
            array_push($subcategoria_id, $subcategoria->id);
        }

        $anunciosMapa = $this->findAnuncioMapa($subcategoria_id);
        if($anunciosMapa){
            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        $nomeCategoria = $this->getEntityManager()->find("System\Model\Categoria", $id);

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select(array("COUNT(c.id) as anuncios"))
             ->where('c.reactivated >= :date')
             ->andWhere("c.subcategoria_id IN (:subcategorias)")
             ->andWhere('c.status = 1');

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $qb->setParameter('subcategorias', $subcategoria_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        $categoria = $qb->getQuery()->getResult();

        $view->setVariables(array(
                'qtdAnuncios'      => $categoria[0]['anuncios'],
                'qtdSubcategorias' => count($subcategoria_id),
                "nomeCategoria" => $nomeCategoria->__get('nome'),
        ));

        return $view;
    }

    /**
     * Apresenta tela de busca por categoria
     * @return ViewModel
     */
    public function subcategoryAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";
        $mainSearchForm    = new MainSearchForm;

        $view =  new ViewModel(array(
            'subcategoria_id' => $id,
            'mainSearchForm' => $mainSearchForm,
        ));

        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        $anunciosMapa = $this->findAnuncioMapa($id);
        if($anunciosMapa){
            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select(array("COUNT(c.id) as anuncios", "s.nome as subcategoria_nome", "categoria.id as categoria_id"))
             ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
             ->where('c.reactivated >= :date')
             ->andWhere('c.status = 1')
             ->andWhere("c.subcategoria_id = :subcategoria");

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $qb->setParameter('subcategoria', $id);

        $subcategoria = $qb->getQuery()->getResult();

        $view->setVariables(array(
                'qtdAnuncios'      => $subcategoria[0]['anuncios'],
                'nomeSubcategoria' => $subcategoria[0]['subcategoria_nome'],
                'categoria_id' => $subcategoria[0]['categoria_id'],
        ));

        return $view;
    }

    /**
     * Mostra os posts cadastrados
     * @return ViewModel
     */
    public function indexAction()
    {
        $this->layout()->title = "Encontre tudo para festas grátis! Aproveite!";

        $view =  new ViewModel(array(
        ));

        $anunciosEmpresa = $this->findAnuncioEmpresa();
        if($anunciosEmpresa){
            $view->setVariables(array(
                'paginatorAnuncioEmpresa' => $anunciosEmpresa,
            ));
        }

        $anunciosMapa = $this->findAnuncioMapa();
        if($anunciosMapa){
            $view->setVariables(array(
                'anunciosMapa' => $anunciosMapa,
            ));
        }

        return $view;
    }

    private function findAnuncioMapa($subcategoria = null, $cidade = null, $palavra_chave = null, $tag = null){
        $retorno = null;

        $fields = array(
            "c.id",
            "u.id as userId",
            "c.titulo",
            "c.valor",
            "c.latitude",
            "c.longitude",
            "c.subcategoria_id",
            "ci.nome as imagemNome",
            //"ci.mime_type as imagemMimeType",
            //"ci.file_memory as imagemData",
            "categoria.nome as nome_categoria",
            "categoria.icone as icone_categoria",
            "categoria.map_marker as map_marker_categoria"
        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where($qb->expr()->gte('c.reactivated', ':periodoAnuncio'))
             ->andWhere($qb->expr()->eq('c.status', '1'))
             ->andWhere($qb->expr()->isNotNull('c.latitude'))
             ->andWhere($qb->expr()->neq("c.latitude", "''"))
             ->orderBy("c.reactivated", "DESC");
        $qb->setParameter('periodoAnuncio', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if(isset($palavra_chave) && $palavra_chave != "")
        {
            $qb->andWhere("c.titulo LIKE :palavra_chave");
            $qb->setParameter('palavra_chave', '%'.$palavra_chave.'%');

            $qb->orWhere("c.descricao LIKE :palavra_chave");
            $qb->setParameter('palavra_chave', '%'.htmlentities($palavra_chave).'%');
        }

        if(isset($tag) && $tag != ""){
            $qb->andWhere("c.tags LIKE :tag");
            $qb->setParameter('tag', '%'.trim($tag).'%');
        }

        if(isset($subcategoria) && $subcategoria > 0)
        {
            if(is_array($subcategoria)){
                $qb->andWhere("c.subcategoria_id IN (:subcategorias)");
                $qb->setParameter('subcategorias', $subcategoria, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
            } else if(is_int($subcategoria)){
                $qb->andWhere("c.subcategoria_id = :subcategoria");
                $qb->setParameter('subcategoria', $subcategoria);
            }
        }

        if(isset($cidade))
        {
            if(is_array($cidade) && count($cidade) > 0){
                $qb->andWhere("cidade.id IN (:cidades)");
                $qb->setParameter('cidades', $cidade, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
            } else if(is_int($cidade) && $cidade){
                $qb->andWhere("cidade.id = :cidade");
                $qb->setParameter('cidade', $cidade);
            }
        }

        $query    = $qb->getQuery();
        $anuncios = $query->getScalarResult();
        if(count($anuncios) > 0){
            $retorno = $anuncios;
        }

        return $retorno;
    }

    private function findAnuncioEmpresa(){

        $retorno = null;

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $anuncios = $this->getEntityManager()
            ->getRepository("System\Model\ClassificadoEmpresa")
            ->createQueryBuilder("ce");
        $anuncios->select(array("ce"))
            ->leftJoin('ce.empresa_id', 'e', "WITH")
            ->where("ce.status = 1")
            ->andWhere('ce.reactivated >= :periodoAnuncio')
            ->orderBy("ce.reactivated", "DESC");

        $anuncios->setParameter('periodoAnuncio', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if(count($anuncios->getQuery()->getResult()) > 0){
            $anuncios = $anuncios->getQuery()->getResult();
            shuffle($anuncios);
            $anuncios = array_slice($anuncios, 0, $this->getQtdEmpresa());
            $retorno = $anuncios;
        }

        return $retorno;
    }

    private function findAnuncioNormal(){

        $retorno = null;

        $anunciosDestaque = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $anunciosDestaque->select(array("c"))
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("ci.capa = 1")
             ->orderBy("c.reactivated", "DESC");

        $paginatorAnuncioDestaqueAdapter = new DoctrineAdapter(new ORMPaginator($anunciosDestaque));
        $paginatorAnuncioDestaque        = new ZendPaginator($paginatorAnuncioDestaqueAdapter);
        $paginatorAnuncioDestaque->setDefaultItemCountPerPage(12);
        $paginatorAnuncioDestaque->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($anunciosDestaque->getQuery()->getResult()) > 0){
            $retorno = $paginatorAnuncioDestaque;
        }

        return $retorno;
    }

    /**
     * Apresenta tela de cadastro de usuário
     * @return ViewModel
     */
    public function registerAction()
    {
        $this->layout()->title = "Cadastro de Usuário";
        $registerForm   = new RegisterForm;
        $comboboxEstado = new ComboboxEstado($this->getServiceLocator());

        return new ViewModel(
            array(
                'registerForm'  => $registerForm,
                'comboboxEstado'  => $comboboxEstado
            )
        );
    }
}