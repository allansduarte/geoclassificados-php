<?php
namespace Site\Controller;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

use System\Model\User as UserModel;
use System\Model\Perfil as PerfilModel;

use System\Form\User as UserForm;
use Site\Form\EsqueciMinhaSenha as EsqueciMinhaSenhaForm;

use Doctrine\ORM\EntityManager;

/**
 * Controlador que gerencia os posts
 * 
 * @category Site
 * @package Controller
 * @author  Allan Soares Duarte <allan.duarte@temprafesta.com>
 */
class UserController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * Mostra os usuário cadastrados
     * @return void
     */
    public function indexAction()
    {
        $this->layout()->title = "Página principal";
        //echo "aqui";
        //exit();
        $users = $this->getEntityManager()
                      ->getRepository('System\Model\User')
                      ->findAll();

        return new ViewModel(array(
            'users' => $users
        ));
    }

    /**
     * Envia uma senha gerada ao e-mail informado pelo usuário
     * @return void
     */
    public function esqueciMinhaSenhaAction()
    {

        $this->layout()->title = "Esqueci Minha Senha";
        $esqueciMinhaSenhaForm = new EsqueciMinhaSenhaForm;

        return new ViewModel(array(
            'esqueciMinhaSenhaForm' => $esqueciMinhaSenhaForm
        ));
    }

    /**
     * Gera a senha do usuário e envia para o mesmo via e-mail
     * @return void
     */
    public function esqueciMinhaSenhaGerarAction()
    {
        $this->layout()->title = "Esqueci Minha Senha";
        $request = $this->getRequest();
        $urlRetorno = "/user/esqueci-minha-senha";

        if(!$request->isPost())
            return $this->redirect()->toUrl("/user/esqueci-minha-senha");

        $email    = $request->getPost()["username"];
        $password = null;

        $user  = $this->getEntityManager()
                      ->getRepository("System\Model\User")
                      ->findBy(array(
                            "email"  => $email,
                            "status" => 1,
                      ));

        if(count($user) > 0){
            $password   = md5(uniqid(rand(), true));
            $passwordDB = md5($password);

            $sent = $this->sendMailPassword($password, $email);
            if($sent){
                try{
                    $user[0]->__set('senha', $passwordDB);
                    $this->getEntityManager()->persist($user[0]);
                    $this->getEntityManager()->flush();
                    $this->messages()->flashSuccess('A sua senha foi enviada por e-mail.');
                } catch(\Exception $e){
                    $this->messages()->flashError('Ocorreu um erro ao redefinir sua senha. Por favor desconsidere o e-mail enviado.');
                }
            } else{
                $this->messages()->flashError('Não foi possível enviar o e-mail de recuperação de senha. Por favor tente novamente mais tarde.');
            }
        } else{
            $this->messages()->flashError('Desculpe, não há nenhuma conta associada a este endereço de e-mail. <a href="/user/register">Crie uma conta.</a>');
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Ativa o usuário de acordo com o token recebido
     * @param Zend/Http/Request $token
     * @return void
     */
    public function activationAction()
    {
        $this->layout()->title = "Ativação de Conta";
        $request = $this->getRequest();
        $token = $this->params()->fromRoute('token', 0);

        $user = $this->getEntityManager()
                      ->getRepository('System\Model\User')
                      ->findBy(array("activation_token" => $token, "status" => 0), array());

        $userActive = false;
        if($token == $user[0]->activation_token){
            $user[0]->__set('status', 1);
            try{
                $this->getEntityManager()->persist($user[0]);
                $this->getEntityManager()->flush();
                $userActive = true;
            } catch(\Exception $e){
                $userActive = false;
            }
        }

        return new ViewModel(array(
            'active' => $userActive
        ));
    }

    /**
     * Cria um usuário
     * @return void
     */
    public function saveAction()
    {
        $request = $this->getRequest();

        if($request->isXMLHttpRequest()){
            $requestData = $request->getPost();
            $response = $this->getResponse();
            $content = array();

            $user = $this->getEntityManager()->getRepository('System\Model\User')->findBy( array('email' => $requestData['username']) );
            if($user){
                $content["error_id"] = 4;
                $content = json_encode($content);
                $response->setStatusCode(500);
                $response->setContent($content);
                return $response;
            }

            unset($requestData['submit']);
            $requestData['password'] = md5($requestData['password']);
            $requestData['created']  = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

            $token = bin2hex(openssl_random_pseudo_bytes(127));

            $userModel = new UserModel;
            $userModel->__set('email', $requestData["username"]);
            $userModel->__set('senha', $requestData["password"]);
            $userModel->__set('status', 0);
            $userModel->__set('activation_token', $token);
            $userModel->__set('created', $requestData["created"]);
            
            try{
                $this->getEntityManager()->persist($userModel);
                $this->getEntityManager()->flush();

                try{
                    $requestData['tipo'] = 1;
                    $perfilModel = new PerfilModel;
                    $perfilModel->__set('nome', $requestData["nome"]);
                    $perfilModel->__set('tipo', $requestData["tipo"]);
                    $perfilModel->__set('cidade_id', $requestData["cidade_id"]);
                    $perfilModel->__set('usuario_id', $userModel->__get('id'));
                    $perfilModel->__set('created', $requestData["created"]);

                    $this->getEntityManager()->persist($perfilModel);
                    $this->getEntityManager()->flush();

                    $response->setStatusCode(200);
                    $sendEmail = $this->sendMailActivation($token, $requestData["username"]);
                    if(!$sendEmail){
                        $content["error_id"] = 2;
                        $content["user_id"] = $userModel->__get('id');

                        $content = json_encode($content);
                        $response->setStatusCode(500);
                        $response->setContent($content);
                    }

                } catch(\Exception $e){
                    $content["error_id"] = 3;
                    $content = json_encode($content);
                    $response->setStatusCode(500);
                    $response->setContent($content);
                }
            } catch(\Exception $e){
                $content["error_id"] = 3;
                $content = json_encode($content);
                $response->setStatusCode(500);
                $response->setContent($content);
            }

            return $response;
        } else{
            return $this->redirect()->toUrl('/user/register');
        }
    }

    /**
     * Reenvia o e-mail do usuário
     * @return void
     */
    public function resendEmailAction(){
        $request = $this->getRequest();

        if($request->isXMLHttpRequest()){
            $requestData = $request->getPost();
            $response = $this->getResponse();
            $content = array();

            $userData = $this->getEntityManager()->find("System\Model\User", $requestData["user_id"]);

            if($userData){
                $token     = $userData->activation_token;
                $remetente = $userData->email;
                $sendEmail = $this->sendMailActivation($token, $remetente);

                if(!$sendEmail){
                    $content["error_id"] = 2;
                    $content["user_id"] = $userData->id;

                    $content = json_encode($content);
                    $response->setStatusCode(500);
                    $response->setContent($content);
                } else{
                    $response->setStatusCode(200);
                }
            } else{
                $content["error_id"] = 3;
                $content = json_encode($content);
                $response->setStatusCode(500);
                $response->setContent($content);
            }

            return $response;
        } else{
            return $this->redirect()->toUrl('/');
        }
    }

    /**
     * Realiza o envio de e-mail de esqueci minha senha ao usuário
     * @return boolean
     */
    private function sendMailPassword($password, $remetente) {
        $sent = true;

        $message = new Message();
        $message->addTo($remetente)
            ->addFrom('suporte@temprafesta.com')
            ->setSubject('Esqueceu a Senha da Conta do Tem Pra Festa?');

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase = $helper->__invoke();

        $textHtml = '<div style="margin:0;padding:40px;background-color:#eee">';
        $textHtml .= '<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF">';
        $textHtml .= '<tbody>';
        $textHtml .= '<tr>';
        $textHtml .= '<td colspan="3" bgcolor="#F7F7F7"><img src="'.$urlBase.'/img/system/recuperacao_senha.png" alt="Recupera&ccedil;&atilde;o de Senha Tem Pra Festa"></td>';
        $textHtml .= '</tr>';
        $textHtml .= '<td width="60" bgcolor="#F7F7F7"></td><td width="510" bgcolor="#F7F7F7" style="font:12px/150% Arial;color:#000">';
        $textHtml .= '<br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Ol&aacute; ,<br><br>Ent&atilde;o quer dizer que voc&ecirc; esqueceu sua senha? N&atilde;o tem problema, acabamos de gerar uma nova para voc&ecirc;.<br></font>';
        $textHtml .= '<br><br><br><div align="center">';
        $textHtml .= '<table cellpadding="0" cellspacing="0" border="0" align="center" style="display:block;padding:20px;border:1px solid #ccc;font-size:20px">';
        $textHtml .= '<tbody><tr>';
        $textHtml .= '<td>';
        $textHtml .= '<strong>Nova Senha:</strong> '.$password;
        $textHtml .= '</td>';
        $textHtml .= '</tr></tbody></table>';
        $textHtml .= '<br><br><br>';
        $textHtml .= '<a href="'.$urlBase.'/system/perfil" target="_blank"><img src="'.$urlBase.'/img/system/botao_alterar_senha.png" alt="Alterar a minha senha" title="Alterar a minha senha" style="display:block;border:none"></a>';
        $textHtml .= '</div><br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Obrigado,<br>Equipe Temprafesta</font>';
        $textHtml .= '</td><td width="30" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '<tr><td width="600" colspan="3" height="40" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '</tbody>';
        $textHtml .= '</table>';
        $textHtml .= '</div>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
        } catch(\Exception $e){
            $sent = false;
        }

        return $sent;
    }

    /**
     * Realiza o envio de e-mail de ativação ao usuário
     * @return boolean
     */
    private function sendMailActivation($token, $remetente) {
        $sent = true;

        $message = new Message();
        $message->addTo($remetente)
            ->addFrom('allan.duarte@temprafesta.com')
            ->setSubject('Ative sua Conta no Temprafesta');

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'name'              => 'no-reply',
            'host'              => 'smtp.gmail.com',
            'port' => 587,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'suporte@temprafesta.com',
                'password' => 'spttpf123@',
                'ssl'       => 'tls',
            ),
        ));
         

        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlBase = $helper->__invoke('');

        $textHtml = '<div style="margin:0;padding:40px;background-color:#eee">';
        $textHtml .= '<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#FFFFFF">';
        $textHtml .= '<tbody>';
        $textHtml .= '<tr>';
        $textHtml .= '<td colspan="3" bgcolor="#F7F7F7"><img src="'.$urlBase.'/img/system/confirmacao_cadastro.png"></td>';
        $textHtml .= '</tr>';
        $textHtml .= '<td width="60" bgcolor="#F7F7F7"></td><td width="510" bgcolor="#F7F7F7" style="font:12px/150% Arial;color:#000">';
        $textHtml .= '<br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Ol&aacute; ,<br><br>Voc&ecirc; est&aacute; a um passo de fazer parte do maior site especializado em classificados de festas do Brasil!<br>';
        $textHtml .= 'Para isto, basta clicar no bot&atilde;o abaixo e em seguida voc&ecirc; poder&aacute; anunciar gratuitamente os seus produtos relacionados a festas e vend&ecirc;-los rapidamente:</font>';
        $textHtml .= '<br><br><br><div align="center">';
        $textHtml .= '<a href="'.$urlBase.'/user/activation/'.$token.'" target="_blank"><img src="'.$urlBase.'/img/system/botao_anunciar_gratis_agora.png" alt="Anunciar Gr&aacute;tis Agora!" title="Anunciar Gr&aacute;tis Agora!" style="display:block;border:none"></a>';
        $textHtml .= '</div><br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Para sua seguran&ccedil;a a sua conta s&oacute; ser&aacute; ativada ap&oacute;s a sua confirma&ccedil;&atilde;o.</font>';
        $textHtml .= '<br><br>';
        $textHtml .= '<font face="Arial, Helvetica, sans-serif" size="2" color="#000000">Obrigado,<br>Equipe Tem Pra Festa</font>';
        $textHtml .= '</td><td width="30" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '<tr><td width="600" colspan="3" height="40" bgcolor="#F7F7F7"></td></tr>';
        $textHtml .= '</tbody>';
        $textHtml .= '</table>';
        $textHtml .= '</div>';

        $html = new MimePart($textHtml);
        $html->type = "text/html";
         
        $body = new MimeMessage();
        $body->addPart($html);
         
        $message->setBody($body);
         
        $transport->setOptions($options);

        try{
            $transport->send($message);
        } catch(\Exception $e){
            $sent = false;
        }

        return $sent;
    }
}