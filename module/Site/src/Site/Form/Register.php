<?php
namespace Site\Form;

use Zend\Form\Form;

class Register extends Form
{
    public function __construct()
    {
        parent::__construct('register');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/site/user/save');
        $this->setAttribute('class', 'form-item login-form');
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'text input-textarea half',
                'placeholder' => 'Nome completo',
                'id'    => 'nome-completo',
            ),
            'options' => array(
                'label' => 'Nome completo',
            ),
        ));
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'text input-textarea half',
                'placeholder' => 'E-mail',
                'id'    => 'email',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
                'class' => 'text input-textarea half',
                'placeholder' => 'Senha',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cidade_id',
            'options' => array(
                'label' => "Cidade",
                'class' => 'form-control',
                'id'    => 'cidade',
                'empty_option' => "--Escolha o estado--",
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar e anunciar',
                'id'    => 'edit-submit',
                'class' => 'btn form-submit',
            ),
        ));
    }
}