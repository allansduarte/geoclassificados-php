<?php
namespace Site\Form;

use Zend\Form\Form;

class ContatoAnunciante extends Form
{
    public function __construct()
    {
        parent::__construct('contatoAnunciante');
        //$this->setAttribute('method', 'post');
        //$this->setAttribute('action', '/search/all');
        $this->setAttribute('id', 'contact-form');
        $this->setAttribute('accept-charset', 'UTF-8');
        $this->setAttribute('class', 'contactform');

        $this->add(array(
            'name' => 'contactName',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'input-textarea half',
                'placeholder' => 'Nome Completo',
                'id'    => 'contactName',
                'maxLength' => 128,
            ),
            'options' => array(
                'label' => 'Nome Completo',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'input-textarea half',
                'placeholder' => 'E-mail',
                'id'    => 'email',
                'style' => 'margin-right:0px !important;',
                'maxLength' => 128,
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'comments',
            'attributes' => array(
                'type'  => 'textArea',
                'placeholder' => 'Escreva sua mensagem aqui...',
                'id'    => 'commentsText',
                'cols' => 8,
                'rows' => 5,
                'maxLength' => 4000,
            ),
            'options' => array(
                'label' => 'Mensagem',
            ),
        ));
        $this->add(array(
            'name' => 'submitted',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Contatar anunciante',
                'class' => 'input-submit',
            ),
        ));
    }
}