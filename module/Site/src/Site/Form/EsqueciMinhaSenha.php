<?php
namespace Site\Form;

use Zend\Form\Form;

class EsqueciMinhaSenha extends Form
{
    public function __construct()
    {
        parent::__construct('esqueciMinhaSenha');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/site/user/esqueciMinhaSenhaGerar');
        $this->setAttribute('class', 'form-item login-form');
        
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'text input-textarea half',
                'placeholder' => 'E-mail',
                'id'    => 'email',
                'style' => 'width: 85% !important;',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Continuar',
                'id'    => 'edit-submit',
                'class' => 'btn form-submit',
            ),
        ));
    }
}