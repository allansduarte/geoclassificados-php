<?php
namespace Site\Form;

use Zend\Form\Form;

class MainSearch extends Form
{
    public function __construct()
    {
        parent::__construct('mainSearch');
        $this->setAttribute('method', 'get');
        $this->setAttribute('action', '/search/all');
        $this->setAttribute('id', 'views-exposed-form-search-view-other-ads-page');
        $this->setAttribute('accept-charset', 'UTF-8');
        
        $this->add(array(
            'name' => 'palavra_chave',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-text',
                'placeholder' => 'Ex.: Fantasia, Infantil, DJ, Mágico, etc...',
                'id'    => 'edit-search-api-views-fulltext',
                'size'  => 30,
                'maxLength' => 128,
            ),
            'options' => array(
                'label' => 'Buscar',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'button',
                'value' => 'Buscar',
                'id'    => 'edit-submit-search-view',
                'class' => 'btn btn-primary form-submit',
            ),
        ));
    }
}