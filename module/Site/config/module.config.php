<?php

namespace Site;

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'index',
                        'module'     => 'site'
                    ),
                ),
            ),
            'sobre' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/sobre',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'sobre',
                        'module'     => 'site'
                    ),
                ),
            ),
            'categorias' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/categorias',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'categorias',
                        'module'     => 'site'
                    ),
                ),
            ),
            'planos-de-anuncio' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/planos-de-anuncio',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'planos-de-anuncio',
                        'module'     => 'site'
                    ),
                ),
            ),
            'contato' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/contato',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action'     => 'contato',
                        'module'     => 'site'
                    ),
                ),
            ),
            'register' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/register',
                    'defaults' => array(
                        'controller'    => 'Site\Controller\Index',
                        'action'        => 'register',
                        'module'     => 'site',
                    ),
                ),
            ),
            'esqueciMinhaSenha' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/esqueci-minha-senha',
                    'defaults' => array(
                        'controller'    => 'Site\Controller\User',
                        'action'        => 'esqueciMinhaSenha',
                        'module'     => 'site',
                    ),
                ),
            ),
            'activation' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/activation[/:token]',
                    'defaults' => array(
                        'controller'    => 'Site\Controller\User',
                        'action'        => 'activation',
                        'module'        => 'site',
                    ),
                ),
            ),
            'classificadoEntry' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/classificado[/:nomeClassificado][/:classificadoId]',
                    'constraints' => array(
                        'nomeClassificado' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'classificadoId'   => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller'    => 'Site\Controller\Classificado',
                        'action'        => 'index',
                        'module'        => 'site',
                    ),
                ),
            ),
            'classificadoEmpresa' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/empresa[/:nome][/:id]',
                    'constraints' => array(
                        'nome' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'   => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller'    => 'Site\Controller\Classificado',
                        'action'        => 'empresa',
                        'module'        => 'site',
                    ),
                ),
            ),
            'searchTag' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/search/tag[/:tag]',
                    /*'constraints' => array(
                        'tag'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),*/
                    'defaults' => array(
                        'controller'    => 'Site\Controller\Search',
                        'action'        => 'tag',
                        'module'        => 'site',
                    ),
                ),
            ),
            'searchCategory' => array(
                //'type' => 'Zend\Mvc\Router\Http\Segment',
                'type' => 'Segment',
                'options' => array(
                    'route' => '/search[/:action][/:id]',
                    'constraints' => array(
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'         => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller'    => 'Site\Controller\Search',
                        //'action'        => 'index',
                        'module'        => 'site',
                    ),
                ),
            ),
            /*'url-marketing' => array(
                    //'type' => 'Zend\Mvc\Router\Http\Segment',
                    'type' => 'Segment',
                    'options' => array(
                        'route' => '[/:app]',
                        //'constraints' => array(
                        //    'var' => '[a-zA-Z0-9_-]*', // Define o regexp de busca desejado
                        //),
                        'defaults' => array(
                            'controller'    => 'Site\Controller\Index',
                            'action'        => 'url-marketing',
                            'module'     => 'site',
                            'app' => null,
                        ),
                    ),
                ),*/
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /site/:controller/:action
            'site' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/site',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Site\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'site',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
            'Session' => function($sm) {
                return new \Zend\Session\Container('SiteTemPraFesta');
            },
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
           //'translator' => 'MvcTranslator',
        ),
    ),
    /*'translator' => array(
        'locale' => 'pt_BR',
        'translation_file_patterns' => array(
            array(
                'type'     => 'phparray',
                'base_dir' => __DIR__. '/../language',
                'pattern'  => '%s.php'
            ),
        ),
    ),*/
    'controllers' => array(
        'invokables' => array(
            'Site\Controller\Index'             => 'Site\Controller\IndexController',
            'Site\Controller\User'              => 'Site\Controller\UserController',
            'Site\Controller\Classificado'      => 'Site\Controller\ClassificadoController',
            'Site\Controller\Search'            => 'Site\Controller\SearchController',
        ),
    ),
    'module_layout' => array(
        'Site' => 'layout/layout_site.phtml'
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            //'layout/layout'           => __DIR__ . '/../view/layout/layout_site.phtml',
            'site/index/index' => __DIR__ . '/../view/site/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'site' => __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'apc',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model'),
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'apc',
                'query_cache' => 'apc',
                'result_cache' => 'apc',
                'hydration_cache' => 'apc',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
            ),
        ),
    )
);