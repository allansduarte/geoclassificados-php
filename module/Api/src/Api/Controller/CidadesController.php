<?php
namespace Api\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Controlador responsável pelo retorno de cidades
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class CidadesController extends AbstractRestfulController
//extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Retorno de cidades e cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function indexAction()
    {
        $request = $this->getRequest();
        $id      = $this->params()->fromRoute('id', 0);

        $fields = array(
                "c.id",
                "c.nome",
                "e.nome as nome_estado",
                "e.uf",
                "e.id estado_id"
        );

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select($fields)
            ->from("System\Model\Cidades", "c")
            ->leftJoin('System\Model\Estados', 'e', "WITH", 'c.estado_id = e.id');

        if($id > 0){
            $qb->where('c.id = :id');
            $qb->setParameter('id', $id);
        }

        $query   = $qb->getQuery();
        $cidades = $query->getResult();

        return new JsonModel($cidades);
    }

    /**
    * Retorno de cidades e cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function estadoAction()
    {
        $request  = $this->getRequest();
        $estadoID = (int) $this->params()->fromRoute('id', 0);

        $fields = array(
                "c.id",
                "c.nome",
                "e.nome as nome_estado",
                "e.uf",
                "e.id estado_id"
        );

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select($fields)
            ->from("System\Model\Cidades", "c")
            ->leftJoin('System\Model\Estados', 'e', "WITH", 'c.estado_id = e.id');
        if($estadoID > 0){

            $qb->where("c.estado_id = :estado");
            $qb->setParameter("estado", $estadoID);

            $query = $qb->getQuery();

            $cidades = $query->getResult();
        } else{

            $query = $qb->getQuery();

            $cidades = $query->getResult();
        }

        return new JsonModel($cidades);
    }
}