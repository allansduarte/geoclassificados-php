<?php
namespace Api\Controller;

use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
//use Zend\View\Model\ViewModel;

/**
 * Controlador responsável pelo fornecimento de classificados
 * 
 * @category Api
 * @package Controller
 * @author  Allan Duarte <allan@temprafesta.com.br>
 */
class ClassificadosController
extends AbstractRestfulController
//extends ActionController
{

    private $periodoAnuncio = "-30 days";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    private function accessControlAllowOrigin(){
        return header("Access-Control-Allow-Origin: *");
    }

    /**
    * Retorno de todas as categorias vinculada com as subcategorias
    * @return Zend\Http\Response 
    */
    public function locaisPopularesAction()
    {
        $this->accessControlAllowOrigin();

        $request       = $this->getRequest();
        
        /*$cacheDriver = new \Doctrine\Common\Cache\ApcCache();
        if($cacheDriver->contains('_API_locaisPopularesAction_')){
            return new JsonModel(
                $cacheDriver->fetch('_API_locaisPopularesAction_')
            );
        }*/

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()
                   ->getRepository("System\Model\Classificado")
                   ->createQueryBuilder("c");
        $qb->select(array("estado.id as estado_id", "estado.nome as estado_nome"))
           ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->innerJoin("System\Model\Subcategoria", "subcategoria", "WITH", "subcategoria.id = c.subcategoria_id")
             ->where('c.reactivated >= :date')
             ->andWhere('u.status = 1')
             ->andWhere('subcategoria.status = 1')
             ->groupBy('estado.id')
             ->orderBy("estado.nome", "ASC");
        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $query = $qb->getQuery();

        $locais = $query->getResult();

        foreach ($locais as $key => $value) {
            $qb = $this->getEntityManager()
                   ->getRepository("System\Model\Classificado")
                   ->createQueryBuilder("c");
            $qb->select(array("cidade.id as cidade_id", "cidade.nome as cidade_nome"))
               ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
                 ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
                 ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
                 ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
                 ->innerJoin("System\Model\Subcategoria", "subcategoria", "WITH", "subcategoria.id = c.subcategoria_id")
                 ->where('c.reactivated >= :date')
                 ->andWhere('u.status = 1')
                 ->andWhere('estado.id = '.$locais[$key]['estado_id'])
                 ->andWhere('subcategoria.status = 1')
                 ->groupBy('cidade.id')
                 ->orderBy("cidade.nome", "ASC");
            $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

           $results = $qb->getQuery()->getResult();
           
           $locais[$key]["cidades"] = $results;
        }

        //$cacheDriver->save('_API_locaisPopularesAction_', $locais, 3600);

        return new JsonModel(
            $locais
        );
    }

    /**
    * Retorno dos anúncios populares
    * @param int $limit - variável recebe número limitador de registros
    * @param int $exceto - variável recebe id de anuncio para desconsiderar na consulta
    * @param int $categoria - variável recebe id de categoria para filtro na consulta
    * @param int $subcategoria - variável recebe id de subcategoria para filtro na consulta
    * @param string $palavra_chave - variável recebe a palavra chave informada pelo usuário para filtro na consulta
    * @param int $cidade - variável recebe a cidade informada pelo usuário para filtro na consulta
    * @param int $estado - variável recebe ao estado informada pelo usuário para filtro na consulta
    * @param string $tag - variável recebe a tag informada pelo usuário para filtro na consulta
    * @return Zend\Http\Response 
    */
    public function popularAction()
    {
        $this->accessControlAllowOrigin();

        $request        = $this->getRequest();
        $limit          = $this->params()->fromRoute('limit', 0);
        $exceto         = $this->params()->fromRoute('exceto', 0);
        $page           = $this->params()->fromRoute('page', 0);
        $categoria      = $this->params()->fromRoute('categoria', 0);
        $subcategoria   = $this->params()->fromRoute('subcategoria', 0);
        $palavra_chave  = $this->params()->fromRoute('palavra_chave', null);
        $cidade         = $this->params()->fromRoute('cidade', 0);
        $estado         = $this->params()->fromRoute('estado', 0);
        $tag            = $this->params()->fromRoute('tag', null);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.descricao",
            "c.valor",
            //"c.status",
            //"c.tipo",
            //"c.subcategoria_id",
            "c.usuario_id",
            //"c.created",
            //"c.modified",
            "categoria.icone",
            "empresa.nome as empresa_nome",
            "ci.nome as imagemNome",
            //"ci.mime_type as imagemMimeType",
            //"ci.file_memory as imagemData",
        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->leftJoin('System\Model\Empresa', 'empresa', "WITH", 'empresa.id = c.empresa_id')
             /*->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")*/
             ->innerJoin("System\Model\Subcategoria", "subcategoria", "WITH", "subcategoria.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = subcategoria.categoria_id")
             ->where('c.reactivated >= :date')
             ->andWhere('c.status = 1')
             ->orderBy("c.visualizacoes", "DESC");

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if($palavra_chave)
        {
            $qb->andWhere("c.titulo LIKE :palavra_chave");
            $qb->setParameter('palavra_chave', '%'.$palavra_chave.'%');

            $qb->orWhere("c.descricao LIKE :palavra_chave_html");
            $qb->setParameter('palavra_chave_html', '%'.htmlentities($palavra_chave).'%');
        }

        if($tag)
        {
            $qb->andWhere("c.tags LIKE :tag");
            $qb->setParameter('tag', '%'.$tag.'%');
        }

        if($cidade > 0){
            $qb->andWhere("cidade.id = :cidade");
            $qb->setParameter('cidade', $cidade);
        } else if($estado > 0){
            $cidades = $this->getEntityManager()
                             ->getRepository("System\Model\Cidades")
                             ->findBy(array("estado_id" => $estado));
            $cidade_id = array();
            foreach($cidades as $cidade){
                array_push($cidade_id, $cidade->id);
            }

            $qb->andWhere("cidade.id IN (:cidades)");
            $qb->setParameter('cidades', $cidade_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        if($exceto > 0){
            $qb->andWhere("c.id <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        if($subcategoria > 0){
            $qb->andWhere("subcategoria.id = :subcategoria");
            $qb->setParameter('subcategoria', $subcategoria);
        } else if($categoria > 0){
            $subcategorias = $this->getEntityManager()
                             ->getRepository("System\Model\Subcategoria")
                             ->findBy(array("categoria_id" => $categoria));

            $subcategoria_id = array();
            foreach($subcategorias as $subcategoria){
                array_push($subcategoria_id, $subcategoria->id);
            }

            $qb->andWhere("subcategoria.id IN (:subcategorias)");
            $qb->setParameter('subcategorias', $subcategoria_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        if($page > 0 && $limit > 0){
            $offset = ($page -1) * $limit;
            $qb->setFirstResult($offset);
            $allRecord = count($qb->getQuery()->getResult());
            $maxPage = ceil($allRecord / $limit);
        }

        if($limit > 0){
            $qb->setMaxResults($limit);
        }

        $anuncios = $qb->getQuery()->getResult();

        //Tratamento para converter um resource do banco de dados para uma string em binário e retorna-lá como uma especificação JSON
        //foreach ($anuncios as $key => $value) {
        //    $anuncios[$key]['imagemData'] = base64_encode( stream_get_contents($anuncios[$key]['imagemData']) );
       //}

        if($page > 0 && $limit > 0 && count($qb->getQuery()->getResult()) > 0)
            $anuncios[0]['__max_page'] = $maxPage;

        return new JsonModel($anuncios);
    }

    /**
    * Retorno dos últimos anúncios cadastrados
    * @param int $limit - variável recebe número limitador de registros
    * @param int $exceto - variável recebe id de anuncio para desconsiderar na consulta
    * @param int $categoria - variável recebe id de categoria para filtro na consulta
    * @param int $subcategoria - variável recebe id de subcategoria para filtro na consulta
    * @param string $palavra_chave - variável recebe a palavra chave informada pelo usuário para filtro na consulta
    * @param int $cidade - variável recebe a cidade informada pelo usuário para filtro na consulta
    * @param int $estado - variável recebe ao estado informada pelo usuário para filtro na consulta
    * @param string $tag - variável recebe a tag informada pelo usuário para filtro na consulta
    * @param int $empresa - variável recebe o id da empresa relacionada a um classificado
    * @return Zend\Http\Response 
    */
    public function ultimoAction()
    {
        $this->accessControlAllowOrigin();

        $request        = $this->getRequest();
        $limit          = $this->params()->fromRoute('limit', 0);
        $exceto         = $this->params()->fromRoute('exceto', 0);
        $page           = $this->params()->fromRoute('page', 0);
        $categoria      = $this->params()->fromRoute('categoria', 0);
        $subcategoria   = $this->params()->fromRoute('subcategoria', 0);
        $palavra_chave  = $this->params()->fromRoute('palavra_chave', null);
        $cidade         = $this->params()->fromRoute('cidade', 0);
        $estado         = $this->params()->fromRoute('estado', 0);
        $tag            = $this->params()->fromRoute('tag', null);
        $empresa        = $this->params()->fromRoute('empresa', 0);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.descricao",
            "c.valor",
            //"c.status",
            //"c.tipo",
            //"c.subcategoria_id",
            "c.usuario_id",
            //"c.created",
            //"c.modified",
            "categoria.icone",
            "empresa.nome as empresa_nome",
            "ci.nome as imagemNome",
            //"ci.mime_type as imagemMimeType",
            //"ci.file_memory as imagemData",
        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->leftJoin('System\Model\Empresa', 'empresa', "WITH", 'empresa.id = c.empresa_id')
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->innerJoin("System\Model\Subcategoria", "subcategoria", "WITH", "subcategoria.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = subcategoria.categoria_id")
             ->where('c.reactivated >= :date')
             ->andWhere('c.status = 1')
             ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if($palavra_chave)
        {
            $qb->andWhere("c.titulo LIKE :palavra_chave");
            $qb->setParameter('palavra_chave', '%'.$palavra_chave.'%');

            $qb->orWhere("c.descricao LIKE :palavra_chave_html");
            $qb->setParameter('palavra_chave_html', '%'.htmlentities($palavra_chave).'%');
        }

        if($tag)
        {
            $qb->andWhere("c.tags LIKE :tag");
            $qb->setParameter('tag', '%'.$tag.'%');
        }

        if($empresa > 0){
            $qb->andWhere("c.empresa_id = :empresa");
            $qb->setParameter('empresa', $empresa);
        }

        if($cidade > 0){
            $qb->andWhere("cidade.id = :cidade");
            $qb->setParameter('cidade', $cidade);
        } else if($estado > 0){
            $cidades = $this->getEntityManager()
                             ->getRepository("System\Model\Cidades")
                             ->findBy(array("estado_id" => $estado));
            $cidade_id = array();
            foreach($cidades as $cidade){
                array_push($cidade_id, $cidade->id);
            }

            $qb->andWhere("cidade.id IN (:cidades)");
            $qb->setParameter('cidades', $cidade_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        if($exceto > 0){
            $qb->andWhere("c.id <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        if($subcategoria > 0){
            $qb->andWhere("subcategoria.id = :subcategoria");
            $qb->setParameter('subcategoria', $subcategoria);
        } else if($categoria > 0){
            $subcategorias = $this->getEntityManager()
                             ->getRepository("System\Model\Subcategoria")
                             ->findBy(array("categoria_id" => $categoria));

            $subcategoria_id = array();
            foreach($subcategorias as $subcategoria){
                array_push($subcategoria_id, $subcategoria->id);
            }

            $qb->andWhere("subcategoria.id IN (:subcategorias)");
            $qb->setParameter('subcategorias', $subcategoria_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        if($page > 0 && $limit > 0){
            $offset = ($page -1) * $limit;
            $qb->setFirstResult($offset);
            $allRecord = count($qb->getQuery()->getResult());
            $maxPage = ceil($allRecord / $limit);
        }

        if($limit > 0){
            $qb->setMaxResults($limit);
        }

        $anuncios = $qb->getQuery()->getResult();

        //Tratamento para converter um resource do banco de dados para uma string em binário e retorna-lá como uma especificação JSON
        //foreach ($anuncios as $key => $value) {
        //    $anuncios[$key]['imagemData'] = base64_encode( stream_get_contents($anuncios[$key]['imagemData']) );
        //}

        if($page > 0 && $limit > 0 && count($qb->getQuery()->getResult()) > 0)
            $anuncios[0]['__max_page'] = $maxPage;

        return new JsonModel($anuncios);
    }

    /**
    * Retorno de anúncios relacionados com os parametros recebidos
    * @param int $limit - variável recebe número limitador de registros
    * @param int $exceto - variável recebe id de anuncio para desconsiderar na consulta
    * @param int $subcategoria - variável recebe id de subcategoria para relacioonar
    * @return Zend\Http\Response 
    */
    public function relacionadoAction()
    {
        $this->accessControlAllowOrigin();

        $request        = $this->getRequest();
        $limit          = $this->params()->fromRoute('limit', 0);
        $exceto         = $this->params()->fromRoute('exceto', 0);
        $subcategoria   = $this->params()->fromRoute('subcategoria', 0);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.descricao",
            "c.valor",
            "c.status",
            "c.tipo",
            "c.subcategoria_id",
            "c.usuario_id",
            "c.reactivated",
            "c.modified",
            "ci.nome as imagemNome",
            "ci.mime_type as imagemMimeType",
            //"ci.file_memory as imagemData",
        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->innerJoin("System\Model\Subcategoria", "subcategoria", "WITH", "subcategoria.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = subcategoria.categoria_id")
             ->where("c.id <> :exceto")
             ->andWhere('c.reactivated >= :date')
             ->andWhere('c.status = 1')
             ->andWhere('c.subcategoria_id = :subcategoria_id')
             ->orderBy("c.reactivated", "DESC");

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $qb->setParameter('exceto', $exceto);
        $qb->setParameter('subcategoria_id', $subcategoria);

        $anuncios = $qb->getQuery()->getResult();

        shuffle($anuncios);

        if($limit > 0){
            $anuncios = array_slice($anuncios, 0, $limit);
        }

        //Tratamento para converter um resource do banco de dados para uma string em binário e retorna-lá como uma especificação JSON
        //foreach ($anuncios as $key => $value) {
        //    $anuncios[$key]['imagemData'] = base64_encode( stream_get_contents($anuncios[$key]['imagemData']) );
        //}

        return new JsonModel($anuncios);
    }

    /**
    * Retorno das tags de anúncios relacionados
    * @param int $limit - variável recebe número limitador de registros
    * @param string $tag - variável recebe a tag informada pelo usuário para filtro na consulta
    * @param string $exceto - variável recebe a tag informada pelo usuário para desconsiderar no filtro
    * @param int $categoria - variável recebe id de categoria para filtro
    * @return Zend\Http\Response 
    */
    public function tagAction()
    {
        $this->accessControlAllowOrigin();

        $limit          = $this->params()->fromRoute('limit', 0);
        $categoria      = $this->params()->fromRoute('categoria', 0);
        $tag            = $this->params()->fromRoute('tag', null);
        $exceto         = $this->params()->fromRoute('exceto', null);

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select()
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where("c.status = 1")
             ->andWhere('c.reactivated >= :date')
             ->andWhere("c.tags != ''")
             ->orderBy("c.reactivated", "DESC");
        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if($tag)
        {
            $qb->andWhere("c.tags LIKE :tag");
            $qb->setParameter('tag', '%'.trim($tag).'%');
        }

        if($exceto){
            $qb->andWhere("c.tags <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        if($categoria > 0){
            $qb->andWhere("categoria.id = :categoria");
            $qb->setParameter('categoria', $categoria);
        }

        if($limit > 0){
            $qb->setMaxResults($limit);
        }

        $anuncios = $qb->getQuery()->getResult();

        $tags = array();
        foreach($anuncios as $anuncio)
        {
            $tagsExplode = explode(',', $anuncio->tags);
            if(count($tagsExplode) > 1)
            {
                foreach ($tagsExplode as $tag) {
                    if($tag == $exceto)
                        continue;
                    else
                        array_push($tags, $tag);
                }
            } else{
                array_push($tags, $anuncio->tags);
            }
        }

        $tags = array_unique($tags);

        sort($tags);

        return new JsonModel($tags);
    }

    /**
    * Retorno de anúncios em destaque
    * @param int $limit     - variável recebe número limitador de registros
    * @param int $exceto    - variável recebe id de anuncio para desconsiderar na consulta
    * @param int $categoria - variável recebe id de categoria para filtro na consulta
    * @param int $subcategoria - variável recebe id de subcategoria para filtro na consulta
    * @param string $tag - variável recebe a tag informada pelo usuário para filtro na consulta
    * @param int $empresa - variável recebe id da empresa relacionada ao classificado
    * @return Zend\Http\Response 
    */
    public function destaqueAction()
    {
        $this->accessControlAllowOrigin();

        $request       = $this->getRequest();
        $limit         = $this->params()->fromRoute('limit', 0);
        $exceto        = $this->params()->fromRoute('exceto', 0);
        $categoria     = $this->params()->fromRoute('categoria', 0);
        $subcategoria  = $this->params()->fromRoute('subcategoria', 0);
        $tag           = $this->params()->fromRoute('tag', null);
        $empresa       = $this->params()->fromRoute('empresa', 0);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.descricao",
            "c.valor",
            "c.status",
            "c.tipo",
            "c.subcategoria_id",
            "c.usuario_id",
            "c.reactivated",
            "c.modified",
            "categoria.icone",
            "empresa.nome as empresa_nome",
            "IDENTITY(ce.empresa_id) as empresa_id",
            "ci.nome as imagemNome",
            //"ci.mime_type as imagemMimeType",
            //"ci.file_memory as imagemData",
        );

        //$now           = new \DateTime("now");
        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->leftJoin('System\Model\Empresa', 'empresa', "WITH", 'empresa.id = c.empresa_id AND empresa.status = 1')
             ->leftJoin('System\Model\ClassificadoEmpresa', 'ce', 'WITH', 'ce.empresa_id = empresa.id AND ce.status = 1 AND ce.reactivated >= :date')
             ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
             /*->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")*/
             ->where("c.tipo = 1")
             ->andWhere("c.status = 1")
             ->andWhere('c.reactivated >= :date')
             ->orderBy("c.reactivated", "DESC");

             $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if($exceto > 0){
            $qb->andWhere("c.id <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        if($tag)
        {
            $qb->andWhere("c.tags LIKE :tag");
            $qb->setParameter('tag', '%'.$tag.'%');
        }

        if($empresa > 0){
            $qb->andWhere("c.empresa_id = :empresa");
            $qb->setParameter('empresa', $empresa);
        }

        if($subcategoria > 0){
            $qb->andWhere("s.id = :subcategoria");
            $qb->setParameter('subcategoria', $subcategoria);
        } else if($categoria > 0){
            $subcategorias = $this->getEntityManager()
                             ->getRepository("System\Model\Subcategoria")
                             ->findBy(array("categoria_id" => $categoria));

            $subcategoria_id = array();
            foreach($subcategorias as $subcategoria){
                array_push($subcategoria_id, $subcategoria->id);
            }

            $qb->andWhere("s.id IN (:subcategorias)");
            $qb->setParameter('subcategorias', $subcategoria_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }

        //$anunciosDestaque = $qb->getQuery()->getResult();

        //shuffle($anunciosDestaque);

        if($limit > 0){
            //$anunciosDestaque = array_slice($anunciosDestaque, 0, $limit);
            $rows = count($qb->getQuery()->getScalarResult());
            $offset = max(0, rand(0, $rows - $limit - 1));
            $qb->setMaxResults($limit)
            ->setFirstResult($offset);
        }

        $anunciosDestaque = $qb->getQuery()->getArrayResult();

        //Tratamento para converter um resource do banco de dados para uma string em binário e retorna-lá como uma especificação JSON
        //foreach ($anunciosDestaque as $key => $value) {
        //    $anunciosDestaque[$key]['imagemData'] = base64_encode( stream_get_contents($anunciosDestaque[$key]['imagemData']) );
        //}

        return new JsonModel($anunciosDestaque);
    }

    /**
    * Retorno de dados de geolocalização dos anúncios
    * @param int $limit  - variável recebe número limitador de registros
    * @param int $exceto - variável recebe id de anuncio para desconsiderar na consulta
    * @return Zend\Http\Response
    */
    public function geocoderAction()
    {
        $this->accessControlAllowOrigin();

        $request  = $this->getRequest();
        $limit    = $this->params()->fromRoute('limit', 0);
        $exceto   = $this->params()->fromRoute('exceto', 0);

        $fields = array(
            "c.id",
            "c.titulo",
            "c.valor",
            "c.latitude",
            "c.longitude",
            "c.subcategoria_id",
            "ci.nome as imagemNome",
            "ci.mime_type as imagemMimeType",
            "ci.file_memory as imagemData",
            "categoria.nome as nome_categoria",
        );

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $qb->select($fields)
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'ci.capa = 1')
             ->innerJoin("System\Model\Subcategoria", "s", "WITH", "s.id = c.subcategoria_id")
             ->innerJoin("System\Model\Categoria", "categoria", "WITH", "categoria.id = s.categoria_id")
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = c.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = c.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where("c.status = 1")
             ->andWhere('c.reactivated >= :date')
             ->orderBy("c.reactivated", "DESC");

        if($exceto > 0){
            $qb->andWhere("c.id <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $anunciosDestaque = $qb->getQuery()->getResult();

        shuffle($anunciosDestaque);

        if($limit > 0){
            $anunciosDestaque = array_slice($anunciosDestaque, 0, $limit);
        }

        //Tratamento para converter um resource do banco de dados para uma string em binário e retorna-lá como uma especificação JSON
        foreach ($anunciosDestaque as $key => $value) {
            $anunciosDestaque[$key]['imagemData'] = base64_encode( stream_get_contents($anunciosDestaque[$key]['imagemData']) );
        }

        return new JsonModel($anunciosDestaque);
    }

    /**
    * Retorno de anúncios de empresa
    * @param int $limit     - variável recebe número limitador de registros
    * @param int $exceto    - variável recebe id de anuncio para desconsiderar na consulta
    * @param int $categoria - variável recebe id de categoria para filtro na consulta
    * @param int $subcategoria - variável recebe id de subcategoria para filtro na consulta
    * @param string $tag - variável recebe a tag informada pelo usuário para filtro na consulta
    * @return Zend\Http\Response 
    */
    public function empresaAction()
    {
        $this->accessControlAllowOrigin();
        
        $request       = $this->getRequest();
        $limit         = $this->params()->fromRoute('limit', 0);
        $page           = $this->params()->fromRoute('page', 0);
        $exceto        = $this->params()->fromRoute('exceto', 0);
        $categoria     = $this->params()->fromRoute('categoria', 0);
        $subcategoria  = $this->params()->fromRoute('subcategoria', 0);
        $tag           = $this->params()->fromRoute('tag', null);

        $fields = array(
            "ce.id",
            "u.id as user_id",
            "e.nome as empresa_nome",
            "e.id as empresa_id",
            "e.nome as imagemNome",
            "e.mime_type_logo as imagemMimeType",
            "e.imagem",
        );

        //$now           = new \DateTime("now");
        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
             ->getRepository("System\Model\ClassificadoEmpresa")
             ->createQueryBuilder("ce");
        $qb->select($fields)
             ->innerJoin('System\Model\Empresa', 'e', "WITH", 'e.id = ce.empresa_id AND e.status = 1')
             ->innerJoin("System\Model\User", "u", "WITH", "u.id = e.usuario_id")
             ->innerJoin("System\Model\Perfil", "p", "WITH", "u.id = p.usuario_id")
             ->innerJoin("System\Model\Cidades", "cidade", "WITH", "cidade.id = e.cidade_id")
             ->innerJoin("System\Model\Estados", "estado", "WITH", "estado.id = cidade.estado_id")
             ->where("ce.status = 1")
             ->andWhere('ce.reactivated >= :date')
             ->orderBy("ce.reactivated", "DESC");

             $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        if($exceto > 0){
            $qb->andWhere("ce.id <> :exceto");
            $qb->setParameter('exceto', $exceto);
        }

        if($tag)
        {
            //$qb->andWhere("c.tags LIKE :tag");
            //$qb->setParameter('tag', '%'.$tag.'%');
        }

        /*if($subcategoria > 0){
            $qb->andWhere("s.id = :subcategoria");
            $qb->setParameter('subcategoria', $subcategoria);
        } else if($categoria > 0){
            $subcategorias = $this->getEntityManager()
                             ->getRepository("System\Model\Subcategoria")
                             ->findBy(array("categoria_id" => $categoria));

            $subcategoria_id = array();
            foreach($subcategorias as $subcategoria){
                array_push($subcategoria_id, $subcategoria->id);
            }

            $qb->andWhere("s.id IN (:subcategorias)");
            $qb->setParameter('subcategorias', $subcategoria_id, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        }*/

        if($page > 0 && $limit > 0){
            $offset = ($page -1) * $limit;
            $qb->setFirstResult($offset);
            $allRecord = count($qb->getQuery()->getResult());
            $maxPage = ceil($allRecord / $limit);
        }

        if($limit > 0){
            $qb->setMaxResults($limit);
        }

        $anuncios = $qb->getQuery()->getResult();

        if($page > 0 && $limit > 0 && count($qb->getQuery()->getResult()) > 0)
            $anuncios[0]['__max_page'] = $maxPage;

        return new JsonModel($anuncios);
    }
}