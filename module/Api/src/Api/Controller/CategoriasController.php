<?php
namespace Api\Controller;

use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Controlador responsável pelo gerenciamento das categorias
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class CategoriasController
extends AbstractRestfulController
//extends ActionController
{

    private $periodoAnuncio = "-30 days";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    private function accessControlAllowOrigin(){
        return header("Access-Control-Allow-Origin: *");
    }

    /**
    * Retorno de todas as categorias
    * @return Zend\Http\Response 
    */
    public function indexAction()
    {
        $this->accessControlAllowOrigin();

        $request = $this->getRequest();
        $categoria   = (int) $this->params()->fromRoute('id', 0);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select("c.id, c.nome")
           ->from("System\Model\Categoria", "c");
        if($categoria > 0){
            $qb->where("c.id = :categoria");
            $qb->setParameter("categoria", $categoria);
        }

        $qb->orderBy("c.nome", "ASC");

        $query = $qb->getQuery();

        $categorias = $query->getResult();

        return new JsonModel(
            $categorias
        );
    }

    /**
    * Retorno de todas as categorias com um contador agrupado por classificado
    * @return Zend\Http\Response 
    */
    public function recordCountAction()
    {
        $this->accessControlAllowOrigin();

        $request = $this->getRequest();
        $categoria   = (int) $this->params()->fromRoute('id', 0);
        $empresa   = (int) $this->params()->fromRoute('empresa', 0);

        $twentyDaysAgo = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(array("c.id","c.nome", "COUNT(s.id) as record_count", "c.icone"))
           ->from("System\Model\Categoria", "c")
           ->innerJoin("System\Model\Subcategoria", "s", "WITH", 'c.id = s.categoria_id')
           ->innerJoin('System\Model\Classificado', 'classificado', "WITH", 's.id = classificado.subcategoria_id')
           ->where("c.status = 1")
           ->andWhere('classificado.reactivated >= :date')
           ->andWhere('classificado.status = 1')
           ->groupBy("c.id");

        $qb->setParameter('date', $twentyDaysAgo, \Doctrine\DBAL\Types\Type::DATETIME);

        if($categoria > 0){
            $qb->andWhere("c.id = :categoria");
            $qb->setParameter("categoria", $categoria);
        }

        if($empresa > 0){
            $qb->andWhere("classificado.empresa_id = :empresa");
            $qb->setParameter("empresa", $empresa);
        }

        $query = $qb->getQuery();

        $categorias = $query->getResult();

        return new JsonModel(
            $categorias
        );
    }

    /**
    * Retorno de todas as categorias vinculada com as subcategorias
    * @return Zend\Http\Response
    */
    public function subcategoriasAction()
    {
        $this->accessControlAllowOrigin();

        $request     = $this->getRequest();
        $categoria   = (int) $this->params()->fromRoute('id', 0);
        $status      = (int) $this->params()->fromRoute('status', 1);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(array("c.id","c.nome", "c.icone", "c.map_marker"))
           ->from("System\Model\Categoria", "c")
           ->where("c.status = 1");

        $query = $qb->getQuery();

        $categorias = $query->getResult();

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        foreach ($categorias as $key => $value) {
            $qb = $this->getEntityManager()->createQueryBuilder();

            if($status == 1){
                $qb->select(array("s.id as subcategoria_id","s.nome as subcategoria_nome", "COUNT(s.id) as record_count"));
                $qb->from("System\Model\Subcategoria", "s");
                $qb->innerJoin('System\Model\Classificado', 'c', "WITH", 's.id = c.subcategoria_id');
            } else{
                $qb->select(array("s.id as subcategoria_id","s.nome as subcategoria_nome", "COUNT(c.id) as record_count"));
                $qb->from("System\Model\Subcategoria", "s");
                $qb->leftJoin('System\Model\Classificado', 'c', "WITH", 's.id = c.subcategoria_id AND c.reactivated >= :date AND c.status = 1');
            }

            $qb->where("s.categoria_id = ".$categorias[$key]["id"])
               ->andWhere("s.status = 1");
            
            if($status == 1){
                $qb->andWhere('c.reactivated >= :date')
                   ->andWhere('c.status = 1');
            }

            $qb->groupBy("s.id")
               ->orderBy("s.nome", "DESC");

            $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

           $results = $qb->getQuery()->getResult();
           
           $categorias[$key]["subcategorias"] = $results;
        }

        return new JsonModel(
            $categorias
        );
    }
}