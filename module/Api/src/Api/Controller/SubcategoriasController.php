<?php
namespace Api\Controller;

use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Controlador responsável pelo retorno de subcategorias
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class SubcategoriasController extends AbstractRestfulController
//extends ActionController
{

    private $periodoAnuncio = "-30 days";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Retorno de todas as subcategorias
    * @return Zend\Http\Response 
    */
    public function indexAction()
    {
        $request = $this->getRequest();

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select("s.id, s.nome")
              ->from("System\Model\Subcategoria", "s")
              ->orderBy("s.nome", "ASC");

        $query = $qb->getQuery();

        $subcategorias = $query->getResult();

        //print_r($subcategorias);
        //exit();

        return new JsonModel(
            $subcategorias
        );
    }

    /**
    * Retorno das subcategorias de uma categoria
    * @param Integer categoria
    * @return Zend\Http\Response 
    */
    public function categoriaAction()
    {

        $request = $this->getRequest();
        $categoria   = (int) $this->params()->fromRoute('id', 0);

        $periodoAnuncio = new \DateTime($this->periodoAnuncio);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(array("s.id", "s.nome", "COUNT(classificado.id) as record_count", "categoria.icone"))
              ->from("System\Model\Subcategoria", "s")
              ->leftJoin('System\Model\Classificado', 'classificado', "WITH", 's.id = classificado.subcategoria_id')
              ->innerJoin('System\Model\Categoria', 'categoria', "WITH", 'categoria.id = '.$categoria)
              ->where("s.categoria_id = :categoria")
              ->andWhere('classificado.status = 1')
              ->andWhere('classificado.reactivated >= :date')
              ->groupBy('s.id')
              ->orderBy("s.nome", "ASC");
        $qb->setParameter("categoria", $categoria);
        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $query = $qb->getQuery();

        $subcategorias = $query->getResult();

        return new JsonModel(
            $subcategorias
        );
    }

    /**
    * Retorno das subcategorias de uma categoria para popular combobox do sistema
    * @param Integer categoria
    * @return Zend\Http\Response 
    */
    public function comboboxAction()
    {

        $request = $this->getRequest();
        $categoria   = (int) $this->params()->fromRoute('id', 0);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select(array("s.id", "s.nome", "categoria.icone"))
              ->from("System\Model\Subcategoria", "s")
              ->innerJoin('System\Model\Categoria', 'categoria', "WITH", 'categoria.id = '.$categoria)
              ->where("s.categoria_id = :categoria")
              ->groupBy('s.id')
              ->orderBy("s.nome", "ASC");
        $qb->setParameter("categoria", $categoria);

        $query = $qb->getQuery();

        $subcategorias = $query->getResult();

        return new JsonModel(
            $subcategorias
        );
    }
}