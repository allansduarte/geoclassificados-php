<?php

namespace Api;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }


/**
 * Executada no bootstrap do módulo
 * 
 * @param MvcEvent $e
 */
    public function onBootstrap($e)
    {
        /** @var \Zend\ModuleManager\ModuleManager $moduleManager */
        $moduleManager = $e->getApplication()->getServiceManager()->get('modulemanager');
        /** @var \Zend\EventManager\SharedEventManager $sharedEvents */
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();

        //adiciona eventos ao módulo
        //pré e pós-processadores do controller Rest
        //$sharedEvents->attach('Api\Controller\RestController', \Zend\Mvc\MvcEvent::EVENT_DISPATCH, array(new PostProcessor, 'process'), -100);
        //$sharedEvents->attach('Api\Controller\RestController', \Zend\Mvc\MvcEvent::EVENT_DISPATCH, array(new PreProcessor, 'process'), 100);

        //adiciona eventos ao módulo
        //$sharedEvents->attach('Zend\Mvc\Controller\AbstractActionController', \Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'verifyAuthorization'), 100);

        //Define sessão do usuário para ser utilizada nos layouts
        //$session = $e->getApplication()->getServiceManager()->get('Session');
        //$e->getViewModel()->setVariable('userSession', $session->offsetGet('user'));
    }

/**
 * Verifica se precisa fazer a autorização do acesso
 * @param  MvcEvent $event Evento
 * @return boolean
 */
    public function verifyAuthorization($event)
    {
        $di = $event->getTarget()->getServiceLocator();
        $routeMatch = $event->getRouteMatch();
        $moduleName = $routeMatch->getParam('module');
        $controllerName = $routeMatch->getParam('controller');
        $actionName = $routeMatch->getParam('action');

        $authService = $di->get('System\Service\Auth');
        if ($controllerName != 'System\Controller\Auth') {
            if (! $authService->authorize($moduleName, $controllerName, $actionName)) {
                $redirect = $event->getTarget()->redirect();
                $redirect->toUrl('/system/auth');
                
            }
        }

        /* if ($moduleName == 'system' && $controllerName != 'System\Controller\Auth') {
             $authService = $di->get('System\Service\Auth');
             if (! $authService->authorize()) {
                 $redirect = $event->getTarget()->redirect();
                 $redirect->toUrl('/system/auth');
             }
         }*/

        return true;
    }
}