<?php

namespace Api;

// module/Api/conﬁg/module.config.php:
return array(
    'controllers' => array( //add module controllers
        'invokables' => array(
            //'Api\Controller\Index' => 'Api\Controller\IndexController',
            'Api\Controller\Cidades'       => 'Api\Controller\CidadesController',
            'Api\Controller\Subcategorias' => 'Api\Controller\SubcategoriasController',
            'Api\Controller\Categorias'    => 'Api\Controller\CategoriasController',
            'Api\Controller\Classificados' => 'Api\Controller\ClassificadosController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'api' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/v1[/page/:page]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'api'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                    
                ),
            ),
            'categoria' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/categorias',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Categorias',
                        'action'        => 'index',
                        'module'        => 'api',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:action]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ), 
                ),
            ),
            'subcategoria' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/subcategorias[/:action][/id/:id]',
                    'constraints' => array(
                        'id'         => '[0-9]*',
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Subcategorias',
                        'action'        => 'index',
                        'module'        => 'api'
                    ),
                ),
            ),
            'cidade' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/api/cidades[/:action][/id/:id]',
                    'constraints' => array(
                        'id'         => '[0-9]*',
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Api\Controller',
                        'controller'    => 'Cidades',
                        'action'        => 'index',
                        'module'        => 'api'
                    ),
                ),
            ),
        ),
    ),
    //the module can have a specific layout
    'module_layout' => array(
        'Api' => 'layout/layout_api.phtml'
    ),
    'view_manager' => array( 
        /*'template_path_stack' => array(
            'api' => __DIR__ . '/../view',
        ),*/
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'cache' => 'Doctrine\Common\Cache\ArrayCache',
            'paths' => array(__DIR__.'/../src/'.__NAMESPACE__.'/Model')
        ),
    ),
    /*'view_helpers' => array(
        'invokables'=> array(
            'Header' => 'Api\View\Helper\Header',
        )
    ),*/
    'service_manager' => array(
        'factories' => array(
            'Session' => function($sm) {
                return new \Zend\Session\Container('APITemPraFesta');
            },
            'Api\Service\Auth' => function($sm) {
                $dbAdapter = $sm->get('DbAdapter');
                return new \Api\Service\Auth($dbAdapter);
            },
            'Cache' => function($sm) {
                $config = $sm->get('Configuration');
                $cache = StorageFactory::factory(
                    array(
                        'adapter' => $config['cache']['adapter'],
                        'plugins' => array(
                            'exception_handler' => array('throw_exceptions' => false),
                            'Serializer'
                        ),
                    )
                );

                return $cache;
            },
            'Doctrine\ORM\EntityManager' => function($sm) {
                $config = $sm->get('Configuration');
                
                $doctrineConfig = new \Doctrine\ORM\Configuration();
                $cache = new $config['doctrine']['driver']['cache'];
                $doctrineConfig->addCustomStringFunction('date_format', 'Mapado\MysqlDoctrineFunctions\DQL\MysqlDateFormat');
                $doctrineConfig->addCustomStringFunction('if', 'Mapado\MysqlDoctrineFunctions\DQL\MysqlIfElse');
                $doctrineConfig->setQueryCacheImpl($cache);
                $doctrineConfig->setProxyDir('/tmp');
                $doctrineConfig->setProxyNamespace('EntityProxy');
                $doctrineConfig->setAutoGenerateProxyClasses(true);
                
                $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(
                    new \Doctrine\Common\Annotations\AnnotationReader(),
                    array($config['doctrine']['driver']['paths'])
                );
                $doctrineConfig->setMetadataDriverImpl($driver);
                $doctrineConfig->setMetadataCacheImpl($cache);
                \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
                    getenv('PROJECT_ROOT'). '/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
                );
                $em = \Doctrine\ORM\EntityManager::create(
                    $config['doctrine']['connection'],
                    $doctrineConfig
                );
                return $em;

            },
        )    
    )
    /*'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model' => __NAMESPACE__ . '_driver'
                )
            )
        )
    )*/
    /*'db' => array( //module can have a specific db configuration
        'driver' => 'PDO_SQLite',
        'dsn' => 'sqlite:' . __DIR__ .'/../data/api.db',
        'driver_options' => array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    )*/
);