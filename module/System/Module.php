<?php

namespace System;

//use Doctrine\ORM\Tools\Setup;

class Module
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }


/**
 * Executada no bootstrap do módulo
 * 
 * @param MvcEvent $e
 */
    public function onBootstrap($e)
    {
        /** @var \Zend\ModuleManager\ModuleManager $moduleManager */
        $moduleManager = $e->getApplication()->getServiceManager()->get('modulemanager');
        /** @var \Zend\EventManager\SharedEventManager $sharedEvents */
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();

        //adiciona eventos ao módulo
        $sharedEvents->attach('Zend\Mvc\Controller\AbstractActionController', \Zend\Mvc\MvcEvent::EVENT_DISPATCH, array($this, 'verifyAuthorization'), 100);

        $system = $e->getParam('application');
        $system->getEventManager()->attach('render', array($this, 'setLayoutTitle'));

        //Define sessão do usuário para ser utilizada nos layouts
        $session = $e->getApplication()->getServiceManager()->get('Session');
        $e->getViewModel()->setVariable('userSession', $session->offsetGet('user'));

        //Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src/Model"), false, "data/DoctrineORMModule/Proxy");
    }

/**
 * Verifica se precisa fazer a autorização do acesso
 * @param  MvcEvent $event Evento
 * @return boolean
 */
    public function verifyAuthorization($event)
    {
        $di = $event->getTarget()->getServiceLocator();
        $routeMatch = $event->getRouteMatch();
        $moduleName = $routeMatch->getParam('module');
        $controllerName = $routeMatch->getParam('controller');
        $actionName = $routeMatch->getParam('action');

        $authService = $di->get('System\Service\Auth');
        if ($controllerName != 'System\Controller\Auth') {
            if (! $authService->authorize($moduleName, $controllerName, $actionName)) {
                $redirect = $event->getTarget()->redirect();
                $redirect->toUrl('/system/auth');
                
            }
        }

        /* if ($moduleName == 'system' && $controllerName != 'System\Controller\Auth') {
             $authService = $di->get('System\Service\Auth');
             if (! $authService->authorize()) {
                 $redirect = $event->getTarget()->redirect();
                 $redirect->toUrl('/system/auth');
             }
         }*/

        return true;
    }

    /**
     * @param  \Zend\Mvc\MvcEvent $e The MvcEvent instance
     * @return void
     */
    public function setLayoutTitle($e)
    {
        $matches    = $e->getRouteMatch();

        if($matches != null){
            $action     = $matches->getParam('action');
            $controller = $matches->getParam('controller');
        } else{
            $action     = "";
            $controller = "";
        }

        $module     = __NAMESPACE__;
        $siteName   = 'Temprafesta';

        // Instancia a classe view helper manager do serviço gerenciador da aplicação
        $viewHelperManager = $e->getApplication()->getServiceManager()->get('viewHelperManager');

        // Recebe o helper headTitle() da classe view helper manager
        $headTitleHelper   = $viewHelperManager->get('headTitle');

        // Configura o separador das palavras
        $headTitleHelper->setSeparator(' | ');

        // Configura action, controller, module e nome do site como o título do layout
        $headTitleHelper->append($siteName);
        $headTitleHelper->append($action);
        //$headTitleHelper->append($controller);
        //$headTitleHelper->append($module);
    }
}