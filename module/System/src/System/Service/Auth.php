<?php
namespace System\Service;

use Core\Service\Service;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Db\Sql\Select;
use ZealMessages\Controller\Plugin;

/**
 * Serviço responsável pela autenticação da aplicação
  * 
 * @category System
 * @package Service
 * @author  Allan Duarte<allan.duarte@duismag.com>
 */
class Auth extends Service
{
    /**
     * Adapter usado para a autenticação
     * @var Zend\Db\Adapter\Adapter
     */
    private $dbAdapter;
    private $message;

    /** 
     * Construtor da classe
     *
     * @return void
     */
    public function __construct($dbAdapter = null)
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * Faz a autenticação dos usuários
     * 
     * @param array $params
     * @return array
     */
    public function authenticate($params)
    {

        if (!isset($params['username']) || !isset($params['password'])) {
            //throw new \Exception("Parâmetros inválidos");
            $this->setMessage('Usuário ou Senha inválidos!');
            return false;
        }

        if (empty($params['username']) || empty($params['password'])) {
            //throw new \Exception("Parâmetros inválidos");
            //$this->messages()->flashSuccess('Usuário ou Senha inválidos!', 'error');
            $this->setMessage('Usuário ou Senha inválidos!');
            return false;
        }

        $password = md5($params['password']);
        $auth = new AuthenticationService();
        $authAdapter = new AuthAdapter($this->dbAdapter);
        $authAdapter
            ->setTableName('usuario')
            ->setIdentityColumn('email')
            ->setCredentialColumn('senha')
            ->setIdentity($params['username'])
            ->setCredential($password);

        $select = $authAdapter->getDbSelect();
        $select->where('status = "1"');
        $result = $auth->authenticate($authAdapter);

        if (! $result->isValid()) {
            //throw new \Exception("Login ou senha inválidos");
            $this->setMessage('Usuário ou Senha inválidos!');
            return false;
        }

        $_em = $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $userObject = $authAdapter->getResultRowObject();
        $usuarioPerfil = $_em
             ->getRepository("System\Model\User")
             ->createQueryBuilder("u");
        $usuarioPerfil->select(array("u", "p"))
             ->leftJoin('System\Model\Perfil', 'p', "WITH", 'u.id = p.usuario_id')
             ->where("u.id = :usuario_id");;
        $usuarioPerfil->setParameter("usuario_id", $userObject->id);
        $usuarioPerfilObject = $usuarioPerfil->getQuery()->getResult();

        $usuarioPerfilObject[1]->file_memory = base64_encode(stream_get_contents($usuarioPerfilObject[1]->file_memory));

        //salva o user na sessão
        $session = $this->getService('Session');
        $session->offsetSet('user', $usuarioPerfilObject);

        /*if($usuarioPerfilObject[0]->status == 0){
            $this->setMessage('Usuário ou Senha inválidos!');
            return false;
        }*/

        return true;
    }

    /**
     * Faz o logout do sistema
     *
     * @return void
     */
    public function logout() {
        $auth = new AuthenticationService();
        $session = $this->getServiceManager()->get('Session');
        $session->offsetUnset('user');
        $auth->clearIdentity();
        return true;
    }


    /**
     * Faz a autorização do usuário para acessar o recurso
     * @param string $moduleName Nome do módulo sendo acessado
     * @param string $controllerName Nome do controller
     * @param string $actionName Nome da ação
     * @return boolean
     */
    public function authorize($moduleName, $controllerName, $actionName)
    {
        $auth = new AuthenticationService();

        $session = $this->getService('Session');
        $user = $session->offsetGet('user');
        $role = 'visitante';
        if ($auth->hasIdentity() && $user) {

            $role = "";
            switch ($user[1]->tipo) {
                case 0:
                    $role = "administrador";
                    break;
                case 1:
                    $role = "anunciante";
                    break;
                case 2:
                    $role = "visitante";
                    break;
                default:
                    //Definir regra e desenvolver exceção
                    break;
            }
        }

        $resource = $controllerName . '.' . $actionName;
        $acl = $this->getServiceManager()->get('Core\Acl\Builder')->build();
        if ($acl->isAllowed($role, $resource)) {

            return true;
        }

        return false;
    }

    /**
     * Faz a autorização do usuário para acessar o recurso
     * @return boolean
     */
    /*public function authorize()
    {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity()) {
            return true;
        }
        return false;
    }*/

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
    }

}