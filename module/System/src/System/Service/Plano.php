<?php
namespace System\Service;

use Core\Service\Service;

/**
 * Serviço responsável por definir preços e tempo
  * 
 * @category System
 * @package Service
 * @author  Allan Duarte<allan@temprafesta.com>
 */
class Plano extends Service
{
    /**
     * Adapter usado para a autenticação
     * @var Zend\Db\Adapter\Adapter
     */
    private $message;

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
    }

}