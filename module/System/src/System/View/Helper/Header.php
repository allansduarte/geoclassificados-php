<?php

namespace System\View\Helper;

use Zend\View\Helper\Partial;
use Zend\View\Model\ViewModel;
use Zend\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Helper que renderiza o header da área intranet do sistema de edição de revistas digitais
 * 
 * @category System
 * @package View\Helper
 * @author  Allan Duarte <allan.duarte@duismag.com>
 */
class Header extends AbstractHelper implements ServiceLocatorAwareInterface
{
    /**
     * Set the service locator.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return CustomHelper
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    /**
     * Get the service locator.
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
    public function __invoke()
    {

        return $this->getView()->render("system/index/header");
    }
}