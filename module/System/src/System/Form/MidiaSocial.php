<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class MidiaSocial extends Form
{

    public function __construct()
    {
        parent::__construct('midiaSocial');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/midia-social/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'midiaSocialID'
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome da Mídia Social',
            ),
        ));

        $this->add(array(
            'name' => 'icone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'icone'
            ),
            'options' => array(
                'label' => 'Icone',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Mídia Social',
                'id' => 'submitMidiaSocial',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}