<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class ClassificadoEmpresa extends Form
{

    public function __construct()
    {
        parent::__construct('classificadoEmpresa');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/classificado/save/t/2');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'empresaID'
            ),
        ));

        $this->add(array(
                'type' => 'Zend\Form\Element\Radio',
                'name' => 'status',
                'attributes' => array(
                    'class'  => 'form-control',
//                    'id'     => 'status',
                    'value'  => 'Ativo'
                ),
                'options' => array(
                        'label' => 'Status',
                        'value_options' => array(
                                '0' => 'Ativo',
                                '1' => 'Inativo',
                        ),
                )
         ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Divulgar Minha Empresa!',
                'id' => 'submitEmpresa',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}