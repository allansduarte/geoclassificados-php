<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class ComboboxCategoria extends Form
{

    protected $entityManager;
    protected $serviceManager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm)
    {
        parent::__construct();

        $this->entityManager = $sm->get('Doctrine\ORM\EntityManager');
        $this->serviceManager = $sm;
        $this->init();
    }

    public function init(){

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'categoria',
            'options' => array(
                'label' => "Categoria",
                'class' => 'form-control',
                'id'    => 'categoria',
                'value_options' => $this->getOptionsCategory(),
                'empty_option' => "--Escolha a categoria--",
            ),
        ));
    }

    public function getOptionsCategory()
    {
        $categorias = $this->entityManager
                           ->getRepository("System\Model\Categoria")
                           ->findBy(array("status" => 1), array("nome" => "ASC"));
        $options = array();
        foreach ($categorias as $categoria) {
            $options[$categoria->id] = $categoria->nome;
        }

        return $options;
    }
}