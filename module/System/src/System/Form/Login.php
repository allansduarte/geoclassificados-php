<?php
namespace System\Form;

use Zend\Form\Form;

class Login extends Form
{
    public function __construct()
    {
        parent::__construct('login');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/auth');
        $this->setAttribute('class', 'form-item login-form');
        
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactName',
                'class' => 'text input-textarea half',
                'placeholder' => 'E-mail',
                'style' => 'width: 85% !important;',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'password',
                'class' => 'text input-textarea half',
                'placeholder' => 'Senha',
                'style' => 'width: 85% !important;',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Entrar',
                'id' => 'edit-submit',
                'class' => 'btn form-submit',
            ),
        ));
    }
}