<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Perfil extends Form
{


    public function __construct()
    {
        parent::__construct('perfil');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/perfil/save/t/1');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'perfilID'
            ),
        ));

        $this->add(array(
            'name' => 'file_memory',
            'attributes' => array(
                'type'   => 'file',
                'id'     => 'file_memory'
            ),
            'options' => array(
                'label' => 'Imagem',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'   => 'textarea',
                'class'  => 'form-control',
                'id'     => 'descricao'
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cidade_id',
            'options' => array(
                'label' => "Cidade",
                'class' => 'form-control',
                'id'    => 'cidade',
                'empty_option' => "--Escolha o estado--",
            ),
        ));

        $this->add(array(
            'name' => 'website',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'website',
                "placeholder" => 'http://www.site.com.br',
            ),
            'options' => array(
                'label' => 'Website',
            ),
        ));

        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'telefone',
                'placeholder' => '(XX) 9XXXX-XXXX',
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));

        $this->add(array(
            'name' => 'celular',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'celular',
                'placeholder' => '(XX) 9XXXX-XXXX',
            ),
            'options' => array(
                'label' => 'Celular',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Perfil',
                'id' => 'submitPerfil',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}