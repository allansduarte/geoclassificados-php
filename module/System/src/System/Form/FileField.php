<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\View\Helper\FormCheckbox;

use Doctrine\ORM\EntityManager;

class FileField extends Form
{

    public function __construct()
    {
        parent::__construct();
        $this->init();

    }

    public function init(){

        $this->add(array(
            'name' => 'imagem',
            'attributes' => array(
                'type'   => 'file',
                'id'     => 'imagem'
            ),
            'options' => array(
                'label' => 'Imagem',
            ),
        ));
    }
}