<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class ComboboxEstado extends Form
{

    protected $entityManager;
    protected $serviceManager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm)
    {
        parent::__construct();

        $this->entityManager = $sm->get('Doctrine\ORM\EntityManager');
        $this->serviceManager = $sm;
        $this->init();
    }

    public function init(){

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'estado_id',
            'options' => array(
                'label' => "Estado",
                'class' => 'form-control',
                'id'    => 'estado',
                'value_options' => $this->getOptionsState(),
                'empty_option' => "--Escolha o estado--",
            ),
        ));
    }

    public function getOptionsState()
    {
        $estados = $this->entityManager
                           ->getRepository("System\Model\Estados")
                           ->findBy(array(), array("nome" => "ASC"));
        $options = array();
        foreach ($estados as $estado) {
            $options[$estado->id] = $estado->nome;
        }

        return $options;
    }
}