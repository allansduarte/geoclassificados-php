<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Categoria extends Form
{

    public function __construct()
    {
        parent::__construct('categoria');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/categoria/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'categoriaID'
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome da Categoria',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'   => 'textarea',
                'class'  => 'form-control',
                'id'     => 'descricao'
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'status',
            ),
            'options' => array(
                'label' => 'Ativo',
            ),
        ));

        $this->add(array(
            'name' => 'icone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'HTML do Icone',
            ),
        ));

        $this->add(array(
            'name' => 'map_marker',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'HTML do Marcador do GMap',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Categoria',
                'id' => 'submitCategoria',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}