<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Classificado extends Form
{

    public function __construct()
    {
        parent::__construct('classificado');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/classificado/save');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'classificadoID'
            ),
        ));
        
        $this->add(array(
            'name' => 'titulo',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'titulo',
                'maxLength' => 100
            ),
            'options' => array(
                'label' => 'Título',
            ),
        ));

        $this->add(array(
            'name' => 'latitude',
            'attributes' => array(
                'type'          => 'text',
                'class'         => 'form-control',
                'id'            => 'latitude',
                'maxLength'     => 100,
                'placeholder'   => 'Latitude',
                'readonly'      => true
            ),
            'options' => array(
                'label' => 'Latitude',
            ),
        ));

        $this->add(array(
            'name' => 'longitude',
            'attributes' => array(
                'type'          => 'text',
                'class'         => 'form-control',
                'id'            => 'longitude',
                'maxLength'     => 100,
                'placeholder'   => 'Longitude',
                'readonly'      => true
            ),
            'options' => array(
                'label' => 'Longitude',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes'    => array(
                'type'      => 'textarea',
                'class'     => 'form-control',
                'id'        => 'descricao',
                'maxLength' => 4000,
                'rows'      => '6',
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'subcategoria_id',
            'options' => array(
                'label' => "Subcategoria",
                'class' => 'form-control',
                'id'    => 'subcategoria_id',
                'empty_option' => "--Escolha a categoria--",
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'tipo',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'tipo'
            ),
            'options' => array(
                'label' => 'Torne este anúncio um destaque entre os demais anúncios!',
            ),
        ));

        $this->add(array(
            'name' => 'valor',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'valor',
                'value' => '0,00'
            ),
            'options' => array(
                'label' => 'Valor',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'attributes' => array(
                'type'          => 'text',
                'class'         => 'form-control',
                'id'            => 'tags',
                'data-role'     => 'tagsinput',
            ),
            'options' => array(
                'label' => 'Informe as Tags do seu anúncio',
            ),
        ));

        $this->add(array(
                'type' => 'Zend\Form\Element\Radio',
                'name' => 'status',
                'attributes' => array(
                    'class'  => 'form-control',
//                    'id'     => 'status',
                    'value'  => 'Ativo'
                ),
                'options' => array(
                        'label' => 'Status do classificado',
                        'value_options' => array(
                                '0' => 'Ativo',
                                '1' => 'Inativo',
                        ),
                )
         ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'email',
                "placeholder" => 'Informe um e-mail válido...',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));

        $this->add(array(
            'name' => 'website',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'website',
                "placeholder" => 'http://www.site.com.br',
            ),
            'options' => array(
                'label' => 'Website',
            ),
        ));

        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'telefone',
                'placeholder' => '(XX) 9XXXX-XXXX',
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));

        $this->add(array(
            'name' => 'celular',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'celular',
                'placeholder' => '(XX) 9XXXX-XXXX',
            ),
            'options' => array(
                'label' => 'Celular',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cidade_id',
            'options' => array(
                'label' => "Cidade",
                'class' => 'form-control',
                'id'    => 'cidade',
                'empty_option' => "--Escolha o estado--",
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Classificado',
                'id' => 'submitClassificado',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}