<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Filial extends Form
{

    public function __construct()
    {
        parent::__construct('filiais');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/filial/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'empresaID'
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome da Filial',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'descricao'
            ),
            'options' => array(
                'label' => 'Descrição da Filial',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'status',
            ),
            'options' => array(
                'label' => 'Ativo',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cidade_id',
            'options' => array(
                'label' => "Cidade",
                'class' => 'form-control',
                'id'    => 'cidade',
                'empty_option' => "--Escolha o estado--",
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Filial',
                'id' => 'submitFilial',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}