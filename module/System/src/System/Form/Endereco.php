<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Endereco extends Form
{

    public function __construct()
    {
        parent::__construct('endereco');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'enderecoID'
            ),
        ));
        
        $this->add(array(
            'name' => 'rua',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'rua'
            ),
            'options' => array(
                'label' => 'Rua',
            ),
        ));

        $this->add(array(
            'name' => 'numero',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'numero'
            ),
            'options' => array(
                'label' => 'Número',
            ),
        ));

        $this->add(array(
            'name' => 'bairro',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'bairro'
            ),
            'options' => array(
                'label' => 'Bairro',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Endereço',
                'id' => 'submitEndereco',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}