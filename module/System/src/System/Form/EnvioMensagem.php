<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class EnvioMensagem extends Form
{

    public function __construct()
    {
        parent::__construct('envioMensagem');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/envio-mensagem/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'envioMensagemID'
            ),
        ));
        
        $this->add(array(
            'name' => 'remetente_nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'remetente_nome',
                'placeholder' => 'Nome'
            ),
            'options' => array(
                'label' => 'Nome',
            ),
        ));

        $this->add(array(
            'name' => 'remetente_email',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'remetente_email',
                'placeholder' => 'E-mail'
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));

        $this->add(array(
            'name' => 'remetente_telefone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'remetente_telefone',
                'placeholder' => 'Telefone (opcional)'
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));

        $this->add(array(
            'name' => 'texto',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'texto',
                'placeholder' => 'Mensagem'
            ),
            'options' => array(
                'label' => 'Mensagem',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar Mensagem',
                'id' => 'submitMensagem',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}