<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Planos extends Form
{

    public function __construct()
    {
        parent::__construct('planos');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/planos/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'planosID'
            ),
        ));
        
        $this->add(array(
            'name' => 'destaque',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'destaque'
            ),
            'options' => array(
                'label' => 'Valor do Plano Destaque',
            ),
        ));

        $this->add(array(
            'name' => 'normal',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'normal'
            ),
            'options' => array(
                'label' => 'Valor do Plano Normal',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Atualizar Valores',
                'id' => 'submitPlano',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}