<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Subcategoria extends Form
{

    protected $entityManager;
    protected $serviceManager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm)
    {
        parent::__construct('subcategoria');

        $this->entityManager = $sm->get('Doctrine\ORM\EntityManager');
        $this->serviceManager = $sm;
        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/subcategoria/save');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'subcategoriaID'
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome da Subcategoria',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'descricao'
            ),
            'options' => array(
                'label' => 'Descrição',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'categoria_id',
            'options' => array(
                'label' => "Categoria",
                'class' => 'form-control',
                'id'    => 'subcategoria',
                'value_options' => $this->getOptionsCategory(),
                'empty_option' => "--Escolha a categoria--",
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'status',
            ),
            'options' => array(
                'label' => 'Ativo',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Subcategoria',
                'id' => 'submitCategoria',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }

    public function getOptionsCategory()
    {
        $categorias = $this->entityManager
                           ->getRepository("System\Model\Categoria")
                           ->findBy(array("status" => 1), array("nome" => "ASC"));
        $options = array();
        foreach ($categorias as $categoria) {
            $options[$categoria->id] = $categoria->nome;
        }

        return $options;
    }
}