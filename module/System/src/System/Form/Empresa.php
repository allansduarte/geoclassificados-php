<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Empresa extends Form
{

    public function __construct()
    {
        parent::__construct('empresa');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/empresa/save');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'empresaID'
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'nome'
            ),
            'options' => array(
                'label' => 'Nome da Empresa',
            ),
        ));

        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'descricao'
            ),
            'options' => array(
                'label' => 'Descrição da Empresa',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'status',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'status',
            ),
            'options' => array(
                'label' => 'Ativo',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'cidade_id',
            'options' => array(
                'label' => "Cidade",
                'class' => 'form-control',
                'id'    => 'cidade',
                'empty_option' => "--Escolha o estado--",
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Empresa',
                'id' => 'submitEmpresa',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}