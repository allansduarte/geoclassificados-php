<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class AlterarSenha extends Form
{

    public function __construct()
    {
        parent::__construct('alterarSenha');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/system/perfil/save/t/2');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'alterarSenhaID'
            ),
        ));
        
        $this->add(array(
            'name' => 'senhaAtual',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'senhaAtual'
            ),
            'options' => array(
                'label' => 'Senha atual',
            ),
        ));

        $this->add(array(
            'name' => 'novaSenha',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'novaSenha'
            ),
            'options' => array(
                'label' => 'Nova senha',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Alterar Senha',
                'id' => 'submitPerfil',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}