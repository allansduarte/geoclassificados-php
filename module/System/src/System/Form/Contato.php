<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class Contato extends Form
{

    public function __construct()
    {
        parent::__construct('contato');

        $this->init();
    }

    public function init(){
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'   => 'hidden',
                'id'     => 'contatoID'
            ),
        ));
        
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'   => 'email',
                'class'  => 'form-control',
                'id'     => 'email',
                'placeholder' => 'Informe um e-mail válido...'
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));

        $this->add(array(
            'name' => 'website',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'website',
                "placeholder" => 'http://www.site.com.br',
            ),
            'options' => array(
                'label' => 'Website',
            ),
        ));

        $this->add(array(
            'name' => 'telefone',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'telefone',
                'placeholder' => '(XX) 9XXXX-XXXX',
                //'data-inputmask' => "'mask': '(99) 99999-9999'"
            ),
            'options' => array(
                'label' => 'Telefone',
            ),
        ));

        $this->add(array(
            'name' => 'celular',
            'attributes' => array(
                'type'   => 'text',
                'class'  => 'form-control',
                'id'     => 'celular',
                'placeholder' => '(XX) 9XXXX-XXXX',
                //'data-inputmask' => "'mask': '(99) 99999-9999'"
            ),
            'options' => array(
                'label' => 'Celular',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Cadastrar Contato',
                'id' => 'submitContato',
                'class' => 'btn-lg btn-primary',
            ),
        ));
    }
}