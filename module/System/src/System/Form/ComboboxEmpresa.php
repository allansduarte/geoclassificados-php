<?php
namespace System\Form;

use Zend\Form\Form;
use Zend\Form\Element;

use Doctrine\ORM\EntityManager;

class ComboboxEmpresa extends Form
{

    protected $entityManager;
    protected $serviceManager;

    public function __construct(\Zend\ServiceManager\ServiceManager $sm)
    {
        parent::__construct();

        $this->entityManager = $sm->get('Doctrine\ORM\EntityManager');
        $this->serviceManager = $sm;
        $this->init();
    }

    public function init(){

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'empresa_id',
            'options' => array(
                'label' => "Empresa",
                'class' => 'form-control',
                'id'    => 'empresa',
                'value_options' => $this->getOptionsEnterprise(),
                'empty_option' => "--Escolha a empresa--",
            ),
        ));
    }

    public function getOptionsEnterprise()
    {

        $session = $this->serviceManager->get('Session');
        $userSession = $session->offsetGet('user');

        $empresas = $this->entityManager
                           ->getRepository("System\Model\Empresa")
                           ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC"));
        $options = array();
        foreach ($empresas as $empresa) {
            $options[$empresa->id] = $empresa->nome;
        }

        return $options;
    }
}