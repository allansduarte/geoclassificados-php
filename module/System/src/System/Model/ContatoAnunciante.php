<?php
namespace System\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * Entidade System
 * 
 * @category System
 * @package Model
 *
 * @ORM\Entity
 * @ORM\Table(name="contato_anunciante")
 *
 */
class ContatoAnunciante extends Entity
{

    public function __set($key, $value) 
    {
        $this->$key = $this->valid($key, $value);
    }

    /**
     * @param string $key
     * @return mixed 
     */
    public function __get($key) 
    {
        return $this->$key;
    }
 
    /**
     * @var integer
     * @access private
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="System\Model\User", inversedBy="id")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * @var User|null
     */
    private $usuario_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="System\Model\Classificado", inversedBy="id")
     * @ORM\JoinColumn(name="classificado_id", referencedColumnName="id")
     * @var Classificado|null
     */
    private $classificado_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="System\Model\Empresa", inversedBy="id")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     * @var Empresa|null
     */
    private $empresa_id;
 
    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $nome;

    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $mensagem;

    /**
     * @var \Datetime
     * @access private
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $lido;

    /**
    * Set nome
    *
    * @param string $nome
    * @return Category
    */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
    * Get nome
    *
    * @return string
    */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'nome',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'mensagem',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'created',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'lido',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}