<?php
namespace System\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\File\Extension;
use Core\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * Entidade System
 * 
 * @category System
 * @package Model
 *
 * @ORM\Entity
 * @ORM\Table(name="empresa_midia_social")
 *
 */
class EmpresaMidiaSocial extends Entity
{

    public function __set($key, $value) 
    {
               
        $this->$key = $this->valid($key, $value);
    }

    /**
     * @param string $key
     * @return mixed 
     */
    public function __get($key) 
    {
        return $this->$key;
    }
 
    /**
     * @var integer
     * @access private
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $url;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="System\Model\Empresa", inversedBy="id")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     * @var Empresa|null
     */
    private $empresa_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\ManyToOne(targetEntity="System\Model\MidiaSocial", inversedBy="id")
     * @ORM\JoinColumn(name="midia_social_id", referencedColumnName="id")
     * @var MidiaSocial|null
     */
    private $midia_social_id;

    /**
     * @var integer
     * @access private
     *
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'url',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 255,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'empresa_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'midia_sociais_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'status',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}