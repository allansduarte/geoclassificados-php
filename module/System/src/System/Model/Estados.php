<?php
namespace System\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
 
/**
 * Entidade System
 * 
 * @category System
 * @package Model
 *
 * @ORM\Entity
 * @ORM\Table(name="estados")
 *
 */
class Estados extends Entity
{

    public function __set($key, $value) 
    {
               
        $this->$key = $this->valid($key, $value);
    }

    /**
     * @param string $key
     * @return mixed 
     */
    public function __get($key) 
    {
        return $this->$key;
    }
 
    /**
     * @var integer
     * @access private
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $nome;

    /**
     * @var string
     * @access private
     *
     * @ORM\Column(type="string")
     */
    private $uf;

    /**
    * Set nome
    *
    * @param string $nome
    * @return Category
    */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
    * Get nome
    *
    * @return string
    */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'nome',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 20,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'uf',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 2,
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}