<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

use System\Form\Subcategoria as SubcategoriaForm;

use System\Model\Subcategoria as SubcategoriaModel;
use System\Model\Categoria as CategoriaModel;

/**
 * Controlador responsável pelo gerenciamento das subcategorias
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class SubcategoriaController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de formulário de cadastro e edição de Subcategorias
    * @return ViewModel
    */
    public function cadastroAction(){

        $id = $this->params()->fromRoute("edicao", 0);

        $this->layout()->title = "Subcategoria";
        $this->layout()->titlePage = "Subcategoria";
        $this->layout()->subTitlePage = "cadastro de nova subcategoria";

        $form = new SubcategoriaForm($this->getServiceLocator());

        $view =  new ViewModel();

        if($id > 0){

            $subcategoria = $this->getEntityManager()->find("System\Model\Subcategoria", $id);

            if(!$subcategoria)
                return $this->redirect()->toUrl('/system');

            $hydrator = new ZendReflection;
            $form->setHydrator($hydrator);
            $form->bind($subcategoria);
            $form->get('submit')->setAttribute('value', 'Alterar Subcategoria');

            $this->layout()->title = "Alterar subcategoria";
            $this->layout()->titlePage = "Subategoria";
            $this->layout()->subTitlePage = "alteração da subcategoria ".$subcategoria->__get("nome");
            $view->setVariables(array('edicao' => true));
        }

        $view->setVariables(array('form' => $form));

        return $view;
    }

    /**
     * Cadastro e edição de subcategoria
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $urlRetorno = '/system/subcategoria/cadastro';
        if ($request->isPost()) {

            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $requestData = $request->getPost();

            if(isset($requestData['id']) && $requestData['id'] > 0)
                $urlRetorno .= "/edicao/".$requestData['id'];
            else
                $requestData['status'] = 1;

            $form = new SubcategoriaForm($this->getServiceLocator());
            $subcategoriaModel = new SubcategoriaModel;

            unset($requestData['submit']);

            //if(isset($requestData['id']) && $requestData['id'] > 0)
            //    $urlRetorno .= "/edicao/".$requestData['id'];

            $categoriaModel = new CategoriaModel;
            $categoriaModel = $this->getEntityManager()->find('System\Model\Categoria', $requestData['categoria_id']);

            $requestData['categoria_id'] = $categoriaModel;

            if(isset($requestData['id']) && $requestData['id'] > 0)
                $subcategoriaModel = $this->getEntityManager()->find('System\Model\Subcategoria', $requestData['id']);

            $subcategoriaModel->setData($requestData);

            try{
                $this->getEntityManager()->persist($subcategoriaModel);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Cadastro efetuado com sucesso!');
            } catch(\Exception $e){
                var_dump($e->getMessage());
                exit();
                $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
            }

            if(isset($data['id']) && $data['id'] > 0)
                return $this->redirect()->toUrl($urlRetorno);
            else
                return $this->redirect()->toUrl($urlRetorno);
        } else{
            //$this->messages()->flashInfo("Ocorreu algo inesperado no sistema. A equipe de suporte Duismag já foi avisada e está corrigindo o problema. Agradecemos a paciência.");
            $this->redirect($urlRetorno);
        }
    }

    /**
    * Apresenta tela de consulta de Subcategoria
    * @return ViewModel
    */
    public function consultaAction(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $subcategoria = $this->getEntityManager()
             ->getRepository("System\Model\Subcategoria")
             ->createQueryBuilder("s");
        $subcategoria->select(array("s"))
             ->leftJoin('s.categoria_id', 'c', "WITH", 'c.id = s.categoria_id')
             ->orderBy("s.nome", "DESC");

        $paginatorSubcategoriaAdapter = new DoctrineAdapter(new ORMPaginator($subcategoria));
        $paginatorSubcategoria        = new ZendPaginator($paginatorSubcategoriaAdapter);
        $paginatorSubcategoria->setDefaultItemCountPerPage(10);
        $paginatorSubcategoria->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($subcategoria->getQuery()->getResult()) > 0){

            $view = new ViewModel(array(
                'subcategorias'    => $paginatorSubcategoria,
            ));
        } else{
            $view = new ViewModel();
        }

        return $view;
    }

    /**
     * Exclui uma subcategoria
     * @return void
     */
    public function deleteAction()
    {

        $urlRetorno = "/system/subcategoria/consulta";

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $subcategoria = $this->getEntityManager()->find('System\Model\Subcategoria', $id);
        if($subcategoria){

            try{
                $subcategoria->__set('status', 0);
                $this->getEntityManager()->persist($subcategoria);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Subcategoria desativada com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão de uma subcategoria. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
    * Retorno das subcategorias de uma categoria
    * @param Integer categoria
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $paramFormat = $this->params()->fromRoute('format', 'json');
        $format      = ($paramFormat == 'xml') ? 'xml' : 'json';
        $categoria   = (int) $this->params()->fromRoute('c', 0);

        $serializer = new Serializer(
            array(new GetSetMethodNormalizer()),
            array(
                'xml' => new XmlEncoder(),
                'json' => new JsonEncoder()
            )
        );

        if($categoria > 0){

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select("s.id, s.nome")
                  ->from("System\Model\Subcategoria", "s")
                  ->where("s.categoria_id = :categoria")
                  ->orderBy("s.nome", "ASC");
            $qb->setParameter("categoria", $categoria);

            $query = $qb->getQuery();

            $subcategorias = $query->getResult();
        } else{

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select("s.id, s.nome")
                  ->from("System\Model\Subcategoria", "s")
                  ->orderBy("s.nome", "ASC");

            $query = $qb->getQuery();

            $subcategorias = $query->getResult();
        }

        $content = $serializer->serialize($subcategorias, $format);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($content);
        $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");

        return $response;
    }
}