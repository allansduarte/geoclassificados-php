<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

use System\Form\MidiaSocial as MidiaSocialForm;

use System\Model\MidiaSocial as MidiaSocialModel;

/**
 * Controlador responsável pelo gerenciamento das mídias sociais
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class MidiaSocialController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de formulário de cadastro e edição de Categorias
    * @return ViewModel
    */
    public function cadastroAction(){

        $id = $this->params()->fromRoute("edicao", 0);

        $this->layout()->title = "Mídia Social";
        $this->layout()->titlePage = "Mídia Social";
        $this->layout()->subTitlePage = "cadastro de nova mídia social";

        $form = new MidiaSocialForm();

        if($id > 0){

            $midialSocial = $this->getEntityManager()->find("System\Model\MidiaSocial", $id);

            if(!$midialSocial)
                return $this->redirect()->toUrl('/system');

            $hydrator = new ZendReflection;
            $form->setHydrator($hydrator);
            $form->bind($midialSocial);
            $form->get('icone')->setAttribute('value', html_entity_decode($midialSocial->__get('icone')));
            $form->get('submit')->setAttribute('value', 'Alterar Mídia Social');

            $this->layout()->title = "Alterar mídia social";
            $this->layout()->titlePage = "Midia Social";
            $this->layout()->subTitlePage = "alteração da mídia social ".$midialSocial->__get("nome");
        }

        $view =  new ViewModel(
            array(
                'form' => $form,
            )
        );

        return $view;
    }

    /**
     * Cadastro e edição de categoria
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $urlRetorno = '/system/midia-social/cadastro';
        if ($request->isPost()) {

            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $requestData = $request->getPost();

            $form = new MidiaSocialForm;
            $midialSocialModel = new MidiaSocialModel;

            $form->setInputFilter($midialSocialModel->getInputFilter());
            $requestData['icone'] = htmlentities($requestData['icone']);
            unset($requestData['submit']);
            $form->setData($requestData);

            if(isset($requestData['id']) && $requestData['id'] > 0)
                $urlRetorno .= "/edicao/".$requestData['id'];

            if ($form->isValid()) {

                $data = $form->getData();
                unset($data['submit']);

                if(isset($data['id']) && $data['id'] > 0){
                    $midialSocialModel = $this->getEntityManager()->find('System\Model\MidiaSocial', $data['id']);
                }

                $midialSocialModel->setData($data);

                try{
                    $this->getEntityManager()->persist($midialSocialModel);
                    $this->getEntityManager()->flush();
                    $this->messages()->flashSuccess('Cadastro efetuado com sucesso!');
                } catch(\Exception $e){
                    $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
                }

                if(isset($data['id']) && $data['id'] > 0)
                    return $this->redirect()->toUrl($urlRetorno);
                else
                    return $this->redirect()->toUrl($urlRetorno);

            } else{

                if(isset($requestData['id']) && $requestData['id'] > 0)
                    return $this->redirect()->toUrl($urlRetorno);
                else
                    return $this->redirect()->toUrl($urlRetorno);
            }
        } else{

            $this->redirect($urlRetorno);
        }
    }

    /**
    * Apresenta tela de consulta de Mídia Social
    * @return ViewModel
    */
    public function consultaAction(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $midiasSociais = $this->getEntityManager()
             ->getRepository("System\Model\MidiaSocial")
             ->createQueryBuilder("ms");
        $midiasSociais->select("ms")
             ->orderBy("ms.nome", "DESC");

        $paginatorMidiasSociaisAdapter = new DoctrineAdapter(new ORMPaginator($midiasSociais));
        $paginatorMidiasSociais        = new ZendPaginator($paginatorMidiasSociaisAdapter);
        $paginatorMidiasSociais->setDefaultItemCountPerPage(10);
        $paginatorMidiasSociais->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($midiasSociais->getQuery()->getResult()) > 0){
            $view = new ViewModel(array(
                'midiasSociais'    => $paginatorMidiasSociais,
            ));
        } else{
            $view = new ViewModel();
        }

        //$view->setTerminal(true);

        return $view;
    }

    /**
     * Exclui uma mídia social
     * @return void
     */
    public function deleteAction()
    {

        $urlRetorno = "/system/midia-social/consulta";

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $midiasSociais = $this->getEntityManager()->find('System\Model\MidiaSocial', $id);
        if($midiasSociais){

            try{
                $this->getEntityManager()->remove($midiasSociais);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Mídia Social excluída com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão de uma mídia social. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
    * Retorno dos dados de uma determinada categoria de usuário ou retorna todas as categorias
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $paramFormat = $this->params()->fromRoute('format', 'json');
        $format = ($paramFormat == 'xml') ? 'xml' : 'json';
        $categoriaID = (int) $this->params()->fromRoute('id', 0);
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $serializer = new Serializer(
            array(new GetSetMethodNormalizer()),
            array(
                'xml' => new XmlEncoder(),
                'json' => new JsonEncoder()
            )
        );

        $fields = array(
                "c.id",
                "c.nome",
        );

        if($categoriaID > 0){

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                  ->from("System\Model\Categoria", "c")
                  ->where("c.id = :categoria");
            $qb->setParameter("categoria", $categoriaID);

            $query = $qb->getQuery();

            $categorias = $query->getResult();
        } else{

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                  ->from("System\Model\Categoria", "c");

            $query = $qb->getQuery();

            $categorias = $query->getResult();
        }

        $content = $serializer->serialize($categorias, $format);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($content);
        $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
        return $response;
    }
}