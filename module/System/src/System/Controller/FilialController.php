<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use System\Form\Filial  as FilialForm;
use System\Form\Endereco as EnderecoForm;
use System\Form\Contato  as ContatoForm;
use System\Form\ComboboxEstado;
use System\Form\ComboboxEmpresa;

use System\Model\Filiais  as FilialModel;
use System\Model\Endereco as EnderecoModel;
use System\Model\Contato  as ContatoModel;
use System\Model\MidiaSoial as MidiaSoialModel;
use System\Model\EmpresaMidiaSocial as EmpresaMidiaSocialModel;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Controlador que gerencia filiais vinculadas as empresas
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class FilialController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de formulário de cadastro e edição de Filiais
    * @return ViewModel
    */
    public function cadastroAction(){

        $id = $this->params()->fromRoute("edicao", 0);

        $this->layout()->title = "Filial";
        $this->layout()->titlePage = "Filial";
        $this->layout()->subTitlePage = "cadastro de filial";

        $formFilial          = new FilialForm;
        $formEndereco        = new EnderecoForm;
        $formContato         = new ContatoForm;
        $formComboboxEstado  = new ComboboxEstado($this->getServiceLocator());
        $formComboboxEmpresa = new ComboboxEmpresa($this->getServiceLocator());
        $view                = new ViewModel();

        if($id > 0){

            $view->setVariables(array('edicao' => true));
            $filial = $this->getEntityManager()->find("System\Model\Filiais", $id);

            if(!$filial)
                return $this->redirect()->toUrl('/system');

            $endereco = $this->getEntityManager()
                             ->getRepository("System\Model\Endereco")
                             ->findBy(array("filiais_id" => $id), array());

            $contato = $this->getEntityManager()
                             ->getRepository("System\Model\Contato")
                             ->findBy(array("filiais_id" => $id), array());

            $hydrator = new ZendReflection;
            $formFilial->setHydrator($hydrator);
            $formFilial->bind($filial);
            $formFilial->get('submit')->setAttribute("value", "Alterar filial");

            $cidade = $this->getEntityManager()->find("System\Model\Cidades", $filial->__get("cidade_id"));
            $estado = $this->getEntityManager()->find("System\Model\Estados", $cidade->__get("estado_id"));
            $formComboboxEstado->get("estado_id")->setAttribute('value', $estado->__get('id'));
            $formFilial->get('cidade_id')->setAttribute('options', $this->getOptionsCity($estado->__get('id')));

            $formComboboxEmpresa->get("empresa_id")->setAttribute('value', $filial->__get('empresa_id'));

            $formEndereco->setHydrator($hydrator);
            $formEndereco->bind($endereco[0]);


            if(count($contato) > 0){
                $formContato->setHydrator($hydrator);
                $formContato->bind($contato[0]);
            }

            $this->layout()->title = "Alterar Filial";
            $this->layout()->titlePage = "Filial";
            $this->layout()->subTitlePage = "alteração da filial ".$filial->__get("nome");
        }

        $session     = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        $empresas     = $this->getEntityManager()
                            ->getRepository("System\Model\Empresa")
                            ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC")
                            );

        if(!$empresas)
            $view->setVariables(array('possuiEmpresa' => false));

        $view->setVariables(
            array(
                'formFilial'         => $formFilial,
                'formEndereco'       => $formEndereco,
                'formContato'        => $formContato,
                'formComboboxEstado' => $formComboboxEstado,
                'formComboboxEmpresa' => $formComboboxEmpresa,
            )
        );

        return $view;
    }

    /**
    * Apresenta tela de consulta de Empresa
    * @return ViewModel
    */
    public function consultaAction(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        $view = new ViewModel();

        $empresas     = $this->getEntityManager()
                            ->getRepository("System\Model\Empresa")
                            ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC")
                            );

        if(!$empresas){
            $view->setVariables(array('possuiEmpresa' => false));
            return $view;
        }

        $selectData = array(
                        "f",
                        "cidade.nome as nomeCidade",
                        "estado.uf as ufEstado",
                        "endereco.rua as enderecoRua",
                        "endereco.numero as enderecoNumero",
                        "endereco.bairro as enderecoBairro",
                        "contato.email as contatoEmail",
                        "contato.telefone as contatoTelefone",
                        "contato.celular as contatoCelular",
        );

        $filiais = $this->getEntityManager()
             ->getRepository("System\Model\Filiais")
             ->createQueryBuilder("f");
        $filiais->select($selectData)
             ->leftJoin('f.empresa_id', 'empresa', "WITH", 'empresa.id = f.empresa_id')
             ->leftJoin('f.endereco', 'endereco', "WITH", 'endereco.filiais_id = f.id')
             ->leftJoin('f.contato', 'contato', "WITH", 'contato.filiais_id = f.id')
             ->leftJoin('f.cidade_id', 'cidade', "WITH", 'cidade.id = f.cidade_id')
             ->leftJoin('cidade.estado_id', 'estado', "WITH", 'estado.id = cidade.estado_id')
             ->where("f.usuario_id = :usuario")
             ->orderBy("f.nome", "DESC");
        $filiais->setParameter("usuario", $userSession[0]->id);

        $paginatorFiliaisAdapter = new DoctrineAdapter(new ORMPaginator($filiais));
        $paginatorFiliais        = new ZendPaginator($paginatorFiliaisAdapter);
        $paginatorFiliais->setDefaultItemCountPerPage(10);
        $paginatorFiliais->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($filiais->getQuery()->getResult()) > 0){
             $view->setVariables(
                array(
                    'filiais'    => $paginatorFiliais,
                )
            );
        }

        //$view->setTerminal(true);

        return $view;
    }

    /**
     * Cadastro de filial
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $urlRetorno = '/system/filial/cadastro';

        if ($request->isPost()) {

            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $requestData = $request->getPost();
            $requestData["usuario_id"]      = $userSession[0]->id;
            $requestData["status"]          = 1;

            $enderecoModel = new EnderecoModel;
            $contatoModel  = new ContatoModel;
            $filialModel  = new FilialModel;

            if(isset($requestData->id) && $requestData->id > 0){
                $urlRetorno             .= "/edicao/".$requestData['id'];
                $filialModel             = $this->getEntityManager()->find('System\Model\Filiais', $requestData->id);
                $requestData['modified'] = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
            } else{
                $requestData['created']  = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
            }

            $filialModel->setData($requestData);
            $cidades = $this->getEntityManager()->find("System\Model\Cidades", $requestData['cidade_id']);
            $filialModel->__set("cidade_id", $cidades);
            $empresa = $this->getEntityManager()->find("System\Model\Empresa", $requestData['empresa_id']);
            $filialModel->__set("empresa_id", $empresa);

            try{

                $msg = 'Filial cadastrada com sucesso!';
                if($filialModel->__get('id'))
                    $msg = 'Filial alterada com sucesso!';

                $this->getEntityManager()->persist($filialModel);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess($msg);
                unset($requestData["empresa_id"]);

                try{

                    if(isset($requestData['id']) && $requestData['id'] > 0){
                        $enderecoModel = $this->getEntityManager()
                                              ->getRepository('System\Model\Endereco')
                                              ->findBy(array("filiais_id" => $requestData['id']), array());
                        $enderecoModel = $enderecoModel[0];
                    }

                    $enderecoModel->setData($requestData);
                    $enderecoModel->__set('filiais_id', $filialModel);
                    $this->getEntityManager()->persist($enderecoModel);

                    if($requestData['email'] != "" || $requestData['celular'] != "" || $requestData['telefone'] != ""){
                        if(isset($requestData['id']) && $requestData['id'] > 0){
                            $contatoModel = $this->getEntityManager()
                                                  ->getRepository('System\Model\Contato')
                                                  ->findBy(array("filiais_id" => $requestData['id']), array());
                            $contatoModel = $contatoModel[0];
                        }

                        $contatoModel->setData($requestData);
                        $contatoModel->__set('filiais_id', $filialModel);
                        $this->getEntityManager()->persist($contatoModel);
                    }
                    $this->getEntityManager()->flush();
                } catch(\Exception $e){

                    if(!isset($requestData->id) && $requestData->id <= 0){
                        $filial = $this->getEntityManager()->find('System\Model\Filiais', $filialModel->__get('id'));
                        if($filial){

                            try{
                                $this->getEntityManager()->remove($filial);
                                $this->getEntityManager()->flush();
                            } catch(\Exception $e){
                                //Enviar email com relatório
                            }
                        }
                    }

                    var_dump($e->getMessage());
                    exit();

                    $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
                }
            } catch(\Exception $e){
                var_dump($e->getMessage());
                    exit();

                $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
            }

            return $this->redirect()->toUrl($urlRetorno);
        } else $this->redirect($urlRetorno);
    }

    /**
     * Excluir uma filial
     * @return void
     */
    public function deleteAction()
    {

        $urlRetorno = "/system/filial/consulta";

        $id = (int) $this->params()->fromRoute('id', 0);

        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $filial = $this->getEntityManager()->find('System\Model\Filiais', $id);
        if($filial){

            /*$endereco = $this->getEntityManager()
                             ->getRepository('System\Model\Endereco')
                             ->findBy(array("filiais_id" => $id), array());
            $endereco = $endereco[0];

            $contato = $this->getEntityManager()
                             ->getRepository('System\Model\Contato')
                             ->findBy(array("filiais_id" => $id), array());
            $contato = $contato[0];*/
            try{

                /*if($endereco)
                    $this->getEntityManager()->remove($endereco);

                if($contato)
                    $this->getEntityManager()->remove($contato);*/

                $filial->__set('status', 0);
                $this->getEntityManager()->persist($filial);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Filial desativada com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a desativação da filial. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Constrói um array de cidades de acordo com estado passado
     * @param int $estado_id
     * @return Array
     */
    private function getOptionsCity($estado_id)
    {
        $cidades = $this->getEntityManager()
                        ->getRepository("System\Model\Cidades")
                        ->findBy(array("estado_id" => $estado_id), array("nome" => "ASC"));
        $options = array();
        foreach ($cidades as $cidade) {
            $options[$cidade->id] = $cidade->nome;
        }

        return $options;
    }

    /**
    * Retorno de cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $request = $this->getRequest();

        if($request->isXMLHttpRequest()){
            $paramFormat = $this->params()->fromRoute('format', 'json');
            $format = ($paramFormat == 'xml') ? 'xml' : 'json';
            $estadoID = (int) $this->params()->fromRoute('e', 0);

            $serializer = new Serializer(
                array(new GetSetMethodNormalizer()),
                array(
                    'xml' => new XmlEncoder(),
                    'json' => new JsonEncoder()
                )
            );

            $fields = array(
                    "c.id",
                    "c.nome",
            );

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                ->from("System\Model\Cidades", "c");
            if($estadoID > 0){

                $qb->where("c.estado_id = :estado");
                $qb->setParameter("estado", $estadoID);

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            } else{

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            }

            $content = $serializer->serialize($cidades, $format);

            $response = $this->getResponse();
            $response->setStatusCode(200);
            $response->setContent($content);
            $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
            return $response;
        }
    }
}