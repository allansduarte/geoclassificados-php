<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use System\Model\Favoritos as FavoritosModel;

/**
 * Controlador responsável pelo gerenciamento dos anúncios favoritos
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class FavoritosController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Cria um anúncio favorito
     * @return void
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if(!$request->isXmlHttpRequest())
            return $this->redirect()->toUrl('/');

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        $response = $this->getResponse();
        $response->setStatusCode(500);

        if(!isset($userSession)){
            $response->setContent(0);
            return $response;
        }

        $id = $this->params()->fromRoute('id', 0);

        if($id <= 0)
            return $response;

        $usuario = $this->getEntityManager()->find("System\Model\User", $userSession[0]->id);
        if(!$usuario)
            return $response;

        $exists = $this->getEntityManager()
                       ->getRepository("System\Model\Favoritos")
                       ->findBy(array(
                            "usuario_id"      => $userSession[0]->id,
                            "classificado_id" => $id,
                        ));

        if(count($exists) > 0)
            return $response;

        $classificado = $this->getEntityManager()->find("System\Model\Classificado", $id);
        if(!$classificado)
            return $response;

        try{
            $favoritosModel = new FavoritosModel;
            $favoritosModel->__set('usuario_id', $usuario);
            $favoritosModel->__set('classificado_id', $classificado);

            $this->getEntityManager()->persist($favoritosModel);
            $this->getEntityManager()->flush();
            $response->setStatusCode(200);
        } catch(\Exception $e){
            $response->setStatusCode(500);
            //$response->setContent("");
            echo $e->getMessage();
        }

        return $response;
    }

    /**
     * Deleta um anúncio favorito
     * @return void
     */
    public function removeAction()
    {
        $request = $this->getRequest();

        if(!$request->isXmlHttpRequest())
            return $this->redirect()->toUrl('/');

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        $response = $this->getResponse();
        $response->setStatusCode(500);

        if(!isset($userSession)){
            $response->setContent(0);
            return $response;
        }

        $id = $this->params()->fromRoute('id', 0);

        if($id <= 0)
            return $response;

        $favorito = $this->getEntityManager()
                       ->getRepository("System\Model\Favoritos")
                       ->findBy(array(
                            "usuario_id"      => $userSession[0]->id,
                            "classificado_id" => $id,
                        ));

        if(count($favorito[0]) <= 0)
            return $response;

        try{
            $this->getEntityManager()->remove($favorito[0]);
            $this->getEntityManager()->flush();
            $response->setStatusCode(200);
        } catch(\Exception $e){
            $response->setStatusCode(500);
            echo $e->getMessage();
        }

        return $response;
    }
}