<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

use Zend\Paginator\Paginator as ZendPaginator;
use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use System\Model\ContatoAnunciante as ContatoAnuncianteModel;

/**
 * Controlador responsável pelo Dashboard do sistema
 * 
 * @category System
 * @package Controller
 * @author  Allan Soares Duarte <allan.sduarte@gmail.com>
 */
class IndexController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de Painel de controle
    * @return ViewModel
    */
    public function indexAction()
    {

        $request = $this->getRequest();
        $retornoPagamento = $this->params()->fromRoute("retorno-pagamento", null);

        if(isset($retornoPagamento))
            $this->messages()->flashSuccess('O anúncio foi cadastrado com sucesso e está aguardando aprovação de pagamento.');

        $this->layout()->title = "Meu Temprafesta";
        $this->layout()->titlePage = "Meu Temprafesta";
        $this->layout()->subTitlePage = "meus anúncios, minhas empresas, meus anúncios favoritos, minhas últimas mensagens e configurações de perfil";

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $view = new ViewModel(
            array(
                'userSession' => $userSession,
            )
        );

        return $view;
    }

    /**
    * Apresenta as mensagens ao anunciante de seus classificados
    * @return ViewModel
    */
    public function mensagensAction()
    {
        $request = $this->getRequest();
        if(!$request->isXMLHttpRequest())
            return $this->redirect()->toUrl("/system");

        $action  = (int) $request->getQuery("action");
        if($action > 0)
        {
            $response = $this->getResponse();
            $query_uri = $request->getUri()->getQuery();
            $queryParams = explode("&", $query_uri);
            foreach($queryParams as $params)
            {
                $p = explode('message=', $params);
                $id = (int) $p[1];
                if($id <= 0)
                    continue;

                switch ($action) {
                    case 1://marcar como lida
                        $contatoAnuncianteModel = $this->getEntityManager()->find("System\Model\ContatoAnunciante", $id);
                        $contatoAnuncianteModel->__set('lido', 1);
                        try{
                            $this->getEntityManager()->persist($contatoAnuncianteModel);
                            $this->getEntityManager()->flush();
                            $response->setStatusCode(200);
                        } catch(\Exception $e){
                            $response->setStatusCode(500);
                        }
                        break;
                    case 2://marcar como não lida
                        $contatoAnuncianteModel = $this->getEntityManager()->find("System\Model\ContatoAnunciante", $id);
                        $contatoAnuncianteModel->__set('lido', null);
                        try{
                            $this->getEntityManager()->persist($contatoAnuncianteModel);
                            $this->getEntityManager()->flush();
                            $response->setStatusCode(200);
                        } catch(\Exception $e){
                            $response->setStatusCode(500);
                        }
                        break;
                    case 3://excluir registro
                        $contatoAnuncianteModel = $this->getEntityManager()->find("System\Model\ContatoAnunciante", $id);
                        try{
                            $this->getEntityManager()->remove($contatoAnuncianteModel);
                            $this->getEntityManager()->flush();
                            $response->setStatusCode(200);
                        } catch(\Exception $e){
                            $response->setStatusCode(500);
                        }
                        break;
                    
                    default:
                        //
                        break;
                }
            }
            return $response;
        }

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $mensagens = $this->getEntityManager()
             ->getRepository("System\Model\ContatoAnunciante")
             ->createQueryBuilder("ca");
        $mensagens->select(array("ca"))
             ->innerJoin('ca.classificado_id', 'c', "WITH", 'ca.classificado_id = c.id')
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("ca.usuario_id = :usuario_id")
             ->andWhere("ci.capa = 1")
             ->orderBy("c.reactivated", "DESC");
        $mensagens->setParameter("usuario_id", $userSession[0]->id);

        $paginatorMensagemAdapter = new DoctrineAdapter(new ORMPaginator($mensagens));
        $paginatorMensagem        = new ZendPaginator($paginatorMensagemAdapter);
        $paginatorMensagem->setDefaultItemCountPerPage(10);
        $paginatorMensagem->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($mensagens->getQuery()->getResult()) > 0){
            $view = new ViewModel(array(
                'paginatorMensagem' => $paginatorMensagem,
            ));
        } else{
            $view = new ViewModel();
        }

        $view->setTerminal(true);
        return $view;
    }

    /**
    * Lista paginada dos anúncios favoritos do usuário
    * @return ViewModel
    */
    public function favoritosAction()
    {

        if(!$this->getRequest()->isXMLHttpRequest())
            return $this->redirect()->toUrl("/system");

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $periodoAnuncio = new \DateTime("-30 days");
        $anuncios = $this->getEntityManager()
             ->getRepository("System\Model\Favoritos")
             ->createQueryBuilder("f");
        $anuncios->select(array("f"))
             ->innerJoin('f.classificado_id', 'c', "WITH", 'f.classificado_id = c.id')
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("f.usuario_id = :usuario_id")
             ->andWhere("ci.capa = 1")
             ->andWhere("c.reactivated >= :date")
             ->orderBy("c.reactivated", "DESC");
        $anuncios->setParameter("usuario_id", $userSession[0]->id);
        $anuncios->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);

        $paginatorClassificadoAdapter = new DoctrineAdapter(new ORMPaginator($anuncios));
        $paginatorClassificado        = new ZendPaginator($paginatorClassificadoAdapter);
        $paginatorClassificado->setDefaultItemCountPerPage(5);
        $paginatorClassificado->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        $view = new ViewModel();

        if(count($anuncios->getQuery()->getResult()) > 0){
            $view->setVariables(array(
                'paginatorClassificado' => $paginatorClassificado,
            ));
        }

        $view->setVariables(array(
            'userSession' => $userSession,
        ));

        $view->setTerminal(true);

        return $view;
    }

    /**
    * Lista paginada dos anúncios cadastrados do usuário
    * @return ViewModel
    */
    public function anunciosAction()
    {

        if(!$this->getRequest()->isXMLHttpRequest())
            return $this->redirect()->toUrl("/system");

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $anuncios = $this->getEntityManager()
             ->getRepository("System\Model\Classificado")
             ->createQueryBuilder("c");
        $anuncios->select(array("c"))
             ->leftJoin('c.classificadoImagem', 'ci', "WITH", 'c.id = ci.classificado_id')
             ->where("c.usuario_id = :usuario_id")
             ->andWhere("ci.capa = 1")
             ->orderBy("c.reactivated", "DESC");
        $anuncios->setParameter("usuario_id", $userSession[0]->id);

        $paginatorClassificadoAdapter = new DoctrineAdapter(new ORMPaginator($anuncios));
        $paginatorClassificado        = new ZendPaginator($paginatorClassificadoAdapter);
        $paginatorClassificado->setDefaultItemCountPerPage(5);
        $paginatorClassificado->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        $view = new ViewModel();

        if(count($anuncios->getQuery()->getResult()) > 0){
            $view->setVariables(array(
                'paginatorClassificado' => $paginatorClassificado,
            ));
        }

        $view->setVariables(array(
            'userSession' => $userSession,
        ));

        $view->setTerminal(true);

        return $view;
    }

    /**
    * Lista paginada dos anúncios empresariais cadastrados do usuário
    * @return ViewModel
    */
    public function anunciosEmpresariaisAction()
    {

        if(!$this->getRequest()->isXMLHttpRequest())
            return $this->redirect()->toUrl("/system");

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $empresas = $this->getEntityManager()
                           ->getRepository("System\Model\Empresa")
                           ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC")
                            );
        if(!$empresas){
            $view = new ViewModel(array('possuiEmpresa' => $empresas));
            $view->setTerminal(true);
            return $view;
        }

        $anuncios = $this->getEntityManager()
             ->getRepository("System\Model\ClassificadoEmpresa")
             ->createQueryBuilder("ce");
        $anuncios->select(array("ce"))
             ///->leftJoin('ce.classificadoImagem', 'ci', "WITH", 'ce.id = ci.classificado_empresa_id')
             ->leftJoin('ce.empresa_id', 'e', "WITH", 'e.id = ce.empresa_id')
             ->where("e.usuario_id = :usuario_id")
//             ->andWhere("ci.capa = 1")
             ->orderBy("ce.reactivated", "DESC");
        $anuncios->setParameter("usuario_id", $userSession[0]->id);

        $paginatorClassificadoAdapter = new DoctrineAdapter(new ORMPaginator($anuncios));
        $paginatorClassificado        = new ZendPaginator($paginatorClassificadoAdapter);
        $paginatorClassificado->setDefaultItemCountPerPage(5);
        $paginatorClassificado->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($anuncios->getQuery()->getResult()) > 0){
            $view = new ViewModel(array(
                'paginatorClassificado' => $paginatorClassificado,
            ));
        } else{
            $view = new ViewModel();
        }

        $view->setTerminal(true);

        return $view;
    }
}