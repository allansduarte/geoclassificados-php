<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;
use Core\Controller\EntityUsingController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

use System\Form\Categoria as CategoriaForm;

use System\Model\Categoria as CategoriaModel;

/**
 * Controlador responsável pelo gerenciamento das categorias
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class CategoriaController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de formulário de cadastro e edição de Categorias
    * @return ViewModel
    */
    public function cadastroAction(){

        $id = $this->params()->fromRoute("edicao", 0);

        $this->layout()->title = "Categoria";
        $this->layout()->titlePage = "Categoria";
        $this->layout()->subTitlePage = "cadastro de nova categoria";

        $form = new CategoriaForm($this->getEntityManager());

        $view =  new ViewModel();

        if($id > 0)
        {
            $categoria = $this->getEntityManager()->find("System\Model\Categoria", $id);

            if(!$categoria)
                return $this->redirect()->toUrl('/system');

            $hydrator = new ZendReflection;
            $form->setHydrator($hydrator);
            $form->bind($categoria);
            $form->get('submit')->setAttribute('value', 'Alterar Categoria');

            $this->layout()->title = "Alterar categoria";
            $this->layout()->titlePage = "Categoria";
            $this->layout()->subTitlePage = "alteração da categoria ".$categoria->__get("nome");

            $view->setVariables(array('edicao' => true));
        }

        $view->setVariables(array('form' => $form));

        return $view;
    }

    /**
     * Cadastro e edição de categoria
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $urlRetorno = '/system/categoria/cadastro';
        if ($request->isPost()) {

            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $requestData = $request->getPost();

            if(isset($requestData['id']) && $requestData['id'] > 0)
                $urlRetorno .= "/edicao/".$requestData['id'];
            else
                $requestData['status'] = 1;

            $form = new CategoriaForm;
            $categoriaModel = new CategoriaModel;

            $form->setInputFilter($categoriaModel->getInputFilter());
            unset($requestData['submit']);
            $form->setData($requestData);

            if ($form->isValid()) {

                $data = $form->getData();
                unset($data['submit']);

                if(isset($data['id']) && $data['id'] > 0){
                    $categoriaModel = $this->getEntityManager()->find('System\Model\Categoria', $data['id']);
                }

                $categoriaModel->setData($data);

                try{
                    $this->getEntityManager()->persist($categoriaModel);
                    $this->getEntityManager()->flush();
                    $this->messages()->flashSuccess('Cadastro efetuado com sucesso!');
                } catch(\Exception $e){
                    $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
                }

                if(isset($data['id']) && $data['id'] > 0)
                    return $this->redirect()->toUrl($urlRetorno);
                else
                    return $this->redirect()->toUrl($urlRetorno);

            } else{

                if(isset($requestData['id']) && $requestData['id'] > 0)
                    return $this->redirect()->toUrl($urlRetorno);
                else
                    return $this->redirect()->toUrl($urlRetorno);
            }
        } else{
            //$this->messages()->flashInfo("Ocorreu algo inesperado no sistema. A equipe de suporte Duismag já foi avisada e está corrigindo o problema. Agradecemos a paciência.");
            $this->redirect($urlRetorno);
        }
    }

    /**
    * Apresenta tela de consulta de Categorias
    * @return ViewModel
    */
    public function consultaAction(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $categorias = $this->getEntityManager()
             ->getRepository("System\Model\Categoria")
             ->createQueryBuilder("c");
        $categorias->select("c")
            ->orderBy("c.nome", "DESC");

        $paginatorCategoriaAdapter = new DoctrineAdapter(new ORMPaginator($categorias));
        $paginatorCategoria        = new ZendPaginator($paginatorCategoriaAdapter);
        $paginatorCategoria->setDefaultItemCountPerPage(10);
        $paginatorCategoria->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($categorias->getQuery()->getResult()) > 0){
            $view = new ViewModel(array(
                'categorias'    => $paginatorCategoria,
            ));
        } else{
            $view = new ViewModel();
        }

        //$view->setTerminal(true);

        return $view;
    }

    /**
     * Exclui uma categoria
     * @return void
     */
    public function deleteAction()
    {

        $urlRetorno = "/system/categoria/consulta";

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $categoria = $this->getEntityManager()->find('System\Model\Categoria', $id);
        if($categoria){

            try{
                $categoria->__set('status', 0);
                $this->getEntityManager()->persist($categoria);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Categoria desativada com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão da categoria. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
    * Retorno dos dados de uma determinada categoria de usuário ou retorna todas as categorias
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $paramFormat = $this->params()->fromRoute('format', 'json');
        $format = ($paramFormat == 'xml') ? 'xml' : 'json';
        $categoriaID = (int) $this->params()->fromRoute('id', 0);
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $serializer = new Serializer(
            array(new GetSetMethodNormalizer()),
            array(
                'xml' => new XmlEncoder(),
                'json' => new JsonEncoder()
            )
        );

        $fields = array(
                "c.id",
                "c.nome",
        );

        if($categoriaID > 0){

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                  ->from("System\Model\Categoria", "c")
                  ->where("c.id = :categoria");
            $qb->setParameter("categoria", $categoriaID);

            $query = $qb->getQuery();

            $categorias = $query->getResult();
        } else{

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                  ->from("System\Model\Categoria", "c");

            $query = $qb->getQuery();

            $categorias = $query->getResult();
        }

        $content = $serializer->serialize($categorias, $format);

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->setContent($content);
        $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
        return $response;
    }
}