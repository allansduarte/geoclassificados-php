<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;
use Zend\Paginator\Paginator as ZendPaginator;

use Core\Controller\ActionController;

use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use System\Form\Empresa  as EmpresaForm;
use System\Form\Endereco as EnderecoForm;
use System\Form\Contato  as ContatoForm;
use System\Form\ComboboxEstado;
use System\Form\FileField;

use System\Model\Empresa  as EmpresaModel;
use System\Model\Endereco as EnderecoModel;
use System\Model\Contato  as ContatoModel;
use System\Model\MidiaSoial as MidiaSoialModel;
use System\Model\EmpresaMidiaSocial as EmpresaMidiaSocialModel;

use System\Controller\FilialController;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Controlador que gerencia a empresa
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class EmpresaController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
    * Apresenta tela de formulário de cadastro e edição de Empresas
    * @return ViewModel
    */
    public function cadastroAction(){

        $id = $this->params()->fromRoute("edicao", 0);

        $this->layout()->title = "Empresa";
        $this->layout()->titlePage = "Empresa";
        $this->layout()->subTitlePage = "cadastro de empresa";

        $formEmpresa        = new EmpresaForm;
        $formEndereco       = new EnderecoForm;
        $formContato        = new ContatoForm;
        $formFileField      = new FileField;
        $formComboboxEstado = new ComboboxEstado($this->getServiceLocator());
        $view               = new ViewModel();

        if($id > 0){

            $view->setVariables(array('edicao' => true));
            $empresa = $this->getEntityManager()->find("System\Model\Empresa", $id);

            if(!$empresa)
                return $this->redirect()->toUrl('/system');

            $endereco = $this->getEntityManager()
                             ->getRepository("System\Model\Endereco")
                             ->findBy(array("empresa_id" => $id), array());

            $contato = $this->getEntityManager()
                             ->getRepository("System\Model\Contato")
                             ->findBy(array("empresa_id" => $id), array());

            $hydrator = new ZendReflection;
            $formEmpresa->setHydrator($hydrator);
            $formEmpresa->bind($empresa);
            $formEmpresa->get('submit')->setAttribute("value", "Alterar empresa");

            $cidade = $this->getEntityManager()->find("System\Model\Cidades", $empresa->__get("cidade_id"));
            $estado = $this->getEntityManager()->find("System\Model\Estados", $cidade->__get("estado_id"));
            $formComboboxEstado->get("estado_id")->setAttribute('value', $estado->__get('id'));
            $formEmpresa->get('cidade_id')->setAttribute('options', $this->getOptionsCity($estado->__get('id')));

            $formEndereco->setHydrator($hydrator);
            $formEndereco->bind($endereco[0]);


            if(count($contato) > 0){
                $formContato->setHydrator($hydrator);
                $formContato->bind($contato[0]);
            }

            $empresaMidiaSocial = $this->getEntityManager()
                                       ->getRepository("System\Model\EmpresaMidiaSocial")
                                       ->findBy(array("empresa_id" => $id), array());

            $view->setVariables(array("empresaMidiaSocial" => $empresaMidiaSocial));

            $this->layout()->title = "Alterar Empresa";
            $this->layout()->titlePage = "Empresa";
            $this->layout()->subTitlePage = "alteração da empresa ".$empresa->__get("nome");
        }

        $midiasSociais = $this->getEntityManager()->getRepository("System\Model\MidiaSocial")->findAll();
        $view->setVariables(
            array(
                'formEmpresa'        => $formEmpresa,
                'formEndereco'       => $formEndereco,
                'formContato'        => $formContato,
                'formFileField'      => $formFileField,
                'formComboboxEstado' => $formComboboxEstado,
                'midiasSociais'      => $midiasSociais,
            )
        );

        return $view;
    }

    /**
    * Apresenta tela de consulta de Empresa
    * @return ViewModel
    */
    public function consultaAction(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $selectData = array(
                        "e",
                        "cidade.nome as nomeCidade",
                        "estado.uf as ufEstado",
                        "endereco.rua as enderecoRua",
                        "endereco.numero as enderecoNumero",
                        "endereco.bairro as enderecoBairro",
                        "contato.email as contatoEmail",
                        "contato.telefone as contatoTelefone",
                        "contato.celular as contatoCelular",
                        //"midiaSocial.nome as midiaSocialNome",
                        //"empresaMidiaSocial.url as midiaSocialUrl",
        );

        $empresas = $this->getEntityManager()
             ->getRepository("System\Model\Empresa")
             ->createQueryBuilder("e");
        $empresas->select($selectData)
             ->leftJoin('e.endereco', 'endereco', "WITH", 'endereco.empresa_id = e.id')
             ->leftJoin('e.contato', 'contato', "WITH", 'contato.empresa_id = e.id')
             //->leftJoin('e.empresaMidiaSocial', 'empresaMidiaSocial', "WITH", 'empresaMidiaSocial.empresa_id = e.id')
             //->leftJoin('empresaMidiaSocial.midia_social_id', 'midiaSocial', "WITH", 'midiaSocial.id = empresaMidiaSocial.id')
             ->leftJoin('e.cidade_id', 'cidade', "WITH", 'cidade.id = e.cidade_id')
             ->leftJoin('cidade.estado_id', 'estado', "WITH", 'estado.id = cidade.estado_id')
             ->where("e.usuario_id = :usuario")
             //->groupBy("e.id")
             ->orderBy("e.nome", "DESC");
        $empresas->setParameter("usuario", $userSession[0]->id);

        $paginatorEmpresasAdapter = new DoctrineAdapter(new ORMPaginator($empresas));
        $paginatorEmpresas        = new ZendPaginator($paginatorEmpresasAdapter);
        $paginatorEmpresas->setDefaultItemCountPerPage(10);
        $paginatorEmpresas->setCurrentPageNumber($this->params()->fromRoute('page', 1));

        if(count($empresas->getQuery()->getResult()) > 0){
            $view = new ViewModel(array(
                'empresas'    => $paginatorEmpresas
            ));
        } else{
            $view = new ViewModel();
        }

        //$view->setTerminal(true);

        return $view;
    }

    /**
     * Cadastro de empresa
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $type = (int) $this->params()->fromRoute('t', 0);
        $urlRetorno = '/system/empresa/cadastro';

        if ($request->isPost())
        {
            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $requestData = $request->getPost();
            $requestData["usuario_id"]      = $userSession[0]->id;

            if(!isset($requestData["status"]))
                $requestData["status"]          = 1;

            if($request->getFiles()->imagem['type'] != ""){

                $maxSize = 8388608;//8mb

                $check = getimagesize($request->getFiles()->imagem["tmp_name"]);

                if(!$check){
                    $this->messages()->flashError('Para efetuar o cadastro, é necessário informar uma imagem.');
                    return $this->redirect()->toUrl($urlRetorno);
                }

                if($request->getFiles()->imagem["size"] > $maxSize){
                    $this->messages()->flashError("A imagem informada não pode ter de tamanho maior que 8mb.");
                    return $this->redirect()->toUrl($urlRetorno);
                }

                $ext        = ".".pathinfo($request->getFiles()->imagem["name"], PATHINFO_EXTENSION);
                $image      = bin2hex(openssl_random_pseudo_bytes(10)).$ext;
                $old_image  = $request->getFiles()->imagem["name"];

                //$requestData['imagem_logo']     = file_get_contents($request->getFiles()->imagem['tmp_name']);
                $requestData['imagem']          = $image;
                $requestData['size_logo']       = $request->getFiles()->imagem['size'];
                $requestData['mime_type_logo']  = $request->getFiles()->imagem['type'];
            }

            $enderecoModel = new EnderecoModel;
            $contatoModel  = new ContatoModel;
            $empresaModel  = new EmpresaModel;

            if(isset($requestData['id']) && $requestData['id'] > 0){
                $urlRetorno  .= "/edicao/".$requestData['id'];
                $empresaModel = $this->getEntityManager()->find('System\Model\Empresa', $requestData['id']);
                $requestData['modified']         = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
            } else{
                if(!$request->getFiles()->imagem['type']){
                    $this->messages()->flashError('Por favor, informe um arquivo de imagem.');
                    return $this->redirect()->toUrl($urlRetorno);
                }
                $requestData['created']         = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
            }

            $empresaModel->setData($requestData);
            $cidades = $this->getEntityManager()->find("System\Model\Cidades", $requestData['cidade_id']);
            $empresaModel->__set("cidade_id", $cidades);

            try{

                $msg = 'Empresa cadastrada com sucesso!';
                if($empresaModel->__get('id'))
                    $msg = 'Empresa alterada com sucesso!';

                $em = $this->getEntityManager();
                $em->getConnection()->beginTransaction();

                $em->persist($empresaModel);
                $em->flush();

                if($request->getFiles()->imagem['type'] != ""){
                    $empresa_id      = $empresaModel->__get('id');
                    $baseDirectory   = getcwd()."/public/files/empresa/".$userSession[0]->id;
                    $directory       = $baseDirectory."/".$empresa_id;
                    $file            = $directory."/".$image;

                    if(isset($requestData['id']) && $requestData['id'] > 0){//se em modo de edicao
                        if(file_exists($directory."/".$old_image))
                            unlink($directory."/".$old_image);

                        if(!move_uploaded_file($request->getFiles()->imagem["tmp_name"], $file)){
                            $em->getConnection()->rollback();
                            $em->close();
                            $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod.: im1)');
                            return $this->redirect()->toUrl($urlRetorno);
                        }
                    } else{//se nao em modo de edicao
                        if(!is_dir($baseDirectory))
                            mkdir($baseDirectory, 0777, true);

                        if(!is_dir($directory))
                            mkdir($directory, 0777, true);

                        if(!move_uploaded_file($request->getFiles()->imagem["tmp_name"], $file)){
                            $em->getConnection()->rollback();
                            $em->close();
                            $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod.: im2)');
                            return $this->redirect()->toUrl($urlRetorno);
                        }
                    }
                }

                try{

                    if(isset($requestData['id']) && $requestData['id'] > 0){
                        $enderecoModel = $this->getEntityManager()
                                              ->getRepository('System\Model\Endereco')
                                              ->findBy(array("empresa_id" => $requestData['id']), array());
                        $enderecoModel = $enderecoModel[0];
                    }

                    $enderecoModel->setData($requestData);
                    $enderecoModel->__set('empresa_id', $empresaModel);

                    try{
                        $em->persist($enderecoModel);
                        $em->flush();
                    } catch(\Exception $e){
                        $em->getConnection()->rollback();
                        $em->close();
                        $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod.: empresa 136)');
                        return $this->redirect()->toUrl($urlRetorno);
                    }

                    if($requestData['email'] != "" || $requestData['celular'] != "" || $requestData['telefone'] != ""){
                        if(isset($requestData['id']) && $requestData['id'] > 0){
                            $contatoModel = $this->getEntityManager()
                                                  ->getRepository('System\Model\Contato')
                                                  ->findBy(array("empresa_id" => $requestData['id']), array());
                            if($contatoModel){
                                $contatoModel = $contatoModel[0];
                            } else{
                                $contatoModel = new ContatoModel;
                            }
                        }

                        $contatoModel->setData($requestData);
                        $contatoModel->__set('empresa_id', $empresaModel);
                        try{
                            $em->persist($contatoModel);
                            $em->flush();
                        } catch(\Exception $e){
                            $em->getConnection()->rollback();
                            $em->close();

                            $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod: empresa 137)');
                            return $this->redirect()->toUrl($urlRetorno);
                        }
                    }

                    foreach ($requestData['midiaSocial'] as $key => $midiaSocial) {
                        try{
                            if(isset($requestData['id']) && $requestData['id'] > 0){

                                $empresaMidiaSocial = $this->getEntityManager()
                                                      ->getRepository('System\Model\EmpresaMidiaSocial')
                                                      ->findBy(array("empresa_id" => $requestData['id'], "midia_social_id" => $key), array());

                                if(count($empresaMidiaSocial) <= 0 && $midiaSocial != "" ){

                                    $midia_social_id = $this->getEntityManager()->find("System\Model\MidiaSocial", $key);
                                    $empresaMidiaSocial = new EmpresaMidiaSocialModel;
                                    $empresaMidiaSocial->__set('url', $midiaSocial);
                                    $empresaMidiaSocial->__set('empresa_id', $empresaModel);
                                    $empresaMidiaSocial->__set('midia_social_id', $midia_social_id);
                                    $empresaMidiaSocial->__set('status', 1);
                                } else if(count($empresaMidiaSocial) > 0){

                                    $empresaMidiaSocial = $empresaMidiaSocial[0];
                                    if($empresaMidiaSocial->__get('url') != $midiaSocial)
                                        $empresaMidiaSocial->__set('url', $midiaSocial);
                                }

                            } else{

                                if($midiaSocial != ""){
                                    $midia_social_id = $this->getEntityManager()->find("System\Model\MidiaSocial", $key);
                                    $empresaMidiaSocial = new EmpresaMidiaSocialModel;
                                    $empresaMidiaSocial->__set('url', $midiaSocial);
                                    $empresaMidiaSocial->__set('empresa_id', $empresaModel);
                                    $empresaMidiaSocial->__set('midia_social_id', $midia_social_id);
                                    $empresaMidiaSocial->__set('status', 1);
                                }
                            }

                            if(isset($empresaMidiaSocial) && count($empresaMidiaSocial) <= 0 && $midiaSocial != "" ||
                                isset($empresaMidiaSocial) && count($empresaMidiaSocial) > 0){
                                $em->persist($empresaMidiaSocial);
                                $em->flush();
                            }
                        } catch(\Exception $e){
                            $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro das redes sociais. Por favor tente cadastrar as redes sociais atualizando os dados da empresa. <a href="\system\empresa\consulta\edicao\"'.$empresaModel->__get("id").'>Clique aqui</a>.');
                        }
                    }

                    $em->getConnection()->commit();
                    $this->messages()->flashSuccess($msg);
                } catch(\Exception $e){

                    $em->getConnection()->rollback();
                    $em->close();
                    $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
                }
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.');
            }

            return $this->redirect()->toUrl($urlRetorno);
        } else $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Desativar uma empresa e suas filiais
     * @return void
     */
    public function deleteAction()
    {
        $urlRetorno = "/system/empresa/consulta";

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $filiais = $this->getEntityManager()
                        ->getRepository('System\Model\Filiais')
                        ->findBy(array("empresa_id" => $id), array());

        if(count($filiais) > 0){
            foreach ($filiais as $filial) {
                $this->deleteFilial($filial->__get('id'));
            }
        }

        $empresa = $this->getEntityManager()->find('System\Model\Empresa', $id);
        if($empresa){

            /*$endereco = $this->getEntityManager()
                             ->getRepository('System\Model\Endereco')
                             ->findBy(array("empresa_id" => $id), array());
            $endereco = $endereco[0];

            $contato = $this->getEntityManager()
                             ->getRepository('System\Model\Contato')
                             ->findBy(array("empresa_id" => $id), array());
            $contato = $contato[0];

            $midiasSociais = $this->getEntityManager()
                             ->getRepository('System\Model\EmpresaMidiaSocial')
                             ->findBy(array("empresa_id" => $id), array());*/
            try{

                /*if($endereco)
                    $this->getEntityManager()->remove($endereco);

                if($contato)
                    $this->getEntityManager()->remove($contato);

                if(count($midiasSociais) > 0){
                    foreach ($midiasSociais as $midiaSocial) {
                        $this->getEntityManager()->remove($midiaSocial);
                    }
                }*/

                $empresa->__set('status', 0);
                $this->getEntityManager()->persist($empresa);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Empresa desativada com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a desativação da empresa. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Desativar uma filial
     * @return boolean
     */
    private function deleteFilial($id)
    {

        if (!isset($id) || $id <= 0)
            return false;

        $filial = $this->getEntityManager()->find('System\Model\Filiais', $id);
        if($filial){

            /*$nomeFilial = $filial->__get('nome');

            $endereco = $this->getEntityManager()
                             ->getRepository('System\Model\Endereco')
                             ->findBy(array("filiais_id" => $id), array());
            $endereco = $endereco[0];

            $contato = $this->getEntityManager()
                             ->getRepository('System\Model\Contato')
                             ->findBy(array("filiais_id" => $id), array());
            $contato = $contato[0];*/
            try{

                /*if($endereco)
                    $this->getEntityManager()->remove($endereco);

                if($contato)
                    $this->getEntityManager()->remove($contato);*/

                $filial->__set('status', 0);
                $this->getEntityManager()->persist($filial);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Filial '.$nomeFilial.' desativada com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a desativação da filial '.$nomeFilial.' . Por favor tente novamente mais tarde.');
            }
        }

        return true;
    }

    /**
     * Constrói um array de cidades de acordo com estado passado
     * @param int $estado_id
     * @return Array
     */
    private function getOptionsCity($estado_id)
    {
        $cidades = $this->getEntityManager()
                        ->getRepository("System\Model\Cidades")
                        ->findBy(array("estado_id" => $estado_id), array("nome" => "ASC"));
        $options = array();
        foreach ($cidades as $cidade) {
            $options[$cidade->id] = $cidade->nome;
        }

        return $options;
    }

    /**
    * Retorno de cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $request = $this->getRequest();

        if($request->isXMLHttpRequest()){
            $paramFormat = $this->params()->fromRoute('format', 'json');
            $format = ($paramFormat == 'xml') ? 'xml' : 'json';
            $estadoID = (int) $this->params()->fromRoute('e', 0);

            $serializer = new Serializer(
                array(new GetSetMethodNormalizer()),
                array(
                    'xml' => new XmlEncoder(),
                    'json' => new JsonEncoder()
                )
            );

            $fields = array(
                    "c.id",
                    "c.nome",
            );

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                ->from("System\Model\Cidades", "c");
            if($estadoID > 0){

                $qb->where("c.estado_id = :estado");
                $qb->setParameter("estado", $estadoID);

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            } else{

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            }

            $content = $serializer->serialize($cidades, $format);

            $response = $this->getResponse();
            $response->setStatusCode(200);
            $response->setContent($content);
            $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
            return $response;
        }
    }
}