<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use System\Form\Login;

/**
 * Controlador que responsável por autenticações
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte<allan.sduarte@gmail.com>
 */
class AuthController extends ActionController
{
    /**
     * Apresenta o formulário de login e redireciona o usuário para a tela principal do system, 
     * caso ele já esteja logado, evitando uma segunda tentativa de autenticação, desnecessária
     * do usuário
     * @return void
     */
    public function indexAction()
    {
        $this->layout()->title = "Login de Usuário";
        $this->layout("layout/layout.phtml");

        $request = $this->getRequest();

        //echo md5("ald123@");
        if (!$request->isPost()) {

            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            if(isset($userSession)){
                return $this->redirect()->toUrl('/system/');
            }

            $form = new Login();
            return new ViewModel(array(
                'form' => $form
            ));
        } else{

            $data = $request->getPost();
            $service = $this->getService('System\Service\Auth');
            $auth = $service->authenticate(
                array('username' => $data['username'], 'password' => $data['password'])
            );

            if($auth){
                return $this->redirect()->toUrl('/system/');
            }
             else{
                $this->messages()->flashError($service->getMessage());
                return $this->redirect()->toUrl('/system/auth');
             }
        }
    }

    /**
     * Faz o login do usuário
     * @return void
     */
    public function loginAction()
    {


    }

    /**
     * Faz o logout do usuário
     * @return void
     */
    public function logoutAction()
    {
        $service = $this->getService('System\Service\Auth');
        $auth = $service->logout();
        
        return $this->redirect()->toUrl('/');
    }
}