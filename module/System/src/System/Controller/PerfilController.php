<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;

use Core\Controller\ActionController;

use Doctrine\ORM\EntityManager;

use System\Form\AlterarSenha as AlterarSenhaForm;
use System\Form\Perfil as PerfilForm;
use System\Form\ComboboxEstado;

use System\Model\Perfil as PerfilModel;
use System\Model\User as UserModel;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Controlador responsável por gerenciar as informaões do perfil do usuário
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.duarte@duismag.com>
 */
class PerfilController extends ActionController
{

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Lista os dados do perfil de usuário
     * @return ViewModel
     */
    public function indexAction()
    {

        $alterarSenhaForm = new AlterarSenhaForm;
        $this->layout()->title = "Perfil";
        $this->layout()->titlePage = "Perfil";
        $this->layout()->subTitlePage = "gerenciamento de perfil";
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $perfil = $this->getEntityManager()
             ->getRepository("System\Model\Perfil")
             ->createQueryBuilder("p");
        $perfil->select(array("p", "c.nome as cidade", "e.uf as uf"))
             ->leftJoin('System\Model\Cidades', 'c', "WITH", 'p.cidade_id = c.id')
             ->leftJoin('System\Model\Estados', 'e', "WITH", 'c.estado_id = e.id')
             ->where("p.usuario_id = :usuario_id");
        $perfil->setParameter("usuario_id", $userSession[0]->id);

        return new ViewModel(
            array(
                'perfil'     => $perfil->getQuery()->getResult()[0],
                'formPerfil' => $alterarSenhaForm
            )
        );
    }

    /**
     * Apresenta tela de alteração de Perfil
     * @return ViewModel
     */
    public function alterarPerfilAction(){

        $request = $this->getRequest();
        $urlRetorno = "/system";

        if($request->isXMLHttpRequest()){

            $id = (int) $this->params()->fromRoute('p', 0);
            $formPerfil = new PerfilForm;
            $comboboxEstado = new ComboboxEstado($this->getServiceLocator());

            if($id > 0){
                $session = $this->getServiceLocator()->get('Session');
                $userSession = $session->offsetGet('user');
                $perfil = $this->getEntityManager()->find("System\Model\Perfil", $id);

                $cidade = $this->getEntityManager()->find("System\Model\Cidades", $perfil->__get("cidade_id"));
                $estado = $this->getEntityManager()->find("System\Model\Estados", $cidade->__get("estado_id"));

                $comboboxEstado->get("estado_id")->setAttribute('value', $estado->__get('id'));
                $formPerfil->get('cidade_id')->setAttribute('options', $this->getOptionsCity($estado->__get('id')));
                $formPerfil->get('submit')->setAttribute("value", "Alterar perfil");

                $hydrator = new ZendReflection;
                $formPerfil->setHydrator($hydrator);
                $formPerfil->bind($perfil);
            }

            $view = new ViewModel();
            $view->setVariables(
                array(
                    "formPerfil"     => $formPerfil,
                    "comboboxEstado" => $comboboxEstado
                )
            );
            $view->setTerminal(true);

            return $view;
        } else return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Edição de perfil
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $type = (int) $this->params()->fromRoute('t', 0);
        $urlRetorno = '/system/perfil';

        if ($request->isPost()) {

            switch ($type) {
                case 1:
                    $this->profileSave($request);
                    break;
                case 2:
                    $this->changePassword($request);
                    break;

                default:
                    $this->redirect()->toUrl("/system/perfil");
                    break;
            }
        } else die("out if");//$this->redirect($urlRetorno);
    }

    /**
     * Realiza a mudança de senha do usuário
     * @return void
     */
    private function changePassword($request){

        $urlRetorno = '/system/perfil';

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $requestData = $request->getPost();

        if(!$this->verifyPassword($requestData['senhaAtual'])){
            $this->messages()->flashError('Senha atual não confere! Por favor, informe a senha atual correta.');
            return $this->redirect()->toUrl($urlRetorno);
        }

        $requestData['senha']    = md5($requestData['novaSenha']);
        $requestData['modified'] = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
        unset($requestData['senhaAtual']);
        unset($requestData['novaSenha']);

        $userModel = new UserModel;

        $userModel = $this->getEntityManager()->find('System\Model\User', $userSession[0]->id);
        $userModel->setData($requestData);

        try{
            $this->getEntityManager()->persist($userModel);
            $this->getEntityManager()->flush();
            $this->messages()->flashSuccess('Senha alterada com sucesso!');
        } catch(\Exception $e){
            $this->messages()->flashError('Ocorreu um erro ao efetuar a alteração. Por favor tente novamente mais tarde.');
        }

        $this->saveUserSession();

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Grava (Commit) no banco de dados os dados de perfil do usuário
     * @return void
     */
    private function profileSave($request){

        $urlRetorno = '/system/perfil';

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $requestData = $request->getPost();
        $requestData["usuario_id"]  = $userSession[0]->id;
        $requestData['modified']    = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

        $fileData = $request->getFiles();
        if($fileData->file_memory['error'] != 4){
            $requestData['file_memory'] = file_get_contents($fileData->file_memory['tmp_name']);
            $requestData['file_size']   = $fileData->file_memory['size'];
            $requestData['file_mime_type']   = $fileData->file_memory['type'];
        }

        $perfilModel = new PerfilModel;

        $perfilModel = $this->getEntityManager()->find('System\Model\Perfil', $requestData['id']);
        $perfilModel->setData($requestData);

        try{
            $this->getEntityManager()->persist($perfilModel);
            $this->getEntityManager()->flush();
            $this->messages()->flashSuccess('Perfil alterado com sucesso!');
            $this->saveUserSession();
        } catch(\Exception $e){
            $this->messages()->flashError('Ocorreu um erro ao efetuar a alteração. Por favor tente novamente mais tarde.');
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Constrói um array de cidades de acordo com estado passado
     * @param int $estado_id
     * @return Array
     */
    private function getOptionsCity($estado_id)
    {
        $cidades = $this->getEntityManager()
                        ->getRepository("System\Model\Cidades")
                        ->findBy(array("estado_id" => $estado_id), array("nome" => "ASC"));
        $options = array();
        foreach ($cidades as $cidade) {
            $options[$cidade->id] = $cidade->nome;
        }

        return $options;
    }

    /**
     * Constrói um array de cidades de acordo com estado passado
     * @param int $pwd
     * @return boolean
     */
    private function verifyPassword($pwd)
    {

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $retorno = false;
        
        if( $userSession[0]->senha == md5($pwd) )
            $retorno = true;

        return $retorno;
    }

    /**
     * Salva na sessão os dados do usuário
     * @return void
     */
    private function saveUserSession(){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $usuarioPerfil = $this->getEntityManager()
                              ->getRepository("System\Model\User")
                              ->createQueryBuilder("u");
        $usuarioPerfil->select(array("u", "p"))
                      ->leftJoin('System\Model\Perfil', 'p', "WITH", 'u.id = p.usuario_id')
                      ->where("u.id = :usuario_id");;
        $usuarioPerfil->setParameter("usuario_id", $userSession[0]->id);
        $usuarioPerfilObject = $usuarioPerfil->getQuery()->getResult();

        $type = gettype($usuarioPerfilObject[1]->file_memory);
        if($type == "resource")
            $usuarioPerfilObject[1]->file_memory = base64_encode(stream_get_contents($usuarioPerfilObject[1]->file_memory));

        else
            $usuarioPerfilObject[1]->file_memory = base64_encode($usuarioPerfilObject[1]->file_memory);

        //salva o user na sessão    
        $session->offsetSet('user', $usuarioPerfilObject);
    }

    /**
    * Retorno de cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function wsAction(){
        $request = $this->getRequest();

        //if($request->isXMLHttpRequest()){
            $paramFormat = $this->params()->fromRoute('format', 'json');
            $format = ($paramFormat == 'xml') ? 'xml' : 'json';
            $estadoID = (int) $this->params()->fromRoute('e', 0);

            $serializer = new Serializer(
                array(new GetSetMethodNormalizer()),
                array(
                    'xml' => new XmlEncoder(),
                    'json' => new JsonEncoder()
                )
            );

            $fields = array(
                    "c.id",
                    "c.nome",
            );

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                ->from("System\Model\Cidades", "c");
            if($estadoID > 0){

                $qb->where("c.estado_id = :estado");
                $qb->setParameter("estado", $estadoID);

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            } else{

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            }

            $content = $serializer->serialize($cidades, $format);

            $response = $this->getResponse();
            $response->setStatusCode(200);
            $response->setContent($content);
            $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
            return $response;
        //}
    }
}