<?php
namespace System\Controller;

use Zend\View\Model\ViewModel;
use Zend\Stdlib\Hydrator\Reflection as ZendReflection;

use Core\Controller\ActionController;

use Doctrine\ORM\EntityManager;

use System\Form\Classificado as ClassificadoForm;
use System\Form\ClassificadoEmpresa as ClassificadoEmpresaForm;
use System\Form\ComboboxCategoria;
use System\Form\ComboboxEmpresa;
use System\Form\ComboboxEstado;

use System\Model\Classificado as ClassificadoModel;
use System\Model\ClassificadoImagem as ClassificadoImagemModel;
use System\Model\ClassificadoEmpresa as ClassificadoEmpresaModel;
use System\Model\Local as LocalModel;
use Marketing\Model\Cupom as CupomModel;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Controlador que gerencia os classificados
 * 
 * @category System
 * @package Controller
 * @author  Allan Duarte <allan.sduarte@gmail.com>
 */
class ClassificadoController extends ActionController
{

    private $periodoAnuncio = "-30 days";
    private $tokenPagSeguro = "EE9AAEA02D664DDFA757036D0A086873";//"562A03CE9B8C416C84010D078E0E1B8F";
    private $emailPagSeguro = "allan.sduarte@gmail.com";

    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $_em;

    public function setEntityManager(EntityManager $em)
    {
        $this->_em = $em;
    }
 
    public function getEntityManager()
    {
        if (null === $this->_em) {
            $this->_em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->_em;
    }

    /**
     * Apresenta tela de cadastro para classificados normais
     * @return ViewModel
     */
    public function normalAction()
    {
        $id = $this->params()->fromRoute("edicao", 0);
        $session     = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $this->layout()->title = "Anúncio Normal";
        $this->layout()->titlePage = "Anúncio";
        $this->layout()->subTitlePage = "publicação de anúncio normal";

        $comboboxEmpresa  = new ComboboxEmpresa($this->getServiceLocator());
        $comboboxEmpresa->get('empresa_id')->setOptions(array('label' => 'Deseja vincular este anúncio a uma empresa?'));
        $classificadoForm = new ClassificadoForm;
        $classificadoForm->setAttribute('action', '/system/classificado/save/t/1');
        $comboboxCategoria = new ComboboxCategoria($this->getServiceLocator());
        $comboboxEstado    = new ComboboxEstado($this->getServiceLocator());

        $view                = new ViewModel();

        if($id > 0){
            $view->setVariables(array('edicao' => true));
            $classificado = $this->getEntityManager()->find("System\Model\Classificado", $id);
            $view->setVariables(array('classificado' => $classificado));

            if(!$classificado)
                return $this->redirect()->toUrl('/system');

            if($classificado->tipo == 1)
                $view->setVariables(array('isDestaque' => true));

            $view->setVariables(array(
                'latitude'  => $classificado->__get('latitude'),
                'longitude' => $classificado->__get('longitude'),
            ));

            $classificadoImagem = $this->getEntityManager()
                                       ->getRepository("System\Model\ClassificadoImagem")
                                       ->findBy(array('classificado_id' => $classificado->__get('id')));

            if($classificadoImagem)
                $view->setVariables(array('classificadoImagens' => $classificadoImagem));

            $hydrator = new ZendReflection;
            $classificadoForm->setHydrator($hydrator);
            $classificadoForm->bind($classificado);
            $classificadoForm->get('submit')->setAttribute("value", "Alterar Classificado");
            $classificadoForm->get('descricao')->setAttribute('value', html_entity_decode($classificado->__get('descricao')));

            $subcategoria = $this->getEntityManager()->find("System\Model\Subcategoria", $classificado->__get("subcategoria_id"));

            $comboboxCategoria->get("categoria")->setAttribute('value', $subcategoria->__get('categoria_id'));
            $classificadoForm->get('subcategoria_id')->setAttribute('options', $this->getOptionsSubcategory($subcategoria->__get('categoria_id')));
            $comboboxEmpresa->get("empresa_id")->setAttribute('value', $classificado->__get('empresa_id'));

            $cidade = $this->getEntityManager()->find("System\Model\Cidades", $classificado->__get("cidade_id"));
            $estado = $this->getEntityManager()->find("System\Model\Estados", $cidade->__get("estado_id"));
            $comboboxEstado->get("estado_id")->setAttribute('value', $estado->__get('id'));
            $classificadoForm->get('cidade_id')->setAttribute('options', $this->getOptionsCity($estado->__get('id')));
        } else{
            $classificadoForm->get('email')->setAttribute('value', $userSession[0]->email);
            $classificadoForm->get('website')->setAttribute('value', $userSession[1]->website);
            $classificadoForm->get('telefone')->setAttribute('value', $userSession[1]->telefone);
            $classificadoForm->get('celular')->setAttribute('value', $userSession[1]->celular);

            $qb = $this->getEntityManager()
                 ->getRepository("System\Model\Cidades")
                 ->createQueryBuilder("c");
            $qb->select(array("c.nome as cidade", "e.uf"))
                 ->innerJoin("System\Model\Estados", "e", "WITH", "e.id = c.estado_id")
                 ->where("c.id = :cidade_id");

            $qb->setParameter('cidade_id', $userSession[1]->cidade_id);
            $localizacao = $qb->getQuery()->getResult();
            $view->setVariable("localizacao", $localizacao[0]);

            $cidade = $this->getEntityManager()->find("System\Model\Cidades", $userSession[1]->cidade_id);
            $estado = $this->getEntityManager()->find("System\Model\Estados", $cidade->__get("estado_id"));
            $comboboxEstado->get("estado_id")->setAttribute('value', $estado->__get('id'));
            $classificadoForm->get('cidade_id')->setValueOptions($this->getOptionsCity($estado->__get('id')))
                                               ->setAttributes(array(
                                                'value'     => $userSession[1]->cidade_id,
                                                'selected'  => true,
                                                ));
        }

        $empresas     = $this->getEntityManager()
                             ->getRepository("System\Model\Empresa")
                             ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC")
                             );

        if(!$empresas)
            $view->setVariables(array('naoPossuiEmpresa' => false));

        $view->setVariables(
            array(
                'formClassificado'      => $classificadoForm,
                'comboboxCategoria'     => $comboboxCategoria,
                'comboboxEstado'        => $comboboxEstado,
                'formComboboxEmpresa'   => $comboboxEmpresa,
                'userSession'           => $userSession
            )
        );

        return $view;
    }

    /**
     * Apresenta tela de cadastro para classificados de empresas
     * @return ViewModel
     */
    public function empresaAction()
    {
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $classificadoEmpresaForm = new ClassificadoEmpresaForm;
        $comboboxEmpresa = new ComboboxEmpresa($this->getServiceLocator());

        $this->layout()->title = "Anúncio Empresarial";
        $this->layout()->titlePage = "Anúncio";
        $this->layout()->subTitlePage = "publicação de anúncio empresarial";

        $empresas = $this->getEntityManager()
                           ->getRepository("System\Model\Empresa")
                           ->findBy(array(
                                    "status" => 1,
                                    "usuario_id" => $userSession[0]->id
                                    ),
                                    array("nome" => "ASC")
                            );

        return new ViewModel(
            array(
                'formClassificadoEmpresa' => $classificadoEmpresaForm,
                'formComboboxEmpresa'     => $comboboxEmpresa,
                'possuiEmpresas'          => $empresas
            )
        );
    }

    /**
     * Cadastro de anúncio
     * @return void
     */
    public function saveAction()
    {

        $request = $this->getRequest();
        $type = (int) $this->params()->fromRoute('t', 0);
        $urlRetorno = '/system';

        if ($request->isPost()) {

            switch ($type) {
                case 1:
                    $this->normalSave($request);
                    break;
                case 2:
                    $this->empresaSave($request);
                    break;

                default:
                    $this->redirect()->toUrl("/system");
                    break;
            }
        } else $this->redirect($urlRetorno);
    }

    /**
     * Grava (Commit) no banco de dados para os dados de anúncio normal
     * @return void
     */
    private function normalSave($request){

        $urlRetorno = '/system/classificado/normal';

        $session            = $this->getServiceLocator()->get('Session');
        $userSession        = $session->offsetGet('user');
        $classificadoModel  = new ClassificadoModel;
        $mensagemRetorno    = "";
        $dataFixa               = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

        $requestData = $request->getPost();
        $valorExplode               = explode(".", str_replace(",", ".", $requestData['valor']));
        $requestData['valor']       = str_replace(",".end($valorExplode), "", $requestData['valor']);
        $requestData['valor']       = str_replace(",", "", $requestData['valor']);
        $requestData['valor']       = $requestData['valor'].".".end($valorExplode);
        $requestData["usuario_id"]  = $userSession[0]->id;
        $requestData["status"]      = 1;
        $requestData['empresa_id']  = ((int) $requestData['empresa_id'] > 0 ? $requestData['empresa_id'] : null);
        $requestData['descricao']   = htmlentities($requestData['descricao']);

        if(strlen(trim($requestData["local"])) <= 0){
            $requestData["latitude"] = "";
            $requestData["longitude"] = "";
        }

        if(isset($requestData['id']) && $requestData['id'] > 0){
            $urlRetorno  .= "/edicao/".$requestData['id'];
            $classificadoModel = $this->getEntityManager()->find('System\Model\Classificado', $requestData['id']);

            if($classificadoModel->__get('status') != 1)
                $requestData["status"] = $classificadoModel->__get('status');

            $requestData['modified']     = $dataFixa;

            $classificadoImagemModel = $this->getEntityManager()
                                            ->getRepository("System\Model\ClassificadoImagem")
                                            ->findBy(array("classificado_id" => $classificadoModel->__get('id')));
        } else{
            $requestData['created'] = $requestData['reactivated'] = $dataFixa;
        }

        $cupom = $requestData["cupom"];
        unset($requestData["cupom"]);
        $cupomModel = null;
        try{
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select("c")
               ->from("Marketing\Model\Cupom", 'c')
               ->where($qb->expr()->eq("c.token", ":cupom"))
               ->andWhere($qb->expr()->eq("c.email", ":email"))
               ->andWhere($qb->expr()->eq("c.type", 1))
               ->andWhere($qb->expr()->isNull('c.used'));
            $qb->setParameter("cupom", $cupom);
            $qb->setParameter("email", $userSession[0]->email);
            $cupomModel = $qb->getQuery()->getResult();
        } catch(\Doctrine\ORM\NoResultException $e){
            $cupomModel = null;
        }

        if(isset($requestData["tipo"]) && $requestData["tipo"] > 0 && !$cupomExist){
            $requestData["status"] = 3;//Aguardando pagamento
            $email = $this->emailPagSeguro;
            $token = $this->tokenPagSeguro;
            $urlPaymentRequest = "https://ws.pagseguro.uol.com.br/v2/checkout/?email=".$email."&token=".$token;
            $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
            $urlSystem = $helper->__invoke('/system/index/index/retorno-pagamento/1');

            $data['email'] = $email;
            $data['token'] = $token;
            $data['currency'] = 'BRL';
            $data['itemId1'] = '0001';
            $data['itemDescription1'] = 'Anuncio Destaque de Produto ou Servico';
            $data['itemAmount1'] = '15.00';
            $data['itemQuantity1'] = '1';
            $data['reference'] = "c".bin2hex(openssl_random_pseudo_bytes(99));
            /*$data['senderName'] = 'Jose Comprador';
            $data['senderAreaCode'] = '11';
            $data['senderPhone'] = '56273440';
            $data['senderEmail'] = 'comprador@uol.com.br';
            $data['shippingType'] = '1';
            $data['shippingAddressStreet'] = 'Av. Brig. Faria Lima';
            $data['shippingAddressNumber'] = '1384';
            $data['shippingAddressComplement'] = '5o andar';
            $data['shippingAddressDistrict'] = 'Jardim Paulistano';
            $data['shippingAddressPostalCode'] = '01452002';
            $data['shippingAddressCity'] = 'Sao Paulo';
            $data['shippingAddressState'] = 'SP';
            $data['shippingAddressCountry'] = 'BRA';*/
            $data['redirectURL'] = $urlSystem;

            $requestData['transaction_code'] = $data['reference'];

            $data = http_build_query($data);
            $curl = curl_init($urlPaymentRequest);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $xml= curl_exec($curl);
            if($xml == 'Unauthorized'){
                //Insira seu código de prevenção a erros

                $this->messages()->flashError('Ops! Estamos com problemas no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
                return $this->redirect()->toUrl("/system");
                exit();//Mantenha essa linha
            }

            $xml= simplexml_load_string($xml);

            if(count($xml->error) > 0){
                //Insira seu código de tratamento de erro, talvez seja útil enviar os códigos de erros.

                $this->messages()->flashError('Ops! Ocorreu um erro inesperado no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
                return $this->redirect()->toUrl($urlRetorno);
                exit();
            }

            curl_close($curl);
        }

        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        if(count($cupomModel) > 0 && $cupom !== ""){
            $cupomModel = $cupomModel[0];
            $requestData["tipo"] = 1;
            $cupomModel->__set("used", $dataFixa);
            try{
                $em->persist($cupomModel);
                $em->flush();
            } catch(\Exception $e){
                $em->getConnection()->rollback();
                $em->close();
                $this->messages()->flashError('Ops! Ocorreu um erro ao processar o cupom. Tente novamente mais tarde.');
                return $this->redirect()->toUrl($urlRetorno);
            }
        } else if(strlen($cupom) > 0){
            $em->getConnection()->rollback();
            $em->close();
            $this->messages()->flashError('O cupom informado não é válido.');
            return $this->redirect()->toUrl($urlRetorno);
        }


        $classificadoModel->setData($requestData);

        try{
            $em->persist($classificadoModel);
            $em->flush();

            if(isset($requestData['id']) && $requestData['id'] > 0)
                $mensagemRetorno = 'Anúncio alterado com sucesso!';
            else
                $mensagemRetorno = 'Anúncio cadastrado com sucesso!';

            $i = 0;
            while($i < 7){
                $capa = 0;

                if($i == 0) $capa = 1;

                if($request->getFiles()->imagem[$i]["error"] == 0){

                    try{

                        $created = $dataFixa;
                        if(isset($requestData['id']) && $requestData['id'] > 0){
                            if(!$classificadoImagemModel[$i]){
                                $ciModel = new ClassificadoImagemModel;
                            } else{
                                $ciModelId = $classificadoImagemModel[$i]->id;
                                $created   = $classificadoImagemModel[$i]->created;
                                $ciModel = $classificadoImagemModel[$i];
                            }
                        } else{
                            $ciModel = new ClassificadoImagemModel;
                        }

                        $ext        = ".".pathinfo($request->getFiles()->imagem[$i]["name"], PATHINFO_EXTENSION);
                        $image      = bin2hex(openssl_random_pseudo_bytes(10)).$ext;
                        $old_image  = $ciModel->__get('nome');

                        $ciModel->__set('nome', $image);
                        $ciModel->__set('size', $request->getFiles()->imagem[$i]["size"]);
                        $ciModel->__set('mime_type', $request->getFiles()->imagem[$i]["type"]);
                        $ciModel->__set('capa', $capa);
                        $ciModel->__set('classificado_id', $classificadoModel);

                        $maxSize = 8388608;//8mb

                        $check = getimagesize($request->getFiles()->imagem[$i]["tmp_name"]);

                        if(!$check){
                            $em->getConnection()->rollback();
                            $em->close();
                            $this->messages()->flashError('Para efetuar o cadastro, é necessário informar uma imagem.');
                            return $this->redirect()->toUrl($urlRetorno);
                        }

                        if($request->getFiles()->imagem[$i]["size"] > $maxSize){
                            $em->getConnection()->rollback();
                            $em->close();
                            $this->messages()->flashError('A imagem '.$request->getFiles()->imagem[$i]["name"]." não pode ter um tamanho maior que 8mb.");
                            return $this->redirect()->toUrl($urlRetorno);
                        }

                        $classificado_id = $classificadoModel->__get('id');
                        $baseDirectory   = getcwd()."/public/files/classificado/".$userSession[0]->id;
                        $directory       = $baseDirectory."/".$classificado_id;
                        $file            = $directory."/".$image;

                        if(isset($requestData['id']) && $requestData['id'] > 0){//se em modo de edicao
                            if(file_exists($directory."/".$old_image))
                                unlink($directory."/".$old_image);

                            if(!move_uploaded_file($request->getFiles()->imagem[$i]["tmp_name"], $file)){
                                $em->getConnection()->rollback();
                                $em->close();
                                $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod: im)');
                                return $this->redirect()->toUrl($urlRetorno);
                            }
                        } else{//se nao em modo de edicao
                            if(!is_dir($baseDirectory))
                                mkdir($baseDirectory, 0777, true);

                            if(!is_dir($directory))
                                mkdir($directory, 0777, true);

                            if(!move_uploaded_file($request->getFiles()->imagem[$i]["tmp_name"], $file)){
                                $em->getConnection()->rollback();
                                $em->close();
                                $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod: im)');
                                return $this->redirect()->toUrl($urlRetorno);
                            }
                        }

                        if(isset($requestData['id']) && $requestData['id'] > 0 && isset($ciModelId)){
                            $ciModel->__set('id', $ciModelId);
                            $ciModel->__set('modified', $dataFixa);
                        } else{
                            $ciModel->__set('created', $created);
                        }

                        $em->persist($ciModel);
                        $em->flush();

                        if(!isset($ciModelId)){
                            unset($ciModel);
                        }
                    } catch(\Exception $e){
                        $em->getConnection()->rollback();
                        $em->close();
                        $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod: 186)');
                        return $this->redirect()->toUrl($urlRetorno);
                    }
                }

                $i++;
            }

            if(strlen(trim($requestData["local"])) >0){

                try {
                    $localModel = new LocalModel;
                    $localModel->__set('latitude', $requestData['latitude']);
                    $localModel->__set('longitude', $requestData['longitude']);
                    $localModel->__set('empresa_id', $requestData['empresa_id']);
                    $localModel->__set('created', $dataFixa);
                    $em->persist($localModel);
                    $em->flush();
                } catch (\Exception $e) {
                    //$em->getConnection()->rollback();
                    //$em->close();
                }
            }

            $em->getConnection()->commit();

            if(isset($requestData["tipo"]) && $requestData["tipo"] > 0 && isset($xml->code)):
                return $this->redirect()->toUrl('https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml->code);
            endif;
        } catch(\Exception $e){
            $em->getConnection()->rollback();
            $em->close();
            $this->messages()->flashError('Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde. (cod: 187)');
            return $this->redirect()->toUrl($urlRetorno);
        }


        if(isset($requestData['id']) && $requestData['id'] > 0)
                $mensagemRetorno = 'Anúncio alterado com sucesso!';
        else
            $mensagemRetorno = 'Anúncio cadastrado com sucesso!';

        if(!isset($requestData["tipo"]) || $requestData["tipo"] == 0)
            $this->messages()->flashSuccess($mensagemRetorno);

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Grava (Commit) no banco de dados para os dados de anúncio de empresa
     * @return void
     */
    private function empresaSave($request){

        $urlRetorno = '/system/classificado/empresa';

        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');
        $dataFixa               = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));

        $requestData = $request->getPost();

        $cupom = $requestData["cupom"];
        unset($requestData["cupom"]);
        $cupomModel = null;
        try{
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select("c")
               ->from("Marketing\Model\Cupom", 'c')
               ->where($qb->expr()->eq("c.token", ":cupom"))
               ->andWhere($qb->expr()->eq("c.email", ":email"))
               ->andWhere($qb->expr()->eq("c.type", 2))
               ->andWhere($qb->expr()->isNull('c.used'));
            $qb->setParameter("cupom", $cupom);
            $qb->setParameter("email", $userSession[0]->email);
            $cupomModel = $qb->getQuery()->getResult();
        } catch(\Doctrine\ORM\NoResultException $e){
            $cupomModel = null;
        }

        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        if(count($cupomModel) > 0){
            $cupomModel = $cupomModel[0];
            $cupomModel->__set("used", $dataFixa);
            try{
                $em->persist($cupomModel);
                $em->flush();
            } catch(\Exception $e){
                $em->getConnection()->rollback();
                $em->close();
                $this->messages()->flashError('Ops! Ocorreu um erro ao processar o cupom. Tente novamente mais tarde.');
                return $this->redirect()->toUrl($urlRetorno);
            }
        } else if(strlen($cupom) > 0){
            $em->getConnection()->rollback();
            $em->close();
            $this->messages()->flashError('O cupom informado não é válido.');
            return $this->redirect()->toUrl($urlRetorno);
        }

        $empresa_id = $requestData["empresa_id"];
        $periodoAnuncio = new \DateTime($this->periodoAnuncio);

        $qb = $this->getEntityManager()
                   ->getRepository("System\Model\ClassificadoEmpresa")
                   ->createQueryBuilder("ce");
        $qb->select()
             ->where('ce.reactivated >= :date')
             ->andWhere('ce.empresa_id = '.$empresa_id)
             ->andWhere('ce.status != 3');
        $qb->setParameter('date', $periodoAnuncio, \Doctrine\DBAL\Types\Type::DATETIME);
        $query = $qb->getQuery();
        $existeClassificadoAnuncio = $query->getResult();
        if(count($existeClassificadoAnuncio) > 0){
            $em->getConnection()->rollback();
            $em->close();
            $this->messages()->flashInfo('A empresa selecionada já está sendo divulgada. Tente selecionar outra empresa e impulsione o seu negócio.');
            return $this->redirect()->toUrl($urlRetorno);
        }

        if($this->checkUserPaymentNeed($cupomModel)){
            $urlPaymentRequest = "https://ws.pagseguro.uol.com.br/v2/checkout/?email=".$this->emailPagSeguro."&token=".$this->tokenPagSeguro;
            $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
            $urlSystem = $helper->__invoke('/system/index/index/retorno-pagamento/1');

            $data['email'] = $this->emailPagSeguro;
            $data['token'] = $this->tokenPagSeguro;
            $data['currency'] = 'BRL';
            $data['itemId1'] = '0001';
            $data['itemDescription1'] = 'Anuncio Destaque de Empresa';
            $data['itemAmount1'] = '30.00';
            $data['itemQuantity1'] = '1';
            $data['reference'] = "e".bin2hex(openssl_random_pseudo_bytes(99));
            $data['redirectURL'] = $urlSystem;

            $requestData['transaction_code'] = $data['reference'];

            $data = http_build_query($data);
            $curl = curl_init($urlPaymentRequest);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            $xml= curl_exec($curl);
            if($xml == 'Unauthorized'){
                //Insira seu código de prevenção a erros
                $this->messages()->flashError('Ops! Estamos com problemas no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
                return $this->redirect()->toUrl($urlRetorno);
                exit();//Mantenha essa linha
            }

            $xml= simplexml_load_string($xml);

            if(count($xml->error) > 0){
                //Insira seu código de tratamento de erro, talvez seja útil enviar os códigos de erros.
                $this->messages()->flashError('Ops! Ocorreu um erro inesperado no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
                return $this->redirect()->toUrl($urlRetorno);
                exit();
            }

            curl_close($curl);

            $requestData["status"]  = 3;
        } else{
            $requestData["status"]  = 1;
        }

        
        $requestData['created'] = $requestData['reactivated'] = $dataFixa;

        $classificadoEmpresaModel = new ClassificadoEmpresaModel;
        $classificadoEmpresaModel->setData($requestData);
        $empresa = $this->getEntityManager()->find("System\Model\Empresa", $empresa_id);
        $classificadoEmpresaModel->__set("empresa_id", $empresa);

        try{
            $em->persist($classificadoEmpresaModel);
            $em->flush();
            $em->getConnection()->commit();

            $this->messages()->flashSuccess('Empresa impulsionada com sucesso.');
        } catch(\Exception $e){
            $em->getConnection()->rollback();
            $em->close();
            $this->messages()->flashError('Ocorreu um erro ao efetuar a publicação. Por favor tente novamente mais tarde.');
            return $this->redirect()->toUrl($urlRetorno);
        }
        
        if(!$this->checkUserPaymentNeed($cupomModel))
            return $this->redirect()->toUrl($urlRetorno);

        return $this->redirect()->toUrl('https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml->code);
    }

    /**
     * Excluir um classificado
     * @return void
     */
    public function normalDeleteAction()
    {
        $urlRetorno = "/system";
        $request = $this->getRequest();
        if($request->isGet()){
            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id <= 0)
                return $this->redirect()->toUrl($urlRetorno);

            $classificado = $this->getEntityManager()
                                 ->getRepository('System\Model\Classificado')
                                 ->findBy(array('id' => $id, 'usuario_id' => $userSession[0]->id));

            if(count($classificado) <= 0)
                return $this->redirect()->toUrl($urlRetorno);

            try{

                $this->getEntityManager()->remove($classificado[0]);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Classificado excluído com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão do classificado. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Excluir um classificado favoritado por um usuário
     * @return void
     */
    public function normalFavoritoDeleteAction()
    {
        $urlRetorno = "/system";
        $request = $this->getRequest();
        if($request->isGet()){
            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id <= 0)
                return $this->redirect()->toUrl($urlRetorno);

            $classificado = $this->getEntityManager()
                                 ->getRepository('System\Model\Favoritos')
                                 ->findBy(array('classificado_id' => $id, 'usuario_id' => $userSession[0]->id));

            if(count($classificado) <= 0)
                return $this->redirect()->toUrl($urlRetorno);

            try{

                $this->getEntityManager()->remove($classificado[0]);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Classificado favorito excluído com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão do classificado favorito. Por favor tente novamente mais tarde.');
            }
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Excluir um classificado empresarial
     * @return void
     */
    public function empresaDeleteAction()
    {
        $urlRetorno = "/system";
        $request = $this->getRequest();
        if($request->isGet()){
            $session = $this->getServiceLocator()->get('Session');
            $userSession = $session->offsetGet('user');

            $id = (int) $this->params()->fromRoute('id', 0);
            if ($id <= 0)
                return $this->redirect()->toUrl($urlRetorno);

            $classificado = $this->getEntityManager()->find('System\Model\ClassificadoEmpresa', $id);

            if(!$classificado)
                return $this->redirect()->toUrl($urlRetorno);

            try{

                $this->getEntityManager()->remove($classificado);
                $this->getEntityManager()->flush();
                $this->messages()->flashSuccess('Classificado empresarial excluído com sucesso!');
            } catch(\Exception $e){
                $this->messages()->flashError('Ocorreu um erro ao efetuar a exclusão do classificado empresarial. Por favor tente novamente mais tarde.');
            }
        } else{
            $this->messages()->flashError('Pobremas');   
        }

        return $this->redirect()->toUrl($urlRetorno);
    }

    /**
     * Excluir um classificado
     * @return void
     */
    public function destacarAnuncioNormalAction()
    {
        $urlRetorno = "/system";
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id <= 0)
            return $this->redirect()->toUrl($urlRetorno);

        $classificado = $this->getEntityManager()
                             ->getRepository('System\Model\Classificado')
                             ->findBy(array('id' => $id, 'usuario_id' => $userSession[0]->id));
        if(!$classificado)
            return $this->redirect()->toUrl($urlRetorno);

        $email = $this->emailPagSeguro;
        $token = $this->tokenPagSeguro;

        $urlPaymentRequest = "https://ws.pagseguro.uol.com.br/v2/checkout/?email=".$email."&token=".$token;
        $helper = $this->getServiceLocator()->get('ViewHelperManager')->get('ServerUrl');
        $urlSystem = $helper->__invoke('/system/index/index/retorno-pagamento/1');

        $data['email']            = $email;
        $data['token']            = $token;
        $data['currency']         = 'BRL';
        $data['itemId1']          = '0001';
        $data['itemDescription1'] = 'Anuncio Destaque de Produto ou Servico';
        $data['itemAmount1']      = '15.00';
        $data['itemQuantity1']    = '1';
        $data['reference']        = "c".bin2hex(openssl_random_pseudo_bytes(99));
        $data['redirectURL']      = $urlSystem;

        $requestData['transaction_code'] = $data['reference'];
        $reference = $data['reference'];

        $data = http_build_query($data);
        $curl = curl_init($urlPaymentRequest);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $xml= curl_exec($curl);
        if($xml == 'Unauthorized'){
            //Insira seu código de prevenção a erros

            $this->messages()->flashError('Ops! Estamos com problemas no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
            return $this->redirect()->toUrl($urlRetorno);
            exit;//Mantenha essa linha
        }

        $xml= simplexml_load_string($xml);

        if(count($xml->error) > 0){
            //Insira seu código de tratamento de erro, talvez seja útil enviar os códigos de erros.

            $this->messages()->flashError('Ops! Ocorreu um erro inesperado no meio de pagamento. Este problema já foi avisado a equipe Tem Pra Festa e no máximo em 24h você vai poder utilizar o anúncio destaque. Aguarde!');
            return $this->redirect()->toUrl($urlRetorno);
            exit;
        }

        curl_close($curl);

        try{

            $classificado = $classificado[0];
            $classificado->__set('status', 3);
            $classificado->__set('tipo', 1);
            $classificado->__set('transaction_code', $reference);
            $this->getEntityManager()->persist($classificado);
            $this->getEntityManager()->flush();
        } catch(\Exception $e){
            return $this->redirect()->toUrl($urlRetorno);
            exit();
        }

        return $this->redirect()->toUrl('https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml->code);
    }

    /**
     * Recebe notificações do pagseguro para anúncios destaque
     * @return void
     */
    public function retornoPagamentoDestaqueAction()
    {
        if(isset($_POST['notificationType']) && $_POST['notificationType'] == 'transaction'){
            $email = $this->emailPagSeguro;
            $token = $this->tokenPagSeguro;
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $_POST['notificationCode'] . '?email=' . $email . '&token=' . $token;

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $transaction= curl_exec($curl);
            curl_close($curl);

            if($transaction == 'Unauthorized'){
                //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção

                exit;//Mantenha essa linha
            }
            $transaction = simplexml_load_string($transaction);
            $prefixoReference = substr($transaction->reference, 0, 1);

            if($prefixoReference == "c"){
                $classificadoModel = $this->getEntityManager()
                                          ->getRepository('System\Model\Classificado')
                                          ->findBy(array("transaction_code" => $transaction->reference));

                if(count($classificadoModel) > 0){
                    if($transaction->status == 3){
                        try{
                            $classificadoModel = $classificadoModel[0];
                            $date = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
                            $classificadoModel->__set('reactivated', $date);
                            $classificadoModel->__set('status', 1);
                            $this->getEntityManager()->persist($classificadoModel);
                            $this->getEntityManager()->flush();
                        } catch(\Exception $e){
                            exit();
                        }
                    }
                }
            }
            else if($prefixoReference == "e"){
                $classificadoEmpresaModel = $this->getEntityManager()
                                          ->getRepository('System\Model\ClassificadoEmpresa')
                                          ->findBy(array("transaction_code" => $transaction->reference));

                if(count($classificadoEmpresaModel) > 0){
                    if($transaction->status == 3){
                        try{
                            $classificadoEmpresaModel = $classificadoEmpresaModel[0];
                            $date = new \DateTime("now", new \DateTimeZone('America/Sao_Paulo'));
                            $classificadoEmpresaModel->__set('reactivated', $date);
                            $classificadoEmpresaModel->__set('status', 1);
                            $this->getEntityManager()->persist($classificadoEmpresaModel);
                            $this->getEntityManager()->flush();
                        } catch(\Exception $e){
                            exit();
                        }
                    }
                }
            }

            /*$name = '/var/www/webroot/ROOT/files/arquivo.txt';
            $text = var_export($transaction, true);
            $file = fopen($name, 'a');
            fwrite($file, $text);
            fclose($file);*/
        }

        exit();
    }

    /**
     * Constrói um array de cidades de acordo com estado passado
     * @param int $estado_id
     * @return Array
     */
    private function getOptionsCity($estado_id)
    {
        $cidades = $this->getEntityManager()
                        ->getRepository("System\Model\Cidades")
                        ->findBy(array("estado_id" => $estado_id), array("nome" => "ASC"));
        $options = array();
        foreach ($cidades as $cidade) {
            $options[$cidade->id] = $cidade->nome;
        }

        return $options;
    }

    /**
     * Constrói um array de subcategorias de acordo com a categoria passada
     * @param int $categoria_id
     * @return Array
     */
    private function getOptionsSubcategory($categoria_id)
    {
        $subcategorias = $this->getEntityManager()
                        ->getRepository("System\Model\Subcategoria")
                        ->findBy(array("categoria_id" => $categoria_id), array("nome" => "ASC"));
        $options = array();
        foreach ($subcategorias as $subcategoria) {
            $options[$subcategoria->id] = $subcategoria->nome;
        }

        return $options;
    }

    /**
     * Salva na sessão os dados do usuário
     * @return void
     */
    private function saveUserSession()
    {
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $usuarioPerfil = $this->getEntityManager()
                              ->getRepository("System\Model\User")
                              ->createQueryBuilder("u");
        $usuarioPerfil->select(array("u", "p"))
                      ->leftJoin('System\Model\Perfil', 'p', "WITH", 'u.id = p.usuario_id')
                      ->where("u.id = :usuario_id");;
        $usuarioPerfil->setParameter("usuario_id", $userSession[0]->id);
        $usuarioPerfilObject = $usuarioPerfil->getQuery()->getResult();

        $type = gettype($usuarioPerfilObject[1]->file_memory);
        if($type == "resource")
            $usuarioPerfilObject[1]->file_memory = base64_encode(stream_get_contents($usuarioPerfilObject[1]->file_memory));

        else
            $usuarioPerfilObject[1]->file_memory = base64_encode($usuarioPerfilObject[1]->file_memory);

        //salva o user na sessão    
        $session->offsetSet('user', $usuarioPerfilObject);
    }

    private function checkUserPaymentNeed($cupomModel = null){
        $session = $this->getServiceLocator()->get('Session');
        $userSession = $session->offsetGet('user');

        $perfil_id = $userSession[1]->id;
        $usersNotNeedPayment     = array(1);

        $retorno = true;

        if(in_array($perfil_id, $usersNotNeedPayment))
            $retorno = false;

        if($cupomModel)
            $retorno = false;

        return $retorno;
    }

    /**
    * Retorno de cidades de um determinado estado
    * @return Zend\Http\Response 
    */
    public function wsAction()
    {
        $request = $this->getRequest();

        if($request->isXMLHttpRequest()){
            $paramFormat = $this->params()->fromRoute('format', 'json');
            $format = ($paramFormat == 'xml') ? 'xml' : 'json';
            $estadoID = (int) $this->params()->fromRoute('e', 0);

            $serializer = new Serializer(
                array(new GetSetMethodNormalizer()),
                array(
                    'xml' => new XmlEncoder(),
                    'json' => new JsonEncoder()
                )
            );

            $fields = array(
                    "c.id",
                    "c.nome",
            );

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->select($fields)
                ->from("System\Model\Cidades", "c");
            if($estadoID > 0){

                $qb->where("c.estado_id = :estado");
                $qb->setParameter("estado", $estadoID);

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            } else{

                $query = $qb->getQuery();

                $cidades = $query->getResult();
            }

            $content = $serializer->serialize($cidades, $format);

            $response = $this->getResponse();
            $response->setStatusCode(200);
            $response->setContent($content);
            $response->getHeaders()->addHeaderLine('Content-Type', "application/$format");
            return $response;
        }
    }
}