<?php

namespace System;

// module/System/conﬁg/module.config.php:
return array(
    'controllers' => array( //add module controllers
        'invokables' => array(
            'System\Controller\Index' => 'System\Controller\IndexController',
            'System\Controller\Auth' => 'System\Controller\AuthController',
            'System\Controller\User' => 'System\Controller\UserController',
            'System\Controller\Profile' => 'System\Controller\ProfileController',
            'System\Controller\Subcategoria' => 'System\Controller\SubcategoriaController',
            'System\Controller\Categoria' => 'System\Controller\CategoriaController',
            'System\Controller\MidiaSocial' => 'System\Controller\MidiaSocialController',
            'System\Controller\Perfil' => 'System\Controller\PerfilController',
            'System\Controller\Classificado' => 'System\Controller\ClassificadoController',
            'System\Controller\Empresa' => 'System\Controller\EmpresaController',
            'System\Controller\Filial' => 'System\Controller\FilialController',
            'System\Controller\Favoritos' => 'System\Controller\FavoritosController',
            'System\Controller\Questionario' => 'System\Controller\QuestionarioController'
        ),
    ),

    'router' => array(
        'routes' => array(
            'system' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/system[/page/:page]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'System\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                        'module'        => 'system'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array( //permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                    
                ),
            ),
        ),
    ),
    //the module can have a specific layout
    'module_layout' => array(
        'System' => 'layout/layout_system.phtml'
    ),
    'view_manager' => array( 
        'template_path_stack' => array(
            'system' => __DIR__ . '/../view',
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'cache' => 'Doctrine\Common\Cache\ArrayCache',
            'paths' => array(__DIR__.'/../src/'.__NAMESPACE__.'/Model')
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'Header' => 'System\View\Helper\Header',
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'Session' => function($sm) {
                return new \Zend\Session\Container('SystemTemPraFesta');
            },
            'System\Service\Auth' => function($sm) {
                $dbAdapter = $sm->get('DbAdapter');
                return new \System\Service\Auth($dbAdapter);
            },
            /*'Cache' => function($sm) {
                $config = $sm->get('Configuration');
                $cache = StorageFactory::factory(
                    array(
                        'adapter' => $config['cache']['adapter'],
                        'plugins' => array(
                            'exception_handler' => array('throw_exceptions' => false),
                            'Serializer'
                        ),
                    )
                );

                return $cache;
            },*/
            'Doctrine\ORM\EntityManager' => function($sm) {
                $config = $sm->get('Configuration');
                
                $doctrineConfig = new \Doctrine\ORM\Configuration();
                $cache = new $config['doctrine']['driver']['cache'];
                $proxy_dir = new $config['doctrine']['configuration']['orm_default']['proxy_dir'];
                $proxy_namespace = new $config['doctrine']['configuration']['orm_default']['proxy_namespace'];

                $doctrineConfig->addCustomStringFunction('date_format', 'Mapado\MysqlDoctrineFunctions\DQL\MysqlDateFormat');
                $doctrineConfig->addCustomStringFunction('if', 'Mapado\MysqlDoctrineFunctions\DQL\MysqlIfElse');
                $doctrineConfig->addCustomStringFunction('if', 'Mapado\MysqlDoctrineFunctions\DQL\MysqlRand');
                $doctrineConfig->setQueryCacheImpl($cache);
                $doctrineConfig->setProxyDir($proxy_dir);
                $doctrineConfig->setProxyNamespace($proxy_namespace);
                $doctrineConfig->setAutoGenerateProxyClasses(true);
                
                $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(
                    new \Doctrine\Common\Annotations\AnnotationReader(),
                    array($config['doctrine']['driver']['paths'])
                );
                $doctrineConfig->setMetadataDriverImpl($driver);
                $doctrineConfig->setMetadataCacheImpl($cache);
                \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
                    getenv('PROJECT_ROOT'). '/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
                );
                $em = \Doctrine\ORM\EntityManager::create(
                    $config['doctrine']['connection'],
                    $doctrineConfig
                );
                return $em;

            },
        )    
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Model' => __NAMESPACE__ . '_driver'
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'hydration_cache' => 'array',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
            ),
        ),
    )
    /*'db' => array( //module can have a specific db configuration
        'driver' => 'PDO_SQLite',
        'dsn' => 'sqlite:' . __DIR__ .'/../data/system.db',
        'driver_options' => array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        )
    )*/
);