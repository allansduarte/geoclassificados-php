var urlRenderAnunciosPagination = "/system/index/anuncios";
var urlRenderAnunciosEmpresariaisPagination = "/system/index/anuncios-empresariais";
var urlRenderAnunciosFavoritosPagination = "/system/index/favoritos";
var urlRenderAnunciosMensagensPagination = "/system/index/mensagens";

var htmlOverlay = "<div class='overlay'></div>";
var htmlLoading = "<div class='loading-img'></div>";
var htmlImgLoader = "<img src='/img/ajax-loader.gif' width='24' height='24'>";

var message 	  = new Message();
var duismagDialog = new Dialog();
var utils   	  = new Utils();

$(document).ready(function(){

	renderAnuncios();
	renderAnunciosEmpresariais();
	renderAnunciosFavoritos();
	renderMensagens();
});

function renderMensagens(){

	var tabAnunciosMensagensElement = $("#tab_mensagem");

	$.ajax({
		url: urlRenderAnunciosMensagensPagination,
		data: "get",
		beforeSend: function(){
	        tabAnunciosMensagensElement.append(htmlOverlay);
	        tabAnunciosMensagensElement.append(htmlLoading);
		},
		success: function(data){
			tabAnunciosMensagensElement.html(data);
		},
		error: function(request, status){
			var message = "";
			message = "Ops! Não conseguimos atualizar o conteúdo referente as suas mensagens. <a class='tryRenderMenssagesPagination' href='javascript:void(0);'>Clique aqui e tente atualizar</a>, ou tente mais tarde.";
			tabAnunciosMensagensElement.html(message);

			$("#tab_anuncios .tryRenderMenssagesPagination").on("click", function(){
				renderMensagens();
			});
        }
	}).done(function(){

		$("#tab_mensagem .paginator a").on("click", function(e){
			e.preventDefault();
			urlRenderAnunciosMensagensPagination = $(this).attr("href");
			renderMensagens();
			utils.scrollToAnchor(tabAnunciosMensagensElement);
		});

		tabAnunciosMensagensElement.siblings(".loading-img").remove();
		tabAnunciosMensagensElement.siblings(".overlay").remove();
		tabAnunciosMensagensElement.removeAttr("style");

		$("input[type='checkbox'], input[type='radio']").iCheck({
	        checkboxClass: 'icheckbox_minimal',
	        radioClass: 'iradio_minimal'
	    });
/*
		//iCheck for checkbox and radio inputs
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
*/
        //When unchecking the checkbox
        $("#check-all").on('ifUnchecked', function(event) {
            //Uncheck all checkboxes
            $("input[type='checkbox']", ".table-mailbox").iCheck("uncheck");
        });
        //When checking the checkbox
        $("#check-all").on('ifChecked', function(event) {
            //Check all checkboxes
            $("input[type='checkbox']", ".table-mailbox").iCheck("check");
        });

        $("#markAsRead").on("click", function()
        {
        	var idLength = $(".table-mailbox input[type='checkbox']:checked").length;
        	if(idLength <= 0)
        		return true;

	        var id = $(".table-mailbox input[type='checkbox']:checked").serialize();
        	_messageAction(id, 1);
        });

        $("#markAsUnread").on("click", function()
        {
        	var idLength = $(".table-mailbox input[type='checkbox']:checked").length;
        	if(idLength <= 0)
        		return true;

        	var id = $(".table-mailbox input[type='checkbox']:checked").serialize();
        	_messageAction(id, 2);
        });

        $("#deleteMessage").on("click", function()
        {
        	var idLength = $(".table-mailbox input[type='checkbox']:checked").length;
        	if(idLength <= 0)
        		return true;

        	var id = $(".table-mailbox input[type='checkbox']:checked").serialize();
        	_messageAction(id, 3);
        });

        $("#tab_mensagem .table-mailbox tr a").on("click", function(){
        	var fromEmail = $(this).parent().siblings('.fromEmail').text();
        	var dsMessage = $(this).parent().siblings('.message').text();
        	var dsTitle = "Mensagem";
        	var dsHTML = "";
        	dsHTML    += '<div class="row">';
        	dsHTML    += 	'<div class="col-xs-12">';
    		dsHTML    += 		'<div class="form-group">';
    		dsHTML    += 			'<label>Enviado por:</label>';
          	dsHTML	  += 			'<p><a href="mailto:'+fromEmail+'">'+fromEmail+'</a></p>';
  			dsHTML    +=		'</div>';
  			dsHTML    += 	'</div>';
  			dsHTML    += 	'<div class="col-xs-12">';
    		dsHTML    += 		'<div class="form-group">';
    		dsHTML    += 			'<label>Mensagem</label>';
          	dsHTML	  += 			'<p>'+dsMessage+'</p>';
  			dsHTML    +=		'</div>';
  			dsHTML    += 	'</div>';
  			dsHTML    += '</div>';
        	
        	BootstrapDialog.show({
		        title: dsTitle,
		        message: dsHTML,
		        buttons: [{
			            label: 'Responder E-mail',
			            action: function(dialogRef){
			                window.location = "mailto:"+fromEmail;
			            },
			            cssClass: 'btn-success'
			        },{
		            label: 'Fechar',
		            action: function(dialogRef){
		                dialogRef.close();
		            },
		            cssClass: 'btn-danger'
		        },]
		    });

        	if($(this).parent().parent().hasClass('unread'))
        	{
        		$(this).parent().parent().removeClass();
		    	var id = "message="+$(this).parent().siblings(".small-col").find("input[type='checkbox']").val();
		    	_messageAction(id, 1, true);//terceira variavel impede a requisição ajax
        	}
        });
	});

	function _messageAction(id, action, invisible){
		invisible = (invisible === undefined ? false : invisible);
		var tabAnunciosMensagensElement = $("#tab_mensagem");
		var url = urlRenderAnunciosMensagensPagination;
		url += "?"+id;
		url += "&action="+action;

		$.ajax({
			url: url,
			data: "get",
			beforeSend: function(){
				if(!invisible){
		        	tabAnunciosMensagensElement.append(htmlOverlay);
		        	tabAnunciosMensagensElement.append(htmlLoading);
		        }
			},
			success: function(data){
				if(!invisible)
					renderMensagens();
			},
			error: function(request, status)
			{
				if(!invisible){
					tabAnunciosMensagensElement.siblings(".loading-img").remove();
					tabAnunciosMensagensElement.siblings(".overlay").remove();
					tabAnunciosMensagensElement.removeAttr("style");
				}
	        }
		}).done(function(){
			if(!invisible){
				tabAnunciosMensagensElement.siblings(".loading-img").remove();
				tabAnunciosMensagensElement.siblings(".overlay").remove();
				tabAnunciosMensagensElement.removeAttr("style");
			}
		});
	}
}

function renderAnunciosFavoritos(){

	var tabAnunciosFavoritosElement = $("#tab_favoritos");

	$.ajax({
		url: urlRenderAnunciosFavoritosPagination,
		data: "get",
		beforeSend: function(){
			console.log(htmlLoading);
	        tabAnunciosFavoritosElement.append(htmlOverlay);
	        tabAnunciosFavoritosElement.append(htmlLoading);
		},
		success: function(data){
			tabAnunciosFavoritosElement.html(data);
		},
		error: function(request, status){
			var message = "";
			message = "Ops! Não conseguimos atualizar o conteúdo referente aos seus anúncios favoritos. <a class='tryRenderAnunciosFavoritosPagination' href='javascript:void(0);'>Clique aqui e tente atualizar</a>, ou tente mais tarde.";
			tabAnunciosFavoritosElement.html(message);

			$("#tab_anuncios .tryRenderAnunciosFavoritosPagination").on("click", function(){
				renderAnunciosFavoritos();
			});
        }
	}).done(function(){

		$("#tab_favoritos .paginator a").on("click", function(e){
			e.preventDefault();
			urlRenderAnunciosPagination = $(this).attr("href");
			renderAnunciosFavoritos();
			utils.scrollToAnchor(tabAnunciosFavoritosElement);
		});

		tabAnunciosFavoritosElement.siblings(".loading-img").remove();
		tabAnunciosFavoritosElement.siblings(".overlay").remove();
		tabAnunciosFavoritosElement.removeAttr("style");
	});
}

function renderAnuncios(){

	console.log("ola");
	var tabAnunciosElement = $("#tab_anuncios");

	$.ajax({
		url: urlRenderAnunciosPagination,
		data: "get",
		beforeSend: function(){
	        tabAnunciosElement.append(htmlOverlay);
	        tabAnunciosElement.append(htmlLoading);
		},
		success: function(data){
			tabAnunciosElement.html(data);
		},
		error: function(request, status){
			var message = "";
			message = "Ops! Não conseguimos atualizar o conteúdo referente aos seus anúncios. <a class='tryRenderAnunciosPagination' href='javascript:void(0);'>Clique aqui e tente atualizar</a>, ou tente mais tarde.";
			tabAnunciosElement.html(message);

			$("#tab_anuncios .tryRenderAnunciosPagination").on("click", function(){
				renderAnuncios();
			});
        }
	}).done(function(){

		//$("[data-toggle='tooltip']").tooltip();
		$("#tab_anuncios .paginator a").on("click", function(e){
			e.preventDefault();
			urlRenderAnunciosPagination = $(this).attr("href");
			renderAnuncios();
			utils.scrollToAnchor(tabAnunciosElement);
		});

		$("#tab_anuncios a.delete").on("click", function(e){
			e.preventDefault();
			var url = $(this).attr("href");

			var dsTitle   = "Confirmação!";
			var dsMessage = "Os dados do seu classificado serão excluídos! Tem certeza que deseja proseguir?";

			BootstrapDialog.show({
		        title: dsTitle,
		        message: dsMessage,
		        buttons: [{
		            label: 'Cancelar',
		            action: function(dialogRef){
		                dialogRef.close();
		            },
		        },
		        {
		            label: 'OK',
		            action: function(dialogRef){
		                window.location = url;
		            },
		            cssClass: 'btn-primary'
		        }]
		    });
		});

		$("#tab_anuncios a.destacar").on("click", function(e){
			e.preventDefault();
			var url = $(this).attr("href");

			var dsTitle   = "Confirmação!";
			var dsMessage = "Para utilizar o serviço destaque de anúncio do Tem Pra Festa, você será redirecionado ao Pagseguro para o pagamento de R$ 15,00.";

			BootstrapDialog.show({
		        title: dsTitle,
		        message: dsMessage,
		        buttons: [{
		            label: 'Cancelar',
		            action: function(dialogRef){
		                dialogRef.close();
		            },
		        },
		        {
		            label: 'Prosseguir',
		            action: function(dialogRef){
		                window.location = url;
		            },
		            cssClass: 'btn-primary'
		        }]
		    });
		});

		tabAnunciosElement.siblings(".loading-img").remove();
		tabAnunciosElement.siblings(".overlay").remove();
		tabAnunciosElement.removeAttr("style");
	});
}

function renderAnunciosEmpresariais(){

	var tabAnunciosEmpresariaisElement = $("#tab_anuncios_empresariais");

	$.ajax({
		url: urlRenderAnunciosEmpresariaisPagination,
		data: "get",
		beforeSend: function(){
	        tabAnunciosEmpresariaisElement.append(htmlOverlay);
	        tabAnunciosEmpresariaisElement.append(htmlLoading);
		},
		success: function(data){
			tabAnunciosEmpresariaisElement.html(data);
		},
		error: function(request, status){
			var message = "";
			message = "Ops! Não conseguimos atualizar o conteúdo referente aos seus anúncios empresariais. <a class='tryRenderAnunciosEmpresariaisPagination' href='javascript:void(0);'>Clique aqui e tente atualizar</a>, ou tente mais tarde.";
			tabAnunciosEmpresariaisElement.html(message);

			$("#tab_anuncios_empresariais .tryRenderAnunciosEmpresariaisPagination").on("click", function(){
				renderAnunciosEmpresariais();
			});
        }
	}).done(function(){

		//$("[data-toggle='tooltip']").tooltip();
		$("#tab_anuncios_empresariais .paginator a").on("click", function(e){
			e.preventDefault();
			urlRenderAnunciosEmpresariaisPagination = $(this).attr("href");
			renderAnunciosEmpresariais();
			utils.scrollToAnchor(tabAnunciosEmpresariaisElement);
		});

		$("#tab_anuncios_empresariais a.delete").on("click", function(e){
			e.preventDefault();
			var url = $(this).attr("href");

			var dsTitle   = "Confirmação!";
			var dsMessage = "Os dados do seu classificado serão excluídos! Tem certeza que deseja proseguir?";

			BootstrapDialog.show({
		        title: dsTitle,
		        message: dsMessage,
		        buttons: [{
		            label: 'Cancelar',
		            action: function(dialogRef){
		                dialogRef.close();
		            },
		        },
		        {
		            label: 'OK',
		            action: function(dialogRef){
		                window.location = url;
		            },
		            cssClass: 'btn-primary'
		        }]
		    });
		});

		tabAnunciosEmpresariaisElement.siblings(".loading-img").remove();
		tabAnunciosEmpresariaisElement.siblings(".overlay").remove();
		tabAnunciosEmpresariaisElement.removeAttr("style");
	});
}