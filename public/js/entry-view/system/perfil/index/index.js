var loader = "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>";
var dialog  = new Dialog();
var message = new Message();
var utils = new Utils();

$(document).ready(function(){

	$("center").on({
		mouseenter: function(){
			$(this).children("em").show();
			$(this).css("backgroundColor", "#F4F4F4");
		},
		mouseleave: function(){
			$(this).children("em").hide();
			$(this).removeAttr("style");
		}
	});

	$("center a").on("click", function(){
		var idp = $(this).attr('data-perfil');
		carregaFormularioPerfil(idp);
	});


	$("form#alterarSenha").validate({
		rules: {
			senhaAtual: "required",
			novaSenha: "required",
		},
		messages: {
			senhaAtual: "Por favor, informe o campo Senha atual.",
			novaSenha: "Por favor, informe o campo Nova senha.",
		}
	});
});

function carregaFormularioPerfil(idp){

	$elTabPerfil = $("#tab_perfil");
	var htmlLoading = "<center>"+loader+"</center>";

	$.ajax({
		url: "/system/perfil/alterar-perfil/p/"+idp,
		data: "get",
		beforeSend: function(){
	        $elTabPerfil.html(htmlLoading);
		},
		success: function(formPerfil){
			$elTabPerfil.html(formPerfil);
		},
		error: function(request, status){
			var message = "";
			message = "<i class='glyphicon glyphicon-refresh'></i>  Ops! Não conseguimos atualizar o conteúdo da sua página. <a class='tryRenderFormPerfil' href='javascript:void(0);'>Clique aqui e tente novamente</a>, ou tente mais tarde.";
			$elTabPerfil.html(message);

			$("#tab_perfil .tryRenderFormPerfil").on("click", function(){
				carregaFormularioPerfil();
			});
        }
	}).done(function(){

		$("form#perfil").validate({
			rules: {
				nome: "required",
				estado_id: "required",
				cidade_id: "required",
			},
			messages: {
				nome: "Por favor, informe um valor para o campo Nome.",
				estado_id: "Por favor, selecione um Estado.",
				cidade_id: "Por favor, selecione uma Cidade.",
			}
		});

		$("#estados select").change(function(){
			var eid = $("#estados select option:selected").val();
			carregarComboboxCidade(eid);
		});

		$("#file_memory").change(function(e){
			var handleFileSelected = new HandleFileSelected();
			handleFileSelected.setFileType('image.*');
			handleFileSelected.setMessageFileType(message.getValidationMessage(6));
			handleFileSelected.setOutputType('default');
			handleFileSelected.run(e, $(this), 'img-circle imagem_perfil');
		});
	});
}

function carregarComboboxCidade(eid){
	var format = "json";

	if(isNaN(parseInt(eid))){
		$("#cidades select").html("<option value=''>--Escolha o estado--</option>");
		return false;
	}

	$.getJSON(
		"/api/cidades/"+"estado/id/"+eid+"/format/"+format, 
		function(cidades){

			if(utils.isEmpty(cidades)){

				error = true;
				//submitButton.addClass("disabled");

		        var titulo 		= message.getErrorMessage(4);
		        var mensagem 	= 'Esse problema geralmente ocorre quando a categoria vinculada ao aplicativo foi depreciada ou excluída pelo sistema. Alterar a categoria do aplicativo vai resolver o problema.';
		        var labelButton = 'Alterar a categoria do aplicativo';
		        var urlButton   = "/system/perfil";
		        dialog.showDBRegisterFail(titulo, mensagem, labelButton, urlButton);
	        } else{
	        	var htmlSelect = "";
	        	for(var obj in cidades){
	        		htmlSelect += "<option value="+cidades[obj].id+">"+cidades[obj].nome+"</option>";
	        	}

	        	$("#cidades select").html(htmlSelect);
	        }
		}
	)
	.fail(function(){

		//submitButton.addClass("disabled");
        
        /*var titulo = message.getErrorMessage(4);
        var mensagem = message.getInfoMessage(2);
        dialog.showError(titulo, mensagem);*/
	})
	.done(function(){

	});
}