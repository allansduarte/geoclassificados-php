$(document).ready(function(){

	$("[data-modal]").click(function(){
		var dsTitle   = $(this).parent().parent().siblings('.nome').html();
		var dsMessage = getMessage($(this));
		BootstrapDialog.show({
	        title: dsTitle,
	        message: dsMessage,
	        buttons: [{
	            label: 'Fechar',
	            action: function(dialogRef){
	                dialogRef.close();
	            },
	            cssClass: 'btn-danger'
	        },]
	    });
	});


	$("a.delete").on("click", function(e){
		e.preventDefault();
		var url = $(this).attr("href");

		var dsTitle   = "Confirmação!";
		var dsMessage = "Os dados da sua empresa e as filiais vinculadas a esta empresa serão desativados! Tem certeza que deseja proseguir?";

		BootstrapDialog.show({
	        title: dsTitle,
	        message: dsMessage,
	        buttons: [{
	            label: 'Cancelar',
	            action: function(dialogRef){
	                dialogRef.close();
	            },
	        },
	        {
	            label: 'OK',
	            action: function(dialogRef){
	                window.location = url;
	            },
	            cssClass: 'btn-primary'
	        }]
	    });
	});

});

function getMessage(clickRef){
	var html = "";

	var elTr = clickRef.parent().parent();
	var image = elTr.siblings('.img').html();
	var nome = elTr.siblings('.nome').html();
	var descricao = elTr.siblings('.descricao').html();
	var status = elTr.siblings('.status').html();

	var dataEmpresa = elTr.siblings(".data-empresa");
	var cidade = dataEmpresa.children(".cidade").html();
	var estado = dataEmpresa.children(".estado").html();
	var enderecoRua = dataEmpresa.children(".rua").html();
	var enderecoNumero = dataEmpresa.children(".numero").html();
	var enderecoBairro = dataEmpresa.children(".bairro").html();
	var contatoEmail = dataEmpresa.children(".email").html();
	contatoEmail = (contatoEmail == "" ? "não informado" : contatoEmail);
	var contatoTelefone = dataEmpresa.children(".telefone").html();
	contatoTelefone = (contatoTelefone == "" ? "não informado" : contatoTelefone);
	var contatoCelular = dataEmpresa.children(".celular").html();
	contatoCelular = (contatoCelular == "" ? "não informado" : contatoCelular);



	html += "<div class='col-xs-12 marginBottom15'>";
	html += 	"<div class='col-md-3'>";
	html += 		image;
	html += 	"</div>";
	html += 	"<div class='col-md-9'>";
	html += 		"<h3>"+nome+"</h3>";
	html += 		"<p>"+descricao+"</p>";
	html += 	"</div>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-3'>";
	html += 		"<p><a href='javascript:void(0);' class='btn btn-success btn-sm'>"+status+"</a></p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-9'>";
	html += 		"<p>"+cidade+" - "+estado+"</p>";
	html += 	"</div>";
	html += "</div>";

	html += "<div class='col-xs-12'>";
	html += 	"<h4>Dados de endereço</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Rua: "+enderecoRua+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Número: "+enderecoNumero+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Bairro: "+enderecoBairro+"</p>";
	html += 	"</div>";
	html += "</div>";

	html += "<div class='col-xs-12'>";
	html += 	"<h4>Dados de contato</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>E-mail: "+contatoEmail+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Telefone: "+contatoTelefone+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Celular: "+contatoCelular+"</p>";
	html += 	"</div>";
	html += "</div>";

	/*html += "<div class='col-xs-12'>";
	html += 	"<h4>Redes sociais</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += "</div>";*/

	html+= "<div class='clear'></div>";

	return html;
}