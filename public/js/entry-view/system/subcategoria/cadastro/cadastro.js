var form;

$(document).ready(function(){
	form = $("form#subcategoria");

	form.validate({
		rules: {
			nome: "required",
			categoria_id: "required",
		},
		messages: {
			nome: "Por favor, informe o da categoria.",
			categoria_id: "Por favor, escolha uma categoria.",
		}
	});

	form.submit(function(){
		if(form.valid()){
			$("#submitCategoria").attr("disabled", true).val("Cadastrando...");
		}
	});
});