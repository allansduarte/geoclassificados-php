var form;

$(document).ready(function(){
	form = $("form#categoria");

	form.validate({
		//debug: true,
		rules: {
			nome: "required",
		},
		messages: {
			nome: "Por favor, informe o da categoria.",
		}
	});

	form.submit(function(){
		if(form.valid()){
			$("#submitCategoria").attr("disabled", true).val("Cadastrando...");
		}
	});
});