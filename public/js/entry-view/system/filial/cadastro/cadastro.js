var form = $("form#filiais");
var utils = new Utils();
var dialog  = new Dialog();
var message = new Message();

$(document).ready(function(){

	$("#categoria select").change(function(){
		var cid = $("#categoria select option:selected").val();
		carregarComboboxCategoria(cid);
	});

	form.validate({
		rules: {
			empresa_id: "required",
			nome: "required",
			estado_id: "required",
			cidade_id: "required",
			descricao: "required",
			rua: "required",
			numero: "required",
			bairro: "required",
		},
		messages: {
			empresa_id: "Por favor, selecione uma empresa.",
			nome: "Por favor, informe o campo nome.",
			estado_id: "Por favor, selecione um estado.",
			cidade_id: "Por favor, selecione uma cidade.",
			descricao: "Por favor, informe o campo descrição.",
			rua: "Por favor, informe o campo rua.",
			numero: "Por favor, informe o campo número.",
			bairro: "Por favor, informe o campo bairro.",
		}
	});

	$("#estados select").change(function(){
		var eid = $("#estados select option:selected").val();
		carregarComboboxCidade(eid);
	});

	form.submit(function(){
		if(form.valid()){
			$("#submitFilial").attr("disabled", true).val("Cadastrando...");
		}
	});
});

function carregarComboboxCidade(eid){

	if(isNaN(parseInt(eid))){
		$("#cidades select").html("<option value=''>--Escolha o estado--</option>");
		return false;
	}

	$.getJSON(
		"/api/cidades/estado/id/"+eid, 
		function(cidades){

			if(utils.isEmpty(cidades)){

				error = true;
				//submitButton.addClass("disabled");

		        var titulo 		= message.getErrorMessage(4);
		        var mensagem 	= 'Esse problema geralmente ocorre quando a categoria vinculada ao aplicativo foi depreciada ou excluída pelo sistema. Alterar a categoria do aplicativo vai resolver o problema.';
		        var labelButton = 'Alterar a categoria do aplicativo';
		        var urlButton   = "/system/perfil";
		        dialog.showDBRegisterFail(titulo, mensagem, labelButton, urlButton);
	        } else{
	        	var htmlSelect = "";
	        	for(var obj in cidades){
	        		htmlSelect += "<option value="+cidades[obj].id+">"+cidades[obj].nome+"</option>";
	        	}

	        	$("#cidades select").html(htmlSelect);
	        }
		}
	)
	.fail(function(){

		//submitButton.addClass("disabled");
        
        /*var titulo = message.getErrorMessage(4);
        var mensagem = message.getInfoMessage(2);
        dialog.showError(titulo, mensagem);*/
	})
	.done(function(){

	});
}