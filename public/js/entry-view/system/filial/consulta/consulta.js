$(document).ready(function(){

	$("[data-modal]").click(function(){
		var dsTitle   = $(this).parent().parent().siblings('.nome').html();
		var dsMessage = getMessage($(this));
		BootstrapDialog.show({
	        title: dsTitle,
	        message: dsMessage,
	        buttons: [{
	            label: 'Fechar',
	            action: function(dialogRef){
	                dialogRef.close();
	            },
	            cssClass: 'btn-danger'
	        },]
	    });
	});
});

function getMessage(clickRef){
	var html = "";

	var elTr = clickRef.parent().parent();
	var nome = elTr.siblings('.nome').html();
	var descricao = elTr.siblings('.descricao').html();
	var status = elTr.siblings('.status').html();

	var dataFilial = elTr.siblings(".data-filial");
	var cidade = dataFilial.children(".cidade").html();
	var estado = dataFilial.children(".estado").html();
	var enderecoRua = dataFilial.children(".rua").html();
	var enderecoNumero = dataFilial.children(".numero").html();
	var enderecoBairro = dataFilial.children(".bairro").html();
	var contatoEmail = dataFilial.children(".email").html();
	contatoEmail = (contatoEmail == "" ? "não informado" : contatoEmail);
	var contatoTelefone = dataFilial.children(".telefone").html();
	contatoTelefone = (contatoTelefone == "" ? "não informado" : contatoTelefone);
	var contatoCelular = dataFilial.children(".celular").html();
	contatoCelular = (contatoCelular == "" ? "não informado" : contatoCelular);

	html += "<div class='col-xs-12 marginBottom15'>";
	html += 	"<h3>"+nome+"</h3>";
	html += 	"<p>"+descricao+"</p>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-3'>";
	html += 		"<p><a href='javascript:void(0);' class='btn btn-success btn-sm'>"+status+"</a></p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-9'>";
	html += 		"<p>"+cidade+" - "+estado+"</p>";
	html += 	"</div>";
	html += "</div>";

	html += "<div class='col-xs-12'>";
	html += 	"<h4>Dados de endereço</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Rua: "+enderecoRua+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Número: "+enderecoNumero+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Bairro: "+enderecoBairro+"</p>";
	html += 	"</div>";
	html += "</div>";

	html += "<div class='col-xs-12'>";
	html += 	"<h4>Dados de contato</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>E-mail: "+contatoEmail+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Telefone: "+contatoTelefone+"</p>";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<p>Celular: "+contatoCelular+"</p>";
	html += 	"</div>";
	html += "</div>";

	/*html += "<div class='col-xs-12'>";
	html += 	"<h4>Redes sociais</h4>";
	html += "</div>";
	html += "<div class='col-xs-12'>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += 	"<div class='col-xs-4'>";
	html += 		"<img src='' />";
	html += 	"</div>";
	html += "</div>";*/

	html+= "<div class='clear'></div>";

	return html;
}c