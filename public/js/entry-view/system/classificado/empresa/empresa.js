var utils = new Utils();
var dialog  = new Dialog();
var message = new Message();

var form = $("form#classificadoEmpresa");


$(document).ready(function(){

	form.validate({
		rules: {
			empresa_id: "required",
		},
		messages: {
			empresa_id: "Por favor, selecione a empresa que deseja divulgar.",
		}
	});

	form.submit(function(){
		if(form.valid()){
			$("#submitEmpresa").attr("disabled", true).val("Divulgando...");
		}
	});
});