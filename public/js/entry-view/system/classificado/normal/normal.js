var utils = new Utils();
var dialog  = new Dialog();
var message = new Message();

var imagem0 = $("#imagem0");
var imagem1 = $("#imagem1");
var imagem2 = $("#imagem2");
var imagem3 = $("#imagem3");
var imagem4 = $("#imagem4");
var imagem5 = $("#imagem5");
var imagem6 = $("#imagem6");

var form = $("form#classificado");

var inputLocal = $("#local");

$(document).ready(function(){

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "5000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	$('input[data-role=tagsinput]').tagsinput({
	  maxTags: 5,
	  maxChars: 100,
	  trimValue: true,
	  /*typeaheadjs: {
	  	source: function(query){
	  		console.log(query);
	  		return ['Amsterdam', 'Washington', 'Sydney', 'Beijing', 'Cairo']
	  	}
	  }*/
	});

	google.maps.event.addDomListener(window, 'load', initializeGMaps);

	$("#valor").priceFormat({
		prefix: '',
		limit: 12,
		centsSeparator: ',',
    	clearPrefix: false,
    	allowNegative: false
	});

	$("#categoria select").change(function(){
		var cid = $("#categoria select option:selected").val();
		carregarComboboxCategoria(cid);
	});

	$("#estados select").change(function(){
		var eid = $("#estados select option:selected").val();
		carregarComboboxCidade(eid);
	});

	form.validate({
		ignore: ":hidden:not(textarea)",
		rules: {
			categoria: "required",
			subcategoria_id: "required",
			titulo: "required",
			descricao: "required",
			valor: "required",
			email: "required",
			estado_id: "required",
			cidade_id: "required",
		},
		messages: {
			categoria: "Por favor, selecione uma Categoria.",
			subcategoria_id: "Por favor, selecione uma Subcategoria.",
			titulo: "Por favor, informe o campo Título.",
			descricao: "Por favor, informe o campo Descrição.",
			valor: "Por favor, informe o campo Valor.",
			email: "Por favor, informe o campo E-mail.",
			estado_id: "Por favor, selecione um Estado.",
			cidade_id: "Por favor, selecione uma Cidade.",
		},
		//errorElement: "label",
        //wrapper: "div",
        errorPlacement: function(error, element)
        {
            if(element.siblings(".input-group-addon").length){
            	console.log("if");
            	element = element.parent();
            }
            
            error.insertAfter(element);
        }
	});

	definirManipulacaoArquivo();

	$('form#classificado #descricao').wysihtml5({
		"image": false,
		"color": false,
		"html": true,
	});

	form.submit(function(){
		if(form.valid()){
			if(imagem0.val() == "" && $("#classificadoID").val() == ""){
				toastr['warning']("Por favor, informe a foto da capa.");
				return false;
			}

			if($("form#classificado #descricao").val() == ""){
				toastr['warning']("Por favor, informe a descrição do seu anúncio.");
				utils.scrollToAnchor($("form#classificado #descricao").parent());
				return false;
			}

			else{
				$("#submitClassificado").attr("disabled", true).val("Cadastrando...");
			}
		}
	});
});

function definirManipulacaoArquivo(){

	imagem0.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem0.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_capa');
	});

	imagem1.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem1.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});

	imagem2.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem2.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});

	imagem3.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem3.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});

	imagem4.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem4.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});
	
	imagem5.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem5.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});

	imagem6.change(function(e){
		var handleFileSelected = new HandleFileSelected();
		handleFileSelected.setFileType('image.*');
		handleFileSelected.setMessageFileType(message.getValidationMessage(6));
		handleFileSelected.setOutputType('default');
		handleFileSelected.setOutputElement(imagem6.siblings("label").children("output"));
		handleFileSelected.run(e, $(this), 'imagem_nCapa');
	});
}

function carregarComboboxCategoria(cid){

	if(isNaN(parseInt(cid))){
		$("#subcategoria select").html("<option value=''>--Escolha a categoria--</option>");
		return false;
	}

	$.getJSON(
		"/api/subcategorias/combobox/id/"+cid, 
		function(subcategorias){

			if(utils.isEmpty(subcategorias)){

				error = true;
				//submitButton.addClass("disabled");

		        /*var titulo 		= message.getErrorMessage(4);
		        var mensagem 	= 'Esse problema geralmente ocorre quando a subcategoria vinculada ao aplicativo foi depreciada ou excluída pelo sistema. Atualizar a página atual vai resolver o problema.';
		        var labelButton = 'Atualizar página';
		        var urlButton   = "/system/classificado/normal";
		        dialog.showDBRegisterFail(titulo, mensagem, labelButton, urlButton);*/
	        } else{
	        	var htmlSelect = "";
	        	for(var obj in subcategorias){
	        		htmlSelect += "<option value="+subcategorias[obj].id+">"+subcategorias[obj].nome+"</option>";
	        	}

	        	$("#subcategoria select").html(htmlSelect);
	        }
		}
	)
	.fail(function(){

		//submitButton.addClass("disabled");
        
        /*var titulo = message.getErrorMessage(4);
        var mensagem = message.getInfoMessage(2);
        dialog.showError(titulo, mensagem);*/
	})
	.done(function(){

	});
}

function carregarComboboxCidade(eid){

	if(isNaN(parseInt(eid))){
		$("#cidades select").html("<option value=''>--Escolha o estado--</option>");
		return false;
	}

	$.getJSON(
		"/api/cidades/estado/id/"+eid, 
		function(cidades){

			if(utils.isEmpty(cidades)){

				error = true;
				//submitButton.addClass("disabled");

		        /*var titulo 		= message.getErrorMessage(4);
		        var mensagem 	= 'Esse problema geralmente ocorre quando a categoria vinculada ao aplicativo foi depreciada ou excluída pelo sistema. Alterar a categoria do aplicativo vai resolver o problema.';
		        var labelButton = 'Alterar a categoria do aplicativo';
		        var urlButton   = "/system/perfil";
		        dialog.showDBRegisterFail(titulo, mensagem, labelButton, urlButton);*/
	        } else{
	        	var htmlSelect = "";
	        	for(var obj in cidades){
	        		htmlSelect += "<option value="+cidades[obj].id+">"+cidades[obj].nome+"</option>";
	        	}

	        	$("#cidades select").html(htmlSelect);
	        }
		}
	)
	.fail(function(){

		//submitButton.addClass("disabled");
        
        /*var titulo = message.getErrorMessage(4);
        var mensagem = message.getInfoMessage(2);
        dialog.showError(titulo, mensagem);*/
	})
	.done(function(){

	});
}

var initialPoint;
var zoomLevel;
var options;
var map;
var marker;
var geocoder;
var infoWindow;
var markerImageSrc = '/img/system/marcador_temprafesta.png';
var latitudeRecovery;
var longitudeRecovery;

function initializeGMaps(){

	latitudeRecovery  = jQuery("#latitudeRecovery").val();
	latitudeRecovery  = (latitudeRecovery == "" ? undefined : latitudeRecovery);
	longitudeRecovery = jQuery("#longitudeRecovery").val();
	longitudeRecovery = (longitudeRecovery == "" ? undefined : longitudeRecovery)
	zoomLevel = 2;

	if(latitudeRecovery !== undefined && longitudeRecovery !== undefined){
		initialPoint = new google.maps.LatLng(latitudeRecovery, longitudeRecovery);
		zoomLevel = 17;
	} else {
		initialPoint = new google.maps.LatLng("0", "-36.5625");
	}

	infoWindow = new google.maps.InfoWindow();
	geocoder = new google.maps.Geocoder();
	options = {
		zoom: zoomLevel,
		center: initialPoint,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
    map = new google.maps.Map(document.getElementById("map"), options);

    marker = new google.maps.Marker({
    	position: initialPoint,
    	map: map,
    	icon: markerImageSrc,
    	draggable: true,
    	animation: google.maps.Animation.DROP,
    });

    loadMap();
}

function loadMap() {

	$("#localSearch").click(function(){
		var address = $("#local").val();

		geocoder.geocode({'address': address}, function(results, status){
			if(status == google.maps.GeocoderStatus.OK){
				__updateDescriptions(results[0], true);
			} else{
				toastr['warning']("Localização não encontrada. Por favor, tente novamente.");
			}
		});
	});

	var autocomplete = new google.maps.places.Autocomplete(document.getElementById('local'));
	autocomplete.bindTo('bounds', map);

	google.maps.event.addListener(autocomplete, 'place_changed', function(){
		var place = autocomplete.getPlace();

		if(!place.geometry){
			return;
		}

		if(place.geometry.viewport){
			map.fitBounds(place.geometry.viewport);
		} else{
			map.setCenter(place.geometry.location);
			map.setZoom(17);
		}

		__updateDescriptions(place, true);
	});

    var location = map.getCenter();

    __updateDescriptions(location);

    google.maps.event.addListener(map, 'click', function(e){
    	location = e.latLng;
    	__placeMarker(location);
    	__updateDescriptions(location);
    });

    google.maps.event.addListener(map, 'zoom_changed', function(){
    	zoomLevel = map.getZoom();
    });

    google.maps.event.addListener(marker, 'dblclick', function(){
    	zoomLevel = map.getZoom() + 1;
    	
    	if(zoomLevel == 20)
    		zoomLevel = 10;

    	map.setZoom(zoomLevel);
    	map.setCenter(location);
    });

    function __updateDescriptions(location, autocompleteDescription){

    	if(autocompleteDescription === undefined || autocompleteDescription === false)
    		__updateDescriptionToLatLng(location);
    	else{

    		$("#latitude").val(location.geometry.location.lat());
    		$("#longitude").val(location.geometry.location.lng());
    		marker.setPosition(location.geometry.location);
    		infoWindow.setContent(location.formatted_address);
    		infoWindow.open(map, marker);
    		inputLocal.val(location.formatted_address);
    		map.setZoom(17);
    	}

    	function __updateDescriptionToLatLng(location){

    		$("#latitude").val(location.lat());
    		$("#longitude").val(location.lng());

	    	geocoder.geocode({'latLng': location}, function(results, status){
	    		if(status == google.maps.GeocoderStatus.OK){
	    			if(results[1]){
	    				infoWindow.setContent(results[1].formatted_address);
	    				infoWindow.open(map, marker);
	    				inputLocal.val(results[1].formatted_address);
	    			} else{
	    				__clearInfoLocation();
						inputLocal.attr("placeholder", "Ainda não podemos determinar a localização do seu produto.");
	    			}
	    		} else{
	    			__clearInfoLocation();
	    			inputLocal.attr("placeholder", "Ainda não podemos determinar a localização do seu produto.");
	    		}

	    		function __clearInfoLocation(){
	    			if($("#local").val().length > 0){
	    				inputLocal.val(null);
	    				infoWindow.close();
	    			}
	    		}
	    	});
	    }
    }

    function __placeMarker(position){
    	marker.setPosition(position);
    	map.panTo(position);
    }

    google.maps.event.addListener(marker, 'dragend', function(e){
    	var position = marker.getPosition();
    	map.panTo(position);
    	__updateDescriptions(e.latLng);
    });

    google.maps.event.addListener(marker, 'drag', function(){
    	inputLocal.attr("placeholder", "Movendo o marcador...");
    });
}

