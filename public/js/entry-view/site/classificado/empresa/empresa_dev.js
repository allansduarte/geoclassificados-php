var mapDiv,
	map,
	infobox,
	latitude,
	longitude,
	geocoder,
	error,
	idTimeOut,
	infoImagem,
	infoTitulo,
	infoPreco;

var utils = new Utils();
var loaderImg = "<img class='loader' src='/img/ajax-loader.gif' alt='Processando...' />";
var contactForm;

jQuery(document).ready(function($)
{
	contactForm = jQuery("#contact-form");

	listarUltimosAnunciosAdicionados();
	listarAnunciosDestaque();
	listarAnunciosDestaqueSection();
	listarCategorias();

	error = false;

	var address = $("#localRecovery").val();
	geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': address}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	    	longitude = results[0].geometry.location.lng();
	    	latitude  = results[0].geometry.location.lat();
	    } else {
			error = true;
	    }

	    __checkLatLng();
	});

	contactForm.on('submit', function(){
		var contactName 	= $("#contactName").val();
		var email 			= $("#email").val();
		var commentsText 	= $("#commentsText").val();

		if(contactName == "" || email == "" || commentsText == ""){
			toastr['warning']('Precisamos que você informe os todos os campos do formulário de contato.', 'Ops! O formulário está incompleto.');
		} else{
			enviarEmailAnunciante();
		}
		return false;
	});
});

function __checkLatLng(){
	if(error)
		$("#single-page-map").hide();
	else if(latitude !== undefined){
		loadMap(latitude, longitude);
	} else{
		address = $("#ad-address span").text();
		geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 'address': address}, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {
		    	longitude = results[0].geometry.location.lng();
		    	latitude  = results[0].geometry.location.lat();
		    } else {
				error = true;
		    }
  		});

		idTimeOut = setTimeout(function(){
			loadMap(latitude, longitude);
		}, 3000);
	}
}

function enviarEmailAnunciante(){
	var empresaIdRecovery = jQuery("#empresaIdRecovery").val();
	var url = "/site/classificado/contatar-empresa?";
	url   	+= contactForm.serialize();
	url  	+= '&empresa='+empresaIdRecovery;

	jQuery.ajax({
        url : url,
        beforeSend : function(){
        	jQuery("form#contact-form input.input-submit").hide();
        	jQuery("form#contact-form .btn-container").append(loaderImg);
        },
        success : function(data) {
            toastr['success']('Foi enviado um e-mail contatando o anunciante.', 'E-mail enviado!');
        }
    })
    .done(function()
    {
    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
    	var responseText = jqXHR.responseText;
    	/*try{
    		responseText = JSON.parse(responseText);
    	} catch(e){
    		jQuery("form#register .btn-container .loader").remove();
    		jQuery("form#register #edit-submit").fadeIn(300);
    		return false;
    	}*/

    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);

        toastr['warning']('Ocorreu um problema ao enviar o e-mail para o anunciante, tente novamente, ou tente mais tarde.', 'E-mail não enviado');
    });
}

function actionFavoritos(acao){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();

	jQuery.getJSON(
		"/system/favoritos/"+acao+"/id/"+classificadoIdRecovery, 
		function(result){

			var html;
			if(acao == 'add'){
				html  = 'Já está no seu Favoritos';
				html += '<span class="ad-detail">';
				html +=		'<a href="javascript:void(0);" id="btn-favorite" class="added">Remover <i class="fa fa-star"></i></a>';
	    		html += '</span>';
			} else if(acao == 'remove'){
				html  = 'Adicione aos Favoritos';
				html += '<span class="ad-detail">';
	    		html +=		'<a href="javascript:void(0);" id="btn-favorite">Clique aqui <i class="fa fa-star-o"></i></a>';
	    		html += '</span>';
			}

			jQuery("span.favoritos").html(html).fadeIn(300);
		}
	)
	.fail(function(jQXHR, response){
		var responseText = jQXHR.responseText;

		if(responseText == 0){
			window.location = "/system/auth";
			return  false;
		}

		jQuery("span.favoritos").fadeIn(300);
	})
	.done(function(){
		jQuery("#btn-favorite").on('click', function(){
			jQuery("span.favoritos").hide();
			if(jQuery(this).hasClass('added')){
				actionFavoritos('remove');
			} else{
				actionFavoritos('add');
			}
		});
	});
}

function loadMap(latitude, longitude){
	clearTimeout(idTimeOut);
	infoImagem = jQuery(".author-avatar img").attr('src');
	infoTitulo = jQuery(".ad-title h2").text();

	mapDiv 	  = jQuery("#single-page-main-map");
	mapDiv.height(400).gmap3({
		map: {
			options: {
				"center": [latitude,longitude]
				,"zoom": 17
				,"draggable": true
				,"mapTypeControl": true
				,"mapTypeId": google.maps.MapTypeId.ROADMAP
				,"scrollwheel": false
				,"panControl": true
				,"rotateControl": false
				,"scaleControl": true
				,"streetViewControl": true
				,"zoomControl": true
			}
		}
		,marker: {
			values: [
					 	{											
							latLng: [latitude,longitude],
							options: {
								icon: "http://www.temprafesta.com.br/img/site/map_marker/map_marker_empresa.png",
								shadow: "img/site/shadow.png"
							},
							data: '<div class="marker-holder"><div class="marker-content"><div class="marker-image"><img src="'+infoImagem+'" /></div><div class="marker-info-holder"><div class="marker-info"><div class="marker-info-title">'+infoTitulo+'</div><div class="marker-info-extra"></div></div></div><div class="arrow-down"></div><div class="close"></div></div></div>'
						}	
					],
			options:{
				draggable: false
			},
			/*cluster:{
          		radius: 20,
				// This style will be used for clusters with more than 0 markers
				0: {
					content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
					width: 62,
					height: 62
				},
				// This style will be used for clusters with more than 20 markers
				20: {
					content: "<div class='cluster cluster-2'>CLUSTER_COUNT</div>",
					width: 82,
					height: 82
				},
				// This style will be used for clusters with more than 50 markers
				50: {
					content: "<div class='cluster cluster-3'>CLUSTER_COUNT</div>",
					width: 102,
					height: 102
				},
				events: {
					click: function(cluster) {
						map.panTo(cluster.main.getPosition());
						map.setZoom(map.getZoom() + 2);
					}
				}
          	},*/
			events: {
				click: function(marker, event, context){
					map.panTo(marker.getPosition());

					var ibOptions = {
					    pixelOffset: new google.maps.Size(-125, -88),
					    alignBottom: true
					};

					infobox.setOptions(ibOptions)

					infobox.setContent(context.data);
					infobox.open(map,marker);

					// if map is small
					var iWidth = 560;
					var iHeight = 560;
					if((mapDiv.width() / 2) < iWidth ){
						var offsetX = iWidth - (mapDiv.width() / 2);
						map.panBy(offsetX,0);
					}
					if((mapDiv.height() / 2) < iHeight ){
						var offsetY = -(iHeight - (mapDiv.height() / 2));
						map.panBy(0,offsetY);
					}

				}
			}
		}
 	});

	map = mapDiv.gmap3("get");

    infobox = new InfoBox({
    	pixelOffset: new google.maps.Size(-50, -65),
    	closeBoxURL: '',
    	enableEventPropagation: true
    });
    mapDiv.delegate('.infoBox .close','click',function () {
    	infobox.close();
    });

    if (Modernizr.touch){
    	map.setOptions({ draggable : false });
        var draggableClass = 'inactive';
        var draggableTitle = "Activate map";
        var draggableButton = $('<div class="draggable-toggle-button '+draggableClass+'">'+draggableTitle+'</div>').appendTo(mapDiv);
        draggableButton.click(function () {
        	if($(this).hasClass('active')){
        		$(this).removeClass('active').addClass('inactive').text("Activate map");
        		map.setOptions({ draggable : false });
        	} else {
        		$(this).removeClass('inactive').addClass('active').text("Deactivate map");
        		map.setOptions({ draggable : true });
        	}
        });
    }
}

function listarUltimosAnunciosAdicionados(){

	var empresaIdRecovery = jQuery("#empresaIdRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/ultimo/empresa/"+empresaIdRecovery+"/limit/4", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery(".jw-recent-posts-widget").parent().parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var descricao = jQuery("<div>").html(classificados[obj].descricao).text();
	        		descricao     = utils.limparHTML(descricao);
	        		descricao 	  = utils.resumeText(descricao, 44);
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		htmlImage = '<img class="widget-ad-image" src="'+imgSrc+'" />';
	        		var tituloUrl = utils.stringToURL(classificados[obj].titulo);

	        		htmlClassificado += '<li class="widget-ad-list latestads-widget">';
						htmlClassificado += '<a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<span class="widget-ad-list-content">';
						htmlClassificado += '<span class="widget-ad-list-content-title"><a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+classificados[obj].titulo+'</a></span>';
						htmlClassificado += '<p class="add-price">R$ '+classificados[obj].valor+'</p>';
						htmlClassificado += '<p>'+descricao+'</p>';
					htmlClassificado += '</li>';
	        	}

	        	jQuery(".cat-widget-content .jw-recent-posts-widget ul").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarAnunciosDestaque(){

	var empresaIdRecovery = jQuery("#empresaIdRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/destaque/empresa/"+empresaIdRecovery+"/limit/3", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery("#destaques").parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	count = 1;
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;

	        		if(count == 1){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 300px;height: 250px;" />';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        		}
	        		else if(count == 2){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:left">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}
	        		else if(count == 3){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:right">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}

					count++;
	        	}

	        	jQuery("#destaques .textwidget div").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarAnunciosDestaqueSection(){

	var error = false;
	var empresaId = jQuery("#empresaIdRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/destaque/limit/11/empresa/"+empresaId, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage = '<img src="'+imgSrc+'" />';
	        		var tituloUrl = utils.stringToURL(classificados[obj].titulo);
	        		var nomeEmpresa = classificados[obj].empresa_nome;

	        		htmlClassificado += '<div class="ad-box span3">';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="ad-hover-content">';
						htmlClassificado +=	'<div class="ad-category">';
						htmlClassificado +=		'<div class="category-icon-box"><i class="'+classificados[obj].icone+'"></i></div>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '<div class="post-title">';
						htmlClassificado +=		'<a href="javascript:void(0);">'+classificados[obj].titulo+'</a>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '</div>';
						htmlClassificado += '<div class="add-price"><span>R$ '+classificados[obj].valor+'</span></div>';

						if(nomeEmpresa)
							htmlClassificado += '<div class="ads-empresa"><a href="#">'+nomeEmpresa+'</a></div>';

					htmlClassificado += '</div>';
	        	}

	        	jQuery("#featured-abs #projects-carousel").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			jQuery('#projects-carousel').carouFredSel({
				auto: true,
				prev: '#carousel-prev',
				next: '#carousel-next',
				pagination: "#carousel-pagination",
				mousewheel: true,
				scroll: 1,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		}
	});
}

function listarCategorias(){

	var empresaIdRecovery = jQuery("#empresaIdRecovery").val();

	jQuery.getJSON(
		"/api/categorias/record-count/empresa/"+empresaIdRecovery, 
		function(categorias){

			if(utils.isEmpty(categorias)){

				jQuery("#encontre-por-categoria").parent().hide();
	        } else{
	        	var htmlCategoria = "";
	        	count = 1;
	        	for(var obj in categorias)
	        	{
	        		htmlCategoria += '<li>';
	        		htmlCategoria += '<div class="category-icon-box"><i class="'+categorias[obj].icone+'"></i></div>';
	        		htmlCategoria += '<a href="/search/category/'+categorias[obj].id+'" title="Encontre tudo para '+categorias[obj].nome+'">'+categorias[obj].nome+'</a>';
	        		htmlCategoria += '<span class="category-counter">'+categorias[obj].record_count+'</span>';
	        		htmlCategoria += '</li>';
	        	}

	        	jQuery("#encontre-por-categoria ul").html(htmlCategoria);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}