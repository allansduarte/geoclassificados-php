var mapDiv,
	map,
	infobox,
	latitude,
	longitude,
	geocoder,
	error,
	idTimeOut,
	infoImagem,
	infoTitulo,
	infoPreco,
	map_marker;

var utils = new Utils();
var loaderImg = "<img class='loader' src='/img/ajax-loader.gif' alt='Processando...' />";
var contactForm;

jQuery(document).ready(function($)
{
	contactForm = jQuery("#contact-form");

	listarAnunciosRelacionados();
	listarUltimosAnunciosAdicionados();
	listarAnunciosDestaque();
	listarCategorias();
	listarNuvemTags();

	latitude  = $("#latitudeRecovery").val();
	latitude  = (latitude == "" ? undefined : latitude);
	longitude = $("#longitudeRecovery").val();
	longitude = (longitude == "" ? undefined : longitude);
	error = false;

	if(latitude === undefined){
		var address = $("#localRecovery").val();
		geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 'address': address}, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {
		    	longitude = results[0].geometry.location.lng();
		    	latitude  = results[0].geometry.location.lat();
		    } else {
				error = true;
		    }

		    __checkLatLng();
  		});
	} else loadMap(latitude, longitude);

	jQuery("#btn-favorite").on('click', function(){
		jQuery("span.favoritos").hide();
		if(jQuery(this).hasClass('added')){
			actionFavoritos('remove');
		} else{
			actionFavoritos('add');
		}
	});

	contactForm.on('submit', function(){
		var contactName 	= $("#contactName").val();
		var email 			= $("#email").val();
		var commentsText 	= $("#commentsText").val();

		if(contactName == "" || email == "" || commentsText == ""){
			toastr['warning']('Precisamos que você informe os todos os campos do formulário de contato.', 'Ops! O formulário está incompleto.');
		} else{
			enviarEmailAnunciante();
		}
		return false;
	});
});

function __checkLatLng(){
	if(error)
		$("#single-page-map").hide();
	else if(latitude !== undefined){
		loadMap(latitude, longitude);
	} else{
		address = $("#ad-address span").text();
		geocoder = new google.maps.Geocoder();
			geocoder.geocode( { 'address': address}, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {
		    	longitude = results[0].geometry.location.lng();
		    	latitude  = results[0].geometry.location.lat();
		    } else {
				error = true;
		    }
  		});

		idTimeOut = setTimeout(function(){
			loadMap(latitude, longitude);
		}, 3000);
	}
}

function listarNuvemTags(){

	var categoriaRecovery   = jQuery("#categoriaRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/tag/limit/10/categoria/"+categoriaRecovery, 
		function(tags){

			if(utils.isEmpty(tags)){

				jQuery("#tagcloud").hide();
	        } else{
	        	var htmlCloudTags = "";
	        	for(var obj in tags)
	        	{
	        		var nomeTag = tags[obj];
	        		
	        		htmlCloudTags += '<a href="/search/tag/'+nomeTag+'" title="'+nomeTag+'">'+nomeTag+'</a>'
	        	}

	        	jQuery("#tagcloud .tagcloud").html(htmlCloudTags);
	        }
		}
	)
	.fail(function(){
		jQuery("#tagcloud").hide();
	})
	.done(function(){
	});
}

function enviarEmailAnunciante(){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();
	var url = "/site/classificado/contatar-anunciante?";
	url   	+= contactForm.serialize();
	url  	+= '&anuncio='+classificadoIdRecovery;

	jQuery.ajax({
        url : url,
        beforeSend : function(){
        	jQuery("form#contact-form input.input-submit").hide();
        	jQuery("form#contact-form .btn-container").append(loaderImg);
        },
        success : function(data) {
            toastr['success']('Foi enviado um e-mail contatando o anunciante.', 'E-mail enviado!');
        }
    })
    .done(function()
    {
    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
    	/*var responseText = jqXHR.responseText;
    	try{
    		responseText = JSON.parse(responseText);
    	} catch(e){
    		jQuery("form#register .btn-container .loader").remove();
    		jQuery("form#register #edit-submit").fadeIn(300);
    		return false;
    	}*/

    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);
        toastr['warning']('Ocorreu um problema ao enviar o e-mail para o anunciante, tente novamente, ou tente mais tarde.', 'E-mail não enviado');
    });
}

function actionFavoritos(acao){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();

	jQuery.getJSON(
		"/system/favoritos/"+acao+"/id/"+classificadoIdRecovery, 
		function(result){

			var html;
			if(acao == 'add'){
				html  = 'Já está no seu Favoritos';
				html += '<span class="ad-detail">';
				html +=		'<a href="javascript:void(0);" id="btn-favorite" class="added">Remover <i class="fa fa-star"></i></a>';
	    		html += '</span>';
			} else if(acao == 'remove'){
				html  = 'Adicione aos Favoritos';
				html += '<span class="ad-detail">';
	    		html +=		'<a href="javascript:void(0);" id="btn-favorite">Clique aqui <i class="fa fa-star-o"></i></a>';
	    		html += '</span>';
			}

			jQuery("span.favoritos").html(html).fadeIn(300);
		}
	)
	.fail(function(jQXHR, response){
		var responseText = jQXHR.responseText;

		if(responseText == 0){
			window.location = "/system/auth";
			return  false;
		}

		jQuery("span.favoritos").fadeIn(300);
	})
	.done(function(){
		jQuery("#btn-favorite").on('click', function(){
			jQuery("span.favoritos").hide();
			if(jQuery(this).hasClass('added')){
				actionFavoritos('remove');
			} else{
				actionFavoritos('add');
			}
		});
	});
}

function loadMap(latitude, longitude){
	clearTimeout(idTimeOut);
	infoImagem = jQuery("#basic li .capa").attr('src');
	infoTitulo = jQuery("#tituloRecovery").val();
	infoPreco  = jQuery("#basic .single-ad-price").html();
	map_marker = jQuery("#map_markerRecovery").val();


	mapDiv 	  = jQuery("#single-page-main-map");
	mapDiv.height(400).gmap3({
		map: {
			options: {
				"center": [latitude,longitude]
				,"zoom": 17
				,"draggable": true
				,"mapTypeControl": true
				,"mapTypeId": google.maps.MapTypeId.ROADMAP
				,"scrollwheel": false
				,"panControl": true
				,"rotateControl": false
				,"scaleControl": true
				,"streetViewControl": true
				,"zoomControl": true
			}
		}
		,marker: {
			values: [
					 	{											
							latLng: [latitude,longitude],
							options: {
								icon: map_marker,
								shadow: "/img/site/shadow.png"
							},
							data: '<div class="marker-holder"><div class="marker-content"><div class="marker-image"><img src="'+infoImagem+'" /></div><div class="marker-info-holder"><div class="marker-info"><div class="marker-info-title">'+infoTitulo+'</div><div class="marker-info-extra"><div class="marker-info-price">'+infoPreco+'</div></div></div></div><div class="arrow-down"></div><div class="close"></div></div></div>'
						}	
					],
			options:{
				draggable: false
			},
			/*cluster:{
          		radius: 20,
				// This style will be used for clusters with more than 0 markers
				0: {
					content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
					width: 62,
					height: 62
				},
				// This style will be used for clusters with more than 20 markers
				20: {
					content: "<div class='cluster cluster-2'>CLUSTER_COUNT</div>",
					width: 82,
					height: 82
				},
				// This style will be used for clusters with more than 50 markers
				50: {
					content: "<div class='cluster cluster-3'>CLUSTER_COUNT</div>",
					width: 102,
					height: 102
				},
				events: {
					click: function(cluster) {
						map.panTo(cluster.main.getPosition());
						map.setZoom(map.getZoom() + 2);
					}
				}
          	},*/
			events: {
				click: function(marker, event, context){
					map.panTo(marker.getPosition());

					var ibOptions = {
					    pixelOffset: new google.maps.Size(-125, -88),
					    alignBottom: true
					};

					infobox.setOptions(ibOptions)

					infobox.setContent(context.data);
					infobox.open(map,marker);

					// if map is small
					var iWidth = 560;
					var iHeight = 560;
					if((mapDiv.width() / 2) < iWidth ){
						var offsetX = iWidth - (mapDiv.width() / 2);
						map.panBy(offsetX,0);
					}
					if((mapDiv.height() / 2) < iHeight ){
						var offsetY = -(iHeight - (mapDiv.height() / 2));
						map.panBy(0,offsetY);
					}

				}
			}
		}
 	});

	map = mapDiv.gmap3("get");

    infobox = new InfoBox({
    	pixelOffset: new google.maps.Size(-50, -65),
    	closeBoxURL: '',
    	enableEventPropagation: true
    });
    mapDiv.delegate('.infoBox .close','click',function () {
    	infobox.close();
    });

    if (Modernizr.touch){
    	map.setOptions({ draggable : false });
        var draggableClass = 'inactive';
        var draggableTitle = "Activate map";
        var draggableButton = $('<div class="draggable-toggle-button '+draggableClass+'">'+draggableTitle+'</div>').appendTo(mapDiv);
        draggableButton.click(function () {
        	if($(this).hasClass('active')){
        		$(this).removeClass('active').addClass('inactive').text("Activate map");
        		map.setOptions({ draggable : false });
        	} else {
        		$(this).removeClass('inactive').addClass('active').text("Deactivate map");
        		map.setOptions({ draggable : true });
        	}
        });
    }
}

function listarAnunciosRelacionados(){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();
	var subcategoriaRecovery   = jQuery("#subcategoriaRecovery").val();

	var error = false;
	jQuery.getJSON(
		"/api/v1/classificados/relacionado/exceto/"+classificadoIdRecovery+"/subcategoria/"+subcategoriaRecovery+"/limit/3", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
				jQuery("#basic-related").parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage 		= '<img src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);

	        		htmlClassificado += '<li>';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="related-ad-price">R$ '+classificados[obj].valor+'</div>';
						htmlClassificado += '<div class="related-ad-title">'+classificados[obj].titulo+'</div>';
					htmlClassificado += '</li>';
	        	}

	        	jQuery("#basic-related ul").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			var $frame  = jQuery('#basic-related');
			var $slidee = $frame.children('ul').eq(0);
			var $wrap   = $frame.parent();

			// Call Sly on frame
			$frame.sly({
				horizontal: 1,
				itemNav: 'basic',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 3,
				scrollBar: $wrap.find('.scrollbar'),
				scrollBy: 1,
				pagesBar: $wrap.find('.pages'),
				activatePageOn: 'click',
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1,

				// Buttons
				forward: $wrap.find('.forward'),
				backward: $wrap.find('.backward'),
				prev: $wrap.find('.prev'),
				next: $wrap.find('.next'),
				prevPage: $wrap.find('.prevPage'),
				nextPage: $wrap.find('.nextPage')
			});

			// To Start button
			$wrap.find('.toStart').on('click', function () {
				var item = $(this).data('item');
				// Animate a particular item to the start of the frame.
				// If no item is provided, the whole content will be animated.
				$frame.sly('toStart', item);
			});

			// To Center button
			$wrap.find('.toCenter').on('click', function () {
				var item = $(this).data('item');
				// Animate a particular item to the center of the frame.
				// If no item is provided, the whole content will be animated.
				$frame.sly('toCenter', item);
			});

			// To End button
			$wrap.find('.toEnd').on('click', function () {
				var item = $(this).data('item');
				// Animate a particular item to the end of the frame.
				// If no item is provided, the whole content will be animated.
				$frame.sly('toEnd', item);
			});

			// Add item
			$wrap.find('.add').on('click', function () {
				$frame.sly('add', '<li>' + $slidee.children().length + '</li>');
			});

			// Remove item
			$wrap.find('.remove').on('click', function () {
				$frame.sly('remove', -1);
			});
		}
	});
}

function listarUltimosAnunciosAdicionados(){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/ultimo/exceto/"+classificadoIdRecovery+"/limit/4", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery(".jw-recent-posts-widget").parent().parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var descricao 		= jQuery("<div>").html(classificados[obj].descricao).text();
	        		descricao     		= utils.limparHTML(descricao);
	        		descricao 	  		= utils.resumeText(descricao, 44);
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage 		= '<img class="widget-ad-image" src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);

	        		htmlClassificado += '<li class="widget-ad-list latestads-widget">';
						htmlClassificado += '<a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<span class="widget-ad-list-content">';
						htmlClassificado += '<span class="widget-ad-list-content-title"><a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+classificados[obj].titulo+'</a></span>';
						htmlClassificado += '<p class="add-price">R$ '+classificados[obj].valor+'</p>';
						htmlClassificado += '<p>'+descricao+'</p>';
					htmlClassificado += '</li>';
	        	}

	        	jQuery(".cat-widget-content .jw-recent-posts-widget ul").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarAnunciosDestaque(){

	var classificadoIdRecovery = jQuery("#classificadoIdRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/destaque/exceto/"+classificadoIdRecovery+"/limit/3", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery("#destaques").parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	count = 1;
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;

	        		if(count == 1){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 300px;height: 250px;" />';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        		}
	        		else if(count == 2){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:left">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}
	        		else if(count == 3){
	        			htmlImage = '<img src="'+imgSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:right">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}

					count++;
	        	}

	        	jQuery("#destaques .textwidget div").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarCategorias(){

	jQuery.getJSON(
		"/api/categorias/record-count", 
		function(categorias){

			if(utils.isEmpty(categorias)){

				jQuery("#encontre-por-categoria").parent().hide();
	        } else{
	        	var htmlCategoria = "";
	        	count = 1;
	        	for(var obj in categorias)
	        	{
	        		htmlCategoria += '<li>';
	        		htmlCategoria += '<div class="category-icon-box"><i class="'+categorias[obj].icone+'"></i></div>';
	        		htmlCategoria += '<a href="/search/category/'+categorias[obj].id+'" title="Encontre tudo para '+categorias[obj].nome+'">'+categorias[obj].nome+'</a>';
	        		htmlCategoria += '<span class="category-counter">'+categorias[obj].record_count+'</span>';
	        		htmlCategoria += '</li>';
	        	}

	        	jQuery("#encontre-por-categoria ul").html(htmlCategoria);
	        }
		}
	)
	.fail(function(){

	})
	.done(function(){
	});
}