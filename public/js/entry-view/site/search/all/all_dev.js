var utils = new Utils();
var queryString;

jQuery(document).ready(function(){

	jQuery("ul.tabs").tabs("> .pane", {effect: 'fade', fadeIn: 200});
	queryString = utils.queryStringParse();

	listarAnunciosDestaque();
	requisitarCategorias();
	listarUltimosAnunciosAdicionados(1, true);
	listarAnunciosPopulares(1, false);
	listarAnunciosEmpresas(1, false);
	popularComboboxLocais();

	jQuery("#edit-ad-estado").chosen().change(function(){
		var comboboxValue = jQuery(this).val();
		popularComboboxLocais(comboboxValue);
	});
});

function listarAnunciosEmpresas(nrPage, show){

	jQuery.getJSON(
		"/api/v1/classificados/empresa/limit/12/page/"+nrPage, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="random-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   = "";
	        		var tituloUrl     = utils.stringToURL(classificados[obj].empresa_nome);
	        		var titulo 		  = classificados[obj].empresa_nome;
	        		var empresa_id 			  = classificados[obj].empresa_id;
	        		var user_id       = classificados[obj].user_id;
	        		var imagemSrc     = classificados[obj].imagem;
	        		var imagem 		  = '<img width="270" height="220" src="/files/empresa/'+user_id+'/'+empresa_id+'/'+imagemSrc+'" class="attachment-270x220" alt="'+titulo+'">';

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/empresa/'+tituloUrl+'/'+empresa_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		//htmlClassificado += 		'<div class="ad-category">';
	        		//htmlClassificado += 			'<div class="category-icon-box"><i class=""></i></div>';
	        		//htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/empresa/'+tituloUrl+'/'+empresa_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .random-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .random-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosEmpresas(nrPage, true);
		});
	});
}

function listarAnunciosPopulares(nrPage, show){

	var categoria 	  = queryString.categoria;
	var estado    	  = queryString.estado;
	var cidade    	  = queryString.cidade;
	var palavra_chave = queryString.palavra_chave;

	var url = "/api/v1/classificados/popular/limit/12/page/"+nrPage;

	if(estado != "")
		url    += "/estado/"+estado;

	if(cidade != "")
		url    += "/cidade/"+cidade;

	if(palavra_chave != "")
		url    += "/palavra_chave/"+palavra_chave;

	if(categoria != ""){
		var categoria_split = categoria.split('_');
		var categoria_tipo  = categoria_split[0];
		var categoria_valor = categoria_split[1];

		if(categoria_tipo == "c")
			url += "/categoria/"+categoria_valor;
		else if(categoria_tipo == "s")
			url += "/subcategoria/"+categoria_valor;
	}

	jQuery.getJSON(
		url, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="popular-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   	= "";
	        		var tituloUrl     	= utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  	= classificados[obj].titulo;
	        		var valor 		  	= classificados[obj].valor;
	        		var classificado_id	= classificados[obj].id;
	        		var icone   	    = classificados[obj].icone;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  	= '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+classificado_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .popular-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .popular-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosPopulares(nrPage, true);
		});
	});
}

function listarUltimosAnunciosAdicionados(nrPage, show){

	var categoria 	  = queryString.categoria;
	var estado    	  = queryString.estado;
	var cidade    	  = queryString.cidade;
	var palavra_chave = queryString.palavra_chave;

	var url = "/api/v1/classificados/ultimo/limit/12/page/"+nrPage;

	if(estado != "")
		url    += "/estado/"+estado;

	if(cidade != "")
		url    += "/cidade/"+cidade;

	if(palavra_chave != "")
		url    += "/palavra_chave/"+palavra_chave;

	if(categoria != ""){
		var categoria_split = categoria.split('_');
		var categoria_tipo  = categoria_split[0];
		var categoria_valor = categoria_split[1];

		if(categoria_tipo == "c")
			url += "/categoria/"+categoria_valor;
		else if(categoria_tipo == "s")
			url += "/subcategoria/"+categoria_valor;
	}

	jQuery.getJSON(
		url, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="latest-ads-grid-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   = "";
	        		var tituloUrl     = utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  = classificados[obj].titulo;
	        		var valor 		  = classificados[obj].valor;
	        		var classificado_id 			  = classificados[obj].id;
	        		var icone   	  = classificados[obj].icone;
	        		var imgSrc  	  = "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  = '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 latest-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+classificado_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .latest-ads-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .latest-ads-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarUltimosAnunciosAdicionados(nrPage, true);
		});
	});
}

function requisitarCategorias(){

	var categoriasJson;
	jQuery.getJSON(
		"/api/categorias/subcategorias",
		function(categorias){

			categoriasJson = categorias;
			if(utils.isEmpty(categorias)){
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		_popularComboboxCategoria(categoriasJson);
	});
}

function _popularComboboxCategoria(categorias)
{
	var htmlCategoria = "<option value=''>Categoria...</option>";
	var count = 1;

	for(var categoriaJson in categorias)
	{
		var subcategorias = categorias[categoriaJson].subcategorias;

		if(utils.isEmpty(subcategorias))
			continue;

		var categoria_nome = categorias[categoriaJson].nome;
		var categoria_id   = categorias[categoriaJson].id;

		htmlCategoria += 	'<option value="c_'+categoria_id+'">'+categoria_nome+'</option>';

		for(var subcategoriaJson in subcategorias)
		{
			var subcategoria_id   = subcategorias[subcategoriaJson].subcategoria_id
			var subcategoria_nome = subcategorias[subcategoriaJson].subcategoria_nome;

			htmlCategoria += '<option value="s_'+subcategoria_id+'">- '+subcategoria_nome+'</option>';
		}
	}

	jQuery("#edit-field-category").append(htmlCategoria);
	jQuery(".form-select").chosen();
}

function popularComboboxLocais(cEstado_id)
{
	cEstado_id = (isNaN(cEstado_id) ? undefined : cEstado_id);

	var error = false;
	jQuery.getJSON(
		"/api/v1/classificados/locais_populares", 
		function(locais){

			var htmlCombobox;

			if(cEstado_id === undefined)
				htmlCombobox = '<option value="">Selecione um estado...</option>';
			else
				htmlCombobox = '<option value="">Escolha uma cidade...</option>'

			for(var locaisJson in locais)
			{
				var cidades = locais[locaisJson].cidades;

				if(utils.isEmpty(cidades))
					continue;

				if(cEstado_id === undefined){
					var estado_nome = locais[locaisJson].estado_nome;
					var estado_id   = locais[locaisJson].estado_id;
					htmlCombobox += 	'<option value="'+estado_id+'">'+estado_nome+'</option>';
				}
				else if(locais[locaisJson].estado_id == cEstado_id){
					for(var cidadesJson in cidades)
					{
						var cidade_id   = cidades[cidadesJson].cidade_id;
						var cidade_nome = cidades[cidadesJson].cidade_nome;

						htmlCombobox += '<option value="'+cidade_id+'">'+cidade_nome+'</option>';
					}
				}
			}

			if(cEstado_id === undefined){
				jQuery("#edit-ad-estado").html(htmlCombobox);
				jQuery("#edit-ad-cidade").html("<option value=''>Escolha um estado...</option>");
			} else{
				jQuery("#edit-ad-cidade").html(htmlCombobox);
			}
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#edit-ad-estado").trigger("chosen:updated");
		jQuery("#edit-ad-cidade").trigger("chosen:updated");
	});
}

function listarAnunciosDestaque(){

	var error = false;
	jQuery.getJSON(
		"/api/v1/classificados/destaque/limit/11",
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var classificado_id = classificados[obj].id;
	        		var imgSrc  	  = "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		htmlImage = '<img src="'+imgSrc+'" />';
	        		var tituloUrl = utils.stringToURL(classificados[obj].titulo);
	        		var icone 	  = classificados[obj].icone;

	        		htmlClassificado += '<div class="ad-box span3">';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="ad-hover-content">';
						htmlClassificado +=	'<div class="ad-category">';
						htmlClassificado +=		'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '<div class="post-title">';
						htmlClassificado +=		'<a href="javascript:void(0);">'+classificados[obj].titulo+'</a>';
						htmlClassificado +=	'</div>';							
						htmlClassificado += '</div>';
						htmlClassificado += '<div class="add-price"><span>R$ '+classificados[obj].valor+'</span></div>'; 
					htmlClassificado += '</div>';
	        	}

	        	jQuery("#featured-abs #projects-carousel").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			jQuery('#projects-carousel').carouFredSel({
				auto: true,
				prev: '#carousel-prev',
				next: '#carousel-next',
				pagination: "#carousel-pagination",
				mousewheel: true,
				scroll: 1,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		}
	});
}