var utils = new Utils();

jQuery(document).ready(function(){

	jQuery("body").addClass("category");
	jQuery("ul.tabs").tabs("> .pane", {effect: 'fade', fadeIn: 200});

	listarCategorias();
	requisitarCategorias();
	listarUltimosAnunciosAdicionados();
	listarAnunciosDestaque();
	popularComboboxLocais();
	listarNuvemTags();

	jQuery("#edit-ad-estado").chosen().change(function(){
		var comboboxValue = jQuery(this).val();
		popularComboboxLocais(comboboxValue);
	});

	listarUltimosAnunciosAdicionadosPaginado(1, true);
	listarAnunciosPopulares(1, false);
	listarAnunciosEmpresas(1, false);
});

function listarAnunciosEmpresas(nrPage, show){

	jQuery.getJSON(
		"/api/v1/classificados/empresa/limit/12/page/"+nrPage, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        }
	        else{
	        	var htmlClassificado = '<div class="random-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss = "";
	        		var tituloUrl   = utils.stringToURL(classificados[obj].empresa_nome);
	        		var titulo 		= classificados[obj].empresa_nome;
	        		var empresa_id	= classificados[obj].empresa_id;
	        		var user_id     = classificados[obj].user_id;
	        		var imagemSrc   = classificados[obj].imagem;
	        		var imagem      = '<img width="270" height="220" src="/files/empresa/'+user_id+'/'+empresa_id+'/'+imagemSrc+'" class="attachment-270x220" alt="'+titulo+'">';

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/empresa/'+tituloUrl+'/'+empresa_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		//htmlClassificado += 		'<div class="ad-category">';
	        		//htmlClassificado += 			'<div class="category-icon-box"><i class=""></i></div>';
	        		//htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/empresa/'+tituloUrl+'/'+empresa_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .random-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .random-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosEmpresas(nrPage, true);
		});
	});
}

function listarNuvemTags(){

	var tagRecovery = jQuery("#tagRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/tag/limit/10/tag/"+tagRecovery+"/exceto/"+tagRecovery, 
		function(tags){

			if(utils.isEmpty(tags)){

				jQuery("#tagcloud").hide();
	        } else{
	        	var htmlCloudTags = "";
	        	for(var obj in tags)
	        	{
	        		var nomeTag = tags[obj];
	        		
	        		htmlCloudTags += '<a href="/search/tag/'+nomeTag+'" title="'+nomeTag+'">'+nomeTag+'</a>';
	        	}

	        	jQuery("#tagcloud .tagcloud").html(htmlCloudTags);
	        }
		}
	)
	.fail(function(){
		jQuery("#tagcloud").hide();
	})
	.done(function(){
	});
}

function requisitarCategorias(){

	var categoriasJson;
	var error = false;
	jQuery.getJSON(
		"/api/categorias/subcategorias",
		function(categorias){

			categoriasJson = categorias;
			if(utils.isEmpty(categorias)){
				error = true;
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		_popularComboboxCategoria(categoriasJson);
	});
}

function _popularComboboxCategoria(categorias)
{
	var htmlCategoria = "<option value=''>Categoria...</option>";
	var count = 1;

	for(var categoriaJson in categorias)
	{
		var subcategorias = categorias[categoriaJson].subcategorias;

		if(utils.isEmpty(subcategorias))
			continue;

		var categoria_nome = categorias[categoriaJson].nome;
		var categoria_id   = categorias[categoriaJson].id;

		htmlCategoria += 	'<option value="c_'+categoria_id+'">'+categoria_nome+'</option>';

		for(var subcategoriaJson in subcategorias){
			var subcategoria_id   = subcategorias[subcategoriaJson].subcategoria_id
			var subcategoria_nome = subcategorias[subcategoriaJson].subcategoria_nome;

			htmlCategoria += '<option value="s_'+subcategoria_id+'">- '+subcategoria_nome+'</option>';
		}
	}

	jQuery("#edit-field-category").append(htmlCategoria);
	jQuery(".form-select").chosen();
}

function popularComboboxLocais(cEstado_id)
{

	cEstado_id = (isNaN(cEstado_id) ? undefined : cEstado_id);

	jQuery.getJSON(
		"/api/v1/classificados/locais_populares", 
		function(locais){
			var htmlCombobox;

			if(cEstado_id === undefined)
				htmlCombobox = '<option value="">Selecione um estado...</option>';
			else
				htmlCombobox = '<option value="">Escolha uma cidade...</option>'

			for(var locaisJson in locais)
			{
				var cidades = locais[locaisJson].cidades;

				if(utils.isEmpty(cidades))
					continue;

				if(cEstado_id === undefined){
					var estado_nome = locais[locaisJson].estado_nome;
					var estado_id   = locais[locaisJson].estado_id;
					htmlCombobox += 	'<option value="'+estado_id+'">'+estado_nome+'</option>';
				} else if(locais[locaisJson].estado_id == cEstado_id){
					for(var cidadesJson in cidades)
					{

						var cidade_id   = cidades[cidadesJson].cidade_id;
						var cidade_nome = cidades[cidadesJson].cidade_nome;

						htmlCombobox += '<option value="'+cidade_id+'">'+cidade_nome+'</option>';
					}
				}
			}

			if(cEstado_id === undefined){
				jQuery("#edit-ad-estado").html(htmlCombobox);
				jQuery("#edit-ad-cidade").html("<option value=''>Escolha um estado...</option>");
			} else{
				jQuery("#edit-ad-cidade").html(htmlCombobox);
			}
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#edit-ad-estado").trigger("chosen:updated");
		jQuery("#edit-ad-cidade").trigger("chosen:updated");
	});
}

function listarAnunciosPopulares(nrPage, show){

	var tagRecovery = jQuery("#tagRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/popular/limit/12/page/"+nrPage+"/tag/"+tagRecovery, 
		function(classificados){

			var htmlClassificado;

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        } else{
	        	htmlClassificado = '<div class="popular-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   	= "";
	        		var tituloUrl     	= utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  	= classificados[obj].titulo;
	        		var valor 		  	= classificados[obj].valor;
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  	= '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';
	        		var icone	      	= classificados[obj].icone;

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+classificado_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .popular-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .popular-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosPopulares(nrPage, true);
		});
	});
}

function listarUltimosAnunciosAdicionadosPaginado(nrPage, show){

	var tagRecovery = jQuery("#tagRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/ultimo/limit/12/page/"+nrPage+"/tag/"+tagRecovery, 
		function(classificados){

			var htmlClassificado;

			if(utils.isEmpty(classificados)){

				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        } else{
	        	htmlClassificado = '<div class="latest-ads-grid-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   	= "";
	        		var tituloUrl     	= utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  	= classificados[obj].titulo;
	        		var valor 		  	= classificados[obj].valor;
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  	= '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';
	        		var icone	      	= classificados[obj].icone;

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 latest-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+classificado_id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .latest-ads-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .latest-ads-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarUltimosAnunciosAdicionadosPaginado(nrPage, true);
		});
	});
}

function listarCategorias(){

	jQuery.getJSON(
		"/api/categorias/record-count", 
		function(categorias){

			if(utils.isEmpty(categorias)){

				jQuery("#encontre-por-categoria").parent().hide();
	        } else{
	        	var htmlCategoria = "";
	        	count = 1;
	        	for(var obj in categorias)
	        	{
	        		htmlCategoria += '<li>';
	        		htmlCategoria += '<div class="category-icon-box"><i class="'+categorias[obj].icone+'"></i></div>';
	        		htmlCategoria += '<a href="/search/category/'+categorias[obj].id+'" title="Encontre tudo para '+categorias[obj].nome+'">'+categorias[obj].nome+'</a>';
	        		htmlCategoria += '<span class="category-counter">'+categorias[obj].record_count+'</span>';
	        		htmlCategoria += '</li>';
	        	}

	        	jQuery("#encontre-por-categoria ul").html(htmlCategoria);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarUltimosAnunciosAdicionados(){

	var tagRecovery = jQuery("#tagRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/ultimo/limit/4/tag/"+tagRecovery, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery(".jw-recent-posts-widget").parent().parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var descricao 		= jQuery("<div>").html(classificados[obj].descricao).text();
	        		descricao     		= utils.limparHTML(descricao);
	        		descricao 	  		= utils.resumeText(descricao, 44);
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage 		= '<img class="widget-ad-image" src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);

	        		htmlClassificado += '<li class="widget-ad-list latestads-widget">';
						htmlClassificado += '<a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<span class="widget-ad-list-content">';
						htmlClassificado += '<span class="widget-ad-list-content-title"><a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+classificados[obj].titulo+'</a></span>';
						htmlClassificado += '<p class="add-price">R$ '+classificados[obj].valor+'</p>';
						htmlClassificado += '<p>'+descricao+'</p>';
					htmlClassificado += '</li>';
	        	}

	        	jQuery(".cat-widget-content .jw-recent-posts-widget ul").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}

function listarAnunciosDestaque(){

	var tagRecovery = jQuery("#tagRecovery").val();

	jQuery.getJSON(
		"/api/v1/classificados/destaque/limit/3/tag/"+tagRecovery, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery("#destaques").parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	count = 1;
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;

	        		if(count == 1){
	        			htmlImage = '<img src="'+imageSrc+'" alt="'+classificados[obj].titulo+'" style="width: 300px;height: 250px;" />';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        		}
	        		else if(count == 2){
	        			htmlImage = '<img src="'+imageSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:left">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}
	        		else if(count == 3){
	        			htmlImage = '<img src="'+imageSrc+'" alt="'+classificados[obj].titulo+'" style="width: 125px;height: 125px;" />';
	        			htmlClassificado += '<div style="float:right">';
	        			htmlClassificado += '<a href="/classificado/'+utils.stringToURL(classificados[obj].titulo)+'/'+classificado_id+'">'+htmlImage+'</a>';
	        			htmlClassificado += '</div>';
	        		}

					count++;
	        	}

	        	jQuery("#destaques .textwidget div").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
	});
}