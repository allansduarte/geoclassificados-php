var utils     = new Utils();
var queryString;
var loaderImg = "<img class='loader' src='/img/ajax-loader.gif' alt='Processando...' />";
var form;

var tourImage 		  = '<img src="/img/marketing/landing-page/conheca/tem-pra-festa-mascote-gamification.png">';
var tourTemplateStep1 = "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div>"+tourImage+"<div class='popover-navigation'><div class='btn-group'><button class='btn btn-sm btn-default' data-role='prev'>SIM</button><button class='btn btn-sm btn-default' data-role='next'>NÃO</button></div><button class='btn btn-sm btn-default' data-role='end'><i class='fa fa-times'></i></button></div></div>";
var tourTemplateEnd   = "<div class='popover tour'><div class='arrow'></div><h3 class='popover-title'></h3><div class='popover-content'></div><div class='popover-navigation'><div class='btn-group'><button class='btn btn-sm btn-default' data-role='prev'>Voltar</button><button class='btn btn-sm btn-default' data-role='next'>Em frente</button></div><button class='btn btn-sm btn-default' data-role='end'><i class='fa fa-times'></i></button></div></div>";
var tourAction        = true;
var tour = new Tour({
  	steps: [
	  {
	    element: "#register-login-block-top",
	    title: "Já possui conta no Tem Pra Festa?",
	    content: "Clique nas opções abaixo para continuar.",
	    placement: "bottom",
	    onPrev: function (tour) {
	    	window.location = "/system/auth";
	    },
	    template: tourTemplateStep1
	  },
	  {
	    element: ".ads-main-page .log-in-logo",
	    title: "Faça o cadastro",
	    content: "Agora é só preencher o formulário de Cadastro de Usuário abaixo e confirmar o e-mail de segurança enviado pelo Tem Pra Festa.",
	    placement: "top",
	    template: tourTemplateEnd,
	    onNext: function (tour) {
	    	jQuery("#nome-completo").focus();
	    },
	    onPrev: function (tour) {
	    	tourAction = false;
	    	console.log("onPrev: "+tourAction);
	    },
	    onHidden: function (tour) {
	    	console.log("onHidden"+tourAction);
	    	if(tourAction)
		  		jQuery("#nome-completo").focus();
		}
	  }
	],
	storage: false
});

jQuery(document).ready(function(){

	queryString = utils.queryStringParse();
	if(Boolean(queryString.tour)){
		tour.init();
		tour.start();
	}

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "5000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}

	form 	= jQuery("form#register");
	jQuery("#estados select").change(function(){
		var eid = jQuery("#estados select option:selected").val();
		carregarComboboxCidade(eid);
	});

	form.validate({
		rules: {
			nome: "required",
			username: "required",
			password: "required",
			estado_id: "required",
			cidade_id: "required"
		},
		messages: {
			nome: "Por favor, informe o campo Nome.",
			username: "Por favor, informe o campo E-mail.",
			password: "Por favor, informe o campo Senha.",
			estado_id: "Por favor, selecione um Estado.",
			cidade_id: "Por favor, seleciona uma Cidade."
		}
	});

	form.submit(function(e){
		e.preventDefault();
		if(form.valid()){
			saveUser();
		}
	});
});

function saveUser(){
	jQuery.ajax({
        url : "/site/user/save",
        type: "POST",
        data: form.serialize(),
        beforeSend : function(){
        	jQuery("form#register #edit-submit").hide();
        	jQuery("form#register .btn-container").append(loaderImg);
        },
        success : function(data) {
            toastr['success']('Foi enviado um e-mail de ativação para você. Confirme-o e você poderá anunciar gratuitamente!', 'Cadastrado!');
        }
    })
    .done(function()
    {
    	jQuery("form#register .btn-container .loader").remove();
        jQuery("form#register #edit-submit").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
    	var responseText = jqXHR.responseText;
    	try{
    		responseText = JSON.parse(responseText);
    	} catch(e){
    		jQuery("form#register .btn-container .loader").remove();
    		jQuery("form#register #edit-submit").fadeIn(300);
    		return false;
    	}

    	showErrors(responseText);

    	jQuery("form#register .btn-container .loader").remove();
    	jQuery("form#register #edit-submit").fadeIn(300);
    });
}

function resendEmail(dataRecovery){
	jQuery.ajax({
        url : "/site/user/resend-email",
        type: "POST",
        data: "user_id="+dataRecovery,
        beforeSend : function(){
        	jQuery("form#register").siblings(".alert").remove();
        	jQuery("form#register").before(loaderImg);
        },
        success : function(data) {
            toastr['success']('Foi enviado um e-mail de ativação para você. Confirme-o e você poderá anunciar gratuitamente!', 'E-mail enviado!');
        }
    })
    .done(function(data, textStatus, jqXHR)
    {
    	jQuery("form#register").siblings(".loader").remove();
    	jQuery("form#register").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
    	var responseText = jqXHR.responseText;

    	try{
    		responseText = JSON.parse(responseText);
    	} catch(e){
    		jQuery("form#register").siblings(".loader").remove();
    		jQuery("form#register").fadeIn(300);
    		return false;
    	}

    	showErrors(responseText);
    	jQuery("form#register").siblings(".loader").remove();
    	if(responseText.error_id == 2)
    		return false;

    	jQuery("form#register").fadeIn(300);
    });
}

function showErrors(responseText){
	if(responseText.error_id == 1){
		toastr['warning']("O e-mail "+jQuery('form#register input#email').val()+" já está em uso.", "E-mail já cadastrado!");
	}
	else if(responseText.error_id == 2){
		var htmlSendEmail = '<div class="alert alert-warning alert-dismissable">';
        		htmlSendEmail += '<i class="fa fa-warning"></i>';
                htmlSendEmail    += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                htmlSendEmail    += '<b>Ops!</b>';
                htmlSendEmail    += '<p>Ocorreu um problema ao enviar o e-mail de ativação para você. Por favor, <a id="resendEmail" href="javascript:void(0);" data-recovery="'+responseText.user_id+'">clique aqui para enviar o e-mail novamente.</a></p>';
                htmlSendEmail    += '<p>Caso o erro persista entre em contato conosco <a href="/contato">clicando aqui.</a></p>';
            htmlSendEmail += '</div>';
		jQuery("form#register").before(htmlSendEmail);
		jQuery("form#register").hide();

		jQuery("#resendEmail").on("click", function(){
			resendEmail(jQuery(this).attr("data-recovery"));
		});
	}
	else if(responseText.error_id == 3){
		toastr['warning']("Ocorreu um erro ao efetuar o cadastro. Por favor tente novamente mais tarde.");
	}
	else if(responseText.error_id == 4){
		toastr['warning']("O e-mail "+jQuery("#register #email").val()+" já está em uso, por favor tente outra vez.");
	}
}

function carregarComboboxCidade(eid){

	if(isNaN(parseInt(eid))){
		jQuery("#cidades select").html("<option value=''>--Escolha o estado--</option>");
		return false;
	}

	jQuery.getJSON(
		"/api/cidades/estado/id/"+eid, 
		function(cidades){

			if(utils.isEmpty(cidades)){

				error = true;
	        } else{
	        	var htmlSelect = "";
	        	for(var obj in cidades){

	        		htmlSelect += "<option value="+cidades[obj].id+">"+cidades[obj].nome+"</option>";
	        	}

	        	jQuery("#cidades select").html(htmlSelect);
	        }
		}
	)
	.fail(function(){})
	.done(function(){});
}