var utils = new Utils();
var htmlImgLoader = "<img src='/img/ajax-loader.gif' width='24' height='24'>";
var baseUrl = "http://temprafesta.web1313.kinghost.net";

jQuery(document).ready(function(){

	try{
		jQuery("ul.tabs").tabs(".pane", {effect: 'fade', fadeIn: 200});
	} catch(e){
		console.log(e);
	}

	listarAnunciosDestaque();
	requisitarCategorias();
	listarUltimosAnunciosAdicionados(1, true);
	listarAnunciosPopulares(1, false);
	listarAnunciosEmpresas(1, false);
	popularComboboxLocais();

	jQuery("#edit-ad-estado").chosen().change(function(){
		var comboboxValue = jQuery(this).val();

		popularComboboxLocais(comboboxValue);
	});
});

function listarAnunciosEmpresas(nrPage, show){
	var error = false;

	jQuery.getJSON(
		baseUrl+"/api/v1/classificados/empresa/limit/12/page/"+nrPage, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="random-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   = "";
	        		var tituloUrl     = utils.stringToURL(classificados[obj].empresa_nome);
	        		var titulo 		  = classificados[obj].empresa_nome;
	        		var id 			  = classificados[obj].empresa_id;
	        		var user_id       = classificados[obj].user_id;
	        		var imagemSrc     = classificados[obj].imagem;
	        		var imagem 		  = '<img width="270" height="220" src="/files/empresa/'+user_id+'/'+id+'/'+imagemSrc+'" class="attachment-270x220" alt="'+titulo+'">';

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/empresa/'+tituloUrl+'/'+id+'" title="'+titulo+'">'+imagem+'</a>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		//htmlClassificado += 		'<div class="ad-category">';
	        		//htmlClassificado += 			'<div class="category-icon-box"><i class=""></i></div>';
	        		//htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/empresa/'+tituloUrl+'/'+id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .random-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .random-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .random-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .random-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosEmpresas(nrPage, true);
		});
	});
}

function listarAnunciosPopulares(nrPage, show){
	var error = false;

	jQuery.getJSON(
		baseUrl+"/api/v1/classificados/popular/limit/12/page/"+nrPage, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";
				jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="popular-ads-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   = "";
	        		var tituloUrl     = utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  = classificados[obj].titulo;
	        		var valor 		  = classificados[obj].valor;
	        		var id 			  = classificados[obj].id;
	        		var imgSrc  	  = "/files/classificado/"+classificados[obj].usuario_id+"/"+id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  = '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';
	        		var icone   	  = classificados[obj].icone;
	        		var nomeEmpresa   = classificados[obj].empresa_nome;
	        		var empresaId     = classificados[obj].empresa_id;

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 popular-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+id+'" title="'+titulo+'">'+imagem+'</a>';
	        		if(empresaId)
	        			htmlClassificado += 	'<div class="ads-empresa"><a href="#">'+nomeEmpresa+'</a></div>;';

	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .popular-ads-grid-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .popular-ads-grid-holder").fadeIn(300);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#ads-homepage .popular-ads-grid-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .popular-ads-grid-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarAnunciosPopulares(nrPage, true);
		});
	});
}

function listarUltimosAnunciosAdicionados(nrPage, show){
	var error = false;

	jQuery("#ads-homepage .latest-ads-holder").css("text-align", "center");
	jQuery("#ads-homepage .latest-ads-holder").html(htmlImgLoader);

	jQuery.getJSON(
		baseUrl+"/api/v1/classificados/ultimo/limit/12/page/"+nrPage, 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
				htmlClassificado = "<p>Nenhum anúncio encontrado.</p>";

				jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        } else{
	        	var htmlClassificado = '<div class="latest-ads-grid-holder">';
	        	var count = 1;
	        	var max_page = 0;

	        	for(var obj in classificados)
	        	{
	        		if(count == 1)
	        			max_page = classificados[obj].__max_page;

	        		var condicaoCss   = "";
	        		var tituloUrl     = utils.stringToURL(classificados[obj].titulo);
	        		var titulo 		  = classificados[obj].titulo;
	        		var valor 		  = classificados[obj].valor;
	        		var id 			  = classificados[obj].id;
	        		var imgSrc   	  = "/files/classificado/"+classificados[obj].usuario_id+"/"+id+"/"+classificados[obj].imagemNome;
	        		var imagem 		  = '<img width="270" height="220" src="'+imgSrc+'" class="attachment-270x220" alt="'+titulo+'">';
	        		var icone   	  = classificados[obj].icone;
	        		var nomeEmpresa   = classificados[obj].empresa_nome;
	        		var empresaId     = classificados[obj].empresa_id;

	        		if((count % 4) == 1){
	        			condicaoCss = "first";
	        		}

	        		
	        		htmlClassificado += '<div class="ad-box span3 latest-posts-grid '+condicaoCss+'">';
	        		htmlClassificado += 	'<a class="ad-image" href="/classificado/'+tituloUrl+'/'+id+'" title="'+titulo+'">'+imagem+'</a>';
	        		if(empresaId)
	        			htmlClassificado += 	'<div class="ads-empresa"><a href="#">'+nomeEmpresa+'</a></div>;';

	        		htmlClassificado += 	'<div class="add-price"><span>R$ '+valor+'</span></div>';
	        		htmlClassificado += 	'<div class="post-title-cat">';
	        		htmlClassificado += 		'<div class="ad-category">';
	        		htmlClassificado += 			'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 		'<div class="post-title">';
	        		htmlClassificado += 			'<a href="/classificado/'+tituloUrl+'/'+id+'">'+titulo+'</a>';
	        		htmlClassificado += 		'</div>';
	        		htmlClassificado += 	'</div>';
	        		htmlClassificado += '</div>';
					count++;
	        	}
	        	htmlClassificado += '</div>';

	        	max_page = max_page+(nrPage-1);

	        	if(max_page > 1){
	        		htmlClassificado += '<div class="pagination">';

	        		if(nrPage > 1)
	        			htmlClassificado += '<a class="prev page-numbers" href="javascript:void(0);">« Previous</a>';

	        		var dots = false;
	        		var countPrevDotPage = 0;
		        	for(var i = 1; i <= max_page; i++){
		        		if(nrPage > 4 && (nrPage -3) > i)
		        			continue;

		        		if(i == nrPage){
		        			htmlClassificado += '<span class="page-numbers current" rel-page="'+i+'">'+i+'</span>';
		        		} else if(i == max_page){
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        			htmlClassificado += '<a class="next page-numbers" href="javascript:void(0);">Next »</a>';
		        		} else if(i != 1 && ( (3/i) < 1) && !dots && countPrevDotPage >= 3){
		        			dots = true;
		        			htmlClassificado += '<span class="page-numbers dots">…</span>';
		        		} else if(!dots){
		        			countPrevDotPage++;
		        			htmlClassificado += '<a class="page-numbers" href="javascript:void(0);" rel-page="'+i+'">'+i+'</a>';
		        		}
		        	}

		        	htmlClassificado += '</div>';
	        	}

	        	jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado);

	        	if(show)
	        		jQuery("#ads-homepage .latest-ads-holder").hide().html(htmlClassificado).fadeIn(300);
	        }
		}
	)
	.fail(function(){
		jQuery("#ads-homepage .latest-ads-holder").removeAttr('style');
	})
	.done(function(){
		jQuery("#ads-homepage .latest-ads-holder").find(".ad-image").each(function(){
			jQuery(this).children("img").load(function(){
			 	if (!this.complete) {
		            console.log('broken image!');
		        } else {
		            console.log("success");
		        }
			});
		});
		jQuery("#ads-homepage .latest-ads-holder").removeAttr('style');
		jQuery("#ads-homepage .latest-ads-holder .pagination a.page-numbers").on('click', function(){
			var nrPage = 0;
			nrPage = jQuery(this).attr('rel-page');

			if(nrPage === undefined)
				nrPage = parseInt(jQuery("#ads-homepage .latest-ads-holder .pagination span.current").attr('rel-page'));

			if(jQuery(this).hasClass("next"))
				nrPage++;
			else if(jQuery(this).hasClass("prev"))
				nrPage--;

			utils.scrollToAnchor(jQuery("#ads-homepage .container ul"));
			listarUltimosAnunciosAdicionados(nrPage, true);
		});
	});
}

function requisitarCategorias(){

	var categoriasJson;
	var error = false;
	jQuery.getJSON(
		baseUrl+"/api/categorias/subcategorias",
		function(categorias){

			categoriasJson = categorias;
			if(utils.isEmpty(categorias)){
				error = true;
	        }
		}
	)
	.fail(function(){

	})
	.done(function(){
		_listarCategorias(categoriasJson);
		_popularComboboxCategoria(categoriasJson);
	});
}

function _popularComboboxCategoria(categorias)
{
	var htmlCategoria = "<option value=''>Categoria...</option>";
	var count = 1;

	for(var categoriaJson in categorias)
	{
		var subcategorias = categorias[categoriaJson].subcategorias;

		if(utils.isEmpty(subcategorias))
			continue;

		var categoria_nome = categorias[categoriaJson].nome;
		var categoria_id   = categorias[categoriaJson].id;

		htmlCategoria += 	'<option value="c_'+categoria_id+'">'+categoria_nome+'</option>';

		for(var subcategoriaJson in subcategorias){
			var subcategoria_id   = subcategorias[subcategoriaJson].subcategoria_id
			var subcategoria_nome = subcategorias[subcategoriaJson].subcategoria_nome;

			htmlCategoria += '<option value="s_'+subcategoria_id+'">- '+subcategoria_nome+'</option>';
		}
	}

	jQuery("#edit-field-category").append(htmlCategoria);

	jQuery(".form-select").chosen();
}

function _listarCategorias(categorias)
{
	var htmlCategoria = "";
	var count = 1;

	for(var categoriaJson in categorias)
	{
		var subcategorias = categorias[categoriaJson].subcategorias;

		if(utils.isEmpty(subcategorias))
			continue;

		var condicaoCss   = "";

		if((count % 4) == 1){
			condicaoCss = "first";
		}

		htmlCategoria += '<div class="category-box span3 '+condicaoCss+'">';
		htmlCategoria += 	'<div class="category-header">';
		htmlCategoria += 		'<div class="category-icon">';
		htmlCategoria += 			'<div class="category-icon-box"><i class="'+categorias[categoriaJson].icone+'"></i></div>';
		htmlCategoria += 		'</div>';
		htmlCategoria +=	'</div>';
		htmlCategoria +=	'<div class="cat-title">';
		htmlCategoria +=		'<a href="/search/category/'+categorias[categoriaJson].id+'">';
		htmlCategoria +=			'<h4>'+categorias[categoriaJson].nome+'</h4>';
		htmlCategoria +=		'</a>';
		htmlCategoria +=	'</div>';
		htmlCategoria +=	'<div class="category-content">';
		htmlCategoria +=		'<ul>';

		for(var subcategoriaJson in subcategorias){
			htmlCategoria += '<li>';
			htmlCategoria += 	'<a href="/search/subcategory/'+subcategorias[subcategoriaJson].subcategoria_id+'" title="Encontre tudo sobre '+subcategorias[subcategoriaJson].subcategoria_nome+'">'+subcategorias[subcategoriaJson].subcategoria_nome+'</a><span class="category-counter">'+subcategorias[subcategoriaJson].record_count+'</span>';
			htmlCategoria += '</li>';
		}

		htmlCategoria +=		'</ul>';
		htmlCategoria +=	'</div>';
		htmlCategoria += '</div>';

		count++;
	}

	htmlCategoria += '<div class="clearfix"></div>';

	jQuery("#categories-homepage .full").append(htmlCategoria);
}

function popularComboboxLocais(cEstado_id)
{
	cEstado_id = (isNaN(cEstado_id) ? undefined : cEstado_id);

	var error = false;
	jQuery.getJSON(
		baseUrl+"/api/v1/classificados/locais_populares", 
		function(locais){
			var htmlCombobox;

			if(cEstado_id === undefined)
				htmlCombobox = '<option value="">Selecione um estado...</option>';
			else
				htmlCombobox = '<option value="">Escolha uma cidade...</option>'

			for(var locaisJson in locais)
			{
				var cidades = locais[locaisJson].cidades;

				if(utils.isEmpty(cidades))
					continue;

				if(cEstado_id === undefined){
					var estado_nome = locais[locaisJson].estado_nome;
					var estado_id   = locais[locaisJson].estado_id;
					htmlCombobox += 	'<option value="'+estado_id+'">'+estado_nome+'</option>';
				} else if(locais[locaisJson].estado_id == cEstado_id){
					for(var cidadesJson in cidades)
					{

						var cidade_id   = cidades[cidadesJson].cidade_id;
						var cidade_nome = cidades[cidadesJson].cidade_nome;

						htmlCombobox += '<option value="'+cidade_id+'">'+cidade_nome+'</option>';
					}
				}
			}

			if(cEstado_id === undefined){
				jQuery("#edit-ad-estado").html(htmlCombobox);
				jQuery("#edit-ad-cidade").html("<option value=''>Escolha um estado...</option>");
			} else{
				jQuery("#edit-ad-cidade").html(htmlCombobox);
			}
		}
	)
	.fail(function(){
	})
	.done(function(){
		jQuery("#edit-ad-estado").trigger("chosen:updated");
		jQuery("#edit-ad-cidade").trigger("chosen:updated");
	});
}

function listarAnunciosDestaque(){

	var error = false;
	jQuery.getJSON(
		baseUrl+"/api/v1/classificados/destaque/limit/11", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado = "";
	        	var imageSrc = null;
	        	var htmlImage = null;

	        	for(var prop in classificados)
	        	{
	        		var titulo 				= classificados[prop].titulo;
	        		var tituloUrl 			= utils.stringToURL(titulo);
	        		var nomeEmpresa 		= classificados[prop].empresa_nome;
	        		var empresaId   		= classificados[prop].empresa_id;
	        		var classificado_id 	= classificados[prop].id;
	        		var usuario_id			= classificados[prop].usuario_id;
	        		var imagemNome 			= classificados[prop].imagemNome;
	        		var icone 				= classificados[prop].icone;
	        		var valorClassificado 	= classificados[prop].valor;

	        		imageSrc  = "/files/classificado/"+usuario_id+"/"+classificado_id+"/"+imagemNome;
	        		htmlImage = '<img src="'+imageSrc+'" />';

	        		htmlClassificado += '<div class="ad-box span3">';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="ad-hover-content">';
						htmlClassificado +=	'<div class="ad-category">';
						htmlClassificado +=		'<div class="category-icon-box"><i class="'+icone+'"></i></div>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '<div class="post-title">';
						htmlClassificado +=		'<a href="javascript:void(0);">'+titulo+'</a>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '</div>';
						htmlClassificado += '<div class="add-price"><span>R$ '+valorClassificado+'</span></div>';

						if(empresaId)
							htmlClassificado += '<div class="ads-empresa"><a href="empresa/'+utils.stringToURL(nomeEmpresa)+'/'+empresaId+'">'+nomeEmpresa+'</a></div>';
					
					htmlClassificado += '</div>';
	        	}

	        	jQuery("#featured-abs #projects-carousel").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			jQuery('#projects-carousel').carouFredSel({
				auto: true,
				prev: '#carousel-prev',
				next: '#carousel-next',
				pagination: "#carousel-pagination",
				mousewheel: true,
				scroll: 1,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		}
	});
}