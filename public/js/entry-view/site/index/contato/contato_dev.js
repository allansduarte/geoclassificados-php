var utils = new Utils();
var contactForm;

var loaderImg = "<img class='loader' src='/img/ajax-loader.gif' alt='Processando...' />";

jQuery(document).ready(function(){

	contactForm = jQuery("#contact-form");

	listarAnunciosDestaque();
	listarNuvemTags();
	listarUltimosAnunciosAdicionados();
	listarCategorias();

	contactForm.on('submit', function(){

		var contactName 	= jQuery("#contactName").val();
		var email 			= jQuery("#email").val();
		var subject 	    = jQuery("#subject").val();
		var commentsText 	= jQuery("#commentsText").val();

		if(contactName == "" || email == "" || subject == "" || commentsText == ""){
			toastr['warning']('Precisamos que você informe os todos os campos do formulário de contato.', 'Ops! O formulário está incompleto.');
		} else{
			enviarEmail();
		}
		return false;
	});
});

function enviarEmail(){
	var url = "/site/index/contatar-site?";
	url   	+= contactForm.serialize();

	jQuery.ajax({
        url : url,
        beforeSend : function(){
        	jQuery("form#contact-form input.input-submit").hide();
        	jQuery("form#contact-form .btn-container").append(loaderImg);
        },
        success : function(data) {
            toastr['success']('Obrigado por entrar em contato conosco.', 'E-mail enviado!');
        }
    })
    .done(function()
    {
    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
    	/*var responseText = jqXHR.responseText;
    	try{
    		responseText = JSON.parse(responseText);
    	} catch(e){
    		jQuery("form#register .btn-container .loader").remove();
    		jQuery("form#register #edit-submit").fadeIn(300);
    		return false;
    	}*/

    	jQuery("form#contact-form .btn-container .loader").remove();
        jQuery("form#contact-form input.input-submit").fadeIn(300);

        toastr['warning']('Ocorreu um problema ao enviar o e-mail, tente novamente ou tente mais tarde.', 'E-mail não enviado');
    });
}

function listarCategorias(){

	jQuery.getJSON(
		"/api/categorias/record-count", 
		function(categorias){

			if(utils.isEmpty(categorias)){

				jQuery("#encontre-por-categoria").parent().hide();
	        } else{
	        	var htmlCategoria = "";
	        	count = 1;
	        	for(var obj in categorias)
	        	{
	        		htmlCategoria += '<li>';
	        		htmlCategoria += '<div class="category-icon-box"><i class="'+categorias[obj].icone+'"></i></div>';
	        		htmlCategoria += '<a href="/search/category/'+categorias[obj].id+'" title="Encontre tudo para '+categorias[obj].nome+'">'+categorias[obj].nome+'</a>';
	        		htmlCategoria += '<span class="category-counter">'+categorias[obj].record_count+'</span>';
	        		htmlCategoria += '</li>';
	        	}

	        	jQuery("#encontre-por-categoria ul").html(htmlCategoria);
	        }
		}
	)
	.fail(function(){
		jQuery("#encontre-por-categoria").parent().hide();
	})
	.done(function(){
	});
}

function listarUltimosAnunciosAdicionados(){

	jQuery.getJSON(
		"/api/v1/classificados/ultimo/limit/4", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				jQuery(".jw-recent-posts-widget").parent().parent().hide();
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var descricao 		= jQuery("<div>").html(classificados[obj].descricao).text();
	        		descricao     		= utils.limparHTML(descricao);
	        		descricao 	  		= utils.resumeText(descricao, 44);
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage 		= '<img class="widget-ad-image" src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);

	        		htmlClassificado += '<li class="widget-ad-list latestads-widget">';
						htmlClassificado += '<a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<span class="widget-ad-list-content">';
						htmlClassificado += '<span class="widget-ad-list-content-title"><a href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+classificados[obj].titulo+'</a></span>';
						htmlClassificado += '<p class="add-price">R$ '+classificados[obj].valor+'</p>';
						htmlClassificado += '<p>'+descricao+'</p>';
					htmlClassificado += '</li>';
	        	}

	        	jQuery(".cat-widget-content .jw-recent-posts-widget ul").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
		jQuery(".jw-recent-posts-widget").parent().parent().hide();
	})
	.done(function(){
	});
}

function listarNuvemTags(){

	jQuery.getJSON(
		"/api/v1/classificados/tag/limit/10", 
		function(tags){

			if(utils.isEmpty(tags)){

				jQuery("#tagcloud").hide();
	        } else{
	        	var htmlCloudTags = "";
	        	for(var obj in tags)
	        	{
	        		var nomeTag = tags[obj];
	        		htmlCloudTags += '<a href="/search/tag/'+nomeTag+'" title="'+nomeTag+'">'+nomeTag+'</a>'
	        	}

	        	jQuery("#tagcloud .tagcloud").html(htmlCloudTags);
	        }
		}
	)
	.fail(function(){
		jQuery("#tagcloud").hide();
	})
	.done(function(){
	});
}

function listarAnunciosDestaque(){

	var error = false;
	jQuery.getJSON(
		"/api/v1/classificados/destaque/limit/11", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var classificado_id	= classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		var htmlImage 			= '<img src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);
	        		var nomeEmpresa 	= classificados[obj].empresa_nome;
	        		var empresaId   	= classificados[obj].empresa_id;

	        		htmlClassificado += '<div class="ad-box span3">';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="ad-hover-content">';
						htmlClassificado +=	'<div class="ad-category">';
						htmlClassificado +=		'<div class="category-icon-box"><i class="'+classificados[obj].icone+'"></i></div>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '<div class="post-title">';
						htmlClassificado +=		'<a href="javascript:void(0);">'+classificados[obj].titulo+'</a>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '</div>';
						htmlClassificado += '<div class="add-price"><span>R$ '+classificados[obj].valor+'</span></div>';

						if(empresaId)
							htmlClassificado += '<div class="ads-empresa"><a href="empresa/'+utils.stringToURL(nomeEmpresa)+'/'+empresaId+'">'+nomeEmpresa+'</a></div>';
					
					htmlClassificado += '</div>';
	        	}

	        	jQuery("#featured-abs #projects-carousel").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			jQuery('#projects-carousel').carouFredSel({
				auto: true,
				prev: '#carousel-prev',
				next: '#carousel-next',
				pagination: "#carousel-pagination",
				mousewheel: true,
				scroll: 1,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		}
	});
}