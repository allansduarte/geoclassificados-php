var utils = new Utils();

jQuery(document).ready(function(){


	listarAnunciosDestaqueCarrousel();
	requisitarCategorias();
});

function requisitarCategorias(){

	var categoriasJson;
	var error = false;
	jQuery.getJSON(
		"/api/categorias/subcategorias/status/0",
		function(categorias){

			categoriasJson = categorias;
			if(utils.isEmpty(categorias)){
				error = true;
	        }
		}
	)
	.fail(function(){

	})
	.done(function(){
		_listarCategorias(categoriasJson);
	});
}

function _listarCategorias(categorias)
{
	var htmlCategoria = "";
	var count = 1;

	for(var categoriaJson in categorias)
	{
		var subcategorias = categorias[categoriaJson].subcategorias;

		if(utils.isEmpty(subcategorias))
			continue;

		var condicaoCss   = "";

		if((count % 4) == 1){
			condicaoCss = "first";
		}

		htmlCategoria += '<div class="category-box span3 '+condicaoCss+'">';
		htmlCategoria += 	'<div class="category-header">';
		htmlCategoria += 		'<div class="category-icon">';
		htmlCategoria += 			'<div class="category-icon-box"><i class="'+categorias[categoriaJson].icone+'"></i></div>';
		htmlCategoria += 		'</div>';
		htmlCategoria +=	'</div>';
		htmlCategoria +=	'<div class="cat-title">';
		htmlCategoria +=		'<a href="/search/category/'+categorias[categoriaJson].id+'">';
		htmlCategoria +=			'<h4>'+categorias[categoriaJson].nome+'</h4>';
		htmlCategoria +=		'</a>';
		htmlCategoria +=	'</div>';
		htmlCategoria +=	'<div class="category-content">';
		htmlCategoria +=		'<ul>';

		for(var subcategoriaJson in subcategorias){
			htmlCategoria += '<li>';
			htmlCategoria += 	'<a href="/search/subcategory/'+subcategorias[subcategoriaJson].subcategoria_id+'" title="Encontre tudo sobre '+subcategorias[subcategoriaJson].subcategoria_nome+'">'+subcategorias[subcategoriaJson].subcategoria_nome+'</a><span class="category-counter">'+subcategorias[subcategoriaJson].record_count+'</span>';
			htmlCategoria += '</li>';
		}

		htmlCategoria +=		'</ul>';
		htmlCategoria +=	'</div>';
		htmlCategoria += '</div>';

		count++;
	}

	htmlCategoria += '<div class="clearfix"></div>';

	jQuery("#categories-homepage .full").append(htmlCategoria);
}

function listarAnunciosDestaqueCarrousel(){

	var error = false;
	jQuery.getJSON(
		"/api/v1/classificados/destaque/limit/11", 
		function(classificados){

			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado = "";
	        	for(var obj in classificados)
	        	{
	        		var classificado_id = classificados[obj].id;
	        		var imgSrc  	  	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificado_id+"/"+classificados[obj].imagemNome;
	        		htmlImage 			= '<img src="'+imgSrc+'" />';
	        		var tituloUrl 		= utils.stringToURL(classificados[obj].titulo);
	        		var nomeEmpresa 	= classificados[obj].empresa_nome;
	        		var empresaId   	= classificados[obj].empresa_id;

	        		htmlClassificado += '<div class="ad-box span3">';
						htmlClassificado += '<a class="ad-image" href="/classificado/'+tituloUrl+'/'+classificado_id+'" title="'+classificados[obj].titulo+'">'+htmlImage+'</a>';		    			
						htmlClassificado += '<div class="ad-hover-content">';
						htmlClassificado +=	'<div class="ad-category">';
						htmlClassificado +=		'<div class="category-icon-box"><i class="'+classificados[obj].icone+'"></i></div>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '<div class="post-title">';
						htmlClassificado +=		'<a href="javascript:void(0);">'+classificados[obj].titulo+'</a>';
						htmlClassificado +=	'</div>';
						htmlClassificado += '</div>';
						htmlClassificado += '<div class="add-price"><span>R$ '+classificados[obj].valor+'</span></div>';

						if(empresaId)
							htmlClassificado += '<div class="ads-empresa"><a href="empresa/'+utils.stringToURL(nomeEmpresa)+'/'+empresaId+'">'+nomeEmpresa+'</a></div>';
					
					htmlClassificado += '</div>';
	        	}

	        	jQuery("#featured-abs #projects-carousel").html(htmlClassificado);
	        }
		}
	)
	.fail(function(){
	})
	.done(function(){

		if(!error){
			jQuery('#projects-carousel').carouFredSel({
				auto: true,
				prev: '#carousel-prev',
				next: '#carousel-next',
				pagination: "#carousel-pagination",
				mousewheel: true,
				scroll: 1,
				swipe: {
					onMouse: true,
					onTouch: true
				}
			});
		}
	});
}