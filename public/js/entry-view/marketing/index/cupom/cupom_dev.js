toastr.options = {
	"closeButton": true,
	"debug": false,
	"positionClass": "toast-top-right",
	"onclick": null,
	"showDuration": "5000",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}
var htmlImgLoader = "<img id='loader' src='/img/ajax-loader.gif' width='24' height='24'>";
var effectTimeDelay 	  = 600;

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();

	$form   = $("#cupom");

	$form.on("submit", function(e){
		e.preventDefault();

		$emailValor = $("#email").val();
		if($emailValor == "" || !isEmail($emailValor)){
			toastr['info']('Informe um e-mail válido para ganhar o cupom.', 'Ops! Informe um e-mail válido.');
			return false;
		}

		$btnSubmit = $(this).children("input");
		$btnSubmit.attr("disabled", true);

		$sectionCupom = $(".cupom-container").parent().parent();

		$.ajax({
			url: "marketing/index/gerar-cupom/email/"+$emailValor,
			beforeSend: function(){
		        $sectionCupom.fadeOut(effectTimeDelay, function(){
		        	$btnSubmit.hide();
		        	$form.after(htmlImgLoader);
		        });
			},
			success: function(data){
				$nrCupons = $("h2 span.img-circle");
				var token = JSON.parse(data);
				$sectionCupom.find(".cupom-token").each(function(i){
					$(this).html(token[i]);
				});

				$("#loader").remove();

				$nrCupons.html(parseInt($nrCupons.text()) - 1);
				$btnSubmit.removeAttr("disabled").fadeIn(effectTimeDelay, function(){
					$sectionCupom.fadeIn(effectTimeDelay);
				});

				toastr['success']('Guarde o seu cupom no e-mail e utilize na plataforma do Tem Pra Festa.', 'CUPOM ENVIADO POR E-MAIL');
			},
			error: function(request, status){
				var textStatusJson = JSON.parse(request.responseText);
				var error = textStatusJson.error;

				$("#loader").remove();
				$btnSubmit.removeAttr("disabled").fadeIn(effectTimeDelay, function(){
					$sectionCupom.fadeIn(effectTimeDelay);
				});

				if(error == 1)
					toastr['warning']('Já foi gerado cupom para este e-mail. Por favor, verifique os cupons no seu e-mail.', 'E-mail já cadastrado');
				else
					toastr['warning']('Não se preocupe que a nossa equipe já foi avisada automaticamente sobre este problema.', 'Ops! Erro ao gerar os cupons.');
	        }
		});
	});
});

function isEmail(email)
{
  var er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
  
  if(er.exec(email))
	{
	  return true;
	} else {
	  return false;
	}
}