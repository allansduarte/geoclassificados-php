var form;
var loaderImg = "<img class='loader' src='/img/ajax-loader.gif' alt='Processando...' />";

jQuery(document).ready(function(){
    form = jQuery("#lancamentoSite");

    jQuery(".call-action a").on("click", function(){
        jQuery(this).hide();
        jQuery("#acompanhar").fadeIn(600, function(){
            form.submit(function(){
                console.log(jQuery("#email").val());
                if(jQuery("#email").val() != "")
                    acompanhar();
                else toastr['warning']('Informe um e-mail válido para acompanhar o lançamento do melhor site de anúncios especializado em festas e eventos!.', 'Ops! Informe um e-mail válido.');
                return false;
            });
        });
    });

    jQuery('#layerslider').layerSlider({
        skinsPath : '/js/theme/marketing/layerslider/skins/',
        skin : 'v5',
        pauseOnHover : false,
        thumbnailNavigation : 'always',
        hoverPrevNext : false,
        autoPlayVideos : false,
        navPrevNext    : false,
        responsive : true,
        responsiveUnder : 960
    });

    var md = new MobileDetect(window.navigator.userAgent);
    if(md.mobile())
        jQuery("header ul li.linkedin").after('<li class="whatsapp"><a href="whatsapp://send?text=http://www.temprafesta.com.br"><i class="flaticon flaticon-whatsapp"></i></a></li>');
});

function acompanhar(){
    var url = "/marketing/index/lancamento-add?";
    url     += form.serialize();

    jQuery.ajax({
        url : url,
        beforeSend : function(){
            jQuery("form#lancamentoSite").hide();
            jQuery("#acompanhar").append(loaderImg);
        },
        success : function(data) {
            toastr['success']('Em breve será enviado um e-mail, avisando sobre o lançamento. Fique de olho!', 'Cadastro efetuado!');
        }
    })
    .done(function()
    {
        jQuery("#acompanhar .loader").remove();
        jQuery("form#lancamentoSite").fadeIn(300);
    })
    .fail(function(jqXHR, status)
    {
        var responseText = jqXHR.responseText;
        
        if(responseText == "1"){
            var email = form.children("#email").val();
            toastr['info']('O e-mail '+email+' já está registrado para acompanhar o lançamento do Temprafesta.', 'Ops! E-mail já registrado!');
        } else{
            toastr['warning']('Ocorreu um problema ao cadastrar o e-mail informado, tente novamente, ou tente mais tarde.', 'Ops! Ocorreu um problema...');
        }

        jQuery("#acompanhar .loader").remove();
        jQuery("form#lancamentoSite").fadeIn(300);
    });
}