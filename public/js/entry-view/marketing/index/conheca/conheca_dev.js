$(document).ready(function(){
  $("#conheca-menu .navbar-right li a").on("click", function(e){
    e.preventDefault();
    if($(this).parent().hasClass("share"))
        return false;

    var left = (screen.width/2)-(400/2);
    var top = (screen.height/2)-(600/2);
    var attrURL = $(this).attr("href");
    window.open( attrURL, "Compartilhando", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=600, top="+top+", left="+left);
  });
 
  $('[data-toggle="tooltip"]').tooltip();

  var frasesGeekTabProduto = ["Você conhece os meus dois irmãos?", "Eles estão espalhados nesta tela.", "Entendeu tudo sobre a vantagem de produtos? Então conheça meu irmão na guia Serviço..."];
  var indiceFraseGeekTabProduto = 0;
  $elPersonagemTabProduto = $("#product-tab .tpfGeek");
  $elPersonagemTabProduto.tooltip({
    title: "Olá!",
    placement: "right",
    trigger: "click | hover"
  });
  $elPersonagemTabProduto.on("show.bs.tooltip", function(){
    if($elPersonagemTabProduto.hasClass("infinite"))
      $elPersonagemTabProduto.removeClass("infinite");
  });
  $elPersonagemTabProduto.on("hidden.bs.tooltip", function(){
    if( indiceFraseGeekTabProduto == frasesGeekTabProduto.length) indiceFraseGeekTabProduto = 0;
    $elPersonagemTabProduto.attr("data-original-title", frasesGeekTabProduto[indiceFraseGeekTabProduto]);
    indiceFraseGeekTabProduto++;
  });

  var frasesGeekTabServico = ["Aqui eu vou falar sobre como alcançar clientes para você...", "Legal as vantagens né? Quer saber mais?", "Eu tenho mais um irmão... Aqui do ladinho, é só clicar na guia Empresa..."];
  var indiceFraseGeekTabServico = 0;
  $elPersonagemTabServico = $("#service-tab .tpfGeek");
  $elPersonagemTabServico.tooltip({
    title: "Então você já conheceu meu irmão na guia Produto? Nós somos gêmeos...",
    placement: "left",
    trigger: "click | hover"
  });
  $elPersonagemTabServico.on("show.bs.tooltip", function(){
    if($elPersonagemTabServico.hasClass("infinite"))
      $elPersonagemTabServico.removeClass("infinite");
  });
  $elPersonagemTabServico.on("hidden.bs.tooltip", function(){
    if( indiceFraseGeekTabServico == frasesGeekTabServico.length) indiceFraseGeekTabServico = 0;
    $elPersonagemTabServico.attr("data-original-title", frasesGeekTabServico[indiceFraseGeekTabServico]);
    indiceFraseGeekTabServico++;
  });

  var frasesGeekTabEmpresa = ["Você tem interesse em impulsionar seu negócio? Seja ele micro, pequeno, médio ou grande?", "Também não precisa ter empresa registrada... O fato é que eu garanto, que o seu negócio vai bombar por aqui...", "Interessado (a)? Pois é, tem mais! Vá descobrir as Características."];
  var indiceFraseGeekTabEmpresa = 0;
  $elPersonagemTabEmpresa = $("#business-tab .tpfGeek");
  $elPersonagemTabEmpresa.tooltip({
    title: "Ah, meu irmão gêmeo comentou sobre as vantagens de serviços né? Agora eu vou falar sobre outro assunto mais legal...",
    placement: "right",
    trigger: "click | hover"
  });
  $elPersonagemTabEmpresa.on("show.bs.tooltip", function(){
    if($elPersonagemTabEmpresa.hasClass("infinite"))
      $elPersonagemTabEmpresa.removeClass("infinite");
  });
  $elPersonagemTabEmpresa.on("hidden.bs.tooltip", function(){
    if( indiceFraseGeekTabEmpresa == frasesGeekTabEmpresa.length) indiceFraseGeekTabEmpresa = 0;
    $elPersonagemTabEmpresa.attr("data-original-title", frasesGeekTabEmpresa[indiceFraseGeekTabEmpresa]);
    indiceFraseGeekTabEmpresa++;
  });
});

$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});

$(function() {
    $('#conheca-menu .navbar-left li a, #conheca-menu .navbar-header a, #footer .footer-content div div div ul li a').on('click', function(e) {
      var $anchor = $(this);

      if($anchor.parent().hasClass("share"))
        return false;

      if($(".navbar-toggle").is(":visible")){
        $(".navbar-menubuilder").removeClass("in").attr('aria-expanded', false);
      }

      $('html, body').stop().animate({
          scrollTop: $($anchor.attr('href')).offset().top
      }, 1500, 'easeInExpo', function(){
        if($anchor.attr("href") == "#plans" && !$("#plans .package-column").hasClass("visible"))
          $("#plans .package-column").removeClass("hiding").addClass("animated flipInY visible");
      });
      e.preventDefault();
    });

    $(".back-to-top").on('click', function(e) {
      $('html, body').stop().animate({
          scrollTop: 0
      }, 1500, 'easeInExpo');
      e.preventDefault();
    });
});

new Waypoint({
  element: document.getElementById('plans'),
  handler: function() {
    if(!$("#plans .package-column").hasClass("visible"))
      $("#plans .package-column").removeClass("hiding").addClass("animated flipInY visible");
  },
  offset: '55%'
});

new Waypoint({
  element: document.getElementById('benefits'),
  handler: function(direction) {
    $backToTop = $(".back-to-top");
    if(direction == "down"){
      if($backToTop.is(":hidden"))
        $backToTop.show();
    } else{
      if($backToTop.is(":visible"))
        $backToTop.hide();
    }
  }
});

new Waypoint({
  element: document.getElementById('footer'),
  handler: function(direction) {
    $socialWrap  = $("#footer .social-wrap");
    $logoWrapper = $("#footer .logo-wrapper img");

    if(direction == "down"){
      if(!$logoWrapper.is(":hidden"))
        $logoWrapper.removeClass("hiding").addClass("animated pulse");

      if(!$socialWrap.is(":hidden"))
        $socialWrap.removeClass("hiding").addClass("animated pulse");
    } else{
      if($socialWrap.is(":visible"))
        $socialWrap.removeClass("animated pulse").addClass("hiding");
    }
  },
  offset: '70%'
});

new Waypoint({
  element: document.getElementById('features'),
  handler: function() {
    $featureTabContent  = $("#features .tab-content");
    $featureList        = $("#features ul");

    if($featureTabContent.hasClass("hiding"))
      $featureTabContent.removeClass("hiding").addClass("animated fadeInLeft");

    if($featureList.hasClass("hiding"))
      $featureList.removeClass("hiding").addClass("animated fadeInRight");
  },
  offset: '45%'
});

new Waypoint({
  element: document.getElementById('features-list'),
  handler: function() {
    $featureListContainer  = $("#features-list .container-fluid");

    if($featureListContainer.hasClass("hiding")){
      $featureListContainer.removeClass("hiding").addClass("animated fadeInDown");
      $featureListContainer.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
          $featureListContainer.find(".feature-active").each(function(){
            if(!$(this).hasClass("animated"))
              $(this).addClass("animated rubberBand");
          });
      });
    }
  },
  offset: '73%'
});