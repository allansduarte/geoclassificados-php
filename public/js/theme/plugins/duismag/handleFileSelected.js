function HandleFileSelected(){

	this.fileType 			= undefined;
	this.messageFileType 	= undefined;
	this.outputType 		= undefined;
	this.htmlOutput 		= undefined;
	this.outputElement 		= undefined

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "5000",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	}
}

HandleFileSelected.prototype.run = function(evt, elementHTML, cssClass, size) {

	var file = evt.target.files[0];

	if(file === undefined) return false;

	var error = false;
	var messages = [];
	var imageOutput = elementHTML.siblings("output");
	var handleFileSelectedSelf = this;

	if(handleFileSelectedSelf.outputElement != undefined)
		imageOutput = handleFileSelectedSelf.outputElement;

	if(!file.type.match(this.fileType)){
		if(handleFileSelectedSelf.outputElement != undefined)
			imageOutput.html('<i class="glyphicon glyphicon-picture"></i>');//reseta a saída de imagem
		else
			imageOutput.html(null);//limpa a saída de imagem

		elementHTML.val(null);//limpa o conteúdo do input file
		toastr['warning'](this.messageFileType);

		return false;
	}

	var span;
	var paragraph;
	var reader = new FileReader();
	reader.onload = (function(f){

		return function(e){
			var html = "";
			span       = document.createElement('span');
			paragraph  = document.createElement('p');
			
			if(handleFileSelectedSelf.outputType === undefined){
				throw "Erro! O objeto outputType está com valor null. Utilize o método setOutputType(type) para solucionar o erro. Sabendo que o parâmetro passado deve ser 'default' ou 'custom'.";
			} else{
				if(handleFileSelectedSelf.outputType == "default"){
					span.innerHTML = ["<img class='"+cssClass+"' src='", e.target.result,
							  "' title='", escape(f.name), "' />"].join('');
				} else if(handleFileSelectedSelf.outputType == "custom"){
					span.innerHTML = handleFileSelectedSelf.htmlOutput;
				} else{
					throw "Erro! O objeto outputType foi definido incorretamente. O parâmetro passado deve ser 'default' ou 'custom'.";
				}
			}

			paragraph.innerHTML = escape(f.name);

			if(size === undefined)
				imageOutput.html(span).append(paragraph);
		};
	})(file);

	if(size != undefined){
		reader.onloadend = function(){
			var image = new Image();
			image.src = reader.result;

			image.onload = function(){

				if( (image.width > (size.maxWidth+3)) || (image.width < size.maxWidth) ){
					error = true;
					messages[0] = "Por favor informe uma imagem onde a largura da mesma deve ser igual a "+size.maxWidth+"px. A largura atual da imagem é "+image.width+".";
				}

				if( (image.height > (size.maxHeight+3)) || (image.height < size.maxHeight) ){
					error = true;
					messages[1] = "Por favor informe uma imagem onde a altura da mesma deve ser igual a "+size.maxHeight+"px. A altura atual da imagem é "+image.height+".";
				}

				if(error){
					imageOutput.css('display', 'none');

					for(key in messages){
						if(messages.hasOwnProperty(key)){
								toastr['warning'](messages[key]);
						}
					}

					elementHTML.val(null);
					imageOutput.html(null);
				}

				else{
					imageOutput.html(span).append(paragraph);
					imageOutput.css('display', 'block');
				}
			};
		};
	}

	reader.readAsDataURL(file);
};

HandleFileSelected.prototype.setFileType = function(type) {
	this.fileType = type;
};

HandleFileSelected.prototype.getFileType = function() {
	return this.fileType;
};

HandleFileSelected.prototype.setMessageFileType = function(message) {
	this.messageFileType = message;
};

HandleFileSelected.prototype.getMessageFileType = function() {
	return this.messageFileType;
};

HandleFileSelected.prototype.setOutputType = function(type) {
	this.outputType = type;
};

HandleFileSelected.prototype.getOutputType = function() {
	return this.outputType;
};

HandleFileSelected.prototype.setHTMLOutput = function(html) {
	this.htmlOutput = html;
};

HandleFileSelected.prototype.getHTMLOutput = function() {
	return this.htmlOutput;
};

HandleFileSelected.prototype.setOutputElement = function(element) {
	this.outputElement = element;
};

HandleFileSelected.prototype.getOutputElement = function() {
	return this.outputElement;
};