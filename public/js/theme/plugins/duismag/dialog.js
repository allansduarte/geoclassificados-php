function Dialog(){

}

Dialog.prototype.showError = function(dsTitle, dsMessage) {

	BootstrapDialog.show({
	    title: dsTitle,
	    message: dsMessage,
	    buttons: [{
	        label: 'Fechar',
	        action: function(dialogRef){
	            dialogRef.close();
	        },
	        cssClass: 'btn-danger'
	    }]
	});
};

Dialog.prototype.showDefect = function(dsTitle, dsMessage, urlButton) {
	BootstrapDialog.show({
        title: dsTitle,
        message: dsMessage,
        buttons: [{
            label: 'Vou escolher outro aplicativo',
            action: function(dialogRef){
                dialogRef.close();
            }
        }, {
            label: 'Atualizar página',
            action: function(dialogRef){
                window.location = urlButton;
            },
            cssClass: 'btn-success'
        }]
    });
};

Dialog.prototype.showDBRegisterFail = function(dsTitle, dsMessage, nameSolutionButton, urlButton) {
	BootstrapDialog.show({
        title: dsTitle,
        message: dsMessage,
        buttons: [{
            label: 'Vou escolher outro aplicativo',
            action: function(dialogRef){
                dialogRef.close();
            }
        }, {
            label: nameSolutionButton,
            action: function(dialogRef){
                window.location = urlButton;
            },
            cssClass: 'btn-success'
        }]
    });
};