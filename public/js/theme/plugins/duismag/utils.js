function Utils(){}

Utils.prototype.isEmpty = function(obj) {
	for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }
 
    return true;
};

Utils.prototype.scrollToAnchor = function(element){
	jQuery('html,body').animate({scrollTop: element.offset().top},'slow');
}

Utils.prototype.stringToURL = function(str){

    if(str === undefined || str === null)
        return str;

	str = str.toLowerCase();
	str = str.trim();
	str = str.replace(new RegExp(" ", 'g'), "-");

	str = str.replace(new RegExp("á", 'g'), "a");
    str = str.replace(new RegExp("à", 'g'), "a");
    str = str.replace(new RegExp("ã", 'g'), "a");
    str = str.replace(new RegExp("ä", 'g'), "a");
    str = str.replace(new RegExp("é", 'g'), "e");
    str = str.replace(new RegExp("è", 'g'), "e");
    str = str.replace(new RegExp("ë", 'g'), "e");
    str = str.replace(new RegExp("ê", 'g'), "e");
    str = str.replace(new RegExp("í", 'g'), "i");
    str = str.replace(new RegExp("ì", 'g'), "i");
    str = str.replace(new RegExp("ï", 'g'), "i");
    str = str.replace(new RegExp("î", 'g'), "i");
    str = str.replace(new RegExp("ó", 'g'), "o");
    str = str.replace(new RegExp("ò", 'g'), "o");
    str = str.replace(new RegExp("õ", 'g'), "o");
    str = str.replace(new RegExp("ö", 'g'), "o");
    str = str.replace(new RegExp("ô", 'g'), "o");
    str = str.replace(new RegExp("ú", 'g'), "u");
    str = str.replace(new RegExp("ù", 'g'), "u");
    str = str.replace(new RegExp("ü", 'g'), "u");
    str = str.replace(new RegExp("û", 'g'), "u");
    str = str.replace(new RegExp("ç", 'g'), "c");
    str = str.replace(new RegExp("ý", 'g'), "y");
    str = str.replace(new RegExp("ÿ", 'g'), "y");
    str = str.replace(new RegExp("´", 'g'), "");
    str = str.replace("*", "-");
    str = str.replace(new RegExp("&", 'g'), "-");
    str = str.replace(new RegExp("!", 'g'), "");
    str = str.replace(new RegExp("@", 'g'), "-");
    str = str.replace(new RegExp("#", 'g'), "");
    str = str.replace(new RegExp("$", 'g'), "");
    //str = str.replace(new RegExp("av.", 'g'), "avenida");
    str = str.replace(new RegExp("'", 'g'),"");
    str = str.replace(new RegExp(",", 'g'), ".");
    str = str.replace(new RegExp("ª", 'g'), "");
    str = str.replace(new RegExp("º", 'g'), "");

    if(str.indexOf("*") !== -1){
    	return StringToURL(str);
    }

    return str;
}

Utils.prototype.resumeText = function(string, nrChars){

    string = string.substring(0, 44);
    return string+"[...]";
}

Utils.prototype.limparHTML = function(str){

    str = str.replace(new RegExp("<ul>", 'g'), "");
    str = str.replace(new RegExp("</ul>", 'g'), "");
    str = str.replace(new RegExp("<li>", 'g'), "");
    str = str.replace(new RegExp("</li>", 'g'), "");
    str = str.replace(new RegExp("<b>", 'g'), "");
    str = str.replace(new RegExp("</b>", 'g'), "");
    str = str.replace(new RegExp("<i>", 'g'), "");
    str = str.replace(new RegExp("</i>", 'g'), "");
    str = str.replace(new RegExp("<u>", 'g'), "");
    str = str.replace(new RegExp("</u>", 'g'), "");
    str = str.replace(new RegExp("<br>", 'g'), "");
    str = str.replace(new RegExp("</br>", 'g'), "");
    str = str.replace(new RegExp("<div>", 'g'), "");
    str = str.replace(new RegExp("</div>", 'g'), "");
    str = str.replace(new RegExp("<blockquote>", 'g'), "");
    str = str.replace(new RegExp("</blockquote>", 'g'), "");
    str = str.replace(new RegExp("<h1>", 'g'), "");
    str = str.replace(new RegExp("</h1>", 'g'), "");
    str = str.replace(new RegExp("<h2>", 'g'), "");
    str = str.replace(new RegExp("</h2>", 'g'), "");
    str = str.replace(new RegExp("<h3>", 'g'), "");
    str = str.replace(new RegExp("</h3>", 'g'), "");
    str = str.replace(new RegExp("<h4>", 'g'), "");
    str = str.replace(new RegExp("</h4>", 'g'), "");
    str = str.replace(new RegExp("<h5>", 'g'), "");
    str = str.replace(new RegExp("</h5>", 'g'), "");
    str = str.replace(new RegExp("<h6>", 'g'), "");
    str = str.replace(new RegExp("</h6>", 'g'), "");
    str = str.replace(new RegExp("<span>", 'g'), "");
    str = str.replace(new RegExp("</span>", 'g'), "");

    /*if(str.indexOf("*") !== -1){
        return StringToURL(str);
    }*/

    return str;
}

Utils.prototype.queryStringParse = function(string){
    var parsed = {};
    string = (string !== undefined) ? string :  window.location.search;

    if (typeof string === "string" && string.length > 0){
      if (string[0] === '?') {
        string = string.substring(1);
      }

      string = string.split('&');

      for (var i = 0, length = string.length; i < length; i++){
        var element = string[i],
            eqPos = element.indexOf('='),
            keyValue, elValue;

        if (eqPos >= 0){
          keyValue = element.substr(0,eqPos);
          elValue = element.substr(eqPos +1);
        }
        else {
          keyValue = element;
          elValue = '';
        }

        elValue = decodeURIComponent(elValue);

        if (parsed[keyValue] === undefined){
          parsed[keyValue] = elValue;
        }
        else if (parsed[keyValue] instanceof Array) {
          parsed[keyValue].push(elValue);
        }
        else {
          parsed[keyValue] = [parsed[keyValue], elValue];
        }
      }
    }

    return parsed;
}

Utils.prototype.queryStringStringify = function(obj){
    var string = [];

    if (!!obj && obj.constructor === Object){
      for (var prop in obj){
        if (obj[prop] instanceof Array){
          for (var i = 0, length = obj[prop].length; i < length; i++){
            string.push([encodeURIComponent(prop),encodeURIComponent(obj[prop][i])].join('='));
          }
        }
        else {
          string.push([encodeURIComponent(prop),encodeURIComponent(obj[prop])].join('='));
        }
      }
    }

    return string.join('&');
}