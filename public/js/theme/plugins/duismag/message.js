function Message(){}

function _builderErrorMessage(){

	var errorMessage = [];
    errorMessage[0]  = "Ops! Ocorreram problemas na conversão do seu arquivo!";
    errorMessage[1]  = "<p>Pedimos que tente executar esta operação, novamente mais tarde. Caso o erro persista entre em contato conosco, é só clicar no botão <b>Informar Problema</b> abaixo e descrever o problema ocorrido, que retornaremos em 24h.</p><p>Obrigado pela paciência.</p><p>Equipe Duismag</p>";
    errorMessage[2]  = "Ops! Não foi possível atualizar o status da sua edição automaticamente!";
    errorMessage[3]  = "<p>O sistema Duismag tentou atualizar o status da sua edição automaticamente e não conseguiu!<br>Para resolver este problema siga os passos abaixo:<br><ul><li>1- Atualize a página;</li><li>2- Realize a publicação manualmente desta edição, que ocorreu o problema.</li></ul>Siga estes passos para que o agendamento automático de sua publicação funcione normalmente.</p>";
    errorMessage[4]  = "Ops! Não encontramos a categoria vinculada ao aplicativo selecionado!";
    errorMessage[5]  = 'Ops! Não foi possível atualizar o status da sua edição!';
    errorMessage[6]  = 'Ops! Não foi possível atualizar o status do seu aplicativo!';

    return errorMessage;
}

Message.prototype.getErrorMessage = function (id){
    
    var retorno = "";
    
    retorno = (_builderErrorMessage[id] === undefined) ? "" : _builderErrorMessage[id];
    
    return retorno;
};

function _builderInfoMessage(){

	var infoMessage = [];
    infoMessage[0]  = "Ops! Ocorreram problemas na conversão do seu arquivo!";
    infoMessage[1]  = "<p>Pedimos que tente executar esta operação, novamente mais tarde. Caso o erro persista entre em contato conosco, é só clicar no botão <b>Informar Problema</b> abaixo e descrever o problema ocorrido, que retornaremos em 24h.</p><p>Obrigado pela paciência.</p><p>Equipe Duismag</p>";
    infoMessage[2]  = "Esse problema geralmente ocorre quando a equipe Duismag está realizando manutenção em seus webservices. Tente realizar novamente esse processo mais tarde.<br><br>Agradecemos a paciência.<br><br>Atenciosamente,<br>Equipe Duismag.";
    infoMessage[3]  = "mensagem 3";

    return infoMessage;
}

Message.prototype.getInfoMessage = function (id){
    
    var retorno = "";
    
    retorno = (_builderInfoMessage[id] === undefined) ? "" : _builderInfoMessage[id];
    
    return retorno;
};

function _builderSuccessMessage(){

	var successMessage = [];
    successMessage[0]  = "Ops! Ocorreram problemas na conversão do seu arquivo!";
    successMessage[1]  = "<p>Pedimos que tente executar esta operação, novamente mais tarde. Caso o erro persista entre em contato conosco, é só clicar no botão <b>Informar Problema</b> abaixo e descrever o problema ocorrido, que retornaremos em 24h.</p><p>Obrigado pela paciência.</p><p>Equipe Duismag</p>";
    successMessage[2]  = "mensagem 2";
    successMessage[3]  = "mensagem 3";

    return successMessage;
}

Message.prototype.getSuccessMessage = function (id){
    
    var retorno = "";
    
    retorno = (_builderSuccessMessage[id] === undefined) ? "" : _builderSuccessMessage[id];
    
    return retorno;
};

function _builderValidationMessage(){

    var validationMessage = [];
    validationMessage[0]  = "Por favor, informe o campo Nome da Edição.";
    validationMessage[1]  = "Por favor, informe o campo Número da Edição.";
    validationMessage[2]  = "Por favor, envie um arquivo de extensão PDF.";
    validationMessage[3]  = "Informe a categoria do seu aplicativo.";
    validationMessage[4]  = "Informe a subcategoria do seu aplicativo.";
    validationMessage[5]  = "Arquivo informado não é suportado. Por favor informe um arquivo do tipo PDF.";
    validationMessage[6]  = "Arquivo informado não é suportado. Por favor informe um arquivo do tipo imagem PNG, JPG, JPEG ou BITMAP.";
    validationMessage[7]  = "Informe o nome do seu aplicativo.";
    validationMessage[8]  = "Descreva um pouco sobre o seu aplicativo.";
    validationMessage[9]  = "Informe o campo Ícone.";
    validationMessage[10] = "Informe o campo Logo.";
    validationMessage[11] = "Informe uma URL válida. Exemplo: http://twitter.com/duismag";
    validationMessage[12] = "Informe uma URL válida. Exemplo http://facebook.com/duismag";
    validationMessage[13] = "Informe no mínimo uma palavra chave.";
    validationMessage[14] = "Informe a categoria do seu aplicativo.";
    validationMessage[15] = "Arquivo informado não é suportado. Por favor informe um arquivo do tipo CSV.";

    return validationMessage;
}

Message.prototype.getValidationMessage = function (id){
    
    var retorno = "";

    retorno = (_builderValidationMessage()[id] === undefined) ? "" : _builderValidationMessage()[id];
    
    return retorno;
};