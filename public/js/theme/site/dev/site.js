var utils = new Utils();
var baseUrl = "http://temprafesta.web1313.kinghost.net";

jQuery(document).ready(function(){

	listarAnunciosDestaqueFooter();
});

function listarAnunciosDestaqueFooter(){

	var error = false;
	jQuery.ajax({
		url: baseUrl+"/api/v1/classificados/destaque/limit/6",
		async: true,
		data: "get",
		beforeSend: function(){},
		success: function(classificados){
			if(utils.isEmpty(classificados)){

				error = true;
	        } else{
	        	var htmlClassificado  = '<ul>';
	        	for(var obj in classificados)
	        	{
	        		var titulo = classificados[obj].titulo;
	        		imageSrc	= "/files/classificado/"+classificados[obj].usuario_id+"/"+classificados[obj].id+"/"+classificados[obj].imagemNome;
	        		htmlImage = '<img alt="'+titulo+'" class="widget-ad-image" src="'+imageSrc+'" />';
	        		var tituloUrl = utils.stringToURL(titulo);
	        		var classificado_id = classificados[obj].id;

        			htmlClassificado += '<li class="widget-ad-list ad-box featurads-widget">';
        				htmlClassificado += '<a class="featured-img" href="/classificado/'+tituloUrl+'/'+classificados[obj].id+'">'+htmlImage+'</a>';
        				htmlClassificado += '<div class="ad-hover-content">';
        					htmlClassificado += '<div class="ad-category">';
        						htmlClassificado += '<div class="category-icon-box"><i class="fa fa-heart"></i></div>';
        					htmlClassificado += '</div>';
        					htmlClassificado += '<div class="post-title">';
								htmlClassificado += '<a href="http://demo.designinvento.net/classiads/health-diet/">Health &amp; Diet</a>';
							htmlClassificado += '</div>';
        				htmlClassificado += '</div>';
        			htmlClassificado += '</li>';
	        	}
	        	htmlClassificado += '</ul>';

	        	jQuery("footer #block-two .jw-recent-posts-widget").html(htmlClassificado);
	        }
		},
		error: function(request, status){

		}
	})
	.fail(function(){
		jQuery("footer #block-two").hide();
	})
	.done(function(){
	});
}