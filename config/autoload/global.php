<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


return array(
  'service_manager' => array(
    'factories' => array(
      'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
  ),
    ),
  'db' => array(
     'driver'         => 'Pdo',
    //'dsn'            => 'mysql:dbname=temprafesta;host=179.188.20.75',
     'dsn'            => 'mysql:dbname=temprafesta;host=191.6.194.21',
    'driver_options' => array(
      PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    ),
  ),
  'acl' => array(
      'roles' => array(
          'visitante'   => null,
          'anunciante'  => 'visitante',
          'administrador'  => 'anunciante',
      ),
      'resources' => array(
          'Site\Controller\Index.index',
          'Site\Controller\Index.register',
          'Site\Controller\Index.sobre',
          'Site\Controller\Index.categorias',
          'Site\Controller\Index.planos-de-anuncio',
          'Site\Controller\Index.contato',

          'Site\Controller\Classificado.index',
          'Site\Controller\Classificado.contatar-anunciante',
          'Site\Controller\Classificado.contatar-empresa',
          'Site\Controller\Index.contatar-site',
          'Site\Controller\Classificado.empresa',

          'Site\Controller\Search.all',
          'Site\Controller\Search.category',
          'Site\Controller\Search.subcategory',
          'Site\Controller\Search.tag',

          'Site\Controller\User.save',
          'Site\Controller\User.activation',
          'Site\Controller\User.resend-email',
          'Site\Controller\User.esqueciMinhaSenha',
          'Site\Controller\User.esqueciMinhaSenhaGerar',

          'System\Controller\Index.index',
          'System\Controller\Index.anuncios',
          'System\Controller\Index.anuncios-empresariais',
          'System\Controller\Index.favoritos',
          'System\Controller\Index.mensagens',

          'System\Controller\Categoria.cadastro',
          'System\Controller\Categoria.save',
          'System\Controller\Categoria.consulta',
          'System\Controller\Categoria.delete',

          'System\Controller\Subcategoria.cadastro',
          'System\Controller\Subcategoria.save',
          'System\Controller\Subcategoria.consulta',
          'System\Controller\Subcategoria.delete',
          'System\Controller\Subcategoria.ws',

          'System\Controller\MidiaSocial.cadastro',
          'System\Controller\MidiaSocial.save',
          'System\Controller\MidiaSocial.consulta',
          'System\Controller\MidiaSocial.delete',

          'System\Controller\Perfil.index',
          'System\Controller\Perfil.save',
          'System\Controller\Perfil.alterar-perfil',
          'System\Controller\Perfil.ws',

          'System\Controller\Classificado.normal',
          'System\Controller\Classificado.empresa',
          'System\Controller\Classificado.save',
          'System\Controller\Classificado.normal-delete',
          'System\Controller\Classificado.normal-favorito-delete',
          'System\Controller\Classificado.empresa-delete',
          'System\Controller\Classificado.retorno-pagamento-destaque',
          'System\Controller\Classificado.destacar-anuncio-normal',

          'System\Controller\Empresa.cadastro',
          'System\Controller\Empresa.consulta',
          'System\Controller\Empresa.save',
          'System\Controller\Empresa.delete',

          'System\Controller\Filial.cadastro',
          'System\Controller\Filial.consulta',
          'System\Controller\Filial.save',
          'System\Controller\Filial.delete',

          'System\Controller\Favoritos.add',
          'System\Controller\Favoritos.remove',

          'System\Controller\Auth.index',
          'System\Controller\Auth.login',
          'System\Controller\Auth.logout',

          'System\Controller\Questionario.cadastro',

          'Api\Controller\Cidades.estado',
          'Api\Controller\Cidades.index',
          'Api\Controller\Categorias.index',
          'Api\Controller\Categorias.subcategorias',
          'Api\Controller\Subcategorias.combobox',
          'Api\Controller\Subcategorias.index',
          'Api\Controller\Subcategorias.categoria',
          'Api\Controller\Classificados.ultimo',
          'Api\Controller\Classificados.relacionado',
          'Api\Controller\Classificados.destaque',
          'Api\Controller\Classificados.empresa',

          'Marketing\Controller\Index.conheca',
          'Marketing\Controller\Index.cupom',
          'Marketing\Controller\Index.gerar-cupom',
          
          'DoctrineORMModule\Yuml\YumlController.index'
      ),
      'privilege' => array(
          'visitante' => array(
              'allow' => array(
                  'Site\Controller\Index.index',
                  'Site\Controller\Index.register',
                  'Site\Controller\Index.sobre',
                  'Site\Controller\Index.categorias',
                  'Site\Controller\Index.planos-de-anuncio',
                  'Site\Controller\Index.contato',

                  'Site\Controller\Classificado.index',
                  'Site\Controller\Classificado.contatar-anunciante',
                  'Site\Controller\Classificado.contatar-empresa',
                  'Site\Controller\Index.contatar-site',
                  'Site\Controller\Classificado.empresa',

                  'Site\Controller\Search.all',
                  'Site\Controller\Search.category',
                  'Site\Controller\Search.subcategory',
                  'Site\Controller\Search.tag',

                  'Site\Controller\User.save',
                  'Site\Controller\User.activation',
                  'Site\Controller\User.resend-email',
                  'Site\Controller\User.esqueciMinhaSenha',
                  'Site\Controller\User.esqueciMinhaSenhaGerar',

                  'System\Controller\Auth.index',
                  'System\Controller\Auth.login',
                  'System\Controller\Auth.logout',

                  'System\Controller\Perfil.ws',

                  'System\Controller\Subcategoria.ws',

                  'System\Controller\Classificado.retorno-pagamento-destaque',

                  'Api\Controller\Cidades.index',
                  'Api\Controller\Cidades.estado',

                  'Api\Controller\Categorias.index',
                  'Api\Controller\Categorias.subcategorias',

                  'Api\Controller\Subcategorias.index',
                  'Api\Controller\Subcategorias.categoria',
                  'Api\Controller\Subcategorias.combobox',

                  'Api\Controller\Classificados.ultimo',
                  'Api\Controller\Classificados.relacionado',
                  'Api\Controller\Classificados.destaque',
                  'Api\Controller\Classificados.empresa',

                  'Marketing\Controller\Index.conheca',
                  'Marketing\Controller\Index.cupom',
                  'Marketing\Controller\Index.gerar-cupom',

                  'DoctrineORMModule\Yuml\YumlController.index'
              )
          ),
          'anunciante' => array(
              'allow' => array(
                'System\Controller\Index.index',
                'System\Controller\Index.anuncios',
                'System\Controller\Index.anuncios-empresariais',
                'System\Controller\Index.favoritos',
                'System\Controller\Index.mensagens',

                'System\Controller\Perfil.index',
                'System\Controller\Perfil.save',
                'System\Controller\Perfil.alterar-perfil',

                'System\Controller\Classificado.normal',
                'System\Controller\Classificado.empresa',
                'System\Controller\Classificado.save',
                'System\Controller\Classificado.normal-delete',
                'System\Controller\Classificado.normal-favorito-delete',
                'System\Controller\Classificado.empresa-delete',
                'System\Controller\Classificado.destacar-anuncio-normal',

                'System\Controller\Empresa.cadastro',
                'System\Controller\Empresa.consulta',
                'System\Controller\Empresa.save',

                'System\Controller\Filial.cadastro',
                'System\Controller\Filial.consulta',
                'System\Controller\Filial.save',

                'System\Controller\Favoritos.add',
                'System\Controller\Favoritos.remove',
              )
          ),
          'administrador' => array(
              'allow' => array(
                  'System\Controller\Categoria.cadastro',
                  'System\Controller\Categoria.save',
                  'System\Controller\Categoria.consulta',
                  'System\Controller\Categoria.delete',

                  'System\Controller\Subcategoria.cadastro',
                  'System\Controller\Subcategoria.save',
                  'System\Controller\Subcategoria.consulta',
                  'System\Controller\Subcategoria.delete',

                  'System\Controller\MidiaSocial.cadastro',
                  'System\Controller\MidiaSocial.save',
                  'System\Controller\MidiaSocial.consulta',
                  'System\Controller\MidiaSocial.delete',

                  'System\Controller\Questionario.cadastro'
              )
          ),
      )
  ),
  'cache' => array(
    'adapter' => 'memory',
    'ttl'     => 100,
    'plugins' => array(
        'exception_handler' => array('throw_exceptions' => false),
        'Serializer'
    ),
    'enable' => false,
  ),
  /*'doctrine' => array(
    'connection'  => array(
      'driver'   => 'pdo_mysql',
      //'host'     => '179.188.20.75',
      'host'     => 'localhost',
      'port'     => '3306',
      'dbname'   => 'temprafesta',
      'charset'  => 'utf8',
      'driverOptions' => array(
        'x_reconnect_attempts' => 2,
      ),
    )
  ),*/
);