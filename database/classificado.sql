-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2014 at 10:18 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `temprafesta`
--

-- --------------------------------------------------------

--
-- Table structure for table `classificado`
--

CREATE TABLE IF NOT EXISTS `classificado` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `valor` decimal(12,2) NOT NULL,
  `status` int(11) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `subcategoria_id` bigint(20) NOT NULL,
  `usuario_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `classificado`
--

INSERT INTO `classificado` (`id`, `titulo`, `descricao`, `valor`, `status`, `tipo`, `subcategoria_id`, `usuario_id`, `created`, `modified`) VALUES
(9, 'teste', 'teste bla bla bla', 29.99, 1, '1', 6, 1, '2014-11-09 18:57:33', NULL),
(10, 'Limousine clássica', 'Limousine clássica para casamentos.\r\nLimousine de cor branca do ano 1973 disponível para aluguem em casamentos. O motorista está incluso no orçamento.', 39.00, 1, '1', 9, 1, '2014-11-09 19:21:36', NULL),
(11, 'Vestido de Ouro maciço', 'Vestido da índia caríssissímo', 9999999.99, 1, '1', 4, 1, '2014-11-09 19:31:45', NULL),
(12, 'Casa de luxo', 'Casa mansão', 9999999.99, 1, '0', 9, 1, '2014-11-09 19:34:48', NULL),
(13, 'teste', 'teste', 9999999999.99, 1, '0', 6, 1, '2014-11-09 19:35:38', NULL),
(14, 'Mansão', 'Teste', 9999999999.99, 1, '1', 9, 1, '2014-11-09 19:36:42', NULL),
(15, 'teste', 'teste', 9999999999.99, 1, '0', 9, 1, '2014-11-09 19:37:23', NULL),
(16, 'teste', 'teste', 9999999999.99, 1, '0', 9, 1, '2014-11-09 19:39:07', NULL),
(17, 'teste', 'teste', 1234567890.12, 1, '0', 9, 1, '2014-11-09 19:55:56', NULL),
(18, 'teste', 'teste', 1234567890.12, 1, '0', 9, 1, '2014-11-09 19:56:12', NULL),
(20, 'teste', 'teste', 29.99, 1, '0', 9, 1, '2014-11-09 21:29:18', NULL),
(21, 'teste', 'teste', 29.99, 1, '1', 2, 1, '2014-11-09 22:51:21', NULL),
(22, 'festança', 'Teste', 290.00, 1, '0', 10, 1, '2014-11-10 19:21:53', NULL),
(23, 'Limousine', 'Teste', 290.00, 1, '1', 9, 1, '2014-11-10 20:12:41', NULL),
(24, 'Limousine clássica', 'teste', 390.00, 1, '0', 9, 1, '2014-11-17 13:18:08', NULL),
(25, 'Duplex em Balneário Camboriú para Temporada de Carnaval', 'teste', 990.00, 1, '1', 10, 1, '2014-12-09 00:00:15', NULL),
(26, 'Limousine clássica', 'blablabla', 390.00, 1, '1', 9, 1, '2014-12-09 00:07:56', NULL),
(27, 'Aluguel de casa para temporada', 'casa para 4 pessoas', 390.00, 1, '1', 10, 1, '2014-12-09 14:37:52', NULL),
(28, 'Aluguel de casa para temporada', 'Casa para alugar durante o carnaval', 100.00, 1, '1', 10, 3, '2014-12-09 16:11:13', NULL),
(29, 'vestido de noiva', 'Vestido de noiva', 100.00, 1, '0', 4, 3, '2014-12-09 16:22:27', NULL),
(30, 'Casa para festaa de bebe', 'testetetstete', 800.00, 1, '1', 13, 1, '2014-12-09 16:33:04', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
