-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2014 at 10:22 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `temprafesta`
--

-- --------------------------------------------------------

--
-- Table structure for table `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rua` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `empresa_id` bigint(20) DEFAULT NULL,
  `filiais_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `endereco`
--

INSERT INTO `endereco` (`id`, `rua`, `numero`, `bairro`, `empresa_id`, `filiais_id`) VALUES
(42, 'Inambú', 550, 'Boa vista', 29, NULL),
(43, 'Inambú', 4000, 'Costa e Silva', 30, NULL),
(45, 'teste', 4000, 'Costa e Silva', NULL, 31),
(46, 'teste', 4000, 'Costa e Silva', NULL, 32),
(48, 'teste', 4000, 'Costa e Silva', NULL, 34),
(49, 'teste', 4000, 'Costa e Silva', NULL, 35),
(50, 'Inambú', 4000, 'Costa e Silva', 30, NULL),
(51, 'são josé dantas', 450, 'Costa e Silva', 31, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
