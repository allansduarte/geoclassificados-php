-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2014 at 10:23 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `temprafesta`
--

-- --------------------------------------------------------

--
-- Table structure for table `filiais`
--

CREATE TABLE IF NOT EXISTS `filiais` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `usuario_id` bigint(20) NOT NULL,
  `empresa_id` bigint(20) NOT NULL,
  `cidade_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `filiais`
--

INSERT INTO `filiais` (`id`, `nome`, `descricao`, `status`, `usuario_id`, `empresa_id`, `cidade_id`, `created`, `modified`) VALUES
(31, 'teste', 'teste', 1, 1, 30, 25, '2014-11-17 14:19:54', NULL),
(32, 'Casamento', 'teste', 1, 1, 29, 24, '2014-11-17 14:20:33', NULL),
(34, 'teste', 'teste', 1, 1, 30, 25, '2014-11-17 14:22:13', NULL),
(35, 'Bokitos', 'testetstetste', 1, 1, 29, 1118, '2014-11-17 14:23:57', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
