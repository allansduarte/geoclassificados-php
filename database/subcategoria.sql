-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2014 at 10:24 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `temprafesta`
--

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE IF NOT EXISTS `subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `nome`, `descricao`, `categoria_id`) VALUES
(2, 'Noivado', '', 4),
(4, 'Vestidos', '', 4),
(5, 'Doces e guloseimas', 'Doces e guloseimas para festas de crianças', 5),
(6, 'Bolos e brigadeiros', 'Bolos e brigadeiros', 5),
(7, 'Equipamentos', 'Cadeiras, balõeszinhos, apitos e etc...', 5),
(8, 'Locais de festas', '', 5),
(9, 'Carros de aluguel', 'Limosine, carros clássicos e etc... Variedades de automóveis para casamentos exóticos!', 4),
(10, 'Casas', 'casas', 6),
(11, 'subteste w', 'sub descrição e', 6),
(12, 'edição', 'sub descrição edição', 7),
(13, 'Festa de bebe', '', 9);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
